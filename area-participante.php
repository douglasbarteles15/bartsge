<?php
require("site/includes/funcoes.php");
if ($_SESSION["logado_site"] != "logado") {
    header("location:" . $SiteHTTP . "acessar/erro/Você precisa estar logado pra acessar essa área.");
} else {
    $objParticipanteLogado = new DAO_Participantes();
    $objParticipanteLogado->select($_SESSION["participante"]);
}
$data_atual = date("Y-m-d");
?>
<!DOCTYPE html>
<html>
<head>
    <title>Área do Participante</title>
    <?php include('meta.php'); ?>
</head>
<body>
<?php include('header-fixo.php'); ?>
<section class="section-page">
    <header>
        <div class="title">
            <h1>Meus Eventos</h1>
            <hr>
        </div>
    </header>
    <section class="form-section">
        <div class="form-section-content">
            <div>
                <ul class="nav nav-tabs" role="tablist">
                    <li role="presentation" class="active">
                        <a href="#home" class="nav-color-blue" aria-controls="home" role="tab" data-toggle="tab">Eventos Confirmados</a>
                    </li>
                    <li role="presentation">
                        <a href="#profile" class="nav-color-yellow" aria-controls="profile" role="tab" data-toggle="tab">Pendentes de Confirmação</a>
                    </li>
                    <!--<li role="presentation">
                        <a href="#messages" class="nav-color-red" aria-controls="messages" role="tab" data-toggle="tab">Certificados</a>
                    </li>-->
                </ul>
                <!-- Tab panes -->
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane active" id="home">
                        <?php
                        $objBanco = new Database();
                        $objInscricoes = new DAO_Inscricoes();
                        $objEventos = new DAO_Eventos();
                        $objCategorias = new DAO_Categorias();
                        $buscaInscricoesConfirmadas = "SELECT * FROM eventos e INNER JOIN inscricoes i ON i.evento_id_INT = e.id WHERE i.participante_id_INT='".$objParticipanteLogado->getid()."' AND i.status_id_INT = '1' AND e.status_id_INT='1' ORDER BY datetime DESC";
                        $totalInscricoesConfirmadas = $objInscricoes->rows($buscaInscricoesConfirmadas);
                        if($totalInscricoesConfirmadas == 0 ){
                            echo '<h4 style="text-align: center;">Você ainda não confirmou inscrição em nenhum evento. </h4>';
                        }
                        $objBanco->Query($buscaInscricoesConfirmadas);
                        while($exibeInscricoesConfirmadas = $objBanco->FetchObject()){
                            $objInscricoes->select($exibeInscricoesConfirmadas->id);
                            $objEventos->select($exibeInscricoesConfirmadas->evento_id_INT);
                            $objCategorias->select($objEventos->getcategoria_id_INT());
                            $verificaPresenca = "SELECT * FROM presenca WHERE participante_id_INT='".$objParticipanteLogado->getid()."' AND evento_id_INT='".$objEventos->getid()."'";
                            $totalPresenca = $objEventos->rows($verificaPresenca);
                            if($totalPresenca == 0){
                                $presenca = false;
                            }else{
                                $presenca = true;
                            }
                        ?>
                            <div class="col-md-12 view-curso">
                                <div class="col-md-6 mr-pad-2">
                                    <div class="ic-title"><?=$objEventos->gettitulo()?></div>
                                    <ul class="list-curso-detalhes">
                                        <li><?=$objCategorias->gettitulo()?></li>
                                        <li>
                                            <strong><?=$objEventos->FDataExibe($objEventos->getdata_inicio())?>
                                                <?php if($objEventos->getdata_final()!=$objEventos->getdata_inicio()){
                                                    echo ' até '.$objEventos->FDataExibe($objEventos->getdata_final());
                                                } ?>
                                            </strong>
                                        </li>
                                        <li class="red"><strong>Inscrito em: </strong><?=$objInscricoes->FDataTimeExibe($objInscricoes->getdatetime())?></li>
                                        <li><i class="fa fa-clock-o" aria-hidden="true"></i>
                                        de <?=substr($objEventos->gethora_inicio(), 0, -3)?>h às <?=substr($objEventos->gethora_final(), 0, -3)?>h</li>
                                    </ul>
                                </div>
                                <div class="col-md-2 mr-pad-2">
                                    <div class="ic-title">&nbsp&nbsp&nbsp&nbsp</div>
                                    <ul class="list-curso-detalhes">
                                        <li>&nbsp&nbsp&nbsp&nbsp</li>
                                        <li>&nbsp&nbsp&nbsp&nbsp</li>
                                    </ul>
                                </div>
                                <div class="col-md-2 mr-pad-2 last-box">
                                    <a class="disabled" href="programacao/<?=$objEventos->getlink()?>">
                                        <div class="box-view-open">
                                            <!--<span class="strong">Reenviar Email</span>-->
                                            <span class="title-view">Ver Programação</span>
                                        </div>
                                    </a>
                                </div>
                                <?php if($objEventos->getdata_inicio() > $data_atual){ ?>
                                <div class="col-md-2 mr-pad-2 last-box">
                                    <a class="disabled" href="cancelar-inscricao/<?=$objEventos->getid()?>/<?=$objParticipanteLogado->getid()?>/<?=$objInscricoes->getid()?>" onclick="return confirm('Tem certeza que deseja cancelar sua inscrição nesse evento?')">
                                        <div class="box-view-open view-cancel" style="background: #dc3737;">
                                            <!--<img src="img/cancel.png">-->
                                            <span class="title-view">Cancelar Inscrição</span>
                                        </div>
                                    </a>
                                </div>
                                <?php }else{?>
                                    <div class="col-md-2 mr-pad-2 last-box" style="cursor: not-allowed;opacity: 0.5;filter: alpha(opacity=50);" title="Você não pode mais cancelar sua inscrição neste evento.">
                                        <a class="disabled">
                                            <div class="box-view-open view-cancel" style="background: #dc3737;">
                                                <!--<img src="img/cancel.png">-->
                                                <span class="title-view">Cancelar Inscrição</span>
                                            </div>
                                        </a>
                                    </div>
                                <?php }?>

                                <?php if($presenca == true && $objEventos->getdata_final() < $data_atual){ ?>
                                <a href="certificado/<?=$objInscricoes->getid()?>" class="btn btn-link" style="float: right;margin-right: 5px;border: 1px solid #999">Baixar Certificado</a>
                                <?php }else{ ?>
                                    <a class="btn btn-link" style="float: right;margin-right: 5px;opacity: 0.5;filter: alpha(opacity=50); cursor: not-allowed; border: 1px solid #999" title="Por enquanto, você ainda não pode baixar o certificado de participação neste evento. Sua presença ainda não foi confirmada ou o evento ainda não terminou.">Baixar Certificado</a>
                                <?php } ?>

                            </div>
                        <?php } ?>
                    </div>
                    <div role="tabpanel" class="tab-pane" id="profile">
                        <?php
                        $objBanco = new Database();
                        $objInscricoes = new DAO_Inscricoes();
                        $objEventos = new DAO_Eventos();
                        $objCategorias = new DAO_Categorias();
                        $buscaInscricoesNaoConfirmadas = "SELECT * FROM eventos e INNER JOIN inscricoes i ON i.evento_id_INT = e.id WHERE i.participante_id_INT='".$objParticipanteLogado->getid()."' AND i.status_id_INT = '0' AND e.status_id_INT='1' ORDER BY datetime DESC";
                        $totalInscricoesNaoConfirmadas = $objInscricoes->rows($buscaInscricoesNaoConfirmadas);
                        if($totalInscricoesNaoConfirmadas == 0 ){
                            echo '<h4 style="text-align: center;">Você não tem eventos pendentes de confirmação de inscrição. </h4>';
                        }
                        $objBanco->Query($buscaInscricoesNaoConfirmadas);
                        while($exibeInscricoesNaoConfirmadas = $objBanco->FetchObject()){
                        $objInscricoes->select($exibeInscricoesNaoConfirmadas->id);
                        $objEventos->select($exibeInscricoesNaoConfirmadas->evento_id_INT);
                        $objCategorias->select($objEventos->getcategoria_id_INT());
                        ?>
                            <div class="col-md-12 view-curso pagamentos">
                                <div class="col-md-6 mr-pad-2">
                                    <div class="ic-title"><?=$objEventos->gettitulo()?></div>
                                    <ul class="list-curso-detalhes">
                                        <li><?=$objCategorias->gettitulo()?></li>
                                        <li>
                                            <strong><?=$objEventos->FDataExibe($objEventos->getdata_inicio())?>
                                                <?php if($objEventos->getdata_final()!=$objEventos->getdata_inicio()){
                                                    echo ' até '.$objEventos->FDataExibe($objEventos->getdata_final());
                                                } ?>
                                            </strong>
                                        </li>
                                        <li class="green"><i class="fa fa-clock-o" aria-hidden="true"></i>
                                            de <?=substr($objEventos->gethora_inicio(), 0, -3)?>h às <?=substr($objEventos->gethora_final(), 0, -3)?>h
                                        </li>

                                    </ul>
                                </div>
                                <div class="col-md-2 mr-pad-2">
                                    <div class="ic-title">&nbsp&nbsp&nbsp&nbsp</div>
                                    <ul class="list-curso-detalhes">
                                        <li>&nbsp&nbsp&nbsp&nbsp</li>
                                        <li>&nbsp&nbsp&nbsp&nbsp</li>
                                    </ul>
                                </div>
                                <div class="col-md-2 mr-pad-2 last-box">
                                    <a class="disabled" href="reenviar-email-confirmacao/<?=$objEventos->getid()?>/<?=$objParticipanteLogado->getid()?>/<?=$objInscricoes->getid()?>">
                                        <div class="box-view-open">
                                            <!--<span class="strong">Reenviar Email</span>-->
                                            <span class="title-view">Reenviar Email</span>
                                        </div>
                                    </a>
                                </div>
                                <?php if($objEventos->getdata_inicio() > $data_atual){ ?>
                                    <div class="col-md-2 mr-pad-2 last-box">
                                        <a class="disabled" href="cancelar-inscricao/<?=$objEventos->getid()?>/<?=$objParticipanteLogado->getid()?>/<?=$objInscricoes->getid()?>" onclick="return confirm('Tem certeza que deseja cancelar sua inscrição nesse evento?')">
                                            <div class="box-view-open view-cancel" style="background: #dc3737;">
                                                <!--<img src="img/cancel.png">-->
                                                <span class="title-view">Cancelar Inscrição</span>
                                            </div>
                                        </a>
                                    </div>
                                <?php }else{?>
                                    <div class="col-md-2 mr-pad-2 last-box" style="cursor: not-allowed;opacity: 0.5;filter: alpha(opacity=50);" title="Você não pode mais cancelar sua inscrição neste evento.">
                                        <a class="disabled">
                                            <div class="box-view-open view-cancel" style="background: #dc3737;">
                                                <!--<img src="img/cancel.png">-->
                                                <span class="title-view">Cancelar Inscrição</span>
                                            </div>
                                        </a>
                                    </div>
                                <?php }?>
                            </div>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>
    </section>
</section>
<?php //include('depoimentos.php'); ?>
<?php include('footer.php') ?>
</body>
</html>