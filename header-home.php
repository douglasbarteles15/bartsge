<?php
$tipo = $_GET['tipo'];
$msg = $_GET['msg'];
if(($msg!="")&&($tipo!="")){ ?>
	<script>
		swal(   'Olá!',   '<?=$msg?>',   <?php if($tipo=="sucesso") { echo "'success'"; }elseif($tipo=="erro") { echo "'error'"; }?> );
	</script>
<?php } ?>
<header class="top-header">
	<div class="content-main">
		<div class="main-header">
			<input type="checkbox" id="control-nav" />
			<label for="control-nav" class="control-nav"></label>
			<label for="control-nav" class="control-nav-close"></label>
			<div class="col-md-2 ic-logo">
                <a href="home">
                    <img src="front-end/layout/logo.png" style="margin-top: 30px;">
                </a>
            </div>
			<div class="col-md-8 ic-menu">
				<div class="box-menu">
					<span class="close" id="close"><i class="fa fa-times-circle" aria-hidden="true"></i></span>
					<img src="front-end/layout/logo_rodape.png">
				</div>
				<ul>
					<li><a href="proximos-eventos">Próximos Eventos</a></li>
					<li><a href="eventos-anteriores">Eventos Anteriores</a></li>
					<li><a href="noticias">Notícias</a></li>
					<li><a href="contato">Contato</a></li>
				</ul>
			</div>
			<div class="col-md-2 ic-area-aluno">
				<?php if ($_SESSION["logado_site"] == "logado") { ?>
					<div class="box-login-user">
						<div class="col-md-6 no-margin">
							<div class="col-md-12 no-margin">
								<?php if($_SESSION["tipo_usuario_site"] == "instrutor"){ ?>
									<span class="user-title">
								Bem-vindo(a)<br><strong style="text-transform: uppercase"><?php echo $_SESSION["instrutor_nome"] ?></strong>
							</span>
								<?php }elseif($_SESSION["tipo_usuario_site"] == "participante"){ ?>
									<span class="user-title">
								Bem-vindo(a)<br><strong style="text-transform: uppercase"><?php echo $_SESSION["participante_nome"] ?></strong>
							</span>
								<?php } ?>
							</div>
							<!--<div class="col-md-12 no-margin"><span class="user-title-1">Fale conosco<br>(32) 3218-8579</span></div>-->
						</div>
						<div class="col-md-6 no-margin">
							<ul class="bt-list">
								<?php if($_SESSION["tipo_usuario_site"] == "instrutor"){ ?>
									<li><a class="bt-list-style" href="area-instrutor" id="ap">Certificados</a></li>
								<?php }elseif($_SESSION["tipo_usuario_site"] == "participante"){ ?>
								<li><a class="bt-list-style" href="area-participante" id="ap">Meus Eventos</a></li>
								<?php } ?>
								<!--<li><a class="bt-list-style" href="editar-dados" id="md">Meus Dados</a></li>-->
								<li><a class="bt-list-style" href="logoff-site" id="sair">Sair</a></li>
							</ul>
						</div>
					</div>
				<?php }else{ ?>
					<a href="acessar" class="mylinkaluno">Área do Participante</a>
				<?php } ?>
			</div>
		</div>

        <?php
        $data_atual = date('Y-m-d');
        $objProximoEvento = new DAO_Eventos();
        $buscaProximoEvento = "SELECT * FROM eventos WHERE status_id_INT='1' AND data_final >= '".$data_atual."' ORDER BY data_inicio ASC, hora_inicio ASC LIMIT 1";
        $objBanco->Query($buscaProximoEvento);
        $exibeProximoEvento = $objBanco->FetchObject();
        $objProximoEvento->select($exibeProximoEvento->id);
        ?>

		<div class="col-md-12 mr-meddle">
			<div class="middle-text">
				<h1><?=$objProximoEvento->gettitulo()?></h1>
				<div class="text"><?=$objProximoEvento->getchamada()?></div>
				<a class="bt-ng" href="evento/<?=$objProximoEvento->getlink()?>">ver evento</a>
			</div>
		</div>
	</div>
</header>