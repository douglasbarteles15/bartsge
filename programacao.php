<?php
require("site/includes/funcoes.php");
if ($_SESSION["logado_site"] != "logado") {
	header("location:" . $SiteHTTP . "acessar/erro/Você precisa estar logado pra acessar essa área.");
} else {
	$objParticipanteLogado = new DAO_Participantes();
	$objParticipanteLogado->select($_SESSION["participante"]);
}
$evento_link = FDados("evento");
$objEvento = new DAO_Eventos();
$objEvento->selectLink($evento_link);
$objCategoria = new DAO_Categorias();
$objCategoria->select($objEvento->getcategoria_id_INT());
$data_atual = date("Y-m-d");
?>
<!DOCTYPE html>
<html>
<head>
	<title>Programação</title>
	<?php include('meta.php'); ?>
</head>
<body>
	<?php include('header-fixo.php'); ?>
	<section class="section-page">
		<header>
			<div class="title">
				<h1>Programação</h1>
				<hr>
			</div>
			<a class="voltarbt" href="area-participante"><img src="img/icon-backpage.png">Voltar</a>
		</header>
		<section class="form-section">
			<div class="form-section-content alter-02">
				<div>
				    <div role="tabpanel" class="tab-pane active" id="home">
				    	<div class="col-md-12 view-curso">
				    		<div class="col-md-6 mr-pad-2">
				    			<div class="ic-title"><?=$objEvento->gettitulo()?></div>
				    			<ul class="list-curso-detalhes">
				    				<li><?=$objCategoria->gettitulo()?></li>
				    				<li>
										<strong><?=$objEvento->FDataExibe($objEvento->getdata_inicio())?>
											<?php if($objEvento->getdata_final()!=$objEvento->getdata_inicio()){
												echo ' até '.$objEvento->FDataExibe($objEvento->getdata_final());
											} ?>
										</strong>
									</li>
									<li><i class="fa fa-clock-o" aria-hidden="true"></i>
										de <?=substr($objEvento->gethora_inicio(), 0, -3)?>h às <?=substr($objEvento->gethora_final(), 0, -3)?>h</li>
				    			</ul>
				    		</div>
				    	</div>
				    </div>
				</div>
			</div>
		</section>
		<section class="myvideo">
			<div class="content-main alter-01">
			<?php
			$objSubEventos = new DAO_Subeventos();
			$objInstrutor = new DAO_Instrutores();
			$objTipoSubevento = new DAO_Tipos_Subeventos();
			$objBancoArquivos = new Database();
			$objArquivos = new DAO_Arquivos();
			$buscaSubeventos = "SELECT * FROM subeventos WHERE evento_id_INT='".$objEvento->getid()."' AND status_id_INT='1' ORDER BY data ASC, hora_inicio ASC";
			$objBanco->Query($buscaSubeventos);
			while($exibeSubeventos = $objBanco->FetchObject()){
			$objSubEventos->select($exibeSubeventos->id);
			$objInstrutor->select($objSubEventos->getinstrutor_id_INT());
			$objTipoSubevento->select($objSubEventos->tipo_subevento_id_INT);
			?>
				<div class="col-md-12 box-modulo completo">
					<div class="left-box-module" style="width: 50%">
						<div class="title-curso" style="padding-bottom: 0px"><?=$objSubEventos->gettitulo()?></div>
						<?php if($objInstrutor->getnome() != "Credenciador"){ ?>
							<div class="title-curso" style="font-size: 0.9em; font-weight: normal"><?=$objInstrutor->getnome()?></div>
						<?php } ?>
						<div class="title-text"><?=$objSubEventos->getdescricao()?></div>
						<div class="title-clock"><i class="fa fa-clock-o" aria-hidden="true"></i> de <?=substr($objSubEventos->gethora_inicio(), 0, -3)?>h às <?=substr($objSubEventos->gethora_final(), 0, -3)?>h</div>
						<?php if($data_unica == false){ ?>
							<p><?=$objSubEventos->FDataExibe($objSubEventos->getdata())?></p>
						<?php } ?>
					</div>
					<?php
					$buscaArquivos = "SELECT * FROM arquivos WHERE subevento_id_INT = '".$objSubEventos->getid()."' ORDER BY datetime DESC";
					$objBancoArquivos->Query($buscaArquivos);
					$totalArquivos = $objEvento->rows($buscaArquivos);
					if($totalArquivos > 0){
					?>
					<div class="right-box-module" style="width: 47%">
						<h4>Arquivos para download:</h4>
						<?php while($exibeArquivos = $objBancoArquivos->FetchObject()){
							$objArquivos->select($exibeArquivos->id); ?>
							<a href="site/uploads/arquivos/<?=$objArquivos->getlink()?>" target="_blank">
								<p style="text-decoration: underline"><?=$objArquivos->gettitulo()?></p>
							</a>
						<?php } ?>
					</div>
					<?php } ?>
				</div>
			<?php } ?>
			</div>
		</section>
	</section>
	<?php //include('depoimentos.php'); ?>
	<?php include('footer.php') ?>
</body>
</html>