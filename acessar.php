<?php
require("site/includes/funcoes.php");
if ($_SESSION["logado_site"] == "logado") {
    header("location:" . $SiteHTTP . "area-participante");
}
$evento_id = FDados("evento");
?>
<!DOCTYPE html>
<html>
<head>
	<title>Acessar</title>
	<?php include('meta.php'); ?>
</head>
<body>
	<?php include('header-fixo.php'); ?>
	<section class="section-page">
		<header>
			<div class="title">
				<h1>Acessar a Área do Participante</h1>
				<hr>
			</div>
		</header>
		<div class="content-main-form">
			<article class="page-access">
				<div class="user-account">
					<form action="login-site" method="post">
                        <input type="hidden" name="evento" value="<?=$evento_id?>">
						<div class="md-col-12"><div class="title-login">já tenho cadastro</div></div>
						<div class="md-col-12"><input type="email" name="email" id="email" placeholder="E-mail" required></div>
						<div class="md-col-12"><input type="password" name="senha" id="senha" placeholder="Senha" required></div>
						<div class="md-col-12"><a class="esqueci-senha" href="esqueci-minha-senha">Esqueci minha senha</a></div>
						<div class="md-col-12"><input type="submit" name="accesar" id="accesar" value="Entrar"></div>
					</form>
				</div>
				<div class="user-create-account">
					<div class="list-create">
						<div class="title-login">não tenho cadastro</div>
						<div class="text-box" style="margin-bottom: 11px;">
							Faça seu cadastro e disfrute de todas as funcionalidades da Área do Participante.
						</div>
						<ul>
							<li>Tenha acesso aos arquivos dos eventos em que está inscrito.</li>
							<li>Visualize e baixe seus certificados.</li>
                            <?php if($evento_id != ""){ ?>
							    <li class="link"><a href="cadastro/<?=$evento_id?>">Cadastrar</a></li>
                            <?php }else{ ?>
                                <li class="link"><a href="cadastro">Cadastrar</a></li>
                            <?php } ?>
						</ul>
					</div>
				</div>
			</article>
		</div>
	</section>
	<?php //include('depoimentos.php'); ?>
	<?php include('footer.php') ?>
</body>
</html>