<?php
require("site/includes/funcoes.php");

$instrutor_id = FDados("instrutor");
$objInstrutor = new DAO_Instrutores();
$objInstrutor->select($instrutor_id);
$subevento_id = FDados("subevento");
$objSubEvento = new DAO_Subeventos();
$objSubEvento->select($subevento_id);
$objEvento = new DAO_Eventos();
$objEvento->select($objSubEvento->getevento_id_INT());

if ($_SESSION["logado_site"] != "logado") {
    header("location:" . $SiteHTTP . "acessar/erro/Você precisa estar logado pra acessar essa área.");
} else {
    if($objInstrutor->getid() == $_SESSION["instrutor"]) {
        $objInstrutorLogado = new DAO_Instrutores();
        $objInstrutorLogado->select($_SESSION["instrutor"]);
    }else{
        header("location:" . $SiteHTTP . "area-instrutor/erro/Aconteceu algo de errado ao solicitar seu certificado.");
    }
}
$data_atual = date("Y-m-d");
?>
<!DOCTYPE html>
<html>
<head>
    <title>Certificado - Área do Instrutor/Palestrante</title>
    <?php include('meta.php'); ?>
    <style>
        @media print {
            body * {
                visibility: hidden;
            }
            #printable, #printable *, .box-footer {
                visibility: visible;
            }
        }
    </style>
</head>
<body>
<?php include('header-fixo.php'); ?>
<section class="section-page">
    <header>
        <div class="title">
            <h1>Certificado</h1>
            <hr>
        </div>
    </header>
    <div class="box box-primary" style="font-family: cursive; padding: 0;" id="printable">
        <div class="box-header">
            <h3 class="box-title">Certificado de Instrutor/Palestrante</h3>
        </div>
        <hr>
        <div class="box-body no-padding">
            <h4><?=$objSubEvento->gettitulo()?></h4>
            <h4>Evento: <?=$objEvento->gettitulo()?></h4>
            <h3><?=$objInstrutorLogado->getnome()?></h3>
            <p>
                <strong><?=$objSubEvento->FDataExibe($objSubEvento->getdata())?></strong>
            </p>
            <p>de <?=substr($objSubEvento->gethora_inicio(), 0, -3)?>h às <?=substr($objSubEvento->gethora_final(), 0, -3)?>h</p>
        </div>
        <div class="box-footer" style="background-color: #202020; padding-top: 10px; padding-bottom: 10px">
            <img src="/front-end/layout/logo.png">
        </div>

    </div>

    <a onclick="window.print();" class="bt-ver" style="cursor: pointer;">Imprimir</a>

</section>
<?php //include('depoimentos.php'); ?>
<?php include('footer.php') ?>
</body>
</html>