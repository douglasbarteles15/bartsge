<section id="bx-azule" class="wow fadeInUp">
	<div class="main clearfix">
    	<div class="span3">
        	<div class="wrap-cont">
            	<div id="w1" class="circleGraphic1 ft-0">90</div>
                <div class="txt-int" style="top:70px"> Mais de <span> 50 eventos realizados </span></div>
                <!--<p>Os participantes aprovaram!</p>-->
            </div>                    
        </div>
        
        <div class="span3">
        	<div class="wrap-cont ">
            	<div id="w2" class="circleGraphic2 ft-0">75</div>
                <div class="txt-int">Mais de <span> 3000 inscritos </span></div>
                <!--<p>Aprovados em concursos regionais e federais</p>-->
            </div>
        </div>
        
        <div class="span3">
        	<div class="wrap-cont">
            	<div id="w3" class="circleGraphic3 ft-0">95</div>
                <div class="txt-int">Mais de<span> 10 tipos</span> de eventos</div>
                <!--<p> Realizamos cursos dos mais diversos concursos</p>-->
            </div>
        </div>
        
        <div class="span3">
        	<div class="wrap-cont">
            	<div id="w4" class="circleGraphic4 ft-0">110</div>
                <div class="txt-int"><a href="#">Inscreva-se você tambem <span>Clique aqui</span></a></div>
                <!--<p> Conheça nossos cursos e faça sua pré-inscrição</p>-->
            </div>
        </div>
	</div>
</section>