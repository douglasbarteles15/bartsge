<?

// Classe da Geleria de Fotos do site
class Galeria {

    // vari�vel que ser� usada enquanto o objeto existir
    var $diretorio;
    var $diretorio2;
    var $path;
    var $galeria;
    var $galeriaLink;

    // M�todo construtor da classe
    // este m�todo coloca as imagens na propor��o e cria os thumbnails da galeria de imagens
    // a classe galeriaFotos precisa de 1 par�metro para ser instanciada.
    // o parametro � o nome da pasta onde os arquivos das imagens est�o
    // tem de passar o caminho completo do local onde est� usando a galeria at� a pasta com as fotos
    //$path_galeria_imagens -> caminho do local onde est� instanciando o objeto at� a pasta galeria_imagens
    //$galeria -> nome do diret�rio
    //$galeria -> tipo das galerias

    function Galeria($path_galeria_imagens, $galeria, $tipo="galerias") {
        if (substr($path_galeria_imagens, -1) == "/") {
            $path_galeria_imagens = strrev(substr(strrev($path_galeria_imagens), 1, strlen($path_galeria_imagens)));
        }

        if (is_dir($path_galeria_imagens . "/" . $tipo . "/" . $galeria)) {
            $this->diretorio = $path_galeria_imagens . "/" . $tipo . "/" . $galeria;
            $this->diretorio2 = "../../" . $path_galeria_imagens . "/" . $tipo . "/" . $galeria;
            $this->path = $path_galeria_imagens;
            $this->galeria = $tipo . "/" . $galeria;
            $this->galeriaLink = $galeria;
        } else {
            echo "ERRO REFERENCIA DO CAMINHO ESTÁ ERRADA";
            echo "<br/>";
            echo "Diretorio: " . $this->diretorio = $path_galeria_imagens . "/" . $tipo . "/" . $galeria;
            echo "<br/>";
            echo "Path: " . $this->path = $path_galeria_imagens;
            echo "<br/>";
            echo "Galerias/Galeria: " . $this->galeria = $tipo . "/" . $galeria;
            echo "<br/>";
            echo "Galeria: " . $this->galeriaLink = $galeria;

            die();
        }
    }

    // M�todo para criar a p�gina com os thumbnails
    // a princ�pio sem fazer nenhum tipo de pagina��o
    function fotosPequenas($path_ajax="/resources/php/ajax.php") {
        if (is_dir($this->diretorio)) {

            $fotos = array();
            $dh = opendir($this->diretorio);
            while (($arquivo = readdir($dh)) !== false) {
                array_push($fotos, $arquivo);
            }
            closedir($dh);
            sort($fotos);

            foreach ($fotos as $arquivo) {
                $arrFile = explode(".", $arquivo);
                if ($arrFile[1] == "jpg" || $arrFile[1] == "JPG") {
                    echo "
                        <div class='div_fotos_pequenas'>
                            <div class='foto_pequena' onclick=\"getDados('foto','" . $this->path . $path_ajax . "?foto=" . $arquivo . "&path=" . $this->diretorio2 . "&folder=" . $this->galeriaLink . "')\" title=''>
                                <img align='' alt='' style='border:4px solid #ffffff;' src='" . $this->path . "/resources/php/imagem.php?largura=72&altura=54&caminho=" . $this->diretorio2 . "/" . $arquivo . "'>
                            </div>
                        </div>
                    ";
                }
            }
        }
    }

    // metodo que retorna o caminho da imagem que aparecer� assim que a pessoa mandar exibir o �lbum de fotografias - a img grande
    function getFotoDestaque($largura, $altura) {
        if (is_dir($this->diretorio)) {
            $fotos = array();
            $dh = opendir($this->diretorio);
            while (($arquivo = readdir($dh)) !== false) {
                $arrFile = explode(".", $arquivo);
                if ($arrFile[1] == "jpg" || $arrFile[1] == "JPG") {
                    array_push($fotos, $arquivo);
                }
            }
            closedir($dh);
            sort($fotos);
        }
        return $this->path . "/resources/php/imagem.php?largura=" . $largura . "&altura=" . $altura . "&caminho=../../" . $this->galeria . "/" . $fotos[0];
    }

    // metodo que retorna o caminho da imagem que aparecer� assim que a pessoa mandar exibir o �lbum de fotografias - a img grande
    function getFoto1($largura, $altura) {
        if (is_dir($this->diretorio)) {
            $fotos = array();
            $dh = opendir($this->diretorio);
            while (($arquivo = readdir($dh)) !== false) {
                $arrFile = explode(".", $arquivo);
                if ($arrFile[1] == "jpg" || $arrFile[1] == "JPG") {
                    array_push($fotos, $arquivo);
                }
            }
            closedir($dh);
            sort($fotos);
        }

        return $this->path . "/resources/php/imagem.php?largura=" . $largura . "&altura=" . $altura . "&caminho=../../" . $this->galeria . "/" . $fotos[0];
    }

    function getComentarioFoto1($tabela="galerias_fotos") {
        $objBanco = new Database();

        if (is_dir($this->diretorio)) {
            $fotos = array();
            $dh = opendir($this->diretorio);
            while (($arquivo = readdir($dh)) !== false) {
                $arrFile = explode(".", $arquivo);
                if ($arrFile[1] == "jpg" || $arrFile[1] == "JPG") {
                    array_push($fotos, $arquivo);
                }
            }
            closedir($dh);
            sort($fotos);
        }

        $diretorio = end(explode("/", $this->diretorio));

        //Coment�rio
        //$sql = "SELECT * FROM `galerias_fotos` WHERE galerias_fotos_arquivo = '{$fotos[0]}' and galerias_id_INT = {$diretorio} and galerias_fotos_comentario <> ''";
        $sql = "SELECT " . $tabela . "_comentario AS comentario FROM " . $tabela . " " .
                "WHERE " . $tabela . "_arquivo = '{$fotos[0]}' AND " .
                $tabela . "_comentario <> ''";

        $objBanco->Query($sql);

        if ($objBanco->rows >= 1) {
            $row = $objBanco->FetchObject();

            return "<div style='font-family:verdana;font-size:11px;color:#666666;text-align:left;width:100%;background-color:#F4F4F4; padding:5px;'>
				      <b>Coment�rio: </b><br/>$row->comentario
			        </div>";
        }
    }

    // metodo que retorna uma imagem caminho da imagem que aparecer� assim que a pessoa mandar exibir o �lbum de fotografias - a img grande
    function getFoto($largura, $altura, $path, $imagem) {
        if (is_dir($this->diretorio)) {
            $fotos = array();
            $dh = opendir($this->diretorio);
            while (($arquivo = readdir($dh)) !== false) {
                $arrFile = explode(".", $arquivo);
                if ($arrFile[1] == "jpg" || $arrFile[1] == "JPG") {
                    array_push($fotos, $arquivo);
                }
            }
            closedir($dh);
            sort($fotos);
        }

        return "../../resources/php/imagem.php?largura=" . $largura . "&altura=" . $altura . "&caminho=../../" . $this->galeria . "/" . $imagem;
    }

    // metodo que retorna o numero de fotos que existem na galeria cadastrada
    function numFotos() {
        if (is_dir($this->diretorio)) {
            $dh = opendir($this->diretorio);
            while (($arquivo = readdir($dh)) !== false) {
                $arrFile = explode(".", $arquivo);
                if ($arrFile[1] == "jpg" || $arrFile[1] == "JPG") {
                    $cont++;
                }
            }
            closedir($dh);
        }
        return $cont;
    }

    // metodo para excluir uma galeria de imagens
    function delete() {
        if (is_dir($this->diretorio)) {
            if (@chmod($this->diretorio, 0777)) {//Altera a permiss�o de escrita do diret�rio
                return false;
            } else {
                $dh = opendir($this->diretorio);
                while (($arquivo = readdir($dh)) !== false) {
                    if (filetype($this->diretorio . "/" . $arquivo) == "file") {
                        @unlink($this->diretorio . "/" . $arquivo);
                    }
                }
                closedir($dh);
                @rmdir($this->diretorio);
                return true;
            }
        } else {
            return false;
        }
    }

}

?>