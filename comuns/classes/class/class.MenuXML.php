<?php

class MenuXML {

    //Atributos
    private $xml;
    private $user;
    private $ajax;
    private $crypt;
    private $permissoes;

    //M�todos
    public function __construct() {
        $this->crypt = new Crypt();
        $this->user = new EXT_Funcionarios();
        $this->user->select($_SESSION["usuario"]);


         if ($this->user->getadministrador() == 1) {
            $this->xml = simplexml_load_file("xml/menu.xml");
        } else {
            $this->xml = simplexml_load_file("xml/menu_usuario.xml");
        }

        $this->jsFunctionMostraLi();

        $this->createMenu();
    }

    private function createMenu() {

        $this->includes();

        echo "<ul id=\"MenuBar1\" class=\"MenuBarHorizontal\">";

        foreach ($this->xml->menu as $m) {
            ++$loop;
            $idMenu = "menu" . $loop;
            //MENU
            $this->abreSubMenu($m["nome"], $idMenu);

            //SUBMENUS
            if (isset($m->submenu[0])) {
                foreach ($m->submenu as $s) {
                    ++$loop;
                    $idSubMenu = "submenu" . $loop;
                    $this->abreSubMenu($s["nome"], $idSubMenu);

                    if (isset($s->submenu[0])) {
                        foreach ($s->submenu as $ss) {
                            ++$loop;
                            $idSubSubMenu = "subsubmenu" . $loop;
                            $this->abreSubMenu($ss["nome"], $idSubSubMenu);

                            foreach ($ss->link as $l) {
                                $this->link($l["href"], $l["parametros"], $l["target"], $l["nome"], $idMenu, $idSubMenu, $idSubSubMenu);

                                print_r($l);
                            }

                            $this->fechaSubMenu();
                        }
                    }

                    foreach ($s->link as $l) {
                        $this->link($l["href"], $l["parametros"], $l["target"], $l["nome"], $idMenu, $idSubMenu);
                    }

                    $this->fechaSubMenu();
                }
            }

            foreach ($m->link as $l) {
                $this->link($l["href"], $l["parametros"], $l["target"], $l["nome"], $idMenu);
            }

            $this->fechaSubMenu();
        }

        echo "</ul>";

        $this->jsMenu();
    }

    private function fAcentos($string) {
        $trans_tbl = get_html_translation_table(HTML_ENTITIES);
        $trans_tbl = array_flip($trans_tbl);
        return utf8_decode(strtr($string, $trans_tbl));
    }

    private function abreSubMenu($nome, $id) {
        echo "
		<li id=\"{$id}\" style=\"display:none;\"><a href=\"#\" class=\"MenuBarItemSubmenu\">{$this->fAcentos($nome)}</a>" . PHP_EOL . "
			<ul>" . PHP_EOL;
    }

    private function fechaSubMenu() {
        echo "
			</ul>" . PHP_EOL . "
		</li>" . PHP_EOL;
    }

    private function link($href, $parametros, $target, $nome, $idMenu, $idSubMenu = false, $idSubSubMenu = false) {

        $parametros = ($parametros != "") ? "&" . str_replace(",", "&", $parametros) : "";

        echo
        "
				<li>" . PHP_EOL . "
					<a href=\"?p=" . $this->crypt->crypt64($href) . $parametros . "\" target=\"{$target}\">{$this->fAcentos($nome)}</a>" . PHP_EOL . "
					" . $this->jsMostraLi($idMenu, $idSubMenu, $idSubSubMenu) . "
				</li>
			" . PHP_EOL;
    }

    private function includes() {
        echo
        "
			<script language=\"javascript\" type=\"text/javascript\" src=\"../../recursos/libs/SpryAssets/js/SpryMenuBar.js\"></script>\n\r
			<link rel=\"stylesheet\" type=\"text/css\" href=\"../../recursos/libs/SpryAssets/css/SpryMenuBarHorizontal.css\"/>\n\r
		";
    }

    private function jsMenu() {
        echo
        "
			<script type=\"text/javascript\">\n\r
				var MenuBar1 = new Spry.Widget.MenuBar(\"MenuBar1\", {imgDown:\"../../recursos/libs/SpryAssets/img/SpryMenuBarDownHover.gif\",imgRight:\"../../recursos/libs/SpryAssets/img/SpryMenuBarRightHover.gif\"});\n\r
			</script>\n\r
		";
    }

    private function jsFunctionMostraLi() {
        echo
        "
			<script type=\"text/javascript\" language=\"javascript\">
				function mostraLiMenu(id)
				{
					document.getElementById(id).style.display = \"block\";
				}
			</script>
		";
    }

    private function jsMostraLi($idMenu, $idSubMenu = false, $idSubSubMenu = false) {
        if (isset($idMenu)) {
            $return =
                    "
				<script type=\"text/javascript\" language=\"javascript\">
					mostraLiMenu('{$idMenu}');
				</script>
			";
        }

        if ($idSubMenu != "") {
            $return.=
                    "
				<script type=\"text/javascript\" language=\"javascript\">
					mostraLiMenu('{$idSubMenu}');
				</script>
			";
        }

        if ($idSubSubMenu != "") {
            $return.=
                    "
				<script type=\"text/javascript\" language=\"javascript\">
					mostraLiMenu('{$idSubSubMenu}');
				</script>
			";
        }

        return $return;
    }

}

?>