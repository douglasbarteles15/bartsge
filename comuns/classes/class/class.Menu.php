<?php
class Menu
{
	//Atributos
	private $arrMenu;
	private $arrPermissoes;
	private $Adm;
	
	private $crypt;
	
	//M�todos
	public function __construct($arrMenu,$arrPermissoes,$Adm,$ajax=false)
	{
		
		$this->arrMenu = $arrMenu;
		
		$this->arrPermissoes = $arrPermissoes;
		
		$this->Adm = $Adm;
		
		$this->crypt = new Crypt();
		
		if($ajax === false)
		{
			$this->createMenu();
		}
		else
		{
			$this->createMenuAjax();
		}
		
	}
	
	private function verificaMenuArea($area)
	{
		
		if($this->Adm == 1)
		{// Se o usu�rio for um administrador
			return true;
		}
		else
		{	
			foreach($this->arrPermissoes as $permissao)
			{
				//Se o par�metro passado for um array
				//Varrer o array para verificar se a est� liberado o acesso � �rea
				if(is_array($area))
				{
					foreach($area as $verificaArea)
					{
						if($verificaArea === $permissao)
						{
							return true;
							break;
						}
					}
				}
				else
				{
					if($area === $permissao)
					{
						return true;
						break;	
					}
				}
			}
		}
		return false;
	}
	
	
	function createMenu()
	{	
		echo "<ul id=\"MenuBar1\" class=\"MenuBarHorizontal\">";
		
			foreach($this->arrMenu as $titulo => $arrSubMenu)
			{			
				if($this->verificaMenuArea($arrSubMenu) === true)
				{
					echo "
					<li><a href=\"#\" class=\"MenuBarItemSubmenu\">$titulo</a>
						<ul>";
							if(is_array($arrSubMenu))
							{
								$this->createSubMenu($arrSubMenu);
							}
							else
							{
								echo "
								<li>
									<a href=\"?p=".$this->crypt->crypt64($arrSubMenu)."\">$titulo</a>
								</li>";
							}	
					echo "
						</ul>
					</li>";
				}
			}
			
		echo "</ul>";
	}
	
	function createSubMenu($Arr)
	{	
		if($this->verificaMenuArea($Arr) === true)
		{			
			foreach($Arr as $tituloSub => $paginaSub)
			{
				if(is_array($paginaSub))
				{
					$this->createMenuArea($tituloSub,$paginaSub);	
				}		
				else
				{
					if($this->verificaMenuArea($paginaSub) === true)
					{
						echo "
						<li>
							<a href=\"?p=".$this->crypt->crypt64($paginaSub)."\">$tituloSub</a>
						</li>";
					}
				}
			}	
		}
	}
	
	function createMenuArea($titulo,$Arr)
	{
		if($this->verificaMenuArea($Arr) === true)
		{	
			echo "
			<li><a href=\"#\" class=\"MenuBarItemSubmenu\">$titulo</a>
				<ul>";
				foreach($Arr as $tituloArea => $paginaArea)
				{			
					if(is_array($paginaArea))
					{
						$this->createSubMenu($paginaArea);
					}
					else
					{
						if($this->verificaMenuArea($paginaArea) === true)
						{
							echo "
							<li>
								<a href=\"?p=".$this->crypt->crypt64($paginaArea)."\">$tituloArea</a>
							</li>";
						}
					}	
				}
			echo "
				</ul>
			</li>";
		}
	}
	
//********************************************** AJAX *********************************************************************
	
	function createMenuAjax($Arr)
	{
		echo "<ul id=\"MenuBar1\" class=\"MenuBarHorizontal\">";
		
			foreach($Arr as $titulo => $arrSubMenu)
			{			
				if($this->verificaMenuArea($arrSubMenu) === true)
				{
					echo "
					<li><a href=\"#\" class=\"MenuBarItemSubmenu\">$titulo</a>
						<ul>";
							if(is_array($arrSubMenu))
							{
								$this->createSubMenuAjax($arrSubMenu);
							}
							else
							{
								echo "
								<li>
									<a onclick=\"getDados('backgroundTabelas','ajax.php?p=".$this->crypt->crypt64($paginaSub)."');\">$tituloSub</a>
								</li>";
							}	
					echo "
						</ul>
					</li>";
				}
			}		
		echo "</ul>";
	}
	
	function createMenuAreaAjax($titulo,$Arr)
	{
		if($this->verificaMenuArea($Arr) === true)
		{	
			echo "
			<li><a href=\"#\" class=\"MenuBarItemSubmenu\">$titulo</a>
				<ul>";
				foreach($Arr as $tituloSub => $pagnaSub)
				{			
					if(is_array($paginaSub))
					{
						$this->createSubMenuAjax($paginaSub);
					}
					else
					{
						echo "
						<li>
							<a onclick=\"getDados('backgroundTabelas','ajax.php?p=".$this->crypt->crypt64($paginaSub)."');\">$tituloSub</a>
						</li>";
					}	
				}
			echo "
				</ul>
			</li>";
		}
	}
	
	function createSubMenuAjax($Arr)
	{
		if($this->verificaMenuArea($Arr) === true)
		{			
			foreach($Arr as $tituloSub => $paginaSub)
			{
				if(is_array($paginaSub))
				{
					$this->createMenuAreaAjax($tituloSub,$paginaSub);	
				}		
				else
				{
					echo "
					<li>
						<a onclick=\"getDados('backgroundTabelas','ajax.php?p=".$this->crypt->crypt64($paginaSub)."');\">$tituloSub</a>
					</li>";
				}
			}	
		}
	}
}
?>