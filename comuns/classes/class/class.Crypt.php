<?php

class Crypt extends AES
{

	var $key128;
	var $key192;
	var $key256;
	
	public function __construct()
	{
		$this->key128	= "2b7e151628aed2a6abf7158809cf4f3c";
		$this->key192	= "8e73b0f7da0e6452c810f32b809079e562f8ead2522c6b7b";
		$this->key256	= "603deb1015ca71be2b73aef0857d77811f352c073b6108d72d9810a30914dff4";
		
		parent::__construct();
		
	}
	
	
	function crypt64($t)
	{
		if(!$t)
		{
			return false;
		}
		else
		{
			
			$tamanho = strlen($t);
        
	        if($tamanho > 16)
	        {
	            $iteracoes = ceil($tamanho / 16);
	            
	            for($i=0; $i<$iteracoes; $i++)
	            {
	                $pedaco = substr($t, $i*16, 16);
	                
	                $thex = $this->stringToHex($pedaco);
	                $tcrypt .= $this->encrypt($thex, $this->key128);
	            }   
	        }
	        else
	        {
				$thex = $this->stringToHex($t);
				$tcrypt = $this->encrypt($thex, $this->key128);
	        }
	        
	        return $tcrypt;
	        	
		}
	}
	
	
	function decrypt($t)     
	{
		
		if(!$t)
		{
			return false;
		}
		else
		{
	        $tamanho = strlen($t);
	        
	        if($tamanho > 32)
	        {
				$iteracoes = ceil($tamanho / 32);  
	            
				for($i=0; $i<$iteracoes; $i++)
	            {
	                $pedaco = substr($t, $i*32, 32);
	                
	                $tcrypt = $this->decryptAES($pedaco, $this->key128);
	                $tstring .= $this->hexToString($tcrypt);
	            }
	        }
	        else
	        {
			    $tcrypt = $this->decryptAES($t, $this->key128);
			    $tstring = $this->hexToString($tcrypt);
	        }
	        
			return $tstring;
			
		}
	}
	
}

?>