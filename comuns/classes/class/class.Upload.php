<?php
class Upload
{
	public $tamanhoMax;
	public $arrPermitido;
	public $uploadPath;
	public $file;
	public $nome;
	public $erro;
	
	protected  $resize;
	protected  $largura;
	protected  $altura;
	protected  $qualidade;
	
	protected  $resizeMiniatura;
	protected  $pathMiniatura;
	protected  $larguraMiniatura;
	protected  $alturaMiniatura;
	protected  $qualidadeMiniatura;

	public function __construct($file,$uploadPath)
	{
		$this->file		  = $_FILES[$file];
		$this->uploadPath = $uploadPath;
	}
	
	public function set_nome($nome)
	{
		$this->file["name"] = $nome;
		
		$this->nome = $nome;
		
	}
	
	public function set_tamanho_max($tamanho)
	{
		$this->tamanhoMax = ($tamanho * 1024);
	}
	
	public function upload()
	{
		
		if($this->file["name"] == "")
		{
			return false;
		}
		else
		{
			
			//ALTERAR A PERMISS�O DE ESCRITA DA PASTA			
			@chmod($this->uploadPath, 0777);
			
			//Se estiver sido setado quais os tipos permitidos
			if($this->arrPermitido != "")
			{
				foreach($this->arrPermitido as $permitido)
				{
					if($permitido == $this->file["type"])
					{
						$permissao = "ok";
					}
				}
				
				if(!$permissao)
				{				
					$this->erro = "Tipo de arquivo enviado n�o permitido";
					return false;
				}
				
			}
			
			if($this->tamanhoMax != "")
			{
				if($this->file["size"] > $this->tamanhoMax)
				{
					$this->erro = "O tamanho do arquivo excede o tamanho pr� estabelecido";
					return false;	
				}
			}
			
			if(move_uploaded_file($this->file["tmp_name"], $this->uploadPath.$this->file["name"]))
			{
				if($this->resize === true)
				{
					$this->resize();
				}
				
				if($this->resizeMiniatura === true)
				{
					$this->resizeMiniatura();
				}
				
				return true;
				
			}
			else
			{
				$this->erro = "N�o foi poss�vel executar o upload";
				return false;
			}
		}
	}
	
	public function set_resize($largura,$altura,$qualidade)
	{
		$this->resize = true;
		$this->largura = $largura;
		$this->altura = $altura;
		$this->qualidade = $qualidade;
	}
	
	public function setResizeMiniatura($path,$largura,$altura,$qualidade)
	{
		$this->resizeMiniatura = true;
		
		$this->pathMiniatura 		= $this->uploadPath.$path;
		$this->larguraMiniatura 	= $largura;
		$this->alturaMiniatura 		= $altura;
		$this->qualidadeMiniatura 	= $qualidade;
	}
	
	private function resize()
	{
		if($this->file["type"] == "image/jpeg" || $this->file["type"] == "image/pjpeg")
		{
			$imagem = $this->uploadPath."/".$this->nome;
			$img_origem = @ImageCreateFromJPEG($imagem); // Criar uma img a partir da img 
			
			$origem_x = ImagesX($img_origem); 
			$origem_y = ImagesY($img_origem); 
			
			$scale = min($this->largura/$origem_x , $this->altura/$origem_y);
			$new_width = floor($scale * $origem_x);
			$new_height = floor($scale * $origem_y); 
			
			$img_final = imagecreatetruecolor($new_width,$new_height); // Tamanho do thumb
			imagecopyresampled($img_final, $img_origem, 0, 0, 0, 0, $new_width, $new_height, $origem_x, $origem_y); 
			ImageJPEG($img_final, $imagem, $this->qualidade); 
			imagedestroy($img_origem); 
			imagedestroy($img_final);
		}
	}
	
	private function resizeMiniatura()
	{
		if($this->file["type"] == "image/jpeg" || $this->file["type"] == "image/pjpeg")
		{
			$imagem = $this->uploadPath."/".$this->nome;
			$imagemMiniatura = $this->pathMiniatura.$this->nome;
			
			$img_origem = @ImageCreateFromJPEG($imagem); // Criar uma img a partir da img 
			
			$origem_x = ImagesX($img_origem); 
			$origem_y = ImagesY($img_origem); 
			
			$scale = min($this->larguraMiniatura/$origem_x , $this->alturaMiniatura/$origem_y);
			$new_width = floor($scale * $origem_x);
			$new_height = floor($scale * $origem_y); 
			
			$img_final = imagecreatetruecolor($new_width,$new_height); // Tamanho do thumb
			imagecopyresampled($img_final, $img_origem, 0, 0, 0, 0, $new_width, $new_height, $origem_x, $origem_y); 
			ImageJPEG($img_final, $imagemMiniatura, $this->qualidadeMiniatura); 
			imagedestroy($img_origem); 
			imagedestroy($img_final);
		}
	}
	
	public function set_caminho($uploadPath)
	{
		
		$this->uploadPath = $uploadPath;
		
	}
	
	
}
?>