<?php
class Curl
{
  var $timeout;
  var $url;
  var $file_contents;

  function getFile($url,$timeout=0)
  {
    # use CURL library to fetch remote file
    $ch = curl_init();
    $this->url = $url;
    $this->timeout = $timeout;
    curl_setopt ($ch, CURLOPT_URL, $this->url);
    curl_setopt ($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt ($ch, CURLOPT_CONNECTTIMEOUT, $this->timeout);

    if($this->seHttps())
    {
        curl_setopt ($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt ($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
    }

    $this->file_contents = curl_exec($ch);
    if ( curl_getinfo($ch,CURLINFO_HTTP_CODE) !== 200 ) {
      return('Bad Data File '.$this->url);
    } else {
      return $this->file_contents;
    }
  }

  function seHttps()
  {
      $url = explode("://", $this->url);
      if(strtolower($url[0])=="https")
      {
          return true;
      }
      else
      {
          return false;
      }
  }

}