<?php
class Html
{
	private $database;
	private $arrBancos;
	
	public function __construct()
	{
		$this->database = new Database();
		
		$this->arrBancos = array(1 => "Banco do Brasil",2 => "Unibanco", 3 => "SICOOB");
		
	}
	
	public function optionsClientes($clientes_id)
	{
		
		if($clientes_id != "")
		{
			$sql = "select * from clientes where clientes_id = ".$clientes_id;
			
			$this->database->Query($sql);
			
			$row = $this->database->FetchObject();
			
			if($row->clientes_nome_fantasia != "")
			{
				echo "<option value='$row->clientes_id'>$row->clientes_nome_fantasia</option>";	
			}
			else
			{
				echo "<option value='$row->clientes_id'>$row->clientes_nome_responsavel</option>";
			}
						
		}
		
		echo "<option value=''>SELECIONE UM CLIENTE</option>";
		
		$sql = "select * from clientes order by clientes_nome_fantasia, clientes_nome_responsavel";
		
		$this->database->Query($sql);
		
		while( $row = $this->database->FetchObject())
		{
		
			if($row->clientes_nome_fantasia != "")
			{
				echo "<option value='$row->clientes_id'>$row->clientes_nome_fantasia</option>";	
			}
			else
			{
				echo "<option value='$row->clientes_id'>$row->clientes_nome_responsavel</option>";
			}
		
		}
		
	}
	
	public function optionsFuncionarios($funcionarios_id)
	{
		
		if($funcionarios_id != "")
		{
			$sql = "select * from funcionarios where funcionarios_id = ".$funcionarios_id;
			
			$this->database->Query($sql);
			
			$row = $this->database->FetchObject();
			
			echo "<option value='$row->funcionarios_id'>$row->funcionarios_nome</option>";
						
		}
		
		echo "<option value=''>SELECIONE UM FUNCION�RIO</option>";
		
		$sql = "select * from funcionarios order by funcionarios_nome";
		
		$this->database->Query($sql);
		
		while( $row = $this->database->FetchObject() )
		{
			echo "<option value='$row->funcionarios_id'>$row->funcionarios_nome</option>";
		}	
		
	}
	
	public function optionsTipoProjetos($projetos_tipo_id)
	{
		
		if($projetos_tipo_id != "")
		{
			$sql = "select * from projetos_tipo where projetos_tipo_id = ".$projetos_tipo_id;
			
			$this->database->Query($sql);
			
			$row = $this->database->FetchObject();
			
			echo "<option value='$row->projetos_tipo_id'>$row->projetos_tipo_nome</option>";
						
		}
		else
		{
			echo "<option value=''>SELECIONE UM TIPO DE SERVI�O</option>";	
		}
				
		$sql = "select * from projetos_tipo order by projetos_tipo_id";
		
		$this->database->Query($sql);
		
		while( $row = $this->database->FetchObject() )
		{
			echo "<option value='$row->projetos_tipo_id'>$row->projetos_tipo_nome</option>";
		}
		
	}
	
	public function optionsBancos($strBanco)
	{
		
		if($strBanco != "")
		{
			foreach($this->arrBancos as $valor => $banco)
			{
				if($strBanco == $valor)
				{
					echo "<option value='$valor'>$banco</option>";	
				}
			}
			
		}
		
		echo "<option value=''>SELECIONE UM BANCO</option>";
		
		foreach($this->arrBancos as $valor => $banco)
		{
			echo "<option value='$valor'>$banco</option>";
		}
		
	}
	
	public function optionsAtivoInativo($status)
	{
		if($status != "")
		{
			$strStatus = ($status == 1)?"Ativo":"Inativo";
			
			echo "<option value='$status'>$strStatus</option>";
		}
		
		echo "<option value=''>INFORME O STATUS</option>";
		echo "<option value='1'>Ativo</option>";
		echo "<option value='2'>Inativo</option>";
			
	}
	
	public function optionsSituacao($status)
	{
		if($status != "")
		{
			$strStatus = ($status == 1)?"ATIVO":"POTENCIAL";
			
			echo "<option value='$status'>$strStatus</option>";
		}
		
		echo "<option value=''>INFORME A SITUA��O</option>";
		echo "<option value='1'>ATIVO</option>";
		echo "<option value='2'>POTENCIAL</option>";
			
	}
	
	public function optionsEstados($estado_id)
	{
		if($estado_id != "")
		{
			$sql = "select * from estados where ID_ESTADO = $estado_id";
			
			$this->database->Query($sql);
			
			$row = $this->database->FetchObject();
			
			echo "<option value='$row->ID_ESTADO'>$row->DSC_ESTADO</option>\n\r";
			
		}
		else
		{
			echo "<option value=''>SELECIONE UM ESTADO</option>\n\r";		
		}
		
		$sql = "select * from estados order by DSC_ESTADO";
			
		$this->database->Query($sql);
		
		while($row = $this->database->FetchObject())
		{
			echo "<option value='$row->ID_ESTADO'>$row->DSC_ESTADO</option>\n\r";	
		}
		
	}
	
	public function optionsCidades($estado_id,$cidade_id)
	{
		if($cidade_id != "")
		{
			$sql = "select * from cidades where ID_CIDADE = '$cidade_id'";
			
			$this->database->Query($sql);
			
			$row = $this->database->FetchObject();
			
			echo "<option value='$row->ID_CIDADE'>$row->DSC_CIDADE</option>\n\r";
			
		}
				
		if($estado_id != "")
		{
			$sql = "select * from cidades where COD_ESTADO = $estado_id order by DSC_CIDADE";
			
			$this->database->Query($sql);
			
			while($row = $this->database->FetchObject())
			{
				echo "<option value='$row->ID_CIDADE'>$row->DSC_CIDADE</option>\n\r";	
			}	
		}
		else
		{
			echo "<option value=''>SELECIONE UM ESTADO</option>\n\r";		
		}	
	}
	
}
?>