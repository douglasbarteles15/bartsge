<?php
    class XML
	{
		
		public $xmlFile;
		
		public function XML($xml)
		{
			$this->xmlFile = simplexml_load_file($xml);
		}
		
		private function FAcentos($string)
		{
		    $trans_tbl = get_html_translation_table(HTML_ENTITIES);
		    $trans_tbl = array_flip ($trans_tbl);
		    return utf8_decode(strtr($string , $trans_tbl));
		}
		
		public function getElemento($elemento)
		{
			return $this->FAcentos($this->xmlFile->$elemento);
		}
    }
?>