<?php
class Helper
{
	
	private $crypt;
        private $url_site;
	
	public function __construct()
	{
		$this->crypt = new Crypt();
                $this->url_site = "localhost/patriciaalvim.com.br";
	}
	
	public function fTemplate()
	{		
		$page = $this->fDados("page");
		
		if(!$page)
		{
			return "site/php/main.php";
		}
		else
		{
			$strPage=str_replace(".","",$page);
			$strPage=str_replace("/","",$strPage);
			return "site/php/" . $strPage . ".php";
		}
	}
	
	public function fDados($dado)
	{
		$strDado = (!$_POST[$dado]) ? $_GET[$dado] : $_POST[$dado] ;	
		$strDado = htmlspecialchars($strDado);
		$strDado = html_entity_decode($strDado);
		
		if(!get_magic_quotes_gpc())
		{
			$strDado = addslashes($strDado);
		}
			
	    return $strDado;
	    
	}
	
	public function fNumeros($numero)
	{
		$Fnumero=str_replace(".","",$numero);
		$Fnumero=str_replace(",",".",$Fnumero);
		return number_format($Fnumero,2,".","");
	}

	public function setPaginacao($pagina)
	{	
		$objLoja = new EXT_Config_loja();	
		
		if (!$pagina)
		{
		   $inicio = 0;
		   $pagina = 1;
		}
		else
		{
		   $inicio = ($pagina - 1) * $objLoja->getconfig_loja_tamanho_listas_INT();
		}
		
		return " limit ".$inicio.",".$objLoja->getconfig_loja_tamanho_listas_INT();
	}
	
	public function selectPaginacao($sql,$action,$pagina)
	{	
		$objBanco = new Database();
		$objLoja = new EXT_Config_loja();
		
		$objBanco->Query($sql);
		
		$total_paginas = ceil($objBanco->rows / $objLoja->getconfig_loja_tamanho_listas_INT());
	
		echo "<select onchange=\"javascript:ajaxPaginacao('resultados','{$action}?pagina='+this.value);\">".PHP_EOL;
			for($i=1; $i <= $total_paginas; $i++)
			{
				$selected = ($i == $pagina)?"selected=\"selected\"":"";
				
				echo "<option value=\"{$i}\" {$selected}>{$i}</option>".PHP_EOL;
				
			}
		echo "</select>".PHP_EOL;	
	}
	
	public function fMoeda($numero)
	{
		if(strpos($numero,",") != false)
		{
			$numero = str_replace(".","",$numero);
			
			$numero = str_replace(",",".",$numero);	
		}
				
		return "R$ ".number_format($numero,2,",",".");
	}
	
	public function fMoedaSemCifrao($numero)
	{
		if(strpos($numero,",") != false)
		{
			$numero = str_replace(".","",$numero);
			
			$numero = str_replace(",",".",$numero);	
		}
				
		return number_format($numero,2,",",".");
	}
	
	public function fPeso($Peso)
	{
		str_replace(".","",$Peso);
		str_replace(",","",$Peso);
		return $Peso;
	}
	
	public function selectOrdenacao($sql,$page,$grupos_id,$sub_grupos_id,$fabricantes_id)
	{
		
		$menor_preco = $this->crypt->crypt64($sql." order by produtos_preco_pf_FLOAT asc, produtos_preco_promocional_pf_FLOAT asc, produtos_preco_pj_FLOAT asc, produtos_preco_promocional_pj_FLOAT asc ");
		$maior_preco = $this->crypt->crypt64($sql." order by produtos_preco_pf_FLOAT desc, produtos_preco_promocional_pf_FLOAT desc, produtos_preco_pj_FLOAT desc, produtos_preco_promocional_pj_FLOAT desc ");
		$nome = $this->crypt->crypt64($sql." order by produtos_nome asc ");
		$recente = $this->crypt->crypt64($sql." order by produtos_id desc ");
		
		echo 
		"
		<select name=\"ordenacao\" id=\"ordenacao\" onchange=\"javascript:ajaxOrdenacao(this.value,'{$page}','{$grupos_id}','{$sub_grupos_id}','{$fabricantes_id}');\" class=\"form_campo6\" onfocus=\"this.className='input_focus6';\" onblur=\"this.className='form_campo6';\">
			<option value=\"\">Ordernar por:</option>
			<option value=\"\"></option>
			<option value=\"{$menor_preco}\">Menor Pre&ccedil;o</option>
			<option value=\"{$maior_preco}\">Maior Pre&ccedil;o</option>
			<option value=\"{$nome}\">Nome</option>
			<option value=\"{$recente}\">Mais Recentes</option>
		</select>
		";
		
	}
	
	
	//-------- FUN��ES DE DATA ----------------------------------------------------------------
	
	public function fData($data)
	{
		$dataTrat = explode("/",$data);
		$data= $dataTrat[2]."-".$dataTrat[1]."-".$dataTrat[0];
		return $data;
	}
	
	public function getNumeroDias($mes,$ano)
	{ 
		if (((fmod($ano,4)==0) and (fmod($ano,100)!=0)) or (fmod($ano,400)==0))
		$dias_fevereiro = 29;
		else
		$dias_fevereiro = 28;
			
		switch($mes)
		{
			case 1:   return 31; break;
			case 2:   return $dias_fevereiro; break;
			case 3:   return 31; break;
			case 4:   return 30; break;
			case 5:   return 31; break;
			case 6:   return 30; break;
			case 7:   return 31; break;
			case 8:   return 31; break;
			case 9:   return 30; break;
			case 10:  return 31; break;
			case 11:  return 30; break;
			case 12:  return 31; break;
		}
	}
	
	//ACEITA DATAS NO FORMATO DO BRASIL
	public function convertToTimeStamp($data)
	{
		if(!$data)
		{
			return false;	
		}
		else
		{
			$arrData = explode("/",$data);
			
			$dia	= (strlen($arrData[0]) == 1)?"0".$arrData[0]:$arrData[0];
			 
			$mes	= (strlen($arrData[1]) == 1)?"0".$arrData[1]:$arrData[1];
			
			$ano	= substr($arrData[2],0,4);
			
			//int mktime  ($hora , $minuto , $second , $mes , $dia , $ano);
			return mktime(0,0,0, $mes,$dia,$ano);
		}
	}
	
	//RETORNA O DIA DA SEMANA DE UMA DATA
	public function getDiaSemana($dia,$mes,$ano)
	{
		$diasemana = date( "w", mktime(0,0,0,$mes,$dia,$ano) );
	
		switch($diasemana)
		{
			case"0": $diasemana     =   "Domingo";          break;
			case"1": $diasemana     =   "Segunda Feira";    break;
			case"2": $diasemana     =   "Ter�a Feira";      break;
			case"3": $diasemana     =   "Quarta Feira";     break;
			case"4": $diasemana     =   "Quinta Feira";     break;
			case"5": $diasemana     =   "Sexta Feira";      break;
			case"6": $diasemana     =   "S�bado";           break;
		}
		return $diasemana;
	}
	
	public function exibeData($Data)
	{
	    $data=substr($Data,8,2)."/".substr($Data,5,2)."/".substr($Data,0,4)." ".substr($Data,11,8);
	    return $data;
	}

	public function exibeDataSemHora($data)
	{
	    if(!$data)
	    {
	        return $data;
	    }else
	    {
	    	//Dec 1 2007 12:00AM
	    	$dia = (strpos(substr($data,4,2)," ") === false)?substr($data,4,2):substr($data,4,1);
	    	
	    	$mes = substr($data,0,3);
	    	switch($mes)
	    	{
	    		case "Jan":
	    			$mes = 1;
	    			break;
	    		case "Feb":
	    			$mes = 2;
	    			break;
	    		case "Mar":
	    			$mes = 3;
	    			break;
	    		case "Apr":
	    			$mes = 4;
	    			break;
	    		case "May":
	    			$mes = 5;
	    			break;
	    		case "Jun":
	    			$mes = 6;
	    			break;
	    		case "Jul":
	    			$mes = 7;
	    			break;
	    		case "Aug":
	    			$mes = 8;
	    			break;
	    		case "Sep":
	    			$mes = 9;
	    			break;
	    		case "Oct":
	    			$mes = 10;
	    			break;
	    		case "Nov":
	    			$mes = 11;
	    			break;
	    		case "Dec":
	    			$mes = 12;
	    			break;
	    	}
	    	
	    	$ano = (stripos(substr($data,6,4)," ") === false)?substr($data,6,4):substr($data,7,4);
	        
	    	return "$dia/$mes/$ano";
	    }
	}
	
	public function fChecked($valor,$validador)
	{
		//$valor que est� sendo passado
		//$validador para retornar checked ou vazio
		
		return ($valor == $validador)?"checked=\"checked\"":"";	
	}

	public function fSelected($valor,$validador)
	{
		//$valor que est� sendo passado
		//$validador para retornar checked ou vazio
		
		return ($valor == $validador)?"selected=\"selected\"":"";	
	}
	
	public function fFloat($numero)
	{
                if(strpos($numero,',') > strpos($numero,'.')){
                    $Fnumero = str_replace(".","",$numero);
                    $Fnumero = str_replace(",",".",$Fnumero);
                    return number_format($Fnumero,2,".","");
                }else{
                    return number_format($numero,2,".","");
                }
	}

	public function analytics()
	{
            if($_SERVER["SERVER_NAME"] == $this->url_site || $_SERVER["SERVER_NAME"] == substr($this->url_site,4))
            {
                echo
		"
                    <script type=\"text/javascript\">
                        var gaJsHost = ((\"https:\" == document.location.protocol) ? \"https://ssl.\" : \"http://www.\");
                        document.write(unescape(\"%3Cscript src='\" + gaJsHost + \"google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E\"));
                        </script>
                        <script type=\"text/javascript\">
                        try {
                        var pageTracker = _gat._getTracker(\"UA-10385955-1\");
                        pageTracker._trackPageview();
                        } catch(err) {}
                    </script>
		";
            }
	}
	
	public function porcentagem($strTotal,$strParcial)
	{
		$regra3=$strParcial*100;
		$regra3=$regra3/$strTotal;
		return number_format($regra3,2);
	}
	
	public function jsAlert()
	{
		$msg = $_GET["txtMSG"];
		if($msg!="")
		{
	        echo "<script language=\"javascript\" type=\"text/javascript\">alert('$msg');</script>";
		}		
	}	
	
	//	fun��es espec�ficas do projeto da loja
	public function fNomeProduto($nome)
	{
		$nome = str_replace("'","",$nome);
		$nome = str_replace("\"","",$nome);
		
		return $nome;
	}
	
	public function identificaAcesso()
	{
		
		$pagina = $this->fDados("page");
		
		$protegidas = array("meu_cadastro","meus_pedidos","entrega","pagamento","confirmacao");
		
		foreach($protegidas as $protegida)
		{
			if($protegida == $pagina)
			{
				if($_SESSION["cliente"] == "nao_identificado")
				{
					header("location:index.php?page=identifica&retorno=$pagina");
				}
				
				// NAS P�GINAS ENTREGA E PAGAMENTO � NECESS�RIO QUE EXISTA UMA 
				// SESS�O DE PEDIDO PARA O CLIENTE
				// TER ACESSO A ESTA P�GINA
				if($protegida == "entrega" &&  $_SESSION["pedido"] == "" || $protegida == "pagamento" &&  $_SESSION["pedido"] == "")
				{
					header("location:index.php");
				}
			}		
		}	
	}
	
	public function imgResize($img,$largura,$altura,$style)
	{	
		if(file_exists($img))
		{
			$arrImagem = getimagesize($img);
		
			$origem_x = $arrImagem[0]; 
			$origem_y = $arrImagem[1]; 
			
			$scale = min($largura/$origem_x , $altura/$origem_y);
			$new_width = floor($scale * $origem_x);
			$new_height = floor($scale * $origem_y); 
			
			return "<img src='$img' width='$new_width' height='$new_height' border='0' align='absmiddle' style=\"$style\"/>";	
		}
		else
		{
			return false;
		}	
	}

	public function imgResizeAjax($imgVerificacao,$imgExibicao,$largura,$altura,$style)
	{
		if(file_exists($imgVerificacao))
		{
			$arrImagem = getimagesize($imgVerificacao);
		
			$origem_x = $arrImagem[0]; 
			$origem_y = $arrImagem[1]; 
			
			$scale = min($largura/$origem_x , $altura/$origem_y);
			$new_width = floor($scale * $origem_x);
			$new_height = floor($scale * $origem_y); 
			
			return "<img src='$imgExibicao' width='$new_width' height='$new_height' border='0' style=\"$style\"/>";	
		}
		else
		{
			return false;
		}	
	}
	
	public function seo()
	{
		
		$objLoja = new EXT_Config_loja();
		
		if($this->fDados("produtos_id") != "")
                {
                    $objProduto = new EXT_Produtos();
                    $objProduto->select($this->crypt->decrypt($this->fDados("produtos_id")));
                    // nome do produto - escala - fabricante - marca
                    $titulo_pagina=$objProduto->getprodutos_nome()." ".$objProduto->getNomeEscala()." ".$objProduto->getNomeFabricantes()." ".$objProduto->getNomeMarca()." - ";
                    
                }
		elseif ($this->fDados("grupos_id") != "")
		{
			$objSEO = new DAO_Grupos();
			$objSEO->select($this->crypt->decrypt($this->fDados("grupos_id")));
			
			$titulo_pagina = $objSEO->getgrupos_nome()." - ";
			
			unset($objSEO);		
		}
		elseif ($this->fDados("sub_grupos_id") != "")
		{
			$objSEO = new DAO_Sub_grupos();
			$objSEO->select($this->crypt->decrypt($this->fDados("sub_grupos_id")));
			
			$titulo_pagina = $objSEO->getsub_grupos_nome()." - ";
			
			unset($objSEO);		
		}
		elseif ($this->fDados("fabricantes_id") != "")
		{
			$objSEO = new DAO_Fabricantes();
			$objSEO->select($this->crypt->decrypt($this->fDados("fabricantes_id")));
			
			$titulo_pagina = $objSEO->getfabricantes_nome()." - ";
			
			unset($objSEO);		
		}
                else
		{
			$titulo_pagina = "";
		}
		
		echo "
			
			<meta http-equiv='Content-Type' content='text/html; charset=iso 8859-1' />
			
			<title>".$titulo_pagina." ".$objLoja->getconfig_loja_metatag_titulo()." - ".$objLoja->getconfig_loja_metatag_palavras_chave()."</title>
			
			<meta name='keywords' content='".$titulo_pagina." ".$objLoja->getconfig_loja_metatag_palavras_chave()."'/>
			
			<meta name='description' content='".$titulo_pagina." ".$objLoja->getconfig_loja_metatag_descricao()."'/>
			
			<meta name='robots' content='index.follow'/>
			
			<meta name='rating' content='General'/>
			
			<meta name='revisit-after' content='1 Week'/>
			
			<meta http-equiv='pragma' content='no-cache'/>
			
			<meta http-equiv='imagetoolbar' content='no' />
			
			<meta name='author' content='ambientesolucao.com - DESENVOLVIMENTO WEB | 55+32.3226.8262 | ambiente@ambientesolucao.com'/>
			
			<meta name='generator' content='PDT - PHP Development Tool'/>
			
			<link rel='home' title='Home' href='".$objLoja->getconfig_loja_url()."'/>
			
			<link rel='bookmark' href='favicon.ico'/>
			
			<link rel='shortcut icon' href='favicon.ico'/>
			
		";
		
		unset($objLoja);
		
	}
	
	public function debug($var,$die=false)
    {
        echo "<pre>";
            var_dump($var);
        echo "</pre>";
        if($die !== false)
        {
            die();
        }
    }
    
}
?>
