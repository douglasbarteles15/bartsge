<?	
    
    class Data{
		
		var $mes;
		var $ano;
		
		function Data($mes,$ano){
			$this->mes = $mes;
			$this->ano = $ano;
		}
		
		function getNumeroDias(){ 
			if (((fmod($this->ano,4)==0) and (fmod($this->ano,100)!=0)) or (fmod($this->ano,400)==0))
			   $dias_fevereiro = 29; 
			else
			   $dias_fevereiro = 28;
						
			switch($this->mes) { 
			   case 1:   return 31; break; 
			   case 2:   return $dias_fevereiro; break; 
			   case 3:   return 31; break; 
			   case 4:   return 30; break; 
			   case 5:   return 31; break; 
			   case 6:   return 30; break; 
			   case 7:   return 31; break; 
			   case 8:   return 31; break; 
			   case 9:   return 30; break; 
			   case 10:  return 31; break; 
			   case 11:  return 30; break; 
			   case 12:  return 31; break; 
			}
	    }
		
		function getDiaSemana($dia){
			$ano =  $this->ano;
			$mes =  $this->mes;
			
			$diasemana = date( "w", mktime(0,0,0,$mes,$dia,$ano) );
		
			switch($diasemana) {
				case"0": $diasemana = "Dom";	    break;
				case"1": $diasemana = "Seg";		break;
				case"2": $diasemana = "Ter";		break;
				case"3": $diasemana = "Qua";		break;
				case"4": $diasemana = "Qui";		break;
				case"5": $diasemana = "Sex";		break;
				case"6": $diasemana = "S�b";		break;
			}
		
			return $diasemana;
		}
	}
?>