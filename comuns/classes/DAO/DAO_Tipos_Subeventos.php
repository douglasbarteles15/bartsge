<?php

// ***************************************************************************************************
//
//	DATA CRIAÇÃO:		02.08.2016
//	CLASSE:				DAO_Tipos_Subeventos
//	TABELA MYSQL:		tipos_subeventos
//	BANCO MYSQL:		bartsge
//									
// ***************************************************************************************************
// ***************************************************************************************************
// DECLARAÇÃO DA CLASSE
// ***************************************************************************************************

class DAO_Tipos_Subeventos { // CLASSE INÍCIO
// ***************************************************************************************************
// DECLARAÇÃO DE ATRIBUTOS
// ***************************************************************************************************

    var $id;   // CHAVE PRIMÁRIA - AUTO INCREMENTO
    var $titulo;   // (ATRIBUTO VARCHAR)
    var $database; // INSTANCIA DA CLASSE DATABASE

// ***************************************************************************************************
// MéTODO CONSTRUTOR
// ***************************************************************************************************

    function DAO_Tipos_Subeventos() {

        $this->database = new Database();
    }

// ***************************************************************************************************
// MÉTODOS - GETTER
// ***************************************************************************************************

    function getid() {

        return $this->id;
    }

    function gettitulo() {

        return $this->titulo;
    }


// ***************************************************************************************************
// MÉTODOS - SETTER
// ***************************************************************************************************


    function setid($val) {

        $this->id = $val;
    }

    function settitulo($val) {

        $this->titulo = $val;
    }

// ***************************************************************************************************
// M�TODO  - SELECT - RETORNA UMA LINHA DA TABELA
// ***************************************************************************************************

    function select($id) {

        $sql = "SELECT * FROM tipos_subeventos WHERE id = '$id'";
        $result = $this->database->Query($sql);
        $row = $this->database->FetchObject();


        $this->id = $row->id;

        $this->titulo = $row->titulo;

    }

// ***************************************************************************************************
// METODO QUE RETORNA A LINHA DO ULTIMO REGISTRO CRIADO NA TABELA
// ***************************************************************************************************

    function selectLastInsert() {
        $this->database->Query("select * from tipos_subeventos where id = last_insert_id()");
        $row = $this->database->FetchObject();
        $this->select($row->id);
    }

// ***************************************************************************************************
// MÉTODO  - SELECTBY - RETORNA LINHAS QUE ATENDEM A CONDIÇÃO
// ***************************************************************************************************

    function selectby($field, $val, $tipo_id_INT) {

        if ($tipo_id_INT != "int" || $tipo_id_INT != "float" || $tipo_id_INT != "INT" || $tipo_id_INT != "FLOAT" || $tipo_id_INT != "Int" || $tipo_id_INT != "Float") {
            $tipo_id_INT = "'";
        }

        $field = $this->FDados($field);
        $val = $this->FDados($val);

        $sql = "SELECT * FROM tipos_subeventos WHERE $field=$tipo_id_INT$val$tipo_id_INT";
        $this->database->Query($sql);
        return $this->database->FetchObject();
    }

// ***************************************************************************************************
// MÉTODO  - SELECT APROXIMADO - RETORNA LINHAS QUE ATENDEM A CONDIÇÃO
// ***************************************************************************************************

    function selectlike($field, $val) {

        $field = $this->FDados($field);
        $val = $this->FDados($val);

        $sql = "SELECT * FROM tipos_subeventos WHERE $field LIKE '%$val%'";
        $this->database->Query($sql);
        return $this->database->FetchObject();
    }

// ***************************************************************************************************
// MÉTODO - DELETE
// ***************************************************************************************************

    function delete($id) {

        $sql = "DELETE FROM tipos_subeventos WHERE id = $id;";
        $result = $this->database->Query($sql);
    }

// ***************************************************************************************************
// MÉTODO - INSERT
// ***************************************************************************************************

    function insert() {

        $this->id = ""; // Limpa a chave do auto incremento

        $sql = "INSERT INTO tipos_subeventos ( titulo ) VALUES ( '$this->titulo')";
        $result = $this->database->Query($sql);
    }

// ***************************************************************************************************
// MÉTODO - UPDATE
// ***************************************************************************************************

    function update($id, $tipo_id_INT) {

        if (isset($tipo_id_INT["id"])) {
            $upd.= "id = $this->id, ";
        }
        if (isset($tipo_id_INT["titulo"])) {
            $upd.= "titulo = '$this->titulo', ";
        }

        $upd = substr($upd, 0, -2);


        $sql = " UPDATE tipos_subeventos SET $upd WHERE id = '$id' ";

        $result = $this->database->Query($sql);
    }

// ***************************************************************************************************
// MÉTODO PARA CRIAR VARIAVEIS DE SESSAO
// ***************************************************************************************************

    function createsession() {
        $_SESSION["id"] = $this->getid();
        $_SESSION["titulo"] = $this->gettitulo();
    }

// ***************************************************************************************************
// MÉTODO PARA SETAR CAMPOS POR SESSAO
// ***************************************************************************************************

    function setsession() {
        $this->setid($this->FDados($_SESSION["id"]));
        $this->settitulo($this->FDados($_SESSION["titulo"]));
    }

// ***************************************************************************************************
// M�TODO PARA RETIRAR OS VALORES DAS SESSOES
// ***************************************************************************************************

    function unsetsession() {
        unset($_SESSION["id"]);
        unset($_SESSION["titulo"]);
    }

// ***************************************************************************************************
// M�TODO PARA SETAR CAMPOS POR POST
// ***************************************************************************************************

    function setpost() {
        $this->setid($this->FDados($_POST["id"]));
        $this->settitulo($this->FDados($_POST["titulo"]));
    }

// ***************************************************************************************************
// M�TODO PARA SETAR CAMPOS POR GET
// ***************************************************************************************************

    function setget() {

        $this->setid($this->FDados($_GET["id"]));
        $this->settitulo($this->FDados($_GET["titulo"]));
    }

// ***************************************************************************************************
// METODO QUE RETORNA ARRAY DE TODOS OS ID's EXISTENTES NA TABELA
// ***************************************************************************************************

    function todosIDs() {

        $sql = "SELECT id FROM tipos_subeventos";

        $this->database->Query($sql);

        while ($row = $this->database->FetchObject()) {
            $i++;
            $retorno[$i] = $row->funcionarios_id;
        }

        return $retorno;
    }

// ***************************************************************************************************
// METODO QUE ATUALIZA UM CAMPO
// ***************************************************************************************************

    function updatecampo($id, $campo, $valor, $tipo_id_INT) {

        if ($tipo_id_INT != "int" || $tipo_id_INT != "float" || $tipo_id_INT != "INT" || $tipo_id_INT != "FLOAT" || $tipo_id_INT != "Int" || $tipo_id_INT != "Float") {
            $tipo_id_INT = "'";
        }


        $sql = "UPDATE tipos_subeventos SET $campo = $tipo_id_INT$valor$tipo_id_INT WHERE id = $id";

        $this->database->Query($sql);
    }

// ***************************************************************************************************
// FUNCAO PARA FORMATAR OS VALORES DE DATA E VALORES DECIMAIS PARA GRAVAR NO BANCO
// ***************************************************************************************************

    function dadosBanco() {

    }

// ***************************************************************************************************
// FUNCAO PARA FORMATAR OS VALORES DE DATA E VALORES DECIMAIS PARA EXIBI��O
// ***************************************************************************************************

    function dadosExibe() {
        
    }

// ***************************************************************************************************
// METODOS AUXILIARES PARA FORMATACAO DE DADOS
// ***************************************************************************************************

    function FFloatBanco($numero) {

        if ($numero == "")
            return "0.00";

        $retorno = str_replace(".", "", $numero);
        $retorno = str_replace(",", ".", $retorno);

        return $retorno;
    }

    function FFloatExibe($numero) {

        if ($numero == "")
            return "";

        return number_format($numero, 2, ",", ".");
    }

    function FDataBanco($valor) {

        if ($valor != "" && $valor != null) {
            $partes = explode("/", $valor);

            $retorno = "" . $partes[2] . "-" . $partes[1] . "-" . $partes[0];

            return $retorno;
        }
        else
            return "0000-00-00";
    }

    function FDataExibe($valor) {

        if ($valor == "")
            return "";

        $partes = explode("-", $valor);

        $retorno = substr($partes[2], 0, 2) . "/" . $partes[1] . "/" . $partes[0];

        return $retorno;
    }

    function FDataTimeBanco() {

        $retorno = date("Y-m-d H:i:s");

        return $retorno;
    }

    function FDataTimeExibe($valor) {

        if ($valor == "")
            return "";

        $retorno = substr($valor, 8, 2) . "/" . substr($valor, 5, 2) . "/" . substr($valor, 0, 4) . " " . substr($valor, 11, 8);

        return $retorno;
    }

    function FDados($dado) {

        $strDado = $dado;
        $strDado = htmlspecialchars($strDado);
        $strDado = html_entity_decode($strDado);
        if (!get_magic_quotes_gpc()) {
            $strDado = addslashes($strDado);
        }

        return $strDado;
    }

}

// CLASSE FIM
?>