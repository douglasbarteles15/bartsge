<?php

// ***************************************************************************************************
// DECLARA��O DA CLASSE
// ***************************************************************************************************

class DAO_Noticias_fotos { // CLASSE IN�CIO
// ***************************************************************************************************
// DECLARA��O DE ATRIBUTOS
// ***************************************************************************************************

    var $noticias_fotos_id;   // CHAVE PRIM�RIA - AUTO INCREMENTO
    var $noticias_id_INT;   // (ATRIBUTO NORMAL)
    var $noticias_fotos_arquivo;   // (ATRIBUTO NORMAL)
    var $noticias_fotos_comentario;   // (ATRIBUTO NORMAL)
    var $noticias_fotos_dataCadastro_DATETIME;   // (ATRIBUTO NORMAL)
    var $noticias_fotos_status_INT;   // (ATRIBUTO NORMAL)
    var $noticias_img_mini;   // (ATRIBUTO NORMAL)
    var $noticias_img_full;   // (ATRIBUTO NORMAL)
    var $database; // INSTANCIA DA CLASSE DATABASE

// ***************************************************************************************************
// M�TODO CONSTRUTOR
// ***************************************************************************************************

    function DAO_Noticias_fotos() {

        $this->database = new Database();
    }

// ***************************************************************************************************
// M�TODOS - GETTER
// ***************************************************************************************************

    function getnoticias_fotos_id() {

        return $this->noticias_fotos_id;
    }

    function getnoticias_id_INT() {

        return $this->noticias_id_INT;
    }

    function getnoticias_fotos_arquivo() {

        return $this->noticias_fotos_arquivo;
    }

    function getnoticias_fotos_comentario() {

        return $this->noticias_fotos_comentario;
    }

    function getnoticias_fotos_dataCadastro_DATETIME() {

        return $this->noticias_fotos_dataCadastro_DATETIME;
    }

    function getnoticias_fotos_status_INT() {

        return $this->noticias_fotos_status_INT;
    }

    function getnoticias_img_mini() {

        return $this->noticias_img_mini;
    }

    function getnoticias_img_full() {

        return $this->noticias_img_full;
    }

// ***************************************************************************************************
// M�TODOS - SETTER
// ***************************************************************************************************


    function setnoticias_fotos_id($val) {

        $this->noticias_fotos_id = $val;
    }

    function setnoticias_id_INT($val) {

        $this->noticias_id_INT = $val;
    }

    function setnoticias_fotos_arquivo($val) {

        $this->noticias_fotos_arquivo = $val;
    }

    function setnoticias_fotos_comentario($val) {

        $this->noticias_fotos_comentario = $val;
    }

    function setnoticias_fotos_dataCadastro_DATETIME($val) {

        $this->noticias_fotos_dataCadastro_DATETIME = $val;
    }

    function setnoticias_fotos_status_INT($val) {

        $this->noticias_fotos_status_INT = $val;
    }

    function setnoticias_img_mini($val) {

        $this->noticias_img_full = $val;
    }

// ***************************************************************************************************
// M�TODO  - SELECT - RETORNA UMA LINHA DA TABELA
// ***************************************************************************************************

    function select($id) {

        $sql = "SELECT * FROM noticias_fotos WHERE noticias_fotos_id = $id;";
        $result = $this->database->Query($sql);
        $row = $this->database->FetchObject();


        $this->noticias_fotos_id = $row->noticias_fotos_id;

        $this->noticias_id_INT = $row->noticias_id_INT;

        $this->noticias_fotos_arquivo = $row->noticias_fotos_arquivo;

        $this->noticias_fotos_comentario = $row->noticias_fotos_comentario;

        $this->noticias_fotos_dataCadastro_DATETIME = $row->noticias_fotos_dataCadastro_DATETIME;

        $this->noticias_fotos_status_INT = $row->noticias_fotos_status_INT;

        $this->noticias_img_mini = $row->noticias_img_mini;

        $this->noticias_img_full = $row->noticias_img_full;
    }

// ***************************************************************************************************
// METODO QUE RETORNA A LINHA DO �LTIMO REGISTRO CRIADO NA TABELA
// ***************************************************************************************************

    function selectLastInsert() {
        $this->database->Query("select * from noticias_fotos where noticias_fotos_id = last_insert_id()");
        $row = $this->database->FetchObject();
        $this->select($row->noticias_fotos_id);
    }

// ***************************************************************************************************
// M�TODO  - SELECTBY - RETORNA LINHAS QUE ATENDEM A CONDI��O
// ***************************************************************************************************

    function selectby($field, $val, $tipo) {

        if ($tipo != "int" || $tipo != "float" || $tipo != "INT" || $tipo != "FLOAT" || $tipo != "Int" || $tipo != "Float") {
            $tipo = "'";
        }

        $field = $this->FDados($field);
        $val = $this->FDados($val);

        $sql = "SELECT * FROM noticias_fotos WHERE $field=$tipo$val$tipo";
        $this->database->Query($sql);
        return $this->database->FetchObject();
    }

// ***************************************************************************************************
// M�TODO  - SELECT APROXIMADO - RETORNA LINHAS QUE ATENDEM A CONDI��O
// ***************************************************************************************************

    function selectlike($field, $val) {

        $field = $this->FDados($field);
        $val = $this->FDados($val);

        $sql = "SELECT * FROM noticias_fotos WHERE $field LIKE '%$val%'";
        $this->database->Query($sql);
        return $this->database->FetchObject();
    }

// ***************************************************************************************************
// M�TODO - DELETE
// ***************************************************************************************************

    function delete($id) {

        $sql = "DELETE FROM noticias_fotos WHERE noticias_fotos_id = $id;";
        $result = $this->database->Query($sql);
    }

// ***************************************************************************************************
// M�TODO - INSERT
// ***************************************************************************************************

    function insert() {

        $this->noticias_fotos_id = ""; // Limpa a chave do auto incremento

        $sql = "INSERT INTO noticias_fotos ( noticias_id_INT,noticias_fotos_arquivo,noticias_fotos_comentario,noticias_fotos_dataCadastro_DATETIME,noticias_fotos_status_INT, noticias_img_mini, noticias_img_full ) VALUES ( $this->noticias_id_INT,'$this->noticias_fotos_arquivo','$this->noticias_fotos_comentario','$this->noticias_fotos_dataCadastro_DATETIME',$this->noticias_fotos_status_INT, '$this->noticias_img_mini', '$this->noticias_img_full' )";
        $result = $this->database->Query($sql);
    }

// ***************************************************************************************************
// M�TODO - UPDATE
// ***************************************************************************************************

    function update($id, $tipo) {

        if (isset($tipo["noticias_id_INT"])) {
            $upd.= "noticias_id_INT = $this->noticias_id_INT, ";
        }
        if (isset($tipo["noticias_fotos_arquivo"])) {
            $upd.= "noticias_fotos_arquivo = '$this->noticias_fotos_arquivo', ";
        }
        if (isset($tipo["noticias_fotos_comentario"])) {
            $upd.= "noticias_fotos_comentario = '$this->noticias_fotos_comentario', ";
        }
        if (isset($tipo["noticias_fotos_dataCadastro_DATETIME"])) {
            $upd.= "noticias_fotos_dataCadastro_DATETIME = '$this->noticias_fotos_dataCadastro_DATETIME', ";
        }
        if (isset($tipo["noticias_fotos_status_INT"])) {
            $upd.= "noticias_fotos_status_INT = $this->noticias_fotos_status_INT, ";
        }
        if (isset($tipo["noticias_img_mini"])) {
            $upd.= "noticias_img_mini = '$this->noticias_img_mini,' ";
        }
        if (isset($tipo["noticias_img_full"])) {
            $upd.= "noticias_img_full = '$this->noticias_img_full,' ";
        }
        $upd = substr($upd, 0, -2);


        $sql = " UPDATE noticias_fotos SET $upd WHERE noticias_fotos_id = $id ";

        $result = $this->database->Query($sql);
    }

// ***************************************************************************************************
// M�TODO PARA CRIAR VARIAVEIS DE SESSAO
// ***************************************************************************************************

    function createsession() {
        $_SESSION["noticias_fotos_id"] = $this->getnoticias_fotos_id();
        $_SESSION["noticias_id_INT"] = $this->getnoticias_id_INT();
        $_SESSION["noticias_fotos_arquivo"] = $this->getnoticias_fotos_arquivo();
        $_SESSION["noticias_fotos_comentario"] = $this->getnoticias_fotos_comentario();
        $_SESSION["noticias_fotos_dataCadastro_DATETIME"] = $this->getnoticias_fotos_dataCadastro_DATETIME();
        $_SESSION["noticias_fotos_status_INT"] = $this->getnoticias_fotos_status_INT();
        $_SESSION["noticias_img_mini"] = $this->getnoticias_img_mini();
        $_SESSION["noticias_img_full"] = $this->getnoticias_img_full();
    }

// ***************************************************************************************************
// M�TODO PARA SETAR CAMPOS POR SESSAO
// ***************************************************************************************************

    function setsession() {
        $this->setnoticias_fotos_id($this->FDados($_SESSION["noticias_fotos_id"]));
        $this->setnoticias_id_INT($this->FDados($_SESSION["noticias_id_INT"]));
        $this->setnoticias_fotos_arquivo($this->FDados($_SESSION["noticias_fotos_arquivo"]));
        $this->setnoticias_fotos_comentario($this->FDados($_SESSION["noticias_fotos_comentario"]));
        $this->setnoticias_fotos_dataCadastro_DATETIME($this->FDados($_SESSION["noticias_fotos_dataCadastro_DATETIME"]));
        $this->setnoticias_fotos_status_INT($this->FDados($_SESSION["noticias_fotos_status_INT"]));
        $this->setnoticias_img_mini($this->FDados($_SESSION["noticias_img_mini"]));
        $this->setnoticias_img_full($this->FDados($_SESSION["noticias_img_full"]));
    }

// ***************************************************************************************************
// M�TODO PARA RETIRAR OS VALORES DAS SESSOES
// ***************************************************************************************************

    function unsetsession() {
        unset($_SESSION["noticias_fotos_id"]);
        unset($_SESSION["noticias_id_INT"]);
        unset($_SESSION["noticias_fotos_arquivo"]);
        unset($_SESSION["noticias_fotos_comentario"]);
        unset($_SESSION["noticias_fotos_dataCadastro_DATETIME"]);
        unset($_SESSION["noticias_fotos_status_INT"]);
        unset($_SESSION["noticias_img_mini"]);
        unset($_SESSION["noticias_img_full"]);
    }

// ***************************************************************************************************
// M�TODO PARA SETAR CAMPOS POR POST
// ***************************************************************************************************

    function setpost() {
        $this->setnoticias_fotos_id($this->FDados($_POST["noticias_fotos_id"]));
        $this->setnoticias_id_INT($this->FDados($_POST["noticias_id_INT"]));
        $this->setnoticias_fotos_arquivo($this->FDados($_POST["noticias_fotos_arquivo"]));
        $this->setnoticias_fotos_comentario($this->FDados($_POST["noticias_fotos_comentario"]));
        $this->setnoticias_fotos_dataCadastro_DATETIME($this->FDados($_POST["noticias_fotos_dataCadastro_DATETIME"]));
        $this->setnoticias_fotos_status_INT($this->FDados($_POST["noticias_fotos_status_INT"]));
        $this->setnoticias_img_mini($this->FDados($_POST["noticias_img_mini"]));
        $this->setnoticias_img_full($this->FDados($_POST["noticias_img_full"]));
    }

// ***************************************************************************************************
// M�TODO PARA SETAR CAMPOS POR GET
// ***************************************************************************************************

    function setget() {

        $this->setnoticias_fotos_id($this->FDados($_GET["noticias_fotos_id"]));
        $this->setnoticias_id_INT($this->FDados($_GET["noticias_id_INT"]));
        $this->setnoticias_fotos_arquivo($this->FDados($_GET["noticias_fotos_arquivo"]));
        $this->setnoticias_fotos_comentario($this->FDados($_GET["noticias_fotos_comentario"]));
        $this->setnoticias_fotos_dataCadastro_DATETIME($this->FDados($_GET["noticias_fotos_dataCadastro_DATETIME"]));
        $this->setnoticias_fotos_status_INT($this->FDados($_GET["noticias_fotos_status_INT"]));
        $this->setnoticias_img_mini($this->FDados($_GET["noticias_img_mini"]));
        $this->setnoticias_img_full($this->FDados($_GET["noticias_img_full"]));
    }

// ***************************************************************************************************
// METODO QUE RETORNA ARRAY DE TODOS OS ID's EXISTENTES NA TABELA
// ***************************************************************************************************

    function todosIDs() {

        $sql = "SELECT noticias_fotos_id FROM noticias_fotos";

        $this->database->Query($sql);

        while ($row = $this->database->FetchObject()) {
            $i++;
            $retorno[$i] = $row->noticias_fotos_id;
        }

        return $retorno;
    }

// ***************************************************************************************************
// METODO QUE ATUALIZA UM CAMPO
// ***************************************************************************************************

    function updatecampo($id, $campo, $valor, $tipo) {

        if ($tipo != "int" || $tipo != "float" || $tipo != "INT" || $tipo != "FLOAT" || $tipo != "Int" || $tipo != "Float") {
            $tipo = "'";
        }


        $sql = "UPDATE noticias_fotos SET $campo = $tipo$valor$tipo WHERE noticias_fotos_id = $id";

        $this->database->Query($sql);
    }

// ***************************************************************************************************
// FUNCAO PARA FORMATAR OS VALORES DE DATA E VALORES DECIMAIS PARA GRAVAR NO BANCO
// ***************************************************************************************************

    function dadosBanco() {

        if ($this->getnoticias_id_INT() == "") {

            $this->setnoticias_id_INT(0);
        }

        if ($this->getnoticias_fotos_status_INT() == "") {

            $this->setnoticias_fotos_status_INT(0);
        }



        $this->setnoticias_fotos_dataCadastro_DATETIME($this->FDataTimeBanco($this->getnoticias_fotos_dataCadastro_DATETIME()));
    }

// ***************************************************************************************************
// FUNCAO PARA FORMATAR OS VALORES DE DATA E VALORES DECIMAIS PARA EXIBI��O
// ***************************************************************************************************

    function dadosExibe() {

        $this->setnoticias_fotos_dataCadastro_DATETIME($this->FDataTimeExibe($this->getnoticias_fotos_dataCadastro_DATETIME()));
    }

// ***************************************************************************************************
// METODOS AUXILIARES PARA FORMATACAO DE DADOS
// ***************************************************************************************************

    function FFloatBanco($numero) {

        if ($numero == "")
            return "0.00";

        $retorno = str_replace(".", "", $numero);
        $retorno = str_replace(",", ".", $retorno);

        return $retorno;
    }

    function FFloatExibe($numero) {

        if ($numero == "")
            return "";

        return number_format($numero, 2, ",", ".");
    }

    function FDataBanco($valor) {

        if ($valor != "" && $valor != null) {
            $partes = explode("/", $valor);

            $retorno = "" . $partes[2] . "-" . $partes[1] . "-" . $partes[0];

            return $retorno;
        }
        else
            return "0000-00-00";
    }

    function FDataExibe($valor) {

        if ($valor == "")
            return "";

        $partes = explode("-", $valor);

        $retorno = substr($partes[2], 0, 2) . "/" . $partes[1] . "/" . $partes[0];

        return $retorno;
    }

    function FDataTimeBanco() {

        $retorno = date("Y-m-d H:i:s");

        return $retorno;
    }

    function FDataTimeExibe($valor) {

        if ($valor == "")
            return "";

        $retorno = substr($valor, 8, 2) . "/" . substr($valor, 5, 2) . "/" . substr($valor, 0, 4) . " " . substr($valor, 11, 8);

        return $retorno;
    }

    function FDados($dado) {

        $strDado = $dado;
        $strDado = htmlspecialchars($strDado);
        $strDado = html_entity_decode($strDado);
        if (!get_magic_quotes_gpc()) {
            $strDado = addslashes($strDado);
        }

        return $strDado;
    }

}

// CLASSE FIM
?>