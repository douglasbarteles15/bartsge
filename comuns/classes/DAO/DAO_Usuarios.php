<?php

// ***************************************************************************************************
//
//	DATA CRIA��O:		02.08.2016
//	CLASSE:				DAO_Usuarios
//	TABELA MYSQL:		usuarios
//	BANCO MYSQL:		jfprofissionais
//									
// ***************************************************************************************************
// ***************************************************************************************************
// DECLARA��O DA CLASSE
// ***************************************************************************************************

class DAO_Usuarios { // CLASSE IN�CIO
// ***************************************************************************************************
// DECLARA��O DE ATRIBUTOS
// ***************************************************************************************************

    var $id;   // CHAVE PRIM�RIA - AUTO INCREMENTO
    var $nome;   // ATRIBUTO VARCHAR
    var $telefone;   // ATRIBUTO VARCHAR
    var $email;   // ATRIBUTO VARCHAR
    var $senha;   // ATRIBUTO VARCHAR
    var $img_mini;   // ATRIBUTO VARCHAR
    var $img_full;   // ATRIBUTO VARCHAR
    var $status_id_INT;   // ATRIBUTO INT
    var $datetime;   // ATRIBUTO INT
    
    var $database; // INSTANCIA DA CLASSE DATABASE

// ***************************************************************************************************
// M�TODO CONSTRUTOR
// ***************************************************************************************************

    function DAO_Usuarios() {

        $this->database = new Database();
    }

// ***************************************************************************************************
// M�TODOS - GETTER
// ***************************************************************************************************

    function getid() {

        return $this->id;
    }

    function getnome() {

        return $this->nome;
    }

    function gettelefone() {

        return $this->telefone;
    }

    function getemail() {

        return $this->email;
    }

    function getsenha() {

        return $this->senha;
    }

    function getimg_mini() {

        return $this->img_mini;
    }

    function getimg_full() {

        return $this->img_full;
    }

    function getstatus_id_INT() {

        return $this->status_id_INT;
    }

    function getdatetime() {

        return $this->datetime;
    }


// ***************************************************************************************************
// M�TODOS - SETTER
// ***************************************************************************************************


    function setid($val) {

        $this->id = $val;
    }

    function setnome($val) {

        $this->nome = $val;
    }

    function settelefone($val) {

        $this->telefone = $val;
    }

    function setemail($val) {

        $this->email = $val;
    }

    function setsenha($val) {

        $this->senha = $val;
    }

    function setimg_mini($val) {

        $this->img_mini = $val;
    }

    function setimg_full($val) {

        $this->img_full = $val;
    }

    function setstatus_id_INT($val) {

        $this->status_id_INT = $val;
    }

    function setdatetime($val) {

        $this->datetime = $val;
    }

// ***************************************************************************************************
// M�TODO  - SELECT - RETORNA UMA LINHA DA TABELA
// ***************************************************************************************************

    function select($id) {

        $sql = "SELECT * FROM usuarios WHERE id = $id;";
        $result = $this->database->Query($sql);
        $row = $this->database->FetchObject();


        $this->id = $row->id;
        $this->nome = $row->nome;
        $this->email = $row->email;
        $this->telefone = $row->telefone;
        $this->senha = $row->senha;
        $this->img_mini = $row->img_mini;
        $this->img_full = $row->img_full;
        $this->status_id_INT = $row->status_id_INT;
        $this->datetime = $row->datetime;

    }
// ***************************************************************************************************
// M�TODO  - SELECT - RETORNA UMA LINHA DA TABELA
// ***************************************************************************************************

    function selectLink($link) {

        $sql = "SELECT * FROM usuarios WHERE senha = '$link'";
        $result = $this->database->Query($sql);
        $row = $this->database->FetchObject();


        $this->id = $row->id;
        $this->nome = $row->nome;
        $this->email = $row->email;
        $this->telefone = $row->telefone;
        $this->senha = $row->senha;
        $this->img_mini = $row->img_mini;
        $this->img_full = $row->img_full;
        $this->status_id_INT = $row->status_id_INT;
        $this->datetime = $row->datetime;

    }

// ***************************************************************************************************
// METODO QUE RETORNA A LINHA DO �LTIMO REGISTRO CRIADO NA TABELA
// ***************************************************************************************************

    function selectLastInsert() {
        $this->database->Query("select * from usuarios where id = last_insert_id()");
        $row = $this->database->FetchObject();
        $this->select($row->id);
    }

    public function rows($sql) {
        $result = $this->database->Query($sql);
        $quantidade = $this->database->rows;
        //echo $quantidade; break;
        //$result = $this->database->Query($sql);
        return $quantidade;

    }

// ***************************************************************************************************
// M�TODO  - SELECTBY - RETORNA LINHAS QUE ATENDEM A CONDI��O
// ***************************************************************************************************

    function selectby($field, $val, $tipo) {

        if ($tipo != "int" || $tipo != "float" || $tipo != "INT" || $tipo != "FLOAT" || $tipo != "Int" || $tipo != "Float") {
            $tipo = "'";
        }

        $field = $this->FDados($field);
        $val = $this->FDados($val);

        $sql = "SELECT * FROM usuarios WHERE $field=$tipo$val$tipo";
        $this->database->Query($sql);
        return $this->database->FetchObject();
    }

// ***************************************************************************************************
// M�TODO  - SELECT APROXIMADO - RETORNA LINHAS QUE ATENDEM A CONDI��O
// ***************************************************************************************************

    function selectlike($field, $val) {

        $field = $this->FDados($field);
        $val = $this->FDados($val);

        $sql = "SELECT * FROM usuarios WHERE $field LIKE '%$val%'";
        $this->database->Query($sql);
        return $this->database->FetchObject();
    }

// ***************************************************************************************************
// M�TODO - DELETE
// ***************************************************************************************************

    function delete($id) {

        $sql = "DELETE FROM usuarios WHERE id = $id;";
        $result = $this->database->Query($sql);
    }

// ***************************************************************************************************
// M�TODO - INSERT
// ***************************************************************************************************

    function insert() {

        $this->id = ""; // Limpa a chave do auto incremento

        $sql = "INSERT INTO usuarios ( nome, telefone,email,senha, img_mini, img_full, status_id_INT, datetime ) VALUES ( '$this->nome', '$this->telefone', '$this->email', '$this->senha', '$this->img_mini', '$this->img_full', '$this->status_id_INT', '$this->datetime')";
        $result = $this->database->Query($sql);
    }

// ***************************************************************************************************
// M�TODO - UPDATE
// ***************************************************************************************************

    function update($id, $tipo) {

        if (isset($tipo["nome"])) {
            $upd.= "nome = '$this->nome', ";
        }

        if (isset($tipo["telefone"])) {
            $upd.= "telefone = '$this->telefone', ";
        }

        if (isset($tipo["email"])) {
            $upd.= "email = '$this->email', ";
        }

        if (isset($tipo["senha"])) {
            $upd.= "senha = '$this->senha', ";
        }

        if (isset($tipo["img_mini"])) {
            $upd.= "img_mini = '$this->img_mini', ";
        }

        if (isset($tipo["img_full"])) {
            $upd.= "img_full = '$this->img_full', ";
        }

        if (isset($tipo["status_id_INT"])) {
            $upd.= "status_id_INT = '$this->status_id_INT', ";
        }

        if (isset($tipo["datetime"])) {
            $upd.= "datetime = '$this->datetime', ";
        }

        $upd = substr($upd, 0, -2);


        $sql = " UPDATE usuarios SET $upd WHERE id = $id ";

        $result = $this->database->Query($sql);
    }

// ***************************************************************************************************
// M�TODO PARA CRIAR VARIAVEIS DE SESSAO
// ***************************************************************************************************

    function createsession() {
        $_SESSION["id"] = $this->getid();
        $_SESSION["nome"] = $this->getnome();
        $_SESSION["telefone"] = $this->gettelefone();
        $_SESSION["email"] = $this->getemail();
        $_SESSION["senha"] = $this->getsenha();
        $_SESSION["img_mini"] = $this->getimg_mini();
        $_SESSION["img_full"] = $this->getimg_full();
        $_SESSION["status_id_INT"] = $this->getstatus_id_INT();
        $_SESSION["datetime"] = $this->getdatetime();
    }

// ***************************************************************************************************
// M�TODO PARA SETAR CAMPOS POR SESSAO
// ***************************************************************************************************

    function setsession() {
        $this->setid($this->FDados($_SESSION["id"]));
        $this->setnome($this->FDados($_SESSION["nome"]));
        $this->settelefone($this->FDados($_SESSION["telefone"]));
        $this->setemail($this->FDados($_SESSION["email"]));
        $this->setsenha($this->FDados($_SESSION["senha"]));
        $this->setimg_mini($this->FDados($_SESSION["img_mini"]));
        $this->setimg_full($this->FDados($_SESSION["img_full"]));
        $this->setstatus_id_INT($this->FDados($_SESSION["status_id_INT"]));
        $this->setdatetime($this->FDados($_SESSION["datetime"]));
    }

// ***************************************************************************************************
// M�TODO PARA RETIRAR OS VALORES DAS SESSOES
// ***************************************************************************************************

    function unsetsession() {
        unset($_SESSION["id"]);
        unset($_SESSION["nome"]);
        unset($_SESSION["telefone"]);
        unset($_SESSION["email"]);
        unset($_SESSION["senha"]);
        unset($_SESSION["img_mini"]);
        unset($_SESSION["img_full"]);
        unset($_SESSION["status_id_INT"]);
        unset($_SESSION["datetime"]);

    }

// ***************************************************************************************************
// M�TODO PARA SETAR CAMPOS POR POST
// ***************************************************************************************************

    function setpost() {
        $this->setid($this->FDados($_POST["id"]));
        $this->setnome($this->FDados($_POST["nome"]));
        $this->settelefone($this->FDados($_POST["telefone"]));
        $this->setemail($this->FDados($_POST["email"]));
        $this->setsenha($this->FDados($_POST["senha"]));
        $this->setimg_mini($this->FDados($_POST["img_mini"]));
        $this->setimg_full($this->FDados($_POST["img_full"]));
        $this->setstatus_id_INT($this->FDados($_POST["status_id_INT"]));
        $this->setdatetime($this->FDados($_POST["datetime"]));
    }

// ***************************************************************************************************
// M�TODO PARA SETAR CAMPOS POR GET
// ***************************************************************************************************

    function setget() {

        $this->setid($this->FDados($_GET["id"]));
        $this->setnome($this->FDados($_GET["nome"]));
        $this->settelefone($this->FDados($_GET["telefone"]));
        $this->setemail($this->FDados($_GET["email"]));
        $this->setsenha($this->FDados($_GET["senha"]));
        $this->setimg_mini($this->FDados($_GET["img_mini"]));
        $this->setimg_full($this->FDados($_GET["img_full"]));
        $this->setstatus_id_INT($this->FDados($_GET["status_id_INT"]));
        $this->setdatetime($this->FDados($_GET["datetime"]));

    }

// ***************************************************************************************************
// METODO QUE RETORNA ARRAY DE TODOS OS ID's EXISTENTES NA TABELA
// ***************************************************************************************************

    function todosIDs() {

        $sql = "SELECT id FROM usuarios";

        $this->database->Query($sql);

        while ($row = $this->database->FetchObject()) {
            $i++;
            $retorno[$i] = $row->id;
        }

        return $retorno;
    }

// ***************************************************************************************************
// METODO QUE ATUALIZA UM CAMPO
// ***************************************************************************************************

    function updatecampo($id, $campo, $valor, $tipo) {

        if ($tipo != "int" || $tipo != "float" || $tipo != "INT" || $tipo != "FLOAT" || $tipo != "Int" || $tipo != "Float") {
            $tipo = "'";
        }


        $sql = "UPDATE usuarios SET $campo = $tipo$valor$tipo WHERE id = $id";

        $this->database->Query($sql);
    }

// ***************************************************************************************************
// FUNCAO PARA FORMATAR OS VALORES DE DATA E VALORES DECIMAIS PARA GRAVAR NO BANCO
// ***************************************************************************************************

    function dadosBanco() {

        $this->setdatetime(date('Y-m-d H:i:s'));

    }

// ***************************************************************************************************
// FUNCAO PARA FORMATAR OS VALORES DE DATA E VALORES DECIMAIS PARA EXIBI��O
// ***************************************************************************************************

    function dadosExibe() {

    }

// ***************************************************************************************************
// METODOS AUXILIARES PARA FORMATACAO DE DADOS
// ***************************************************************************************************

    function FFloatBanco($numero) {

        if ($numero == "")
            return "0.00";

        $retorno = str_replace(".", "", $numero);
        $retorno = str_replace(",", ".", $retorno);

        return $retorno;
    }

    function FFloatExibe($numero) {

        if ($numero == "")
            return "";

        return number_format($numero, 2, ",", ".");
    }

    function FDataBanco($valor) {

        if ($valor != "" && $valor != null) {
            $partes = explode("/", $valor);

            $retorno = "" . $partes[2] . "-" . $partes[1] . "-" . $partes[0];

            return $retorno;
        }
        else
            return "0000-00-00";
    }

    function FDataExibe($valor) {

        if ($valor == "")
            return "";

        $partes = explode("-", $valor);

        $retorno = substr($partes[2], 0, 2) . "/" . $partes[1] . "/" . $partes[0];

        return $retorno;
    }

    function FDataTimeBanco() {

        $retorno = date("Y-m-d H:i:s");

        return $retorno;
    }

    function FDataTimeExibe($valor) {

        if ($valor == "")
            return "";

        $retorno = substr($valor, 8, 2) . "/" . substr($valor, 5, 2) . "/" . substr($valor, 0, 4) . " " . substr($valor, 11, 8);

        return $retorno;
    }

    function FDados($dado) {

        $strDado = $dado;
        $strDado = htmlspecialchars($strDado);
        $strDado = html_entity_decode($strDado);
        if (!get_magic_quotes_gpc()) {
            $strDado = addslashes($strDado);
        }

        return $strDado;
    }



    // Monta a URL Amigável
    function montaURL($variavel) {
        $variavel = htmlentities(strtolower($variavel));
        $variavel = preg_replace("/&(.)(acute|cedil|circ|ring|tilde|uml);/", "$1", $variavel);
        $variavel = preg_replace("/([^a-z0-9]+)/", "-", html_entity_decode($variavel));
        $variavel = trim($variavel, "-");
        return $variavel;
    }

    //Verifica se já existe URL para poder Montar uma nova
    function montaURLNova($variavel, $idconteudo) {
        $query = "select * from usuarios where senha  = '$variavel'";
        $Banco = new Database();
        $num_rows = mysql_num_rows($Banco->Query($query));
//        $num_rows = @mysql_num_rows($query);
        $Banco->Query($query);
        while ($dados = $Banco->FetchArray()) {
//        while ($dados = mysql_fetch_array($query)) {
            $id = $dados["0"];
        }

        if ($idconteudo <> $id) {
            if ($num_rows == 1) {
                while ($num_rows <> $num_post) {
                    $novo = $variavel . "-" . $num_rows;

                    $sql2 = "select * from usuarios where senha = '$novo'";


                    $query2 = mysql_query($sql2);
                    $num_rows2 = @mysql_num_rows($query2);

                    if ($num_rows2 == 0) {
                        $variavel = $novo;
                        $num_post = $num_rows;
                    } else {
                        $num_rows++;
                    }
                }
            }
        }

        return $variavel;
    }


}

// CLASSE FIM
?>