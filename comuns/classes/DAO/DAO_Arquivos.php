<?php

// ***************************************************************************************************
//
//	DATA CRIA��O:		03.08.2016
//	CLASSE:				DAO_Arquivos
//	TABELA MYSQL:		arquivos
//	BANCO MYSQL:		bartsge
//									
// ***************************************************************************************************
// ***************************************************************************************************
// DECLARA��O DA CLASSE
// ***************************************************************************************************

class DAO_Arquivos { // CLASSE IN�CIO
// ***************************************************************************************************
// DECLARA��O DE ATRIBUTOS
// ***************************************************************************************************

    var $id;   // CHAVE PRIM�RIA - AUTO INCREMENTO
    var $subevento_id_INT;   // ATRIBUTO NORMAL
    var $titulo;   // ATRIBUTO NORMAL
    var $datetime;   // ATRIBUTO NORMAL
    var $link;   // ATRIBUTO NORMAL
    
    var $database; // INSTANCIA DA CLASSE DATABASE

// ***************************************************************************************************
// M�TODO CONSTRUTOR
// ***************************************************************************************************

    function DAO_Arquivos() {

        $this->database = new Database();
    }

// ***************************************************************************************************
// M�TODOS - GETTER
// ***************************************************************************************************

    function getid() {

        return $this->id;
    }

    function getsubevento_id_INT() {

        return $this->subevento_id_INT;
    }

    function gettitulo() {

        return $this->titulo;
    }

    function getdatetime() {

        return $this->datetime;
    }

    function getlink() {

        return $this->link;
    }
    

// ***************************************************************************************************
// M�TODOS - SETTER
// ***************************************************************************************************


    function setid($val) {

        $this->id = $val;
    }

    function setsubevento_id_INT($val) {

        $this->subevento_id_INT = $val;
    }

    function settitulo($val) {

        $this->titulo = $val;
    }

    function setdatetime($val) {

        $this->datetime = $val;
    }

    function setlink($val) {

        $this->link = $val;
    }


// ***************************************************************************************************
// M�TODO  - SELECT - RETORNA UMA LINHA DA TABELA
// ***************************************************************************************************

    function select($id) {

        $sql = "SELECT * FROM arquivos WHERE id = $id;";
        $result = $this->database->Query($sql);
        $row = $this->database->FetchObject();


        $this->id = $row->id;
        $this->subevento_id_INT = $row->subevento_id_INT;
        $this->datetime = $row->datetime;
        $this->titulo = $row->titulo;
        $this->link = $row->link;
    }

// ***************************************************************************************************
// METODO QUE RETORNA A LINHA DO �LTIMO REGISTRO CRIADO NA TABELA
// ***************************************************************************************************

    function selectLastInsert() {
        $this->database->Query("select * from arquivos where id = last_insert_id()");
        $row = $this->database->FetchObject();
        $this->select($row->id);
    }

// ***************************************************************************************************
// M�TODO  - SELECTBY - RETORNA LINHAS QUE ATENDEM A CONDI��O
// ***************************************************************************************************

    function selectby($field, $val, $tipo) {

        if ($tipo != "int" || $tipo != "float" || $tipo != "INT" || $tipo != "FLOAT" || $tipo != "Int" || $tipo != "Float") {
            $tipo = "'";
        }

        $field = $this->FDados($field);
        $val = $this->FDados($val);

        $sql = "SELECT * FROM arquivos WHERE $field=$tipo$val$tipo";
        $this->database->Query($sql);
        return $this->database->FetchObject();
    }

// ***************************************************************************************************
// M�TODO  - SELECT APROXIMADO - RETORNA LINHAS QUE ATENDEM A CONDI��O
// ***************************************************************************************************

    function selectlike($field, $val) {

        $field = $this->FDados($field);
        $val = $this->FDados($val);

        $sql = "SELECT * FROM arquivos WHERE $field LIKE '%$val%'";
        $this->database->Query($sql);
        return $this->database->FetchObject();
    }

// ***************************************************************************************************
// M�TODO - DELETE
// ***************************************************************************************************

    function delete($id) {

        $sql = "DELETE FROM arquivos WHERE id = $id;";
        $result = $this->database->Query($sql);
    }

// ***************************************************************************************************
// M�TODO - INSERT
// ***************************************************************************************************

    function insert() {

        $this->id = ""; // Limpa a chave do auto incremento

        $sql = "INSERT INTO arquivos ( subevento_id_INT, titulo,datetime,link ) VALUES ( '$this->subevento_id_INT', '$this->titulo', '$this->datetime', '$this->link' )";
        $result = $this->database->Query($sql);
    }

// ***************************************************************************************************
// M�TODO - UPDATE
// ***************************************************************************************************

    function update($id, $tipo) {

        if (isset($tipo["subevento_id_INT"])) {
            $upd.= "subevento_id_INT = '$this->subevento_id_INT', ";
        }

        if (isset($tipo["titulo"])) {
            $upd.= "titulo = '$this->titulo', ";
        }

        if (isset($tipo["datetime"])) {
            $upd.= "datetime = '$this->datetime', ";
        }

        if (isset($tipo["link"])) {
            $upd.= "link = '$this->link', ";
        }

        $upd = substr($upd, 0, -2);


        $sql = " UPDATE arquivos SET $upd WHERE id = $id ";

        $result = $this->database->Query($sql);
    }

// ***************************************************************************************************
// M�TODO PARA CRIAR VARIAVEIS DE SESSAO
// ***************************************************************************************************

    function createsession() {
        $_SESSION["id"] = $this->getid();
        $_SESSION["subevento_id_INT"] = $this->getsubevento_id_INT();
        $_SESSION["arquivo_emial"] = $this->gettitulo();
        $_SESSION["datetime"] = $this->getdatetime();
        $_SESSION["link"] = $this->getlink();
    }

// ***************************************************************************************************
// M�TODO PARA SETAR CAMPOS POR SESSAO
// ***************************************************************************************************

    function setsession() {
        $this->setid($this->FDados($_SESSION["id"]));
        $this->setsubevento_id_INT($this->FDados($_SESSION["subevento_id_INT"]));
        $this->settitulo($this->FDados($_SESSION["titulo"]));
        $this->setdatetime($this->FDados($_SESSION["datetime"]));
        $this->setlink($this->FDados($_SESSION["link"]));
    }

// ***************************************************************************************************
// M�TODO PARA RETIRAR OS VALORES DAS SESSOES
// ***************************************************************************************************

    function unsetsession() {
        unset($_SESSION["id"]);
        unset($_SESSION["subevento_id_INT"]);
        unset($_SESSION["titulo"]);
        unset($_SESSION["datetime"]);
        unset($_SESSION["link"]);

    }

// ***************************************************************************************************
// M�TODO PARA SETAR CAMPOS POR POST
// ***************************************************************************************************

    function setpost() {
        $this->setid($this->FDados($_POST["id"]));
        $this->setsubevento_id_INT($this->FDados($_POST["subevento_id_INT"], ENT_COMPAT, "UTF-8"));
        $this->settitulo($this->FDados($_POST["titulo"]));
        $this->setdatetime($this->FDados($_POST["datetime"]));
        $this->setlink($this->FDados($_POST["link"]));
    }

// ***************************************************************************************************
// M�TODO PARA SETAR CAMPOS POR GET
// ***************************************************************************************************

    function setget() {

        $this->setid($this->FDados($_GET["id"]));
        $this->setsubevento_id_INT($this->FDados($_GET["subevento_id_INT"]));
        $this->settitulo($this->FDados($_GET["titulo"]));
        $this->setdatetime($this->FDados($_GET["datetime"]));
        $this->setlink($this->FDados($_GET["link"]));

    }

// ***************************************************************************************************
// METODO QUE RETORNA ARRAY DE TODOS OS ID's EXISTENTES NA TABELA
// ***************************************************************************************************

    function todosIDs() {

        $sql = "SELECT id FROM arquivos";

        $this->database->Query($sql);

        while ($row = $this->database->FetchObject()) {
            $i++;
            $retorno[$i] = $row->id;
        }

        return $retorno;
    }

// ***************************************************************************************************
// METODO QUE ATUALIZA UM CAMPO
// ***************************************************************************************************

    function updatecampo($id, $campo, $valor, $tipo) {

        if ($tipo != "int" || $tipo != "float" || $tipo != "INT" || $tipo != "FLOAT" || $tipo != "Int" || $tipo != "Float") {
            $tipo = "'";
        }


        $sql = "UPDATE arquivos SET $campo = $tipo$valor$tipo WHERE id = $id";

        $this->database->Query($sql);
    }

// ***************************************************************************************************
// FUNCAO PARA FORMATAR OS VALORES DE DATA E VALORES DECIMAIS PARA GRAVAR NO BANCO
// ***************************************************************************************************

    function dadosBanco() {
        $this->setdatetime(date('Y-m-d H:i:s'));
    }

// ***************************************************************************************************
// FUNCAO PARA FORMATAR OS VALORES DE DATA E VALORES DECIMAIS PARA EXIBI��O
// ***************************************************************************************************

    function dadosExibe() {

    }

// ***************************************************************************************************
// METODOS AUXILIARES PARA FORMATACAO DE DADOS
// ***************************************************************************************************

    function FFloatBanco($numero) {

        if ($numero == "")
            return "0.00";

        $retorno = str_replace(".", "", $numero);
        $retorno = str_replace(",", ".", $retorno);

        return $retorno;
    }

    function FFloatExibe($numero) {

        if ($numero == "")
            return "";

        return number_format($numero, 2, ",", ".");
    }

    function FDataBanco($valor) {

        if ($valor != "" && $valor != null) {
            $partes = explode("/", $valor);

            $retorno = "" . $partes[2] . "-" . $partes[1] . "-" . $partes[0];

            return $retorno;
        }
        else
            return "0000-00-00";
    }

    function FDataExibe($valor) {

        if ($valor == "")
            return "";

        $partes = explode("-", $valor);

        $retorno = substr($partes[2], 0, 2) . "/" . $partes[1] . "/" . $partes[0];

        return $retorno;
    }

    function FDataTimeBanco() {

        $retorno = date("Y-m-d H:i:s");

        return $retorno;
    }

    function FDataTimeExibe($valor) {

        if ($valor == "")
            return "";

        $retorno = substr($valor, 8, 2) . "/" . substr($valor, 5, 2) . "/" . substr($valor, 0, 4) . " " . substr($valor, 11, 8);

        return $retorno;
    }

    function FDados($dado) {

        $strDado = $dado;
        $strDado = htmlspecialchars($strDado);
        $strDado = html_entity_decode($strDado);
        if (!get_magic_quotes_gpc()) {
            $strDado = addslashes($strDado);
        }

        return $strDado;
    }



    // Monta a URL Amigável
    function montaURL($variavel) {
        $variavel = htmlentities(strtolower($variavel));
        $variavel = preg_replace("/&(.)(acute|cedil|circ|ring|tilde|uml);/", "$1", $variavel);
        $variavel = preg_replace("/([^a-z0-9]+)/", "-", html_entity_decode($variavel));
        $variavel = trim($variavel, "-");
        return $variavel;
    }

    //Verifica se já existe URL para poder Montar uma nova
    function montaURLNova($variavel, $idconteudo) {
        $query = "select * from arquivos where link  = '$variavel'";
        $Banco = new Database();
        $num_rows = mysql_num_rows($Banco->Query($query));
//        $num_rows = @mysql_num_rows($query);
        $Banco->Query($query);
        while ($dados = $Banco->FetchArray()) {
//        while ($dados = mysql_fetch_array($query)) {
            $id = $dados["0"];
        }

        if ($idconteudo <> $id) {
            if ($num_rows == 1) {
                while ($num_rows <> $num_post) {
                    $novo = $variavel . "-" . $num_rows;

                    $sql2 = "select * from arquivos where link = '$novo'";


                    $query2 = mysql_query($sql2);
                    $num_rows2 = @mysql_num_rows($query2);

                    if ($num_rows2 == 0) {
                        $variavel = $novo;
                        $num_post = $num_rows;
                    } else {
                        $num_rows++;
                    }
                }
            }
        }

        return $variavel;
    }


}

// CLASSE FIM
?>