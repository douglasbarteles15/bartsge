<?php

// ***************************************************************************************************
//
//	DATA CRIACAO:		03.08.2016
//	CLASSE:				DAO_Categorias
//	TABELA MYSQL:		categorias
//	BANCO MYSQL:		bartsge
//
// ***************************************************************************************************
// ***************************************************************************************************
// DECLARACAO DA CLASSE
// ***************************************************************************************************

class DAO_Categorias { // CLASSE INï¿½CIO
// ***************************************************************************************************
// DECLARAï¿½ï¿½O DE ATRIBUTOS
// ***************************************************************************************************

    var $id;   // CHAVE PRIMï¿½RIA - AUTO INCREMENTO
    var $titulo;   // (ATRIBUTO NORMAL)
    var $link;   // (ATRIBUTO NORMAL)
    var $img_mini;   // (ATRIBUTO NORMAL)
    var $img_full;   // (ATRIBUTO NORMAL)

    var $database; // INSTANCIA DA CLASSE DATABASE

// ***************************************************************************************************
// Mï¿½TODO CONSTRUTOR
// ***************************************************************************************************

    function DAO_Categorias() {

        $this->database = new Database();
    }

// ***************************************************************************************************
// Mï¿½TODOS - GETTER
// ***************************************************************************************************

    function getid() {

        return $this->id;
    }

    function gettitulo() {

        return $this->titulo;
    }

    function getlink() {

        return $this->link;
    }

    function getimg_mini() {

        return $this->img_mini;
    }

    function getimg_full() {

        return $this->img_full;
    }

// ***************************************************************************************************
// Mï¿½TODOS - SETTER
// ***************************************************************************************************


    function setid($val) {

        $this->id = $val;
    }

    function settitulo($val) {

        $this->titulo = $val;
    }

    function setlink($val) {

        $this->link = $val;
    }

    function setimg_mini($val) {

        $this->img_mini = $val;
    }

    function setimg_full($val) {

        $this->img_full = $val;
    }

// ***************************************************************************************************
// Mï¿½TODO  - SELECT - RETORNA UMA LINHA DA TABELA
// ***************************************************************************************************

    function select($id) {

        $sql = "SELECT * FROM categorias WHERE id = $id;";
        $result = $this->database->Query($sql);
        $row = $this->database->FetchObject();


        $this->id = $row->id;

        $this->titulo = $row->titulo;

        $this->link = $row->link;

        $this->img_mini = $row->img_mini;

        $this->img_full = $row->img_full;

    }

// ***************************************************************************************************
// Mï¿½TODO  - SELECT - RETORNA UMA LINHA DA TABELA COM O LINK
// ***************************************************************************************************

    function selectLink($link) {

        $sql = "SELECT * FROM categorias WHERE link = '$link'";
        $result = $this->database->Query($sql);
        $row = $this->database->FetchObject();


        $this->id = $row->id;

        $this->titulo = $row->titulo;

        $this->link = $row->link;

        $this->img_mini = $row->img_mini;

        $this->img_full = $row->img_full;

    }

// ***************************************************************************************************
// METODO QUE RETORNA A LINHA DO ï¿½LTIMO REGISTRO CRIADO NA TABELA
// ***************************************************************************************************

    function selectLastInsert() {
        $this->database->Query("select * from categorias where id = last_insert_id()");
        $row = $this->database->FetchObject();
        $this->select($row->id);
    }
    
    public function rows($sql) {
		 $result = $this->database->Query($sql);
		 $quantidade = $this->database->rows; 
		 //echo $quantidade; break;
        //$result = $this->database->Query($sql);
        return $quantidade; 
           
    }	

// ***************************************************************************************************
// Mï¿½TODO  - SELECTBY - RETORNA LINHAS QUE ATENDEM A CONDIï¿½ï¿½O
// ***************************************************************************************************

    function selectby($field, $val, $tipo) {

        if ($tipo != "int" || $tipo != "float" || $tipo != "INT" || $tipo != "FLOAT" || $tipo != "Int" || $tipo != "Float") {
            $tipo = "'";
        }

        $field = $this->FDados($field);
        $val = $this->FDados($val);

        $sql = "SELECT * FROM categorias WHERE $field=$tipo$val$tipo";
        $this->database->Query($sql);
        return $this->database->FetchObject();
    }

// ***************************************************************************************************
// Mï¿½TODO  - SELECT APROXIMADO - RETORNA LINHAS QUE ATENDEM A CONDIï¿½ï¿½O
// ***************************************************************************************************

    function selectlike($field, $val) {

        $field = $this->FDados($field);
        $val = $this->FDados($val);

        $sql = "SELECT * FROM categorias WHERE $field LIKE '%$val%'";
        $this->database->Query($sql);
        return $this->database->FetchObject();
    }

// ***************************************************************************************************
// Mï¿½TODO - DELETE
// ***************************************************************************************************

    function delete($id) {

        $sql = "DELETE FROM categorias WHERE id = $id;";
        $result = $this->database->Query($sql);
    }

// ***************************************************************************************************
// Mï¿½TODO - INSERT
// ***************************************************************************************************

    function insert() {

        $this->id = ""; // Limpa a chave do auto incremento

        $sql = "INSERT INTO categorias (titulo, link, img_mini, img_full) VALUES ( '$this->titulo','$this->link','$this->img_mini','$this->img_full' )";
        $result = $this->database->Query($sql);
    }

// ***************************************************************************************************
// Mï¿½TODO - UPDATE
// ***************************************************************************************************

    function update($id, $tipo) {

        if (isset($tipo["titulo"])) {
            $upd.= "titulo = '$this->titulo', ";
        }
        if (isset($tipo["link"])) {
            $upd.= "link = '$this->link', ";
        }
        if (isset($tipo["img_mini"])) {
            $upd.= "img_mini = '$this->img_mini', ";
        }
        if (isset($tipo["img_full"])) {
            $upd.= "img_full = '$this->img_full', ";
        }

        $upd = substr($upd, 0, -2);


        $sql = " UPDATE categorias SET $upd WHERE id = $id ";

        $result = $this->database->Query($sql);
    }

// ***************************************************************************************************
// Mï¿½TODO PARA CRIAR VARIAVEIS DE SESSAO
// ***************************************************************************************************

    function createsession() {
        $_SESSION["id"] = $this->getid();
        $_SESSION["titulo"] = $this->gettitulo();
        $_SESSION["link"] = $this->getlink();
        $_SESSION["img_mini"] = $this->getimg_mini();
        $_SESSION["img_full"] = $this->getimg_full();

    }

// ***************************************************************************************************
// Mï¿½TODO PARA SETAR CAMPOS POR SESSAO
// ***************************************************************************************************

    function setsession() {
        $this->setid($this->FDados($_SESSION["id"]));
        $this->settitulo($this->FDados($_SESSION["titulo"]));
        $this->setlink($this->FDados($_SESSION["link"]));
        $this->setimg_mini($this->FDados($_SESSION["img_mini"]));
        $this->setimg_full($this->FDados($_SESSION["img_full"]));

    }

// ***************************************************************************************************
// Mï¿½TODO PARA RETIRAR OS VALORES DAS SESSOES
// ***************************************************************************************************

    function unsetsession() {
        unset($_SESSION["id"]);
        unset($_SESSION["titulo"]);
        unset($_SESSION["link"]);
        unset($_SESSION["img_mini"]);
        unset($_SESSION["img_full"]);
    }

// ***************************************************************************************************
// Mï¿½TODO PARA SETAR CAMPOS POR POST
// ***************************************************************************************************

    function setpost() {
        $this->setid($this->FDados($_POST["id"]));
        $this->settitulo($this->FDados($_POST["titulo"]));
//        $this->setlink($this->FDados($this->montaURL($_POST["titulo"])));
        $this->setlink($this->FDados($this->montaURLNova($this->montaURL($_POST["titulo"]), $_POST["id"])));
        $this->setimg_mini($this->FDados($_POST["img_mini"]));
        $this->setimg_full($this->FDados($_POST["img_full"]));
    }

// ***************************************************************************************************
// Mï¿½TODO PARA SETAR CAMPOS POR GET
// ***************************************************************************************************

    function setget() {

        $this->setid($this->FDados($_GET["id"]));
        $this->settitulo($this->FDados($_GET["titulo"]));
        $this->setlink($this->FDados($_GET["link"]));
        $this->setimg_mini($this->FDados($_GET["img_mini"]));
        $this->setimg_full($this->FDados($_GET["img_full"]));
    }

// ***************************************************************************************************
// METODO QUE RETORNA ARRAY DE TODOS OS ID's EXISTENTES NA TABELA
// ***************************************************************************************************

    function todosIDs() {

        $sql = "SELECT id FROM categorias";

        $this->database->Query($sql);

        while ($row = $this->database->FetchObject()) {
            $i++;
            $retorno[$i] = $row->id;
        }

        return $retorno;
    }

// ***************************************************************************************************
// METODO QUE ATUALIZA UM CAMPO
// ***************************************************************************************************

    function updatecampo($id, $campo, $valor, $tipo) {

        if ($tipo != "int" || $tipo != "float" || $tipo != "INT" || $tipo != "FLOAT" || $tipo != "Int" || $tipo != "Float") {
            $tipo = "'";
        }


        $sql = "UPDATE categorias SET $campo = $tipo$valor$tipo WHERE id = $id";

        $this->database->Query($sql);
    }

// ***************************************************************************************************
// FUNCAO PARA FORMATAR OS VALORES DE DATA E VALORES DECIMAIS PARA GRAVAR NO BANCO
// ***************************************************************************************************

    function dadosBanco() {

    }

// ***************************************************************************************************
// FUNCAO PARA FORMATAR OS VALORES DE DATA E VALORES DECIMAIS PARA EXIBIï¿½ï¿½O
// ***************************************************************************************************

    function dadosExibe() {

    }

// ***************************************************************************************************
// METODOS AUXILIARES PARA FORMATACAO DE DADOS
// ***************************************************************************************************

    function FFloatBanco($numero) {

        if ($numero == "")
            return "0.00";

        $retorno = str_replace(".", "", $numero);
        $retorno = str_replace(",", ".", $retorno);

        return $retorno;
    }

    function FFloatExibe($numero) {

        if ($numero == "")
            return "";

        return number_format($numero, 2, ",", ".");
    }

    function FDataBanco($valor) {

        if ($valor != "" && $valor != null) {
            $partes = explode("/", $valor);

            $retorno = "" . $partes[2] . "-" . $partes[1] . "-" . $partes[0];

            return $retorno;
        }
        else
            return "0000-00-00";
    }

    function FDataExibe($valor) {

        if ($valor == "")
            return "";

        $partes = explode("-", $valor);

        $retorno = substr($partes[2], 0, 2) . "/" . $partes[1] . "/" . $partes[0];

        return $retorno;
    }

    function FDataTimeBanco() {

        $retorno = date("Y-m-d H:i:s");

        return $retorno;
    }

    function FDataTimeExibe($valor) {

        if ($valor == "")
            return "";

        $retorno = substr($valor, 8, 2) . "/" . substr($valor, 5, 2) . "/" . substr($valor, 0, 4) . " " . substr($valor, 11, 8);

        return $retorno;
    }

    function FDados($dado) {

        $strDado = $dado;
        $strDado = htmlspecialchars($strDado);
        $strDado = html_entity_decode($strDado);
        if (!get_magic_quotes_gpc()) {
            $strDado = addslashes($strDado);
        }

        return $strDado;
    }

    // Monta a URL AmigÃ¡vel
    function montaURL($variavel) {
        $variavel = htmlentities(strtolower($variavel));
        $variavel = preg_replace("/&(.)(acute|cedil|circ|ring|tilde|uml);/", "$1", $variavel);
        $variavel = preg_replace("/([^a-z0-9]+)/", "-", html_entity_decode($variavel));
        $variavel = trim($variavel, "-");
        return $variavel;
    }

    //Verifica se jÃ¡ existe URL para poder Montar uma nova
    function montaURLNova($variavel, $idconteudo) {
        $query = "select * from categorias where link  = '$variavel'";
        $Banco = new Database();
        $num_rows = mysql_num_rows($Banco->Query($query));
//        $num_rows = @mysql_num_rows($query);
        $Banco->Query($query);
        while ($dados = $Banco->FetchArray()) {
//        while ($dados = mysql_fetch_array($query)) {
            $id = $dados["0"];
        }

        if ($idconteudo <> $id) {
            if ($num_rows == 1) {
                while ($num_rows <> $num_post) {
                    $novo = $variavel . "-" . $num_rows;

                    $sql2 = "select * from categorias where link = '$novo'";


                    $query2 = mysql_query($sql2);
                    $num_rows2 = @mysql_num_rows($query2);

                    if ($num_rows2 == 0) {
                        $variavel = $novo;
                        $num_post = $num_rows;
                    } else {
                        $num_rows++;
                    }
                }
            }
        }

        return $variavel;
    }

}

// CLASSE FIM
?>