<?php

// ***************************************************************************************************
//
//	DATA CRIA��O:		03.08.2016
//	CLASSE:				DAO_Participantes
//	TABELA MYSQL:		participantes
//	BANCO MYSQL:		bartsge
//									
// ***************************************************************************************************
// ***************************************************************************************************
// DECLARA��O DA CLASSE
// ***************************************************************************************************

class DAO_Participantes { // CLASSE IN�CIO
// ***************************************************************************************************
// DECLARA��O DE ATRIBUTOS
// ***************************************************************************************************

    var $id;   // CHAVE PRIM�RIA - AUTO INCREMENTO
    var $nome;   // (ATRIBUTO NORMAL)
    var $email;   // (ATRIBUTO NORMAL)
    var $login;   // (ATRIBUTO NORMAL)
    var $senha;   // (ATRIBUTO NORMAL)
    var $cpf;   // (ATRIBUTO NORMAL)
    var $telefone;   // (ATRIBUTO NORMAL)
    var $img_mini;   // (ATRIBUTO NORMAL)
    var $img_full;   // (ATRIBUTO NORMAL)
    var $cep;   // (ATRIBUTO NORMAL)
    var $endereco;   // (ATRIBUTO NORMAL)
    var $numero;   // (ATRIBUTO NORMAL)
    var $complemento;   // (ATRIBUTO NORMAL)
    var $bairro;   // (ATRIBUTO NORMAL)
    var $estado;   // (ATRIBUTO NORMAL)
    var $cidade;   // (ATRIBUTO NORMAL)
    var $sexo;   // (ATRIBUTO NORMAL)
    var $datetime;   // (ATRIBUTO NORMAL)

    var $database; // INSTANCIA DA CLASSE DATABASE

// ***************************************************************************************************
// M�TODO CONSTRUTOR
// ***************************************************************************************************

    function DAO_Participantes() {

        $this->database = new Database();
    }

// ***************************************************************************************************
// M�TODOS - GETTER
// ***************************************************************************************************

    function getid() {

        return $this->id;
    }

    function getnome() {

        return $this->nome;
    }

    function getemail() {

        return $this->email;
    }

    function getlogin() {

        return $this->login;
    }

    function getsenha() {

        return $this->senha;
    }

    function getcpf() {

        return $this->cpf;
    }

    function gettelefone() {

        return $this->telefone;
    }

    function getimg_mini() {

        return $this->img_mini;
    }

    function getimg_full() {

        return $this->img_full;
    }

    function getcep() {

        return $this->cep;
    }

    function getendereco() {

        return $this->endereco;
    }

    function getnumero() {

        return $this->numero;
    }

    function getcomplemento() {

        return $this->complemento;
    }

    function getbairro() {

        return $this->bairro;
    }

    function getestado() {

        return $this->estado;
    }

    function getcidade() {

        return $this->cidade;
    }

    function getsexo() {

        return $this->sexo;
    }

    function getdatetime() {

        return $this->datetime;
    }


// ***************************************************************************************************
// M�TODOS - SETTER
// ***************************************************************************************************


    function setid($val) {

        $this->id = $val;
    }

    function setnome($val) {

        $this->nome = $val;
    }

    function setemail($val) {

        $this->email = $val;
    }

    function setlogin($val) {

        $this->login = $val;
    }

    function setsenha($val) {

        $this->senha = $val;
    }

    function setcpf($val) {

        $this->cpf = $val;
    }

    function settelefone($val) {

        $this->telefone = $val;
    }

    function setimg_mini($val) {

        $this->img_mini = $val;
    }

    function setimg_full($val) {

        $this->img_full = $val;
    }

    function setcep($val) {

        $this->cep = $val;
    }

    function setendereco($val) {

        $this->endereco = $val;
    }

    function setnumero($val) {

        $this->numero = $val;
    }

    function setcomplemento($val) {

        $this->complemento = $val;
    }

    function setbairro($val) {

        $this->bairro = $val;
    }

    function setestado($val) {

        $this->estado = $val;
    }

    function setcidade($val) {

        $this->cidade = $val;
    }

    function setsexo($val) {

        $this->sexo = $val;
    }

    function setdatetime($val) {

        $this->datetime = $val;
    }


// ***************************************************************************************************
// M�TODO  - SELECT - RETORNA UMA LINHA DA TABELA
// ***************************************************************************************************

    function select($id) {

        $sql = "SELECT * FROM participantes WHERE id = $id;";
        $result = $this->database->Query($sql);
        $row = $this->database->FetchObject();


        $this->id = $row->id;

        $this->nome = $row->nome;

        $this->email = $row->email;

        $this->login = $row->login;

        $this->senha = $row->senha;

        $this->cpf = $row->cpf;

        $this->telefone = $row->telefone;

        $this->img_mini = $row->img_mini;

        $this->img_full = $row->img_full;

        $this->cep = $row->cep;

        $this->endereco = $row->endereco;

        $this->numero = $row->numero;

        $this->complemento = $row->complemento;

        $this->bairro = $row->bairro;

        $this->estado = $row->estado;

        $this->cidade = $row->cidade;

        $this->sexo = $row->sexo;

        $this->datetime = $row->datetime;

    }
// ***************************************************************************************************
// M�TODO  - SELECT - RETORNA UMA LINHA DA TABELA
// ***************************************************************************************************

    function selectLink($estado) {

        $sql = "SELECT * FROM participantes WHERE complemento = '$estado'";
        $result = $this->database->Query($sql);
        $row = $this->database->FetchObject();


        $this->id = $row->id;

        $this->nome = $row->nome;

        $this->email = $row->email;

        $this->login = $row->login;

        $this->senha = $row->senha;

        $this->cpf = $row->cpf;

        $this->telefone = $row->telefone;

        $this->img_mini = $row->img_mini;

        $this->img_full = $row->img_full;

        $this->cep = $row->cep;

        $this->endereco = $row->endereco;

        $this->numero = $row->numero;

        $this->complemento = $row->complemento;

        $this->bairro = $row->bairro;

        $this->estado = $row->estado;

        $this->cidade = $row->cidade;

        $this->sexo = $row->sexo;

        $this->datetime = $row->datetime;

    }

    // ***************************************************************************************************
// M�TODO  - SELECT - RETORNA UMA LINHA DA TABELA
// *****

    function selectTipo($tipo) {

        $sql = "SELECT * FROM participantes WHERE telefone = $tipo;";
        $result = $this->database->Query($sql);
        $row = $this->database->FetchObject();


        $this->id = $row->id;

        $this->nome = $row->nome;

        $this->email = $row->email;

        $this->login = $row->login;

        $this->senha = $row->senha;

        $this->cpf = $row->cpf;

        $this->telefone = $row->telefone;

        $this->img_mini = $row->img_mini;

        $this->img_full = $row->img_full;

        $this->cep = $row->cep;

        $this->endereco = $row->endereco;

        $this->numero = $row->numero;

        $this->complemento = $row->complemento;

        $this->bairro = $row->bairro;

        $this->estado = $row->estado;

        $this->cidade = $row->cidade;

        $this->sexo = $row->sexo;

        $this->datetime = $row->datetime;

    }

// ***************************************************************************************************
// METODO QUE RETORNA A LINHA DO �LTIMO REGISTRO CRIADO NA TABELA
// ***************************************************************************************************

    function selectLastInsert() {
        $this->database->Query("select * from participantes where id = last_insert_id()");
        $row = $this->database->FetchObject();
        $this->select($row->id);
    }
    
    public function rows($sql) {
		 $result = $this->database->Query($sql);
		 $quantidade = $this->database->rows; 
		 //echo $quantidade; break;
        //$result = $this->database->Query($sql);
        return $quantidade; 
           
    }

// ***************************************************************************************************
// M�TODO  - SELECTBY - RETORNA LINHAS QUE ATENDEM A CONDI��O
// ***************************************************************************************************

    function selectby($field, $val, $tipo) {

        if ($tipo != "int" || $tipo != "float" || $tipo != "INT" || $tipo != "FLOAT" || $tipo != "Int" || $tipo != "Float") {
            $tipo = "'";
        }

        $field = $this->FDados($field);
        $val = $this->FDados($val);

        $sql = "SELECT * FROM participantes WHERE $field=$tipo$val$tipo";
        $this->database->Query($sql);
        return $this->database->FetchObject();
    }

// ***************************************************************************************************
// M�TODO  - SELECT APROXIMADO - RETORNA LINHAS QUE ATENDEM A CONDI��O
// ***************************************************************************************************

    function selectlike($field, $val) {

        $field = $this->FDados($field);
        $val = $this->FDados($val);

        $sql = "SELECT * FROM participantes WHERE $field LIKE '%$val%'";
        $this->database->Query($sql);
        return $this->database->FetchObject();
    }

// ***************************************************************************************************
// M�TODO - DELETE
// ***************************************************************************************************

    function delete($id) {

        $sql = "DELETE FROM participantes WHERE id = $id;";
        $result = $this->database->Query($sql);
    }

// ***************************************************************************************************
// M�TODO - INSERT
// ***************************************************************************************************

    function insert() {

        $this->id = ""; // Limpa a chave do auto incremento

        $sql = "INSERT INTO participantes ( nome,email,login,senha,cpf,telefone,img_mini,img_full,cep,endereco,numero, complemento, bairro, estado, cidade, sexo, datetime) VALUES ( '$this->nome','$this->email','$this->login','$this->senha','$this->cpf','$this->telefone','$this->img_mini','$this->img_full','$this->cep','$this->endereco','$this->numero', '$this->complemento', '$this->bairro', '$this->estado', '$this->cidade', '$this->sexo', '$this->datetime' )";
        $result = $this->database->Query($sql);
    }

// ***************************************************************************************************
// M�TODO - UPDATE
// ***************************************************************************************************

    function update($id, $tipo) {

        if (isset($tipo["nome"])) {
            $upd.= "nome = '$this->nome', ";
        }
        if (isset($tipo["email"])) {
            $upd.= "email = '$this->email', ";
        }
        if (isset($tipo["login"])) {
            $upd.= "login = '$this->login', ";
        }
        if (isset($tipo["senha"])) {
            $upd.= "senha = '$this->senha', ";
        }
        if (isset($tipo["cpf"])) {
            $upd.= "cpf = '$this->cpf', ";
        }
        if (isset($tipo["telefone"])) {
            $upd.= "telefone = '$this->telefone', ";
        }
        if (isset($tipo["img_mini"])) {
            $upd.= "img_mini = '$this->img_mini', ";
        }
        if (isset($tipo["img_full"])) {
            $upd.= "img_full = '$this->img_full', ";
        }
        if (isset($tipo["cep"])) {
            $upd.= "cep = '$this->cep', ";
        }
        if (isset($tipo["endereco"])) {
            $upd.= "endereco = '$this->endereco', ";
        }
        if (isset($tipo["numero"])) {
            $upd.= "numero = '$this->numero', ";
        }
        if (isset($tipo["complemento"])) {
            $upd.= "complemento = '$this->complemento', ";
        }
        if (isset($tipo["bairro"])) {
            $upd.= "bairro = '$this->bairro', ";
        }
        if (isset($tipo["estado"])) {
            $upd.= "estado = '$this->estado', ";
        }
        if (isset($tipo["cidade"])) {
            $upd.= "cidade = '$this->cidade', ";
        }
        if (isset($tipo["sexo"])) {
            $upd.= "sexo = '$this->sexo', ";
        }
        if (isset($tipo["datetime"])) {
            $upd.= "datetime = '$this->datetime', ";
        }

        $upd = substr($upd, 0, -2);


        $sql = " UPDATE participantes SET $upd WHERE id = $id ";

        $result = $this->database->Query($sql);
    }

// ***************************************************************************************************
// M�TODO PARA CRIAR VARIAVEIS DE SESSAO
// ***************************************************************************************************

    function createsession() {
        $_SESSION["id"] = $this->getid();
        $_SESSION["nome"] = $this->getnome();
        $_SESSION["email"] = $this->getemail();
        $_SESSION["login"] = $this->getlogin();
        $_SESSION["senha"] = $this->getsenha();
        $_SESSION["cpf"] = $this->getcpf();
        $_SESSION["telefone"] = $this->gettelefone();
        $_SESSION["img_mini"] = $this->getimg_mini();
        $_SESSION["img_full"] = $this->getimg_full();
        $_SESSION["cep"] = $this->getcep();
        $_SESSION["endereco"] = $this->getendereco();
        $_SESSION["numero"] = $this->getnumero();
        $_SESSION["complemento"] = $this->getcomplemento();
        $_SESSION["bairro"] = $this->getbairro();
        $_SESSION["estado"] = $this->getestado();
        $_SESSION["cidade"] = $this->getcidade();
        $_SESSION["sexo"] = $this->getsexo();
        $_SESSION["datetime"] = $this->getdatetime();

    }

// ***************************************************************************************************
// M�TODO PARA SETAR CAMPOS POR SESSAO
// ***************************************************************************************************

    function setsession() {
        $this->setid($this->FDados($_SESSION["id"]));
        $this->setnome($this->FDados($_SESSION["nome"]));
        $this->setemail($this->FDados($_SESSION["email"]));
        $this->setlogin($this->FDados($_SESSION["login"]));
        $this->setsenha($this->FDados($_SESSION["senha"]));
        $this->setcpf($this->FDados($_SESSION["cpf"]));
        $this->settelefone($this->FDados($_SESSION["telefone"]));
        $this->setimg_mini($this->FDados($_SESSION["img_mini"]));
        $this->setimg_full($this->FDados($_SESSION["img_full"]));
        $this->setcep($this->FDados($_SESSION["cep"]));
        $this->setendereco($this->FDados($_SESSION["endereco"]));
        $this->setnumero($this->FDados($_SESSION["numero"]));
        $this->setcomplemento($this->FDados($_SESSION["complemento"]));
        $this->setbairro($this->FDados($_SESSION["bairro"]));
        $this->setestado($this->FDados($_SESSION["estado"]));
        $this->setcidade($this->FDados($_SESSION["cidade"]));
        $this->setsexo($this->FDados($_SESSION["sexo"]));
        $this->setdatetime($this->FDados($_SESSION["datetime"]));

    }

// ***************************************************************************************************
// M�TODO PARA RETIRAR OS VALORES DAS SESSOES
// ***************************************************************************************************

    function unsetsession() {
        unset($_SESSION["id"]);
        unset($_SESSION["nome"]);
        unset($_SESSION["email"]);
        unset($_SESSION["login"]);
        unset($_SESSION["senha"]);
        unset($_SESSION["cpf"]);
        unset($_SESSION["telefone"]);
        unset($_SESSION["img_mini"]);
        unset($_SESSION["img_full"]);
        unset($_SESSION["cep"]);
        unset($_SESSION["endereco"]);
        unset($_SESSION["numero"]);
        unset($_SESSION["complemento"]);
        unset($_SESSION["bairro"]);
        unset($_SESSION["estado"]);
        unset($_SESSION["cidade"]);
        unset($_SESSION["sexo"]);
        unset($_SESSION["datetime"]);

    }

// ***************************************************************************************************
// M�TODO PARA SETAR CAMPOS POR POST
// ***************************************************************************************************

    function setpost() {
        $this->setid($this->FDados($_POST["id"]));
        $this->setnome($this->FDados($_POST["nome"]));
        $this->setemail($this->FDados($_POST["email"]));
        $this->setlogin($this->FDados($_POST["email"]));
        $this->setsenha($this->FDados($_POST["senha"]));
        $this->setcpf($this->FDados($_POST["cpf"]));
        $this->settelefone($this->FDados($_POST["telefone"]));
        $this->setimg_mini($this->FDados($_POST["img_mini"]));
        $this->setimg_full($this->FDados($_POST["img_full"]));
        $this->setcep($this->FDados($_POST["cep"]));
        $this->setendereco($this->FDados($_POST["endereco"]));
        $this->setnumero($this->FDados($_POST["numero"]));
        $this->setcomplemento($this->FDados($_POST["complemento"]));
        $this->setbairro($this->FDados($_POST["bairro"]));
        $this->setestado($this->FDados($_POST["estado"]));
        $this->setcidade($this->FDados($_POST["cidade"]));
        $this->setsexo($this->FDados($_POST["sexo"]));
        $this->setdatetime($this->FDados($this->FDataBanco($_POST["datetime"])));

    }

// ***************************************************************************************************
// M�TODO PARA SETAR CAMPOS POR GET
// ***************************************************************************************************

    function setget() {

        $this->setid($this->FDados($_GET["id"]));
        $this->setnome($this->FDados($_GET["nome"]));
        $this->setemail($this->FDados($_GET["email"]));
        $this->setlogin($this->FDados($_GET["login"]));
        $this->setsenha($this->FDados($_GET["senha"]));
        $this->setcpf($this->FDados($_GET["cpf"]));
        $this->settelefone($this->FDados($_GET["telefone"]));
        $this->setimg_mini($this->FDados($_GET["img_mini"]));
        $this->setimg_full($this->FDados($_GET["img_full"]));
        $this->setcep($this->FDados($_GET["cep"]));
        $this->setendereco($this->FDados($_GET["endereco"]));
        $this->setnumero($this->FDados($_GET["numero"]));
        $this->setcomplemento($this->FDados($_GET["complemento"]));
        $this->setbairro($this->FDados($_GET["bairro"]));
        $this->setestado($this->FDados($_GET["estado"]));
        $this->setcidade($this->FDados($_GET["cidade"]));
        $this->setsexo($this->FDados($_GET["sexo"]));
        $this->setdatetime($this->FDados($_GET["datetime"]));

    }

// ***************************************************************************************************
// METODO QUE RETORNA ARRAY DE TODOS OS ID's EXISTENTES NA TABELA
// ***************************************************************************************************

    function todosIDs() {

        $sql = "SELECT id FROM participantes";

        $this->database->Query($sql);

        while ($row = $this->database->FetchObject()) {
            $i++;
            $retorno[$i] = $row->id;
        }

        return $retorno;
    }

// ***************************************************************************************************
// METODO QUE ATUALIZA UM CAMPO
// ***************************************************************************************************

    function updatecampo($id, $campo, $valor, $tipo) {

        if ($tipo != "int" || $tipo != "float" || $tipo != "INT" || $tipo != "FLOAT" || $tipo != "Int" || $tipo != "Float") {
            $tipo = "'";
        }


        $sql = "UPDATE participantes SET $campo = $tipo$valor$tipo WHERE id = $id";

        $this->database->Query($sql);
    }

// ***************************************************************************************************
// FUNCAO PARA FORMATAR OS VALORES DE DATA E VALORES DECIMAIS PARA GRAVAR NO BANCO
// ***************************************************************************************************

    function dadosBanco() {

        $this->setdatetime(date('Y-m-d H:i:s'));

    }

// ***************************************************************************************************
// FUNCAO PARA FORMATAR OS VALORES DE DATA E VALORES DECIMAIS PARA EXIBI��O
// ***************************************************************************************************

    function dadosExibe() {

    }

// ***************************************************************************************************
// METODOS AUXILIARES PARA FORMATACAO DE DADOS
// ***************************************************************************************************

    function FFloatBanco($numero) {

        if ($numero == "")
            return "0.00";

        $retorno = str_replace(".", "", $numero);
        $retorno = str_replace(",", ".", $retorno);

        return $retorno;
    }

    function FFloatExibe($numero) {

        if ($numero == "")
            return "";

        return number_format($numero, 2, ",", ".");
    }

    function FDataBanco($valor) {

        if ($valor != "" && $valor != null) {
            $partes = explode("/", $valor);

            $retorno = "" . $partes[2] . "-" . $partes[1] . "-" . $partes[0];

            return $retorno;
        }
        else
            return "0000-00-00";
    }

    function FDataExibe($valor) {

        if ($valor == "")
            return "";

        $partes = explode("-", $valor);

        $retorno = substr($partes[2], 0, 2) . "/" . $partes[1] . "/" . $partes[0];

        return $retorno;
    }

    function FDataTimeBanco() {

        $retorno = date("Y-m-d H:i:s");

        return $retorno;
    }

    function FDataTimeExibe($valor) {

        if ($valor == "")
            return "";

        $retorno = substr($valor, 8, 2) . "/" . substr($valor, 5, 2) . "/" . substr($valor, 0, 4) . " " . substr($valor, 11, 8);

        return $retorno;
    }

    function FDados($dado) {

        $strDado = $dado;
        $strDado = htmlspecialchars($strDado);
        $strDado = html_entity_decode($strDado);
        if (!get_magic_quotes_gpc()) {
            $strDado = addslashes($strDado);
        }

        return $strDado;
    }


}

// CLASSE FIM
?>