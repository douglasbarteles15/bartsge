<?php

// ***************************************************************************************************
//
//	DATA CRIA��O:		03.08.2016
//	CLASSE:				DAO_Noticias	
//	TABELA MYSQL:		noticias
//	BANCO MYSQL:		bartsge
//									
// ***************************************************************************************************
// ***************************************************************************************************
// DECLARA��O DA CLASSE
// ***************************************************************************************************

class DAO_Noticias { // CLASSE IN�CIO
// ***************************************************************************************************
// DECLARA��O DE ATRIBUTOS
// ***************************************************************************************************

    var $id;   // CHAVE PRIM�RIA - AUTO INCREMENTO
    var $titulo;   // (ATRIBUTO NORMAL)
    var $chamada;   // (ATRIBUTO NORMAL)
    var $conteudo;   // (ATRIBUTO NORMAL)
    var $img_mini;   // (ATRIBUTO NORMAL)
    var $img_full;   // (ATRIBUTO NORMAL)
    var $status_id_INT;   // (ATRIBUTO NORMAL)
    var $destaque_id_INT;   // (ATRIBUTO NORMAL)
    var $link;   // (ATRIBUTO NORMAL)
    var $data;   // (ATRIBUTO NORMAL)

    var $database; // INSTANCIA DA CLASSE DATABASE

// ***************************************************************************************************
// M�TODO CONSTRUTOR
// ***************************************************************************************************

    function DAO_Noticias() {

        $this->database = new Database();
    }

// ***************************************************************************************************
// M�TODOS - GETTER
// ***************************************************************************************************

    function getid() {

        return $this->id;
    }

    function gettitulo() {

        return $this->titulo;
    }

    function getchamada() {

        return $this->chamada;
    }

    function getconteudo() {

        return $this->conteudo;
    }

    function getimg_mini() {

        return $this->img_mini;
    }

    function getimg_full() {

        return $this->img_full;
    }

    function getstatus_id_INT() {

        return $this->status_id_INT;
    }

    function getdestaque_id_INT() {

        return $this->destaque_id_INT;
    }

    function getlink() {

        return $this->link;
    }

    function getdata() {

        return $this->data;
    }

// ***************************************************************************************************
// M�TODOS - SETTER
// ***************************************************************************************************


    function setid($val) {

        $this->id = $val;
    }

    function settitulo($val) {

        $this->titulo = $val;
    }

    function setchamada($val) {

        $this->chamada = $val;
    }

    function setconteudo($val) {

        $this->conteudo = $val;
    }

    function setimg_mini($val) {

        $this->img_mini = $val;
    }

    function setimg_full($val) {

        $this->img_full = $val;
    }

    function setstatus_id_INT($val) {

        $this->status_id_INT = $val;
    }

    function setdestaque_id_INT($val) {

        $this->destaque_id_INT = $val;
    }

    function setlink($val) {

        $this->link = $val;
    }

    function setdata($val) {

        $this->data = $val;
    }



// ***************************************************************************************************
// M�TODO  - SELECT - RETORNA UMA LINHA DA TABELA
// ***************************************************************************************************

    function select($id) {

        $sql = "SELECT * FROM noticias WHERE id = $id;";
        $result = $this->database->Query($sql);
        $row = $this->database->FetchObject();


        $this->id = $row->id;

        $this->titulo = $row->titulo;

        $this->chamada = $row->chamada;

        $this->conteudo = $row->conteudo;

        $this->img_mini = $row->img_mini;

        $this->img_full = $row->img_full;

        $this->status_id_INT = $row->status_id_INT;

        $this->destaque_id_INT = $row->destaque_id_INT;

        $this->link = $row->link;

        $this->data = $row->data;


    }
// ***************************************************************************************************
// M�TODO  - SELECT - RETORNA UMA LINHA DA TABELA
// ***************************************************************************************************

    function selectLink($link) {

        $sql = "SELECT * FROM noticias WHERE link = '$link'";
        $result = $this->database->Query($sql);
        $row = $this->database->FetchObject();


        $this->id = $row->id;

        $this->titulo = $row->titulo;

        $this->chamada = $row->chamada;

        $this->conteudo = $row->conteudo;

        $this->img_mini = $row->img_mini;

        $this->img_full = $row->img_full;

        $this->status_id_INT = $row->status_id_INT;

        $this->destaque_id_INT = $row->destaque_id_INT;

        $this->link = $row->link;

        $this->data = $row->data;


    }

    // ***************************************************************************************************
// M�TODO  - SELECT - RETORNA UMA LINHA DA TABELA
// *****

    function selectTipo($tipo) {

        $sql = "SELECT * FROM noticias WHERE status_id_INT = $tipo;";
        $result = $this->database->Query($sql);
        $row = $this->database->FetchObject();


        $this->id = $row->id;

        $this->titulo = $row->titulo;

        $this->chamada = $row->chamada;

        $this->conteudo = $row->conteudo;

        $this->img_mini = $row->img_mini;

        $this->img_full = $row->img_full;

        $this->status_id_INT = $row->status_id_INT;

        $this->destaque_id_INT = $row->destaque_id_INT;

        $this->link = $row->link;

        $this->data = $row->data;


    }

// ***************************************************************************************************
// METODO QUE RETORNA A LINHA DO �LTIMO REGISTRO CRIADO NA TABELA
// ***************************************************************************************************

    function selectLastInsert() {
        $this->database->Query("select * from noticias where id = last_insert_id()");
        $row = $this->database->FetchObject();
        $this->select($row->id);
    }
    
    public function rows($sql) {
		 $result = $this->database->Query($sql);
		 $quantidade = $this->database->rows; 
		 //echo $quantidade; break;
        //$result = $this->database->Query($sql);
        return $quantidade; 
           
    }

// ***************************************************************************************************
// M�TODO  - SELECTBY - RETORNA LINHAS QUE ATENDEM A CONDI��O
// ***************************************************************************************************

    function selectby($field, $val, $tipo) {

        if ($tipo != "int" || $tipo != "float" || $tipo != "INT" || $tipo != "FLOAT" || $tipo != "Int" || $tipo != "Float") {
            $tipo = "'";
        }

        $field = $this->FDados($field);
        $val = $this->FDados($val);

        $sql = "SELECT * FROM noticias WHERE $field=$tipo$val$tipo";
        $this->database->Query($sql);
        return $this->database->FetchObject();
    }

// ***************************************************************************************************
// M�TODO  - SELECT APROXIMADO - RETORNA LINHAS QUE ATENDEM A CONDI��O
// ***************************************************************************************************

    function selectlike($field, $val) {

        $field = $this->FDados($field);
        $val = $this->FDados($val);

        $sql = "SELECT * FROM noticias WHERE $field LIKE '%$val%'";
        $this->database->Query($sql);
        return $this->database->FetchObject();
    }

// ***************************************************************************************************
// M�TODO - DELETE
// ***************************************************************************************************

    function delete($id) {

        $sql = "DELETE FROM noticias WHERE id = $id;";
        $result = $this->database->Query($sql);
    }

// ***************************************************************************************************
// M�TODO - INSERT
// ***************************************************************************************************

    function insert() {

        $this->id = ""; // Limpa a chave do auto incremento

        $sql = "INSERT INTO noticias ( titulo,chamada,conteudo,img_mini,img_full,status_id_INT,destaque_id_INT,link,data) VALUES ( '$this->titulo','$this->chamada','$this->conteudo','$this->img_mini','$this->img_full','$this->status_id_INT','$this->destaque_id_INT','$this->link','$this->data' )";
        $result = $this->database->Query($sql);
    }

// ***************************************************************************************************
// M�TODO - UPDATE
// ***************************************************************************************************

    function update($id, $tipo) {

        if (isset($tipo["titulo"])) {
            $upd.= "titulo = '$this->titulo', ";
        }
        if (isset($tipo["chamada"])) {
            $upd.= "chamada = '$this->chamada', ";
        }
        if (isset($tipo["conteudo"])) {
            $upd.= "conteudo = '$this->conteudo', ";
        }
        if (isset($tipo["img_mini"])) {
            $upd.= "img_mini = '$this->img_mini', ";
        }
        if (isset($tipo["img_full"])) {
            $upd.= "img_full = '$this->img_full', ";
        }
        if (isset($tipo["status_id_INT"])) {
            $upd.= "status_id_INT = $this->status_id_INT, ";
        }
        if (isset($tipo["destaque_id_INT"])) {
            $upd.= "destaque_id_INT = '$this->destaque_id_INT', ";
        }
        if (isset($tipo["link"])) {
            $upd.= "link = $this->link, ";
        }
        if (isset($tipo["data"])) {
            $upd.= "data = '$this->data', ";
        }

        $upd = substr($upd, 0, -2);


        $sql = " UPDATE noticias SET $upd WHERE id = $id ";

        $result = $this->database->Query($sql);
    }

// ***************************************************************************************************
// M�TODO PARA CRIAR VARIAVEIS DE SESSAO
// ***************************************************************************************************

    function createsession() {
        $_SESSION["id"] = $this->getid();
        $_SESSION["titulo"] = $this->gettitulo();
        $_SESSION["chamada"] = $this->getchamada();
        $_SESSION["conteudo"] = $this->getconteudo();
        $_SESSION["img_mini"] = $this->getimg_mini();
        $_SESSION["img_full"] = $this->getimg_full();
        $_SESSION["status_id_INT"] = $this->getstatus_id_INT();
        $_SESSION["destaque_id_INT"] = $this->getdestaque_id_INT();
        $_SESSION["link"] = $this->getlink();
        $_SESSION["data"] = $this->getdata();

    }

// ***************************************************************************************************
// M�TODO PARA SETAR CAMPOS POR SESSAO
// ***************************************************************************************************

    function setsession() {
        $this->setid($this->FDados($_SESSION["id"]));
        $this->settitulo($this->FDados($_SESSION["titulo"]));
        $this->setchamada($this->FDados($_SESSION["chamada"]));
        $this->setconteudo($this->FDados($_SESSION["conteudo"]));
        $this->setimg_mini($this->FDados($_SESSION["img_mini"]));
        $this->setimg_full($this->FDados($_SESSION["img_full"]));
        $this->setstatus_id_INT($this->FDados($_SESSION["status_id_INT"]));
        $this->setdestaque_id_INT($this->FDados($_SESSION["destaque_id_INT"]));
        $this->setlink($this->FDados($_SESSION["link"]));
        $this->setdata($this->FDados($_SESSION["data"]));

    }

// ***************************************************************************************************
// M�TODO PARA RETIRAR OS VALORES DAS SESSOES
// ***************************************************************************************************

    function unsetsession() {
        unset($_SESSION["id"]);
        unset($_SESSION["titulo"]);
        unset($_SESSION["chamada"]);
        unset($_SESSION["conteudo"]);
        unset($_SESSION["img_mini"]);
        unset($_SESSION["img_full"]);
        unset($_SESSION["status_id_INT"]);
        unset($_SESSION["destaque_id_INT"]);
        unset($_SESSION["link"]);
        unset($_SESSION["data"]);

    }

// ***************************************************************************************************
// M�TODO PARA SETAR CAMPOS POR POST
// ***************************************************************************************************

    function setpost() {
        $this->setid($this->FDados($_POST["id"]));
        $this->settitulo($this->FDados($_POST["titulo"]));
        $this->setchamada($this->FDados($_POST["chamada"]));
        $this->setconteudo($this->FDados($_POST["conteudo"]));
        $this->setimg_mini($this->FDados($_POST["img_mini"]));
        $this->setimg_full($this->FDados($_POST["img_full"]));
        $this->setstatus_id_INT($this->FDados($_POST["status_id_INT"]));
        $this->setdestaque_id_INT($this->FDados($_POST["destaque_id_INT"]));
        $this->setlink($this->FDados($this->montaURLNova($this->montaURL($_POST["titulo"]), $_POST["id"])));
        $this->setdata($this->FDados($_POST["data"]));

    }

// ***************************************************************************************************
// M�TODO PARA SETAR CAMPOS POR GET
// ***************************************************************************************************

    function setget() {

        $this->setid($this->FDados($_GET["id"]));
        $this->settitulo($this->FDados($_GET["titulo"]));
        $this->setchamada($this->FDados($_GET["chamada"]));
        $this->setconteudo($this->FDados($_GET["conteudo"]));
        $this->setimg_mini($this->FDados($_GET["img_mini"]));
        $this->setimg_full($this->FDados($_GET["img_full"]));
        $this->setstatus_id_INT($this->FDados($_GET["status_id_INT"]));
        $this->setdestaque_id_INT($this->FDados($_GET["destaque_id_INT"]));
        $this->setlink($this->FDados($_GET["link"]));
        $this->setdata($this->FDados($_GET["data"]));

    }

// ***************************************************************************************************
// METODO QUE RETORNA ARRAY DE TODOS OS ID's EXISTENTES NA TABELA
// ***************************************************************************************************

    function todosIDs() {

        $sql = "SELECT id FROM noticias";

        $this->database->Query($sql);

        while ($row = $this->database->FetchObject()) {
            $i++;
            $retorno[$i] = $row->id;
        }

        return $retorno;
    }

// ***************************************************************************************************
// METODO QUE ATUALIZA UM CAMPO
// ***************************************************************************************************

    function updatecampo($id, $campo, $valor, $tipo) {

        if ($tipo != "int" || $tipo != "float" || $tipo != "INT" || $tipo != "FLOAT" || $tipo != "Int" || $tipo != "Float") {
            $tipo = "'";
        }


        $sql = "UPDATE noticias SET $campo = $tipo$valor$tipo WHERE id = $id";

        $this->database->Query($sql);
    }

// ***************************************************************************************************
// FUNCAO PARA FORMATAR OS VALORES DE DATA E VALORES DECIMAIS PARA GRAVAR NO BANCO
// ***************************************************************************************************

    function dadosBanco() {

        if ($this->getstatus_id_INT() == "") {

            $this->setstatus_id_INT(0);
        }

        $this->setdata($this->FDataBanco($this->getdata()));
    }

// ***************************************************************************************************
// FUNCAO PARA FORMATAR OS VALORES DE DATA E VALORES DECIMAIS PARA EXIBI��O
// ***************************************************************************************************

    function dadosExibe() {

    }

// ***************************************************************************************************
// METODOS AUXILIARES PARA FORMATACAO DE DADOS
// ***************************************************************************************************

    function FFloatBanco($numero) {

        if ($numero == "")
            return "0.00";

        $retorno = str_replace(".", "", $numero);
        $retorno = str_replace(",", ".", $retorno);

        return $retorno;
    }

    function FFloatExibe($numero) {

        if ($numero == "")
            return "";

        return number_format($numero, 2, ",", ".");
    }

    function FDataBanco($valor) {

        if ($valor != "" && $valor != null) {
            $partes = explode("/", $valor);

            $retorno = "" . $partes[2] . "-" . $partes[1] . "-" . $partes[0];

            return $retorno;
        }
        else
            return "0000-00-00";
    }

    function FDataExibe($valor) {

        if ($valor == "")
            return "";

        $partes = explode("-", $valor);

        $retorno = substr($partes[2], 0, 2) . "/" . $partes[1] . "/" . $partes[0];

        return $retorno;
    }

    function FDataTimeBanco() {

        $retorno = date("Y-m-d H:i:s");

        return $retorno;
    }

    function FDataTimeExibe($valor) {

        if ($valor == "")
            return "";

        $retorno = substr($valor, 8, 2) . "/" . substr($valor, 5, 2) . "/" . substr($valor, 0, 4) . " " . substr($valor, 11, 8);

        return $retorno;
    }

    function FDados($dado) {

        $strDado = $dado;
        $strDado = htmlspecialchars($strDado);
        $strDado = html_entity_decode($strDado);
        if (!get_magic_quotes_gpc()) {
            $strDado = addslashes($strDado);
        }

        return $strDado;
    }

     // Monta a URL Amigável
    function montaURL($variavel) {
        $variavel = htmlentities(strtolower($variavel));
        $variavel = preg_replace("/&(.)(acute|cedil|circ|ring|tilde|uml);/", "$1", $variavel);
        $variavel = preg_replace("/([^a-z0-9]+)/", "-", html_entity_decode($variavel));
        $variavel = trim($variavel, "-");
        return $variavel;
    }

    //Verifica se já existe URL para poder Montar uma nova
    function montaURLNova($variavel, $idconteudo) {
        $query = "select * from noticias where link  = '$variavel'";
        $Banco = new Database();
        $num_rows = mysql_num_rows($Banco->Query($query));
//        $num_rows = @mysql_num_rows($query);
        $Banco->Query($query);
        while ($dados = $Banco->FetchArray()) {
//        while ($dados = mysql_fetch_array($query)) {
            $id = $dados["0"];
        }

        if ($idconteudo <> $id) {
            if ($num_rows == 1) {
                while ($num_rows <> $num_post) {
                    $novo = $variavel . "-" . $num_rows;

                    $sql2 = "select * from noticias where link = '$novo'";


                    $query2 = mysql_query($sql2);
                    $num_rows2 = @mysql_num_rows($query2);

                    if ($num_rows2 == 0) {
                        $variavel = $novo;
                        $num_post = $num_rows;
                    } else {
                        $num_rows++;
                    }
                }
            }
        }

        return $variavel;
    }

}

// CLASSE FIM
?>