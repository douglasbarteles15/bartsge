<?php

// ***************************************************************************************************
//
//	DATA CRIA��O:		12.12.2011
//	CLASSE:				DAO_Contato
//	TABELA MYSQL:		contato
//	BANCO MYSQL:		qualificar
//
// ***************************************************************************************************
// ***************************************************************************************************
// DECLARA��O DA CLASSE
// ***************************************************************************************************

class DAO_Contato { // CLASSE IN�CIO
// ***************************************************************************************************
// DECLARA��O DE ATRIBUTOS
// ***************************************************************************************************

    var $idcontato;   // CHAVE PRIM�RIA - AUTO INCREMENTO
    var $data;   // (ATRIBUTO NORMAL)
    var $nome;   // (ATRIBUTO NORMAL)
    var $email;   // (ATRIBUTO NORMAL)
    var $telefone;   // (ATRIBUTO NORMAL)
    var $cidade;   // (ATRIBUTO NORMAL)
    var $tipo;   // (ATRIBUTO NORMAL)
    var $mensagem;   // (ATRIBUTO NORMAL)
    var $assunto;   // (ATRIBUTO NORMAL)
    var $database; // INSTANCIA DA CLASSE DATABASE

// ***************************************************************************************************
// M�TODO CONSTRUTOR
// ***************************************************************************************************

    function DAO_Contato() {

        $this->database = new Database();
    }

// ***************************************************************************************************
// M�TODOS - GETTER
// ***************************************************************************************************

    function getidcontato() {

        return $this->idcontato;
    }

    function getdata() {

        return $this->data;
    }

    function getnome() {

        return $this->nome;
    }

    function getemail() {

        return $this->email;
    }

    function gettelefone() {

        return $this->telefone;
    }

    function getcidade() {

        return $this->cidade;
    }

    function gettipo() {

        return $this->tipo;
    }

    function getmensagem() {

        return $this->mensagem;
    }

    function getassunto() {

        return $this->assunto;
    }

// ***************************************************************************************************
// M�TODOS - SETTER
// ***************************************************************************************************


    function setidcontato($val) {

        $this->idcontato = $val;
    }

    function setdata($val) {

        $this->data = $val;
    }

    function setnome($val) {

        $this->nome = $val;
    }

    function setemail($val) {

        $this->email = $val;
    }

    function settelefone($val) {

        $this->telefone = $val;
    }

    function setcidade($val) {

        $this->cidade = $val;
    }

    function settipo($val) {

        $this->tipo = $val;
    }

    function setmensagem($val) {

        $this->mensagem = $val;
    }

    function setassunto($val) {

        $this->assunto = $val;
    }

// ***************************************************************************************************
// M�TODO  - SELECT - RETORNA UMA LINHA DA TABELA
// ***************************************************************************************************

    function select($id) {

        $sql = "SELECT * FROM contato WHERE idcontato = $id;";
        $result = $this->database->Query($sql);
        $row = $this->database->FetchObject();


        $this->idcontato = $row->idcontato;

        $this->data = $row->data;

        $this->nome = $row->nome;

        $this->email = $row->email;

        $this->telefone = $row->telefone;

        $this->cidade = $row->cidade;

        $this->tipo = $row->tipo;

        $this->mensagem = $row->mensagem;

        $this->assunto = $row->assunto;
    }

// ***************************************************************************************************
// METODO QUE RETORNA A LINHA DO �LTIMO REGISTRO CRIADO NA TABELA
// ***************************************************************************************************

    function selectLastInsert() {
        $this->database->Query("select * from contato where idcontato = last_insert_id()");
        $row = $this->database->FetchObject();
        $this->select($row->idcontato);
    }

// ***************************************************************************************************
// M�TODO  - SELECTBY - RETORNA LINHAS QUE ATENDEM A CONDI��O
// ***************************************************************************************************

    function selectby($field, $val, $tipo) {

        if ($tipo != "int" || $tipo != "float" || $tipo != "INT" || $tipo != "FLOAT" || $tipo != "Int" || $tipo != "Float") {
            $tipo = "'";
        }

        $field = $this->FDados($field);
        $val = $this->FDados($val);

        $sql = "SELECT * FROM contato WHERE $field=$tipo$val$tipo";
        $this->database->Query($sql);
        return $this->database->FetchObject();
    }

// ***************************************************************************************************
// M�TODO  - SELECT APROXIMADO - RETORNA LINHAS QUE ATENDEM A CONDI��O
// ***************************************************************************************************

    function selectlike($field, $val) {

        $field = $this->FDados($field);
        $val = $this->FDados($val);

        $sql = "SELECT * FROM contato WHERE $field LIKE '%$val%'";
        $this->database->Query($sql);
        return $this->database->FetchObject();
    }

// ***************************************************************************************************
// M�TODO - DELETE
// ***************************************************************************************************

    function delete($id) {

        $sql = "DELETE FROM contato WHERE idcontato = $id;";
        $result = $this->database->Query($sql);
    }

// ***************************************************************************************************
// M�TODO - INSERT
// ***************************************************************************************************

    function insert() {

        $this->opiniao_id = ""; // Limpa a chave do auto incremento

        $sql = "INSERT INTO contato ( idcontato,data,nome,email,telefone,cidade,tipo,mensagem, assunto ) VALUES ( $this->idcontato,'$this->data','$this->nome','$this->email','$this->telefone','$this->cidade','$this->tipo','$this->mensagem', '$this->assunto' )";
        $result = $this->database->Query($sql);
    }

// ***************************************************************************************************
// M�TODO - UPDATE
// ***************************************************************************************************

    function update($id, $tipo) {

        if (isset($tipo["idcontato"])) {
            $upd.= "idcontato = $this->idcontato, ";
        }
        if (isset($tipo["data"])) {
            $upd.= "data = '$this->data', ";
        }
        if (isset($tipo["nome"])) {
            $upd.= "nome = '$this->nome', ";
        }
        if (isset($tipo["email"])) {
            $upd.= "email = $this->email, ";
        }
        if (isset($tipo["telefone"])) {
            $upd.= "telefone = '$this->telefone', ";
        }
        if (isset($tipo["cidade"])) {
            $upd.= "cidade = '$this->cidade', ";
        }
        if (isset($tipo["tipo"])) {
            $upd.= "tipo = '$this->tipo', ";
        }
        if (isset($tipo["mensagem"])) {
            $upd.= "mensagem = '$this->mensagem', ";
        }
        if (isset($tipo["assunto"])) {
            $upd.= "assunto = '$this->assunto', ";
        }
        $upd = substr($upd, 0, -2);


        $sql = " UPDATE contato SET $upd WHERE idcontato = $id ";

        $result = $this->database->Query($sql);
    }

// ***************************************************************************************************
// M�TODO PARA CRIAR VARIAVEIS DE SESSAO
// ***************************************************************************************************

    function createsession() {
        $_SESSION["idcontato"] = $this->getidcontato();
        $_SESSION["data"] = $this->getdata();
        $_SESSION["nome"] = $this->getnome();
        $_SESSION["email"] = $this->getemail();
        $_SESSION["telefone"] = $this->gettelefone();
        $_SESSION["cidade"] = $this->getcidade();
        $_SESSION["tipo"] = $this->gettipo();
        $_SESSION["mensagem"] = $this->getmensagem();
        $_SESSION["assunto"] = $this->getassunto();
    }

// ***************************************************************************************************
// M�TODO PARA SETAR CAMPOS POR SESSAO
// ***************************************************************************************************

    function setsession() {
        $this->setidcontato($this->FDados($_SESSION["idcontato"]));
        $this->setdata($this->FDados($_SESSION["data"]));
        $this->setnome($this->FDados($_SESSION["nome"]));
        $this->setemail($this->FDados($_SESSION["email"]));
        $this->settelefone($this->FDados($_SESSION["telefone"]));
        $this->setcidade($this->FDados($_SESSION["cidade"]));
        $this->settipo($this->FDados($_SESSION["tipo"]));
        $this->setmensagem($this->FDados($_SESSION["mensagem"]));
        $this->setassunto($this->FDados($_SESSION["assunto"]));
    }

// ***************************************************************************************************
// M�TODO PARA RETIRAR OS VALORES DAS SESSOES
// ***************************************************************************************************

    function unsetsession() {
        unset($_SESSION["idcontato"]);
        unset($_SESSION["data"]);
        unset($_SESSION["nome"]);
        unset($_SESSION["email"]);
        unset($_SESSION["telefone"]);
        unset($_SESSION["cidade"]);
        unset($_SESSION["tipo"]);
        unset($_SESSION["mensagem"]);
        unset($_SESSION["assunto"]);
    }

// ***************************************************************************************************
// M�TODO PARA SETAR CAMPOS POR POST
// ***************************************************************************************************

    function setpost() {
        $this->setidcontato($this->FDados($_POST["idcontato"]));
        $this->setdata($this->FDados($_POST["data"]));
        $this->setnome(htmlentities($this->FDados($_POST["nome"]), ENT_COMPAT, "UTF-8"));
//        $this->setnome($this->FDados($_POST["nome"]));
        $this->setemail($this->FDados($_POST["email"]));
        $this->settelefone($this->FDados($_POST["telefone"]));
        $this->setcidade($this->FDados($_POST["cidade"]));
        $this->settipo($this->FDados($_POST["tipo"]));
        $this->setassunto($this->FDados($_POST["assunto"]));
        $this->setmensagem(htmlentities($this->FDados($_POST["mensagem"]), ENT_COMPAT, "UTF-8"));
//        $this->setmensagem($this->FDados($_POST["mensagem"]));
    }

// ***************************************************************************************************
// M�TODO PARA SETAR CAMPOS POR GET
// ***************************************************************************************************

    function setget() {

        $this->setidcontato($this->FDados($_GET["idcontato"]));
        $this->setdata($this->FDados($_GET["data"]));
        $this->setnome($this->FDados($_GET["nome"]));
        $this->setemail($this->FDados($_GET["email"]));
        $this->settelefone($this->FDados($_GET["telefone"]));
        $this->setcidade($this->FDados($_GET["cidade"]));
        $this->settipo($this->FDados($_GET["tipo"]));
        $this->setmensagem($this->FDados($_GET["mensagem"]));
        $this->setassunto($this->FDados($_GET["assunto"]));
    }

// ***************************************************************************************************
// METODO QUE RETORNA ARRAY DE TODOS OS ID's EXISTENTES NA TABELA
// ***************************************************************************************************

    function todosIDs() {

        $sql = "SELECT idcontato FROM contato";

        $this->database->Query($sql);

        while ($row = $this->database->FetchObject()) {
            $i++;
            $retorno[$i] = $row->idcontato;
        }

        return $retorno;
    }

// ***************************************************************************************************
// METODO QUE ATUALIZA UM CAMPO
// ***************************************************************************************************

    function updatecampo($id, $campo, $valor, $tipo) {

        if ($tipo != "int" || $tipo != "float" || $tipo != "INT" || $tipo != "FLOAT" || $tipo != "Int" || $tipo != "Float") {
            $tipo = "'";
        }


        $sql = "UPDATE contato SET $campo = $tipo$valor$tipo WHERE idcontato = $id";

        $this->database->Query($sql);
    }

// ***************************************************************************************************
// FUNCAO PARA FORMATAR OS VALORES DE DATA E VALORES DECIMAIS PARA GRAVAR NO BANCO
// ***************************************************************************************************

    function dadosBanco() {

        if ($this->getidcontato() == "") {

            $this->setidcontato(0);
        }


        $this->setdata($this->FDataTimeBanco($this->getdata()));
    }

// ***************************************************************************************************
// FUNCAO PARA FORMATAR OS VALORES DE DATA E VALORES DECIMAIS PARA EXIBI��O
// ***************************************************************************************************

    function dadosExibe() {

        $this->setdata($this->FDataTimeExibe($this->getdata()));
    }

// ***************************************************************************************************
// METODOS AUXILIARES PARA FORMATACAO DE DADOS
// ***************************************************************************************************

    function FFloatBanco($numero) {

        if ($numero == "")
            return "0.00";

        $retorno = str_replace(".", "", $numero);
        $retorno = str_replace(",", ".", $retorno);

        return $retorno;
    }

    function FFloatExibe($numero) {

        if ($numero == "")
            return "";

        return number_format($numero, 2, ",", ".");
    }

    function FDataBanco($valor) {

        if ($valor != "" && $valor != null) {
            $partes = explode("/", $valor);

            $retorno = "" . $partes[2] . "-" . $partes[1] . "-" . $partes[0];

            return $retorno;
        }
        else
            return "0000-00-00";
    }

    function FDataExibe($valor) {

        if ($valor == "")
            return "";

        $partes = explode("-", $valor);

        $retorno = substr($partes[2], 0, 2) . "/" . $partes[1] . "/" . $partes[0];

        return $retorno;
    }

    function FDataTimeBanco() {

        $retorno = date("Y-m-d H:i:s");

        return $retorno;
    }

    function FDataTimeExibe($valor) {

        if ($valor == "")
            return "";

        $retorno = substr($valor, 8, 2) . "/" . substr($valor, 5, 2) . "/" . substr($valor, 0, 4) . " " . substr($valor, 11, 8);

        return $retorno;
    }

    function FDados($dado) {

        $strDado = $dado;
        $strDado = htmlspecialchars($strDado);
        $strDado = html_entity_decode($strDado);
        if (!get_magic_quotes_gpc()) {
            $strDado = addslashes($strDado);
        }

        return $strDado;
    }

}

// CLASSE FIM
?>