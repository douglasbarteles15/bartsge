<?php

// ***************************************************************************************************
//
//	DATA CRIAÇÃO:		02.08.2016
//	CLASSE:				DAO_Usuarios_Admin
//	TABELA MYSQL:		usuarios_admin
//	BANCO MYSQL:		bartsge
//									
// ***************************************************************************************************
// ***************************************************************************************************
// DECLARAÇÃO DA CLASSE
// ***************************************************************************************************

class DAO_Usuarios_Admin { // CLASSE INÍCIO
// ***************************************************************************************************
// DECLARAÇÃO DE ATRIBUTOS
// ***************************************************************************************************

    var $id;   // CHAVE PRIMÁRIA - AUTO INCREMENTO
    var $nome;   // (ATRIBUTO VARCHAR)
    var $login;   // (ATRIBUTO VARCHAR)
    var $senha;   // (ATRIBUTO VARCHAR)
    var $email;   // (ATRIBUTO VARCHAR)
    var $tipo_id_INT;   // (ATRIBUTO INT)
    var $database; // INSTANCIA DA CLASSE DATABASE

// ***************************************************************************************************
// MéTODO CONSTRUTOR
// ***************************************************************************************************

    function DAO_Usuarios_Admin() {

        $this->database = new Database();
    }

// ***************************************************************************************************
// MÉTODOS - GETTER
// ***************************************************************************************************

    function getid() {

        return $this->id;
    }

    function getnome() {

        return $this->nome;
    }

    function getlogin() {

        return $this->login;
    }

    function getsenha() {

        return $this->senha;
    }

    function getemail() {

        return $this->email;
    }

    function gettipo_id_INT() {

        return $this->tipo_id_INT;
    }

// ***************************************************************************************************
// MÉTODOS - SETTER
// ***************************************************************************************************


    function setid($val) {

        $this->id = $val;
    }

    function setnome($val) {

        $this->nome = $val;
    }

    function setlogin($val) {

        $this->login = $val;
    }

    function setsenha($val) {

        $this->senha = $val;
    }

    function setemail($val) {

        $this->email = $val;
    }

    function settipo_id_INT($val) {

        $this->tipo_id_INT = $val;
    }

// ***************************************************************************************************
// M�TODO  - SELECT - RETORNA UMA LINHA DA TABELA
// ***************************************************************************************************

    function select($id) {

        $sql = "SELECT * FROM usuarios_admin WHERE id = '$id'";
        $result = $this->database->Query($sql);
        $row = $this->database->FetchObject();


        $this->id = $row->id;

        $this->nome = $row->nome;

        $this->login = $row->login;

        $this->senha = $row->senha;

        $this->email = $row->email;

        $this->tipo_id_INT = $row->tipo_id_INT;
    }

// ***************************************************************************************************
// METODO QUE RETORNA A LINHA DO ULTIMO REGISTRO CRIADO NA TABELA
// ***************************************************************************************************

    function selectLastInsert() {
        $this->database->Query("select * from usuarios_admin where id = last_insert_id()");
        $row = $this->database->FetchObject();
        $this->select($row->id);
    }

// ***************************************************************************************************
// MÉTODO  - SELECTBY - RETORNA LINHAS QUE ATENDEM A CONDIÇÃO
// ***************************************************************************************************

    function selectby($field, $val, $tipo_id_INT) {

        if ($tipo_id_INT != "int" || $tipo_id_INT != "float" || $tipo_id_INT != "INT" || $tipo_id_INT != "FLOAT" || $tipo_id_INT != "Int" || $tipo_id_INT != "Float") {
            $tipo_id_INT = "'";
        }

        $field = $this->FDados($field);
        $val = $this->FDados($val);

        $sql = "SELECT * FROM usuarios_admin WHERE $field=$tipo_id_INT$val$tipo_id_INT";
        $this->database->Query($sql);
        return $this->database->FetchObject();
    }

// ***************************************************************************************************
// MÉTODO  - SELECT APROXIMADO - RETORNA LINHAS QUE ATENDEM A CONDIÇÃO
// ***************************************************************************************************

    function selectlike($field, $val) {

        $field = $this->FDados($field);
        $val = $this->FDados($val);

        $sql = "SELECT * FROM usuarios_admin WHERE $field LIKE '%$val%'";
        $this->database->Query($sql);
        return $this->database->FetchObject();
    }

// ***************************************************************************************************
// MÉTODO - DELETE
// ***************************************************************************************************

    function delete($id) {

        $sql = "DELETE FROM usuarios_admin WHERE id = $id;";
        $result = $this->database->Query($sql);
    }

// ***************************************************************************************************
// MÉTODO - INSERT
// ***************************************************************************************************

    function insert() {

        $this->id = ""; // Limpa a chave do auto incremento

        $sql = "INSERT INTO usuarios_admin ( nome,login,senha,email,tipo_id_INT ) VALUES ( '$this->nome','$this->login','$this->senha','$this->email','$this->tipo_id_INT')";
        $result = $this->database->Query($sql);
    }

// ***************************************************************************************************
// MÉTODO - UPDATE
// ***************************************************************************************************

    function update($id, $tipo_id_INT) {

        if (isset($tipo_id_INT["id"])) {
            $upd.= "id = $this->id, ";
        }
        if (isset($tipo_id_INT["nome"])) {
            $upd.= "nome = '$this->nome', ";
        }
        if (isset($tipo_id_INT["login"])) {
            $upd.= "login = '$this->login', ";
        }
        if (isset($tipo_id_INT["senha"])) {
            $upd.= "senha = '$this->senha', ";
        }
        if (isseZt($tipo_id_INT["email"])) {
            $upd.= "email = '$this->email', ";
        }
        if (isset($tipo_id_INT["tipo_id_INT"])) {
            $upd.= "tipo_id_INT = $this->tipo_id_INT, ";
        }
        $upd = substr($upd, 0, -2);


        $sql = " UPDATE usuarios_admin SET $upd WHERE id = '$id' ";

        $result = $this->database->Query($sql);
    }

// ***************************************************************************************************
// MÉTODO PARA CRIAR VARIAVEIS DE SESSAO
// ***************************************************************************************************

    function createsession() {
        $_SESSION["id"] = $this->getid();
        $_SESSION["nome_funcionario"] = $this->getnome();
        $_SESSION["login_funcionario"] = $this->getlogin();
        $_SESSION["senha_funcionario"] = $this->getsenha();
        $_SESSION["email_funcionario"] = $this->getemail();
        $_SESSION["tipo_id_INT"] = $this->gettipo_id_INT();
    }

// ***************************************************************************************************
// MÉTODO PARA SETAR CAMPOS POR SESSAO
// ***************************************************************************************************

    function setsession() {
        $this->setid($this->FDados($_SESSION["id"]));
        $this->setnome($this->FDados($_SESSION["nome"]));
        $this->setlogin($this->FDados($_SESSION["login"]));
        $this->setsenha($this->FDados($_SESSION["senha"]));
        $this->setemail($this->FDados($_SESSION["email"]));
        $this->settipo_id_INT($this->FDados($_SESSION["tipo_id_INT"]));
    }

// ***************************************************************************************************
// M�TODO PARA RETIRAR OS VALORES DAS SESSOES
// ***************************************************************************************************

    function unsetsession() {
        unset($_SESSION["id"]);
        unset($_SESSION["nome"]);
        unset($_SESSION["login"]);
        unset($_SESSION["senha"]);
        unset($_SESSION["email"]);
        unset($_SESSION["tipo_id_INT"]);
    }

// ***************************************************************************************************
// M�TODO PARA SETAR CAMPOS POR POST
// ***************************************************************************************************

    function setpost() {
        $this->setid($this->FDados($_POST["id"]));
        $this->setnome($this->FDados($_POST["nome"]));
        $this->setlogin($this->FDados($_POST["login"]));
        $this->setsenha($this->FDados($_POST["senha"]));
        $this->setemail($this->FDados($_POST["email"]));
        $this->settipo_id_INT($this->FDados($_POST["tipo_id_INT"]));
    }

// ***************************************************************************************************
// M�TODO PARA SETAR CAMPOS POR GET
// ***************************************************************************************************

    function setget() {

        $this->setid($this->FDados($_GET["id"]));
        $this->setnome($this->FDados($_GET["nome"]));
        $this->setlogin($this->FDados($_GET["login"]));
        $this->setsenha($this->FDados($_GET["senha"]));
        $this->setemail($this->FDados($_GET["email"]));
        $this->settipo_id_INT($this->FDados($_GET["tipo_id_INT"]));
    }

// ***************************************************************************************************
// METODO QUE RETORNA ARRAY DE TODOS OS ID's EXISTENTES NA TABELA
// ***************************************************************************************************

    function todosIDs() {

        $sql = "SELECT id FROM usuarios_admin";

        $this->database->Query($sql);

        while ($row = $this->database->FetchObject()) {
            $i++;
            $retorno[$i] = $row->funcionarios_id;
        }

        return $retorno;
    }

// ***************************************************************************************************
// METODO QUE ATUALIZA UM CAMPO
// ***************************************************************************************************

    function updatecampo($id, $campo, $valor, $tipo_id_INT) {

        if ($tipo_id_INT != "int" || $tipo_id_INT != "float" || $tipo_id_INT != "INT" || $tipo_id_INT != "FLOAT" || $tipo_id_INT != "Int" || $tipo_id_INT != "Float") {
            $tipo_id_INT = "'";
        }


        $sql = "UPDATE usuarios_admin SET $campo = $tipo_id_INT$valor$tipo_id_INT WHERE id = $id";

        $this->database->Query($sql);
    }

// ***************************************************************************************************
// FUNCAO PARA FORMATAR OS VALORES DE DATA E VALORES DECIMAIS PARA GRAVAR NO BANCO
// ***************************************************************************************************

    function dadosBanco() {

    }

// ***************************************************************************************************
// FUNCAO PARA FORMATAR OS VALORES DE DATA E VALORES DECIMAIS PARA EXIBI��O
// ***************************************************************************************************

    function dadosExibe() {
        
    }

// ***************************************************************************************************
// METODOS AUXILIARES PARA FORMATACAO DE DADOS
// ***************************************************************************************************

    function FFloatBanco($numero) {

        if ($numero == "")
            return "0.00";

        $retorno = str_replace(".", "", $numero);
        $retorno = str_replace(",", ".", $retorno);

        return $retorno;
    }

    function FFloatExibe($numero) {

        if ($numero == "")
            return "";

        return number_format($numero, 2, ",", ".");
    }

    function FDataBanco($valor) {

        if ($valor != "" && $valor != null) {
            $partes = explode("/", $valor);

            $retorno = "" . $partes[2] . "-" . $partes[1] . "-" . $partes[0];

            return $retorno;
        }
        else
            return "0000-00-00";
    }

    function FDataExibe($valor) {

        if ($valor == "")
            return "";

        $partes = explode("-", $valor);

        $retorno = substr($partes[2], 0, 2) . "/" . $partes[1] . "/" . $partes[0];

        return $retorno;
    }

    function FDataTimeBanco() {

        $retorno = date("Y-m-d H:i:s");

        return $retorno;
    }

    function FDataTimeExibe($valor) {

        if ($valor == "")
            return "";

        $retorno = substr($valor, 8, 2) . "/" . substr($valor, 5, 2) . "/" . substr($valor, 0, 4) . " " . substr($valor, 11, 8);

        return $retorno;
    }

    function FDados($dado) {

        $strDado = $dado;
        $strDado = htmlspecialchars($strDado);
        $strDado = html_entity_decode($strDado);
        if (!get_magic_quotes_gpc()) {
            $strDado = addslashes($strDado);
        }

        return $strDado;
    }

}

// CLASSE FIM
?>