<?php

// ***************************************************************************************************
//
//	DATA CRIACAO:		03.08.2016
//	CLASSE:				DAO_Subeventos
//	TABELA MYSQL:		subeventos
//	BANCO MYSQL:		bartsge
//
// ***************************************************************************************************
// ***************************************************************************************************
// DECLARA��O DA CLASSE
// ***************************************************************************************************

class DAO_Subeventos { // CLASSE IN�CIO
// ***************************************************************************************************
// DECLARA��O DE ATRIBUTOS
// ***************************************************************************************************

    var $id;   // CHAVE PRIM�RIA - AUTO INCREMENTO
    var $evento_id_INT;   // (ATRIBUTO NORMAL)
    var $tipo_subevento_id_INT;   // (ATRIBUTO NORMAL)
    var $instrutor_id_INT;   // (ATRIBUTO NORMAL)
    var $titulo;   // CHAVE PRIM�RIA - AUTO INCREMENTO
    var $data;   // (ATRIBUTO NORMAL)
    var $hora_inicio;   // (ATRIBUTO NORMAL)
    var $hora_final;   // (ATRIBUTO NORMAL)
    var $descricao;   // (ATRIBUTO NORMAL)
    var $status_id_INT;   // (ATRIBUTO NORMAL)
    var $carga_horaria;   // (ATRIBUTO NORMAL)
    var $link;   // (ATRIBUTO NORMAL)
    var $database; // INSTANCIA DA CLASSE DATABASE

// ***************************************************************************************************
// M�TODO CONSTRUTOR
// ***************************************************************************************************

    function DAO_Subeventos() {

        $this->database = new Database();
    }

// ***************************************************************************************************
// M�TODOS - GETTER
// ***************************************************************************************************

    function getid() {

        return $this->id;
    }

    function getevento_id_INT() {

        return $this->evento_id_INT;
    }

    function gettipo_subevento_id_INT() {

        return $this->tipo_subevento_id_INT;
    }

    function getinstrutor_id_INT() {

        return $this->instrutor_id_INT;
    }

    function gettitulo() {

        return $this->titulo;
    }

    function getdata() {

        return $this->data;
    }

    function gethora_inicio() {

        return $this->hora_inicio;
    }

    function gethora_final() {

        return $this->hora_final;
    }

    function getdescricao() {

        return $this->descricao;
    }

    function getstatus_id_INT() {

        return $this->status_id_INT;
    }

    function getcarga_horaria() {

        return $this->carga_horaria;
    }

    function getlink() {

        return $this->link;
    }


// ***************************************************************************************************
// M�TODOS - SETTER
// ***************************************************************************************************


    function setid($val) {

        $this->id = $val;
    }

    function setevento_id_INT($val) {

        $this->evento_id_INT = $val;
    }

    function settipo_subevento_id_INT($val) {

        $this->tipo_subevento_id_INT = $val;
    }

    function setinstrutor_id_INT($val) {

        $this->instrutor_id_INT = $val;
    }

    function settitulo($val) {

        $this->titulo = $val;
    }

    function setdata($val) {

        $this->data = $val;
    }

    function sethora_inicio($val) {

        $this->hora_inicio = $val;
    }

    function sethora_final($val) {

        $this->hora_final = $val;
    }

    function setdescricao($val) {

        $this->descricao = $val;
    }

    function setstatus_id_INT($val) {

        $this->status_id_INT = $val;
    }

    function setcarga_horaria($val) {

        $this->carga_horaria = $val;
    }

    function setlink($val) {

        $this->link = $val;
    }


// ***************************************************************************************************
// M�TODO  - SELECT - RETORNA UMA LINHA DA TABELA
// ***************************************************************************************************

    function select($id) {

        $sql = "SELECT * FROM subeventos WHERE id = '$id'";
        $result = $this->database->Query($sql);
        $row = $this->database->FetchObject();


        $this->id = $row->id;

        $this->evento_id_INT = $row->evento_id_INT;

        $this->tipo_subevento_id_INT = $row->tipo_subevento_id_INT;

        $this->instrutor_id_INT = $row->instrutor_id_INT;

        $this->titulo = $row->titulo;

        $this->data = $row->data;

        $this->hora_inicio = $row->hora_inicio;

        $this->hora_final = $row->hora_final;

        $this->descricao = $row->descricao;

        $this->status_id_INT = $row->status_id_INT;

        $this->carga_horaria = $row->carga_horaria;

        $this->link = $row->link;

    }
    function selectCategoria($id) {

        $sql = "SELECT * FROM subeventos WHERE evento_id_INT = $id;";
        $result = $this->database->Query($sql);
        $row = $this->database->FetchObject();


        $this->id = $row->id;

        $this->evento_id_INT = $row->evento_id_INT;

        $this->tipo_subevento_id_INT = $row->tipo_subevento_id_INT;

        $this->instrutor_id_INT = $row->instrutor_id_INT;

        $this->titulo = $row->titulo;

        $this->data = $row->data;

        $this->hora_inicio = $row->hora_inicio;

        $this->hora_final = $row->hora_final;

        $this->descricao = $row->descricao;

        $this->status_id_INT = $row->status_id_INT;

        $this->carga_horaria = $row->carga_horaria;

        $this->link = $row->link;
    }
// ***************************************************************************************************
// M�TODO  - SELECT - RETORNA UMA LINHA DA TABELA
// ***************************************************************************************************

    function selectLink($link) {


        $sql = "SELECT * FROM subeventos WHERE hora_final = '$link'";
        $result = $this->database->Query($sql);
        $row = $this->database->FetchObject();


        $this->id = $row->id;

        $this->evento_id_INT = $row->evento_id_INT;

        $this->tipo_subevento_id_INT = $row->tipo_subevento_id_INT;

        $this->instrutor_id_INT = $row->instrutor_id_INT;

        $this->titulo = $row->titulo;

        $this->data = $row->data;

        $this->hora_inicio = $row->hora_inicio;

        $this->hora_final = $row->hora_final;

        $this->status_id_INT = $row->status_id_INT;

        $this->descricao = $row->descricao;

        $this->carga_horaria = $row->carga_horaria;

        $this->link = $row->link;

    }

// ***************************************************************************************************
// METODO QUE RETORNA A LINHA DO �LTIMO REGISTRO CRIADO NA TABELA
// ***************************************************************************************************

    function selectLastInsert() {
        $this->database->Query("select * from subeventos where id = last_insert_id()");
        $row = $this->database->FetchObject();
        $this->select($row->id);
    }

    public function rows($sql) {
		 $result = $this->database->Query($sql);
		 $quantidade = $this->database->rows;
		 //echo $quantidade; break;
        //$result = $this->database->Query($sql);
        return $quantidade;

    }

// ***************************************************************************************************
// M�TODO  - SELECTBY - RETORNA LINHAS QUE ATENDEM A CONDI��O
// ***************************************************************************************************

    function selectby($field, $val, $tipo) {

        if ($tipo != "int" || $tipo != "float" || $tipo != "INT" || $tipo != "FLOAT" || $tipo != "Int" || $tipo != "Float") {
            $tipo = "'";
        }

        $field = $this->FDados($field);
        $val = $this->FDados($val);

        $sql = "SELECT * FROM subeventos WHERE $field=$tipo$val$tipo";
        $this->database->Query($sql);
        return $this->database->FetchObject();
    }

// ***************************************************************************************************
// M�TODO  - SELECT APROXIMADO - RETORNA LINHAS QUE ATENDEM A CONDI��O
// ***************************************************************************************************

    function selectlike($field, $val) {

        $field = $this->FDados($field);
        $val = $this->FDados($val);

        $sql = "SELECT * FROM subeventos WHERE $field LIKE '%$val%'";
        $this->database->Query($sql);
        return $this->database->FetchObject();
    }

// ***************************************************************************************************
// M�TODO - DELETE
// ***************************************************************************************************

    function delete($id) {

        $sql = "DELETE FROM subeventos WHERE id = $id;";
        $result = $this->database->Query($sql);
    }

// ***************************************************************************************************
// M�TODO - INSERT
// ***************************************************************************************************

    function insert() {

        $this->id = ""; // Limpa a chave do auto incremento

        $sql = "INSERT INTO subeventos (evento_id_INT, tipo_subevento_id_INT, instrutor_id_INT, titulo, data, hora_inicio, hora_final, descricao, status_id_INT, carga_horaria, link ) VALUES ( '$this->evento_id_INT', '$this->tipo_subevento_id_INT', '$this->instrutor_id_INT', '$this->titulo', '$this->data', '$this->hora_inicio','$this->hora_final','$this->descricao', '$this->status_id_INT','$this->carga_horaria','$this->link')";
        $result = $this->database->Query($sql);
    }

// ***************************************************************************************************
// M�TODO - UPDATE
// ***************************************************************************************************

    function update($id, $tipo) {

        if (isset($tipo["id"])) {
            $upd.= "id = $this->id, ";
        }
        if (isset($tipo["evento_id_INT"])) {
            $upd.= "evento_id_INT = $this->evento_id_INT, ";
        }
        if (isset($tipo["tipo_subevento_id_INT"])) {
            $upd.= "tipo_subevento_id_INT = '$this->tipo_subevento_id_INT', ";
        }
        if (isset($tipo["instrutor_id_INT"])) {
            $upd.= "instrutor_id_INT = '$this->instrutor_id_INT', ";
        }
        if (isset($tipo["titulo"])) {
            $upd.= "titulo = '$this->titulo', ";
        }
        if (isset($tipo["data"])) {
            $upd.= "data = '$this->data', ";
        }
        if (isset($tipo["hora_inicio"])) {
            $upd.= "hora_inicio = '$this->hora_inicio', ";
        }
        if (isset($tipo["hora_final"])) {
            $upd.= "hora_final = '$this->hora_final', ";
        }
        if (isset($tipo["descricao"])) {
            $upd.= "descricao = '$this->descricao', ";
        }
        if (isset($tipo["status_id_INT"])) {
            $upd.= "status_id_INT = '$this->status_id_INT', ";
        }
        if (isset($tipo["carga_horaria"])) {
            $upd.= "carga_horaria = '$this->carga_horaria', ";
        }
        if (isset($tipo["link"])) {
            $upd.= "link = '$this->link', ";
        }
        $upd = substr($upd, 0, -2);


        $sql = " UPDATE subeventos SET $upd WHERE id = $id ";

        $result = $this->database->Query($sql);
    }

// ***************************************************************************************************
// M�TODO PARA CRIAR VARIAVEIS DE SESSAO
// ***************************************************************************************************

    function createsession() {
        $_SESSION["id"] = $this->getid();
        $_SESSION["evento_id_INT"] = $this->getevento_id_INT();
        $_SESSION["tipo_subevento_id_INT"] = $this->gettipo_subevento_id_INT();
        $_SESSION["instrutor_id_INT"] = $this->getinstrutor_id_INT();
        $_SESSION["titulo"] = $this->gettitulo();
        $_SESSION["data"] = $this->getdata();
        $_SESSION["hora_inicio"] = $this->gethora_inicio();
        $_SESSION["hora_final"] = $this->gethora_final();
        $_SESSION["descricao"] = $this->getdescricao();
        $_SESSION["status_id_INT"] = $this->getstatus_id_INT();
        $_SESSION["carga_horaria"] = $this->getcarga_horaria();
        $_SESSION["link"] = $this->getlink();
    }

// ***************************************************************************************************
// M�TODO PARA SETAR CAMPOS POR SESSAO
// ***************************************************************************************************

    function setsession() {
        $this->setid($this->FDados($_SESSION["id"]));
        $this->setevento_id_INT($this->FDados($_SESSION["evento_id_INT"]));
        $this->settipo_subevento_id_INT($this->FDados($_SESSION["tipo_subevento_id_INT"]));
        $this->setinstrutor_id_INT($this->FDados($_SESSION["instrutor_id_INT"]));
        $this->settitulo($this->FDados($_SESSION["titulo"]));
        $this->setdata($this->FDados($_SESSION["data"]));
        $this->sethora_inicio($this->FDados($_SESSION["hora_inicio"]));
        $this->sethora_final($this->FDados($_SESSION["hora_final"]));
        $this->setdescricao($this->FDados($_SESSION["descricao"]));
        $this->setstatus_id_INT($this->FDados($_SESSION["status_id_INT"]));
        $this->setcarga_horaria($this->FDados($_SESSION["carga_horaria"]));
        $this->setstatus_id_INT($this->FDados($_SESSION["status_id_INT"]));
        $this->setlink($this->FDados($_SESSION["link"]));
    }

// ***************************************************************************************************
// M�TODO PARA RETIRAR OS VALORES DAS SESSOES
// ***************************************************************************************************

    function unsetsession() {
        unset($_SESSION["id"]);
        unset($_SESSION["evento_id_INT"]);
        unset($_SESSION["tipo_subevento_id_INT"]);
        unset($_SESSION["instrutor_id_INT"]);
        unset($_SESSION["titulo"]);
        unset($_SESSION["data"]);
        unset($_SESSION["hora_inicio"]);
        unset($_SESSION["hora_final"]);
        unset($_SESSION["descricao"]);
        unset($_SESSION["status_id_INT"]);
        unset($_SESSION["carga_horaria"]);
        unset($_SESSION["link"]);
    }

// ***************************************************************************************************
// M�TODO PARA SETAR CAMPOS POR POST
// ***************************************************************************************************

    function setpost() {
        $this->setid($this->FDados($_POST["id"]));
        $this->setevento_id_INT($this->FDados($_POST["evento_id_INT"]));
        $this->settipo_subevento_id_INT($this->FDados($_POST["tipo_subevento_id_INT"]));
//        $this->settipo_subevento_id_INT(htmlentities($this->FDados($_POST["tipo_subevento_id_INT"]), ENT_COMPAT, "UTF-8"));
        $this->setinstrutor_id_INT($this->FDados($_POST["instrutor_id_INT"]));
        $this->settitulo($this->FDados($_POST["titulo"]));
        $this->setdata($this->FDados($_POST["data"]));
        $this->sethora_inicio($this->FDados($_POST["hora_inicio"]));
        $this->sethora_final($this->FDados($_POST["hora_final"]));
        $this->setdescricao($this->FDados($_POST["descricao"]));
        $this->setstatus_id_INT($this->FDados($_POST["status_id_INT"]));
        $this->setcarga_horaria($this->FDados($_POST["carga_horaria"]));
        $this->setlink($this->FDados($this->montaURLNova($this->montaURL($_POST["titulo"]), $_POST["id"])));
//        $this->setcurso_valor($this->FDados($_POST["curso_valor"]));
    }
// ***************************************************************************************************
// M�TODO PARA SETAR CAMPOS POR GET
// ***************************************************************************************************

    function setget() {
        $this->setid($this->FDados($_GET["id"]));
        $this->setevento_id_INT($this->FDados($_GET["evento_id_INT"]));
        $this->settipo_subevento_id_INT($this->FDados($_GET["tipo_subevento_id_INT"]));
        $this->setinstrutor_id_INT($this->FDados($_GET["instrutor_id_INT"]));
        $this->settitulo($this->FDados($_GET["titulo"]));
        $this->setdata($this->FDados($_GET["data"]));
        $this->sethora_inicio($this->FDados($_GET["hora_inicio"]));
        $this->sethora_final($this->FDados($_GET["hora_final"]));
        $this->setdescricao($this->FDados($_GET["descricao"]));
        $this->setstatus_id_INT($this->FDados($_GET["status_id_INT"]));
        $this->setcarga_horaria($this->FDados($_GET["carga_horaria"]));
        $this->setlink($this->FDados($_GET["link"]));
    }

// ***************************************************************************************************
// METODO QUE RETORNA ARRAY DE TODOS OS ID's EXISTENTES NA TABELA
// ***************************************************************************************************

    function todosIDs() {

        $sql = "SELECT id FROM subeventos ORDER BY id ASC";

        $this->database->Query($sql);

        while ($row = $this->database->FetchObject()) {
            $i++;
            $retorno[$i] = $row->id;
        }

        return $retorno;
    }

    function todosIDsCategoria($categoria_id) {

        $sql = "SELECT id FROM subeventos WHERE evento_id_INT = '".$categoria_id."' ORDER BY id ASC";

        $this->database->Query($sql);

        while ($row = $this->database->FetchObject()) {
            $i++;
            $retorno[$i] = $row->id;
        }

        return $retorno;
    }

// ***************************************************************************************************
// METODO QUE ATUALIZA UM CAMPO
// ***************************************************************************************************

    function updatecampo($id, $campo, $valor, $tipo) {

        if ($tipo != "int" || $tipo != "float" || $tipo != "INT" || $tipo != "FLOAT" || $tipo != "Int" || $tipo != "Float") {
            $tipo = "'";
        }


        $sql = "UPDATE subeventos SET $campo = $tipo$valor$tipo WHERE id = $id";

        $this->database->Query($sql);
    }

// ***************************************************************************************************
// FUNCAO PARA FORMATAR OS VALORES DE DATA E VALORES DECIMAIS PARA GRAVAR NO BANCO
// ***************************************************************************************************

    function dadosBanco() {

        $this->setdata($this->FDataBanco($this->getdata()));

    }

// ***************************************************************************************************
// FUNCAO PARA FORMATAR OS VALORES DE DATA E VALORES DECIMAIS PARA EXIBI��O
// ***************************************************************************************************

    function dadosExibe() {

    }

// ***************************************************************************************************
// METODOS AUXILIARES PARA FORMATACAO DE DADOS
// ***************************************************************************************************

    function FFloatBanco($numero) {

        if ($numero == "")
            return "0.00";

        $retorno = str_replace(".", "", $numero);
        $retorno = str_replace(",", ".", $retorno);

        return $retorno;
    }

    function FFloatExibe($numero) {

        if ($numero == "")
            return "";

        return number_format($numero, 2, ",", ".");
    }

    function FDataBanco($valor) {

        if ($valor != "" && $valor != null) {
            $partes = explode("/", $valor);

            $retorno = "" . $partes[2] . "-" . $partes[1] . "-" . $partes[0];

            return $retorno;
        }
        else
            return "0000-00-00";
    }

    function FDataExibe($valor) {

        if ($valor == "")
            return "";

        $partes = explode("-", $valor);

        $retorno = substr($partes[2], 0, 2) . "/" . $partes[1] . "/" . $partes[0];

        return $retorno;
    }

    function FDataTimeBanco() {

        $retorno = date("Y-m-d H:i:s");

        return $retorno;
    }

    function FDataTimeExibe($valor) {

        if ($valor == "")
            return "";

        $retorno = substr($valor, 8, 2) . "/" . substr($valor, 5, 2) . "/" . substr($valor, 0, 4) . " " . substr($valor, 11, 8);

        return $retorno;
    }

    function FDados($dado) {

        $strDado = $dado;
        $strDado = htmlspecialchars($strDado);
        $strDado = html_entity_decode($strDado);
        if (!get_magic_quotes_gpc()) {
            $strDado = addslashes($strDado);
        }

        return $strDado;
    }


    // Monta a URL Amigável
    function montaURL($variavel) {
        $variavel = htmlentities(strtolower($variavel));
        $variavel = preg_replace("/&(.)(acute|cedil|circ|ring|tilde|uml);/", "$1", $variavel);
        $variavel = preg_replace("/([^a-z0-9]+)/", "-", html_entity_decode($variavel));
        $variavel = trim($variavel, "-");
        return $variavel;
    }

    //Verifica se já existe URL para poder Montar uma nova
    function montaURLNova($variavel, $idconteudo) {
        $query = mysql_query("select * from subeventos where link  = '$variavel'");
        $num_rows = @mysql_num_rows($query);
        while ($dados = mysql_fetch_array($query)) {
            $id = $dados[0];
        }

        if ($idconteudo <> $id) {
            if ($num_rows == 1) {
                while ($num_rows <> $num_post) {
                    $novo = $variavel . "-" . $num_rows;

                    $sql2 = "select * from subeventos where link = '$novo'";


                    $query2 = mysql_query($sql2);
                    $num_rows2 = @mysql_num_rows($query2);

                    if ($num_rows2 == 0) {
                        $variavel = $novo;
                        $num_post = $num_rows;
                    } else {
                        $num_rows++;
                    }
                }
            }
        }

        return $variavel;
    }


}

// CLASSE FIM
?>