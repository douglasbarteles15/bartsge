<?php

// ***************************************************************************************************
//
//	DATA CRIAÇÃO:		14.11.2011
//	CLASSE:				DAO_Funcionarios	
//	TABELA MYSQL:		funcionarios
//	BANCO MYSQL:		qualificar
//									
// ***************************************************************************************************
// ***************************************************************************************************
// DECLARAÇÃO DA CLASSE
// ***************************************************************************************************

class DAO_Log_Admin { // CLASSE INÍCIO
// ***************************************************************************************************
// DECLARAÇÃO DE ATRIBUTOS
// ***************************************************************************************************

    var $id;   // CHAVE PRIM�RIA - AUTO INCREMENTO
    var $funcionario_id;   // (ATRIBUTO NORMAL)
    var $mensagem;   // (ATRIBUTO NORMAL)
    var $data;   // (ATRIBUTO NORMAL)

    var $database; // INSTANCIA DA CLASSE DATABASE

// ***************************************************************************************************
// MéTODO CONSTRUTOR
// ***************************************************************************************************

    function DAO_Log_Admin() {

        $this->database = new Database();
    }

// ***************************************************************************************************
// MÉTODOS - GETTER
// ***************************************************************************************************

    function getid() {

        return $this->id;
    }

    function getfuncionario_id() {

        return $this->funcionario_id;
    }

    function getmensagem() {

        return $this->mensagem;
    }

    function getdata() {

        return $this->data;
    }



// ***************************************************************************************************
// MÉTODOS - SETTER
// ***************************************************************************************************


    function setid($val) {

        $this->id = $val;
    }

    function setfuncionario_id($val) {

        $this->funcionario_id = $val;
    }

    function setmensagem($val) {

        $this->mensagem = $val;
    }

    function setdata($val) {

        $this->data = $val;
    }



// ***************************************************************************************************
// M�TODO  - SELECT - RETORNA UMA LINHA DA TABELA
// ***************************************************************************************************

    function select($id) {

        $sql = "SELECT * FROM log_admin WHERE id = '$id'";
        $result = $this->database->Query($sql);
        $row = $this->database->FetchObject();


        $this->id = $row->id;

        $this->funcionario_id = $row->funcionario_id;

        $this->mensagem = $row->mensagem;

        $this->data = $row->data;

    }

// ***************************************************************************************************
// METODO QUE RETORNA A LINHA DO ULTIMO REGISTRO CRIADO NA TABELA
// ***************************************************************************************************

    function selectLastInsert() {
        $this->database->Query("select * from log_admin where id = last_insert_id()");
        $row = $this->database->FetchObject();
        $this->select($row->id);
    }

// ***************************************************************************************************
// MÉTODO  - SELECTBY - RETORNA LINHAS QUE ATENDEM A CONDIÇÃO
// ***************************************************************************************************

    function selectby($field, $val, $tipo) {

        if ($tipo != "int" || $tipo != "float" || $tipo != "INT" || $tipo != "FLOAT" || $tipo != "Int" || $tipo != "Float") {
            $tipo = "'";
        }

        $field = $this->FDados($field);
        $val = $this->FDados($val);

        $sql = "SELECT * FROM log_admin WHERE $field=$tipo$val$tipo";
        $this->database->Query($sql);
        return $this->database->FetchObject();
    }

// ***************************************************************************************************
// MÉTODO  - SELECT APROXIMADO - RETORNA LINHAS QUE ATENDEM A CONDIÇÃO
// ***************************************************************************************************

    function selectlike($field, $val) {

        $field = $this->FDados($field);
        $val = $this->FDados($val);

        $sql = "SELECT * FROM log_admin WHERE $field LIKE '%$val%'";
        $this->database->Query($sql);
        return $this->database->FetchObject();
    }

// ***************************************************************************************************
// MÉTODO - DELETE
// ***************************************************************************************************

    function delete($id) {

        $sql = "DELETE FROM log_admin WHERE id = $id;";
        $result = $this->database->Query($sql);
    }

// ***************************************************************************************************
// MÉTODO - INSERT
// ***************************************************************************************************

    function insert($funcionario_id, $mensagem, $data) {

        $this->id = ""; // Limpa a chave do auto incremento

        $sql = "INSERT INTO log_admin ( funcionario_id,mensagem,data ) VALUES ( '$funcionario_id','$mensagem','$data')";
        $result = $this->database->Query($sql);
    }

// ***************************************************************************************************
// MÉTODO - DELETE LOG -- DELETA LOG ADMIN DE MAIS DE 30 DIAS
// ***************************************************************************************************

    function delete_log() {

        $sql = "DELETE FROM log_admin WHERE data < DATE_SUB(NOW(), INTERVAL 30 DAY)";
        $result = $this->database->Query($sql);
    }

// ***************************************************************************************************
// MÉTODO - UPDATE
// ***************************************************************************************************

    function update($id, $tipo) {

        if (isset($tipo["id"])) {
            $upd.= "id = $this->id, ";
        }
        if (isset($tipo["funcionario_id"])) {
            $upd.= "funcionario_id = '$this->funcionario_id', ";
        }
        if (isset($tipo["mensagem"])) {
            $upd.= "mensagem = '$this->mensagem', ";
        }
        if (isset($tipo["data"])) {
            $upd.= "data = '$this->data', ";
        }

        $upd = substr($upd, 0, -2);


        $sql = " UPDATE log_admin SET $upd WHERE id = '$id' ";

        $result = $this->database->Query($sql);
    }

// ***************************************************************************************************
// MÉTODO PARA CRIAR VARIAVEIS DE SESSAO
// ***************************************************************************************************

    function createsession() {
        $_SESSION["id"] = $this->getid();
        $_SESSION["funcionario_id_funcionario"] = $this->getfuncionario_id();
        $_SESSION["mensagem_funcionario"] = $this->getmensagem();
        $_SESSION["data_funcionario"] = $this->getdata();

    }

// ***************************************************************************************************
// MÉTODO PARA SETAR CAMPOS POR SESSAO
// ***************************************************************************************************

    function setsession() {
        $this->setid($this->FDados($_SESSION["id"]));
        $this->setfuncionario_id($this->FDados($_SESSION["funcionario_id"]));
        $this->setmensagem($this->FDados($_SESSION["mensagem"]));
        $this->setdata($this->FDados($_SESSION["data"]));

    }

// ***************************************************************************************************
// M�TODO PARA RETIRAR OS VALORES DAS SESSOES
// ***************************************************************************************************

    function unsetsession() {
        unset($_SESSION["id"]);
        unset($_SESSION["funcionario_id_funcionario"]);
        unset($_SESSION["mensagem_funcionario"]);
        unset($_SESSION["data_funcionario"]);

    }

// ***************************************************************************************************
// M�TODO PARA SETAR CAMPOS POR POST
// ***************************************************************************************************

    function setpost() {
        $this->setid($this->FDados($_POST["id"]));
        $this->setfuncionario_id($this->FDados($_POST["funcionario_id"]));
        $this->setmensagem($this->FDados($_POST["mensagem"]));
        $this->setdata($this->FDados($_POST["data"]));

    }

// ***************************************************************************************************
// M�TODO PARA SETAR CAMPOS POR GET
// ***************************************************************************************************

    function setget() {

        $this->setid($this->FDados($_GET["id"]));
        $this->setfuncionario_id($this->FDados($_GET["funcionario_id"]));
        $this->setmensagem($this->FDados($_GET["mensagem"]));
        $this->setdata($this->FDados($_GET["data"]));

    }

// ***************************************************************************************************
// METODO QUE RETORNA ARRAY DE TODOS OS ID's EXISTENTES NA TABELA
// ***************************************************************************************************

    function todosIDs() {

        $sql = "SELECT id FROM log_admin";

        $this->database->Query($sql);

        while ($row = $this->database->FetchObject()) {
            $i++;
            $retorno[$i] = $row->id;
        }

        return $retorno;
    }

// ***************************************************************************************************
// METODO QUE ATUALIZA UM CAMPO
// ***************************************************************************************************

    function updatecampo($id, $campo, $valor, $tipo) {

        if ($tipo != "int" || $tipo != "float" || $tipo != "INT" || $tipo != "FLOAT" || $tipo != "Int" || $tipo != "Float") {
            $tipo = "'";
        }


        $sql = "UPDATE log_admin SET $campo = $tipo$valor$tipo WHERE id = $id";

        $this->database->Query($sql);
    }

// ***************************************************************************************************
// FUNCAO PARA FORMATAR OS VALORES DE DATA E VALORES DECIMAIS PARA GRAVAR NO BANCO
// ***************************************************************************************************

    function dadosBanco() {

    }

// ***************************************************************************************************
// FUNCAO PARA FORMATAR OS VALORES DE DATA E VALORES DECIMAIS PARA EXIBI��O
// ***************************************************************************************************

    function dadosExibe() {
        
    }

// ***************************************************************************************************
// METODOS AUXILIARES PARA FORMATACAO DE DADOS
// ***************************************************************************************************

    function FFloatBanco($numero) {

        if ($numero == "")
            return "0.00";

        $retorno = str_replace(".", "", $numero);
        $retorno = str_replace(",", ".", $retorno);

        return $retorno;
    }

    function FFloatExibe($numero) {

        if ($numero == "")
            return "";

        return number_format($numero, 2, ",", ".");
    }

    function FDataBanco($valor) {

        if ($valor != "" && $valor != null) {
            $partes = explode("/", $valor);

            $retorno = "" . $partes[2] . "-" . $partes[1] . "-" . $partes[0];

            return $retorno;
        }
        else
            return "0000-00-00";
    }

    function FDataExibe($valor) {

        if ($valor == "")
            return "";

        $partes = explode("-", $valor);

        $retorno = substr($partes[2], 0, 2) . "/" . $partes[1] . "/" . $partes[0];

        return $retorno;
    }

    function FDataTimeBanco() {

        $retorno = date("Y-m-d H:i:s");

        return $retorno;
    }

    function FDataTimeExibe($valor) {

        if ($valor == "")
            return "";

        $retorno = substr($valor, 8, 2) . "/" . substr($valor, 5, 2) . "/" . substr($valor, 0, 4) . " " . substr($valor, 11, 8);

        return $retorno;
    }

    function FDados($dado) {

        $strDado = $dado;
        $strDado = htmlspecialchars($strDado);
        $strDado = html_entity_decode($strDado);
        if (!get_magic_quotes_gpc()) {
            $strDado = addslashes($strDado);
        }

        return $strDado;
    }

}

// CLASSE FIM
?>