<?php

// ***************************************************************************************************
//
//	DATA CRIA��O:		16.11.2011
//	DATA DE ALTERAÇÃO       29.08.2012
//	CLASSE:				DAO_Config_site
//	TABELA MYSQL:		config_site
//	BANCO MYSQL:		Tribuna de Minas
//
// ***************************************************************************************************
// ***************************************************************************************************
// DECLARA��O DA CLASSE
// ***************************************************************************************************

class DAO_Config_site { // CLASSE IN�CIO
// ***************************************************************************************************
// DECLARA��O DE ATRIBUTOS
// ***************************************************************************************************

    var $idconfig_site;   // CHAVE PRIM�RIA - AUTO INCREMENTO
    var $nome_fantasia;   // (ATRIBUTO NORMAL)
    var $razao_social;   // (ATRIBUTO NORMAL)
    var $cnpj;   // (ATRIBUTO NORMAL)
    var $url_site;   // (ATRIBUTO NORMAL)
    var $metatag_titulo;   // (ATRIBUTO NORMAL)
    var $metatag_descricao;   // (ATRIBUTO NORMAL)
    var $metatag_palavrachave;   // (ATRIBUTO NORMAL)
    var $email;   // (ATRIBUTO NORMAL)
    var $email_financeiro   ;   // (ATRIBUTO NORMAL)
    var $endereco;   // (ATRIBUTO NORMAL)
    var $numero;   // (ATRIBUTO NORMAL)
    var $complemento;   // (ATRIBUTO NORMAL)
    var $bairro;   // (ATRIBUTO NORMAL)
    var $cidade;   // (ATRIBUTO NORMAL)
    var $cep;   // (ATRIBUTO NORMAL)
    var $estado;   // (ATRIBUTO NORMAL)
    var $tel1;   // (ATRIBUTO NORMAL)
    var $tel2;   // (ATRIBUTO NORMAL)
    var $cel1;   // (ATRIBUTO NORMAL)
    var $cel2;   // (ATRIBUTO NORMAL)
    var $direitos;   // (ATRIBUTO NORMAL)
    var $marcaDagua;   // (ATRIBUTO NORMAL)
    var $destaque;   // (ATRIBUTO NORMAL)
    var $analytcs;   // (ATRIBUTO NORMAL)
    var $dia_inicio_pre_inscricao;   // (ATRIBUTO NORMAL)
    var $hora_inicio_pre_inscricao;   // (ATRIBUTO NORMAL)
    var $dia_final_pre_inscricao;   // (ATRIBUTO NORMAL)
    var $hora_final_pre_inscricao;   // (ATRIBUTO NORMAL)

    var $database; // INSTANCIA DA CLASSE DATABASE

// ***************************************************************************************************
// M�TODO CONSTRUTOR
// ***************************************************************************************************

    function DAO_Config_site() {

        $this->database = new Database();
    }

// ***************************************************************************************************
// M�TODOS - GETTER
// ***************************************************************************************************

    function getidconfig_site() {

        return $this->idconfig_site;
    }

    function getnome_fantasia() {

        return $this->nome_fantasia;
    }

    function getrazao_social() {

        return $this->razao_social;
    }

    function getcnpj() {

        return $this->cnpj;
    }

    function geturl_site() {

        return $this->url_site;
    }

    function getmetatag_titulo() {

        return $this->metatag_titulo;
    }

    function getmetatag_descricao() {

        return $this->metatag_descricao;
    }

    function getmetatag_palavrachave() {

        return $this->metatag_palavrachave;
    }

    function getemail() {

        return $this->email;
    }

    function getemail_financeiro() {

        return $this->email_financeiro;
    }

    function getendereco() {

        return $this->endereco;
    }

    function getnumero() {

        return $this->numero;
    }

    function getcomplemento() {

        return $this->complemento;
    }

    function getbairro() {

        return $this->bairro;
    }

    function getcidade() {

        return $this->cidade;
    }

    function getcep() {

        return $this->cep;
    }
    function getestado() {

        return $this->estado;
    }

    function gettel1() {

        return $this->tel1;
    }

    function gettel2() {

        return $this->tel2;
    }

    function getcel1() {

        return $this->cel1;
    }

    function getcel2() {

        return $this->cel2;
    }

    function getdireitos() {

        return $this->direitos;
    }

    function getmarcaDagua() {

        return $this->marcaDagua;
    }

    function getdestaque() {

        return $this->destaque;
    }
    function getanalytcs() {

        return $this->analytcs;
    }
    function getdia_inicio_pre_inscricao() {

        return $this->dia_inicio_pre_inscricao;
    }
    function gethora_inicio_pre_inscricao() {

        return $this->hora_inicio_pre_inscricao;
    }
    function getdia_final_pre_inscricao() {

        return $this->dia_final_pre_inscricao;
    }
    function gethora_final_pre_inscricao() {

        return $this->hora_final_pre_inscricao;
    }

// ***************************************************************************************************
// M�TODOS - SETTER
// ***************************************************************************************************


    function setidconfig_site($val) {

        $this->idconfig_site = $val;
    }

    function setnome_fantasia($val) {

        $this->nome_fantasia = $val;
    }

    function setrazao_social($val) {

        $this->razao_social = $val;
    }

    function setcnpj($val) {

        $this->cnpj = $val;
    }

    function seturl_site($val) {

        $this->url_site = $val;
    }

    function setmetatag_titulo($val) {

        $this->metatag_titulo = $val;
    }

    function setmetatag_descricao($val) {

        $this->metatag_descricao = $val;
    }

    function setmetatag_palavrachave($val) {

        $this->metatag_palavrachave = $val;
    }

    function setemail($val) {

        $this->email = $val;
    }

    function setemail_financeiro($val) {

        $this->email_financeiro = $val;
    }

    function setendereco($val) {

        $this->endereco = $val;
    }

    function setnumero($val) {

        $this->numero = $val;
    }

    function setcomplemento($val) {

        $this->complemento = $val;
    }

    function setbairro($val) {

        $this->bairro = $val;
    }

    function setcidade($val) {

        $this->cidade = $val;
    }

    function setcep($val) {

        $this->cep = $val;
    }

    function setestado($val) {

        $this->estado = $val;
    }

    function settel1($val) {

        $this->tel1 = $val;
    }

    function settel2($val) {

        $this->tel2 = $val;
    }

    function setcel1($val) {

        $this->cel1 = $val;
    }

    function setcel2($val) {

        $this->cel2 = $val;
    }

    function setdireitos($val) {

        $this->direitos = $val;
    }

    function setmarcaDagua($val) {

        $this->marcaDagua = $val;
    }

    function setdestaque($val) {

        $this->destaque = $val;
    }

    function setanalytcs($val) {

        $this->analytcs = $val;
    }

    function setdia_inicio_pre_inscricao($val) {

        $this->dia_inicio_pre_inscricao = $val;
    }

    function sethora_inicio_pre_inscricao($val) {

        $this->hora_inicio_pre_inscricao = $val;
    }

    function setdia_final_pre_inscricao($val) {

        $this->dia_final_pre_inscricao = $val;
    }

    function sethora_final_pre_inscricao($val) {

        $this->hora_final_pre_inscricao = $val;
    }

// ***************************************************************************************************
// M�TODO  - SELECT - RETORNA UMA LINHA DA TABELA
// ***************************************************************************************************

    function select($id) {

        $sql = "SELECT * FROM config_site WHERE idconfig_site = $id;";
        $result = $this->database->Query($sql);
        $row = $this->database->FetchObject();


        $this->idconfig_site = $row->idconfig_site;

        $this->nome_fantasia = $row->nome_fantasia;

        $this->razao_social = $row->razao_social;

        $this->cnpj = $row->cnpj;

        $this->url_site = $row->url_site;

        $this->metatag_titulo = $row->metatag_titulo;

        $this->metatag_descricao = $row->metatag_descricao;

        $this->metatag_palavrachave = $row->metatag_palavrachave;

        $this->email = $row->email;

        $this->email_financeiro = $row->email_financeiro;

        $this->endereco = $row->endereco;

        $this->numero = $row->numero;

        $this->complemento = $row->complemento;

        $this->bairro = $row->bairro;

        $this->cidade = $row->cidade;

        $this->cep = $row->cep;

        $this->estado = $row->estado;

        $this->tel1 = $row->tel1;

        $this->tel2 = $row->tel2;

        $this->cel1 = $row->cel1;

        $this->cel2 = $row->cel2;

        $this->direitos = $row->direitos;

        $this->marcaDagua = $row->marcaDagua;

        $this->destaque = $row->destaque;

        $this->analytcs = $row->analytcs;

        $this->dia_inicio_pre_inscricao = $row->dia_inicio_pre_inscricao;

        $this->hora_inicio_pre_inscricao = $row->hora_inicio_pre_inscricao;

        $this->dia_final_pre_inscricao = $row->dia_final_pre_inscricao;

        $this->hora_final_pre_inscricao = $row->hora_final_pre_inscricao;
    }

// ***************************************************************************************************
// METODO QUE RETORNA A LINHA DO �LTIMO REGISTRO CRIADO NA TABELA
// ***************************************************************************************************

    function selectLastInsert() {
        $this->database->Query("select * from config_site where idconfig_site = last_insert_id()");
        $row = $this->database->FetchObject();
        $this->select($row->idconfig_site);
    }

// ***************************************************************************************************
// M�TODO  - SELECTBY - RETORNA LINHAS QUE ATENDEM A CONDI��O
// ***************************************************************************************************

    function selectby($field, $val, $tipo) {

        if ($tipo != "int" || $tipo != "float" || $tipo != "INT" || $tipo != "FLOAT" || $tipo != "Int" || $tipo != "Float") {
            $tipo = "'";
        }

        $field = $this->FDados($field);
        $val = $this->FDados($val);

        $sql = "SELECT * FROM config_site WHERE $field=$tipo$val$tipo";
        $this->database->Query($sql);
        return $this->database->FetchObject();
    }

// ***************************************************************************************************
// M�TODO  - SELECT APROXIMADO - RETORNA LINHAS QUE ATENDEM A CONDI��O
// ***************************************************************************************************

    function selectlike($field, $val) {

        $field = $this->FDados($field);
        $val = $this->FDados($val);

        $sql = "SELECT * FROM config_site WHERE $field LIKE '%$val%'";
        $this->database->Query($sql);
        return $this->database->FetchObject();
    }

// ***************************************************************************************************
// M�TODO - DELETE
// ***************************************************************************************************

    function delete($id) {

        $sql = "DELETE FROM config_site WHERE idconfig_site = $id;";
        $result = $this->database->Query($sql);
    }

// ***************************************************************************************************
// M�TODO - INSERT
// ***************************************************************************************************

    function insert() {

        $this->idconfig_site = ""; // Limpa a chave do auto incremento

        $sql = "INSERT INTO config_site ( nome_fantasia, razao_social, cnpj, url_site, metatag_titulo, metatag_descricao, metatag_palavrachave, email, email_financeiro, endereco, numero, complemento, bairro, cidade, cep, estado, tel1, tel2, cel1, cel2, marcaDagua, destaque, analytcs, dia_inicio_pre_inscricao, hora_inicio_pre_inscricao, dia_final_pre_inscricao, hora_final_pre_inscricao ) VALUES ( '$this->nome_fantasia','$this->razao_social','$this->cnpj','$this->url_site','$this->metatag_titulo','$this->metatag_descricao','$this->metatag_palavrachave','$this->email','$this->email_financeiro', '$this->endereco','$this->numero','$this->complemento','$this->bairro','$this->cidade','$this->cep','$this->estado', '$this->tel1', '$this->tel2', '$this->cel1', '$this->cel2', '$this->marcaDagua', '$this->destaque', '$this->analytcs', '$this->dia_inicio_pre_inscricao', '$this->hora_inicio_pre_inscricao', '$this->dia_final_pre_inscricao', '$this->hora_final_pre_inscricao' )";
        $result = $this->database->Query($sql);
    }

// ***************************************************************************************************
// M�TODO - UPDATE
// ***************************************************************************************************

    function update($id, $tipo) {

        if (isset($tipo["nome_fantasia"])) {
            $upd.= "nome_fantasia = '$this->nome_fantasia', ";
        }
        if (isset($tipo["razao_social"])) {
            $upd.= "razao_social = '$this->razao_social', ";
        }
        if (isset($tipo["cnpj"])) {
            $upd.= "cnpj = '$this->cnpj', ";
        }
        if (isset($tipo["url_site"])) {
            $upd.= "url_site = '$this->url_site', ";
        }
        if (isset($tipo["metatag_titulo"])) {
            $upd.= "metatag_titulo = '$this->metatag_titulo', ";
        }
        if (isset($tipo["metatag_descricao"])) {
            $upd.= "metatag_descricao = '$this->metatag_descricao', ";
        }
        if (isset($tipo["metatag_palavrachave"])) {
            $upd.= "metatag_palavrachave = '$this->metatag_palavrachave', ";
        }
        if (isset($tipo["email"])) {
            $upd.= "email = '$this->email', ";
        }
        if (isset($tipo["email_financeiro"])) {
            $upd.= "email_financeiro = '$this->email_financeiro', ";
        }
        if (isset($tipo["endereco"])) {
            $upd.= "endereco = '$this->endereco', ";
        }
        if (isset($tipo["numero"])) {
            $upd.= "numero = '$this->numero', ";
        }
        if (isset($tipo["complemento"])) {
            $upd.= "complemento = '$this->complemento', ";
        }
        if (isset($tipo["bairro"])) {
            $upd.= "bairro = '$this->bairro', ";
        }
        if (isset($tipo["cidade"])) {
            $upd.= "cidade = '$this->cidade', ";
        }
        if (isset($tipo["cep"])) {
            $upd.= "cep = '$this->cep', ";
        }
        if (isset($tipo["estado"])) {
            $upd.= "estado = '$this->estado', ";
        }
        if (isset($tipo["tel1"])) {
            $upd.= "tel1 = '$this->tel1', ";
        }
        if (isset($tipo["tel2"])) {
            $upd.= "tel2 = '$this->tel2', ";
        }
        if (isset($tipo["cel1"])) {
            $upd.= "cel1 = '$this->cel1', ";
        }
        if (isset($tipo["cel2"])) {
            $upd.= "cel2 = '$this->cel2', ";
        }
        if (isset($tipo["direitos"])) {
            $upd.= "direitos = '$this->direitos', ";
        }
        if (isset($tipo["marcaDagua"])) {
            $upd.= "marcaDagua = '$this->marcaDagua', ";
        }
        if (isset($tipo["destaque"])) {
            $upd.= "destaque = '$this->destaque', ";
        }
        if (isset($tipo["analytcs"])) {
            $upd.= "analytcs = '$this->analytcs', ";
        }
        if (isset($tipo["dia_inicio_pre_inscricao"])) {
            $upd.= "dia_inicio_pre_inscricao = '$this->dia_inicio_pre_inscricao', ";
        }
        if (isset($tipo["hora_inicio_pre_inscricao"])) {
            $upd.= "hora_inicio_pre_inscricao = '$this->hora_inicio_pre_inscricao', ";
        }
        if (isset($tipo["dia_final_pre_inscricao"])) {
            $upd.= "dia_final_pre_inscricao = '$this->dia_final_pre_inscricao', ";
        }
        if (isset($tipo["hora_final_pre_inscricao"])) {
            $upd.= "hora_final_pre_inscricao = '$this->hora_final_pre_inscricao', ";
        }
        $upd = substr($upd, 0, -2);


        $sql = " UPDATE config_site SET $upd WHERE idconfig_site = $id ";

        $result = $this->database->Query($sql);
    }

// ***************************************************************************************************
// M�TODO PARA CRIAR VARIAVEIS DE SESSAO
// ***************************************************************************************************

    function createsession() {
        $_SESSION["idconfig_site"] = $this->getidconfig_site();
        $_SESSION["nome_fantasia"] = $this->getnome_fantasia();
        $_SESSION["razao_social"] = $this->getrazao_social();
        $_SESSION["cnpj"] = $this->getcnpj();
        $_SESSION["url_site"] = $this->geturl_site();
        $_SESSION["metatag_titulo"] = $this->getmetatag_titulo();
        $_SESSION["metatag_descricao"] = $this->getmetatag_descricao();
        $_SESSION["metatag_palavrachave"] = $this->getmetatag_palavrachave();
        $_SESSION["email"] = $this->getemail();
        $_SESSION["email_financeiro"] = $this->getemail_financeiro();
        $_SESSION["endereco"] = $this->getendereco();
        $_SESSION["numero"] = $this->getnumero();
        $_SESSION["complemento"] = $this->getcomplemento();
        $_SESSION["bairro"] = $this->getbairro();
        $_SESSION["cidade"] = $this->getcidade();
        $_SESSION["cep"] = $this->getcep();
        $_SESSION["estado"] = $this->getestado();
        $_SESSION["tel1"] = $this->gettel1();
        $_SESSION["tel2"] = $this->gettel2();
        $_SESSION["cel1"] = $this->getcel1();
        $_SESSION["cel2"] = $this->getcel2();
        $_SESSION["direitos"] = $this->getdireitos();
        $_SESSION["marcaDagua"] = $this->getmarcaDagua();
        $_SESSION["destaque"] = $this->getdestaque();
        $_SESSION["analytcs"] = $this->getanalytcs();
        $_SESSION["dia_inicio_pre_inscricao"] = $this->getdia_inicio_pre_inscricao();
        $_SESSION["hora_inicio_pre_inscricao"] = $this->gethora_inicio_pre_inscricao();
        $_SESSION["dia_final_pre_inscricao"] = $this->getdia_final_pre_inscricao();
        $_SESSION["hora_final_pre_inscricao"] = $this->gethora_final_pre_inscricao();
    }

// ***************************************************************************************************
// M�TODO PARA SETAR CAMPOS POR SESSAO
// ***************************************************************************************************

    function setsession() {
        $this->setidconfig_site($this->FDados($_SESSION["idconfig_site"]));
        $this->setnome_fantasia($this->FDados($_SESSION["nome_fantasia"]));
        $this->setrazao_social($this->FDados($_SESSION["razao_social"]));
        $this->setcnpj($this->FDados($_SESSION["cnpj"]));
        $this->seturl_site($this->FDados($_SESSION["url_site"]));
        $this->setmetatag_titulo($this->FDados($_SESSION["metatag_titulo"]));
        $this->setmetatag_descricao($this->FDados($_SESSION["metatag_descricao"]));
        $this->setmetatag_palavrachave($this->FDados($_SESSION["metatag_palavrachave"]));
        $this->setemail($this->FDados($_SESSION["email"]));
        $this->setemail_financeiro($this->FDados($_SESSION["email_financeiro"]));
        $this->setendereco($this->FDados($_SESSION["endereco"]));
        $this->setnumero($this->FDados($_SESSION["numero"]));
        $this->setcomplemento($this->FDados($_SESSION["complemento"]));
        $this->setbairro($this->FDados($_SESSION["bairro"]));
        $this->setcidade($this->FDados($_SESSION["cidade"]));
        $this->setcep($this->FDados($_SESSION["cep"]));
        $this->setestado($this->FDados($_SESSION["estado"]));
        $this->settel1($this->FDados($_SESSION["tel1"]));
        $this->settel2($this->FDados($_SESSION["tel2"]));
        $this->setcel1($this->FDados($_SESSION["cel1"]));
        $this->setcel2($this->FDados($_SESSION["cel2"]));
        $this->setdireitos($this->FDados($_SESSION["direitos"]));
        $this->setmarcaDagua($this->FDados($_SESSION["marcaDagua"]));
        $this->setdestaque($this->FDados($_SESSION["destaque"]));
        $this->setanalytcs($this->FDados($_SESSION["analytcs"]));
        $this->setdia_inicio_pre_inscricao($this->FDados($_SESSION["dia_inicio_pre_inscricao"]));
        $this->sethora_inicio_pre_inscricao($this->FDados($_SESSION["dia_inicio_pre_inscricao"]));
        $this->setdia_final_pre_inscricao($this->FDados($_SESSION["dia_final_pre_inscricao"]));
        $this->sethora_final_pre_inscricao($this->FDados($_SESSION["hora_final_pre_inscricao"]));
    }

// ***************************************************************************************************
// M�TODO PARA RETIRAR OS VALORES DAS SESSOES
// ***************************************************************************************************

    function unsetsession() {
        unset($_SESSION["idconfig_site"]);
        unset($_SESSION["nome_fantasia"]);
        unset($_SESSION["razao_social"]);
        unset($_SESSION["cnpj"]);
        unset($_SESSION["url_site"]);
        unset($_SESSION["metatag_titulo"]);
        unset($_SESSION["metatag_descricao"]);
        unset($_SESSION["metatag_palavrachave"]);
        unset($_SESSION["email"]);
        unset($_SESSION["email_financeiro"]);
        unset($_SESSION["endereco"]);
        unset($_SESSION["numero"]);
        unset($_SESSION["complemento"]);
        unset($_SESSION["bairro"]);
        unset($_SESSION["cidade"]);
        unset($_SESSION["cep"]);
        unset($_SESSION["estado"]);
        unset($_SESSION["tel1"]);
        unset($_SESSION["tel2"]);
        unset($_SESSION["cel1"]);
        unset($_SESSION["cel2"]);
        unset($_SESSION["direitos"]);
        unset($_SESSION["marcaDagua"]);
        unset($_SESSION["destaque"]);
        unset($_SESSION["analytcs"]);
        unset($_SESSION["dia_inicio_pre_inscricao"]);
        unset($_SESSION["hora_inicio_pre_inscricao"]);
        unset($_SESSION["dia_final_pre_inscricao"]);
        unset($_SESSION["hora_final_pre_inscricao"]);
    }

// ***************************************************************************************************
// M�TODO PARA SETAR CAMPOS POR POST
// ***************************************************************************************************

    function setpost() {
        $this->setidconfig_site($this->FDados($_POST["idconfig_site"]));
        $this->setnome_fantasia($this->FDados($_POST["nome_fantasia"]));
        $this->setrazao_social($this->FDados($_POST["razao_social"]));
        $this->setcnpj($this->FDados($_POST["cnpj"]));
        $this->seturl_site($this->FDados($_POST["url_site"]));
        $this->setmetatag_titulo($this->FDados($_POST["metatag_titulo"]));
        $this->setmetatag_descricao($this->FDados($_POST["metatag_descricao"]));
        $this->setmetatag_palavrachave($this->FDados($_POST["metatag_palavrachave"]));
        $this->setemail($this->FDados($_POST["email"]));
        $this->setemail_financeiro($this->FDados($_POST["email_financeiro"]));
        $this->setendereco($this->FDados($_POST["endereco"]));
        $this->setnumero($this->FDados($_POST["numero"]));
        $this->setcomplemento($this->FDados($_POST["complemento"]));
        $this->setbairro($this->FDados($_POST["bairro"]));
        $this->setcidade($this->FDados($_POST["cidade"]));
        $this->setcep($this->FDados($_POST["cep"]));
        $this->setestado($this->FDados($_POST["estado"]));
        $this->settel1($this->FDados($_POST["tel1"]));
        $this->settel2($this->FDados($_POST["tel2"]));
        $this->setcel1($this->FDados($_POST["cel1"]));
        $this->setcel2($this->FDados($_POST["cel2"]));
        $this->setdireitos($this->FDados($_POST["direitos"]));
        $this->setmarcaDagua($this->FDados($_POST["marcaDagua"]));
        $this->setdestaque($this->FDados($_POST["destaque"]));
        $this->setanalytcs($this->FDados($_POST["analytcs"]));
        $this->setdia_inicio_pre_inscricao($this->FDados($_POST["dia_inicio_pre_inscricao"]));
        $this->sethora_inicio_pre_inscricao($this->FDados($_POST["hora_inicio_pre_inscricao"]));
        $this->setdia_final_pre_inscricao($this->FDados($_POST["dia_final_pre_inscricao"]));
        $this->sethora_final_pre_inscricao($this->FDados($_POST["hora_final_pre_inscricao"]));
    }

// ***************************************************************************************************
// M�TODO PARA SETAR CAMPOS POR GET
// ***************************************************************************************************

    function setget() {

        $this->setidconfig_site($this->FDados($_GET["idconfig_site"]));
        $this->setnome_fantasia($this->FDados($_GET["nome_fantasia"]));
        $this->setrazao_social($this->FDados($_GET["razao_social"]));
        $this->setcnpj($this->FDados($_GET["cnpj"]));
        $this->seturl_site($this->FDados($_GET["url_site"]));
        $this->setmetatag_titulo($this->FDados($_GET["metatag_titulo"]));
        $this->setmetatag_descricao($this->FDados($_GET["metatag_descricao"]));
        $this->setmetatag_palavrachave($this->FDados($_GET["metatag_palavrachave"]));
        $this->setemail($this->FDados($_GET["email"]));
        $this->setemail_financeiro($this->FDados($_GET["email_financeiro"]));
        $this->setendereco($this->FDados($_GET["endereco"]));
        $this->setnumero($this->FDados($_GET["numero"]));
        $this->setcomplemento($this->FDados($_GET["complemento"]));
        $this->setbairro($this->FDados($_GET["bairro"]));
        $this->setcidade($this->FDados($_GET["cidade"]));
        $this->setcep($this->FDados($_GET["cep"]));
        $this->setestado($this->FDados($_GET["estado"]));
        $this->settel1($this->FDados($_GET["tel1"]));
        $this->settel2($this->FDados($_GET["tel2"]));
        $this->setcel1($this->FDados($_GET["cel1"]));
        $this->setcel2($this->FDados($_GET["cel2"]));
        $this->setdireitos($this->FDados($_GET["direitos"]));
        $this->setmarcaDagua($this->FDados($_GET["marcaDagua"]));
        $this->setdestaque($this->FDados($_GET["destaque"]));
        $this->setanalytcs($this->FDados($_GET["analytcs"]));
        $this->setdia_inicio_pre_inscricao($this->FDados($_GET["dia_inicio_pre_inscricao"]));
        $this->sethora_inicio_pre_inscricao($this->FDados($_GET["hora_inicio_pre_inscricao"]));
        $this->setdia_final_pre_inscricao($this->FDados($_GET["dia_final_pre_inscricao"]));
        $this->sethora_final_pre_inscricao($this->FDados($_GET["hora_final_pre_inscricao"]));
    }

// ***************************************************************************************************
// METODO QUE RETORNA ARRAY DE TODOS OS ID's EXISTENTES NA TABELA
// ***************************************************************************************************

    function todosIDs() {

        $sql = "SELECT idconfig_site FROM config_site";

        $this->database->Query($sql);

        while ($row = $this->database->FetchObject()) {
            $i++;
            $retorno[$i] = $row->idconfig_site;
        }

        return $retorno;
    }

// ***************************************************************************************************
// METODO QUE ATUALIZA UM CAMPO
// ***************************************************************************************************

    function updatecampo($id, $campo, $valor, $tipo) {

        if ($tipo != "int" || $tipo != "float" || $tipo != "INT" || $tipo != "FLOAT" || $tipo != "Int" || $tipo != "Float") {
            $tipo = "'";
        }


        $sql = "UPDATE config_site SET $campo = $tipo$valor$tipo WHERE idconfig_site = $id";

        $this->database->Query($sql);
    }

// ***************************************************************************************************
// FUNCAO PARA FORMATAR OS VALORES DE DATA E VALORES DECIMAIS PARA GRAVAR NO BANCO
// ***************************************************************************************************

    function dadosBanco() {

    }

// ***************************************************************************************************
// FUNCAO PARA FORMATAR OS VALORES DE DATA E VALORES DECIMAIS PARA EXIBI��O
// ***************************************************************************************************

    function dadosExibe() {
        
    }

// ***************************************************************************************************
// METODOS AUXILIARES PARA FORMATACAO DE DADOS
// ***************************************************************************************************

    function FFloatBanco($numero) {

        if ($numero == "")
            return "0.00";

        $retorno = str_replace(".", "", $numero);
        $retorno = str_replace(",", ".", $retorno);

        return $retorno;
    }

    function FFloatExibe($numero) {

        if ($numero == "")
            return "";

        return number_format($numero, 2, ",", ".");
    }

    function FDataBanco($valor) {

        if ($valor != "" && $valor != null) {
            $partes = explode("/", $valor);

            $retorno = "" . $partes[2] . "-" . $partes[1] . "-" . $partes[0];

            return $retorno;
        }
        else
            return "0000-00-00";
    }

    function FDataExibe($valor) {

        if ($valor == "")
            return "";

        $partes = explode("-", $valor);

        $retorno = substr($partes[2], 0, 2) . "/" . $partes[1] . "/" . $partes[0];

        return $retorno;
    }

    function FDataTimeBanco() {

        $retorno = date("Y-m-d H:i:s");

        return $retorno;
    }

    function FDataTimeExibe($valor) {

        if ($valor == "")
            return "";

        $retorno = substr($valor, 8, 2) . "/" . substr($valor, 5, 2) . "/" . substr($valor, 0, 4) . " " . substr($valor, 11, 8);

        return $retorno;
    }

    function FDados($dado) {

        $strDado = $dado;
        $strDado = htmlspecialchars($strDado);
        $strDado = html_entity_decode($strDado);
        if (!get_magic_quotes_gpc()) {
            $strDado = addslashes($strDado);
        }

        return $strDado;
    }

}

// CLASSE FIM
?>