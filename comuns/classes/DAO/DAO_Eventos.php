<?php

// ***************************************************************************************************
//
//	DATA CRIACAO:		03.08.2016
//	CLASSE:				DAO_Eventos
//	TABELA MYSQL:		eventos
//	BANCO MYSQL:		bartsge
//
// ***************************************************************************************************
// ***************************************************************************************************
// DECLARA��O DA CLASSE
// ***************************************************************************************************

class DAO_Eventos { // CLASSE IN�CIO
// ***************************************************************************************************
// DECLARA��O DE ATRIBUTOS
// ***************************************************************************************************

    var $id;   // CHAVE PRIM�RIA - AUTO INCREMENTO
    var $usuario_id_INT;   // (ATRIBUTO NORMAL)
    var $categoria_id_INT;   // (ATRIBUTO NORMAL)
    var $titulo;   // (ATRIBUTO NORMAL)
    var $data_inicio;   // (ATRIBUTO NORMAL)
    var $data_final;   // (ATRIBUTO NORMAL)
    var $hora_inicio;   // (ATRIBUTO NORMAL)
    var $hora_final;   // (ATRIBUTO NORMAL)
    var $vagas_total;   // (ATRIBUTO NORMAL)
    var $vagas_restantes;   // (ATRIBUTO NORMAL)
    var $descricao;   // (ATRIBUTO NORMAL)
    var $chamada;   // (ATRIBUTO NORMAL)
    var $pago_id_INT;   // (ATRIBUTO NORMAL)
    var $video;   // (ATRIBUTO NORMAL)
    var $status_id_INT;   // (ATRIBUTO NORMAL)
    var $link;   // (ATRIBUTO NORMAL)
    var $img;   // (ATRIBUTO NORMAL)
    var $img_full;   // (ATRIBUTO NORMAL)
    var $database; // INSTANCIA DA CLASSE DATABASE

// ***************************************************************************************************
// M�TODO CONSTRUTOR
// ***************************************************************************************************

    function DAO_Eventos() {

        $this->database = new Database();
    }

// ***************************************************************************************************
// M�TODOS - GETTER
// ***************************************************************************************************

    function getid() {

        return $this->id;
    }

    function getusuario_id_INT() {

        return $this->usuario_id_INT;
    }

    function getcategoria_id_INT() {

        return $this->categoria_id_INT;
    }

    function gettitulo() {

        return $this->titulo;
    }

    function getdata_inicio() {

        return $this->data_inicio;
    }
    
    function getdata_final() {

        return $this->data_final;
    }

    function gethora_inicio() {

        return $this->hora_inicio;
    }

    function gethora_final() {

        return $this->hora_final;
    }

    function getvagas_total() {

        return $this->vagas_total;
    }

    function getvagas_restantes() {

        return $this->vagas_restantes;
    }
    
    function getdescricao() {

        return $this->descricao;
    }

    function getchamada() {

        return $this->chamada;
    }

    function getpago_id_INT() {

        return $this->pago_id_INT;
    }

    function getvideo() {

        return $this->video;
    }

    function getstatus_id_INT() {

        return $this->status_id_INT;
    }

    function getlink() {

        return $this->link;
    }

    function getimg_mini() {

        return $this->img_mini;
    }

    function getimg_full() {

        return $this->img_full;
    }

// ***************************************************************************************************
// M�TODOS - SETTER
// ***************************************************************************************************


    function setid($val) {

        $this->id = $val;
    }

    function setusuario_id_INT($val) {

        $this->usuario_id_INT = $val;
    }

    function setcategoria_id_INT($val) {

        $this->categoria_id_INT = $val;
    }

    function settitulo($val) {

        $this->titulo = $val;
    }

    function setdata_inicio($val) {

        $this->data_inicio = $val;
    }
    
    function setdata_final($val) {

        $this->data_final = $val;
    }

    function sethora_inicio($val) {

        $this->hora_inicio = $val;
    }

    function sethora_final($val) {

        $this->hora_final = $val;
    }

    function setvagas_total($val) {

        $this->vagas_total = $val;
    }

    function setvagas_restantes($val) {

        $this->vagas_restantes = $val;
    }
    
    function setdescricao($val) {

        $this->descricao = $val;
    }

    function setchamada($val) {

        $this->chamada = $val;
    }

    function setpago_id_INT($val) {

        $this->pago_id_INT = $val;
    }

    function setvideo($val) {

        $this->video = $val;
    }

    function setstatus_id_INT($val) {

        $this->status_id_INT = $val;
    }

    function setlink($val) {

        $this->link = $val;
    }

    function setimg_mini($val) {

        $this->img_mini = $val;
    }

    function setimg_full($val) {

        $this->img_full = $val;
    }

// ***************************************************************************************************
// M�TODO  - SELECT - RETORNA UMA LINHA DA TABELA
// ***************************************************************************************************

    function select($id) {

        $sql = "SELECT * FROM eventos WHERE id = '$id'";
        $result = $this->database->Query($sql);
        $row = $this->database->FetchObject();


        $this->id = $row->id;

        $this->usuario_id_INT = $row->usuario_id_INT;

        $this->categoria_id_INT = $row->categoria_id_INT;

        $this->titulo = $row->titulo;

        $this->data_inicio = $row->data_inicio;
        
        $this->data_final = $row->data_final;

        $this->hora_inicio = $row->hora_inicio;

        $this->hora_final = $row->hora_final;

        $this->vagas_total = $row->vagas_total;

        $this->vagas_restantes = $row->vagas_restantes;
        
        $this->descricao = $row->descricao;

        $this->chamada = $row->chamada;

        $this->pago_id_INT = $row->pago_id_INT;

        $this->video = $row->video;

        $this->status_id_INT = $row->status_id_INT;

        $this->link = $row->link;

        $this->img_mini = $row->img_mini;

        $this->img_full = $row->img_full;
    }
    function selectCategoria($id) {

        $sql = "SELECT * FROM eventos WHERE usuario_id_INT = $id;";
        $result = $this->database->Query($sql);
        $row = $this->database->FetchObject();


        $this->id = $row->id;

        $this->usuario_id_INT = $row->usuario_id_INT;

        $this->categoria_id_INT = $row->categoria_id_INT;

        $this->titulo = $row->titulo;

        $this->data_inicio = $row->data_inicio;
        
        $this->data_final = $row->data_final;

        $this->hora_inicio = $row->hora_inicio;

        $this->hora_final = $row->hora_final;

        $this->vagas_total = $row->vagas_total;

        $this->vagas_restantes = $row->vagas_restantes;
        
        $this->descricao = $row->descricao;

        $this->chamada = $row->chamada;

        $this->pago_id_INT = $row->pago_id_INT;

        $this->video = $row->video;

        $this->status_id_INT = $row->status_id_INT;

        $this->link = $row->link;

        $this->img_mini = $row->img_mini;

        $this->img_full = $row->img_full;
    }
// ***************************************************************************************************
// M�TODO  - SELECT - RETORNA UMA LINHA DA TABELA
// ***************************************************************************************************

    function selectLink($link) {
		
		
        $sql = "SELECT * FROM eventos WHERE link = '$link'";
        $result = $this->database->Query($sql);
        $row = $this->database->FetchObject();


        $this->id = $row->id;
		
        $this->usuario_id_INT = $row->usuario_id_INT;
		
        $this->categoria_id_INT = $row->categoria_id_INT;

        $this->titulo = $row->titulo;

        $this->data_inicio = $row->data_inicio;
        
        $this->data_final = $row->data_final;

        $this->hora_inicio = $row->hora_inicio;

        $this->hora_final = $row->hora_final;

        $this->vagas_total = $row->vagas_total;
		
        $this->vagas_restantes = $row->vagas_restantes;
        
        $this->descricao = $row->descricao;

        $this->chamada = $row->chamada;

        $this->pago_id_INT = $row->pago_id_INT;

        $this->video = $row->video;

        $this->status_id_INT = $row->status_id_INT;

        $this->link = $row->link;

        $this->img_mini = $row->img_mini;

        $this->img_full = $row->img_full;
    }

// ***************************************************************************************************
// METODO QUE RETORNA A LINHA DO �LTIMO REGISTRO CRIADO NA TABELA
// ***************************************************************************************************

    function selectLastInsert() {
        $this->database->Query("select * from eventos where id = last_insert_id()");
        $row = $this->database->FetchObject();
        $this->select($row->id);
    }

    public function rows($sql) {
		 $result = $this->database->Query($sql);
		 $quantidade = $this->database->rows; 
		 //echo $quantidade; break;
        //$result = $this->database->Query($sql);
        return $quantidade; 
           
    }
    
// ***************************************************************************************************
// M�TODO  - SELECTBY - RETORNA LINHAS QUE ATENDEM A CONDI��O
// ***************************************************************************************************

    function selectby($field, $val, $tipo) {

        if ($tipo != "int" || $tipo != "float" || $tipo != "INT" || $tipo != "FLOAT" || $tipo != "Int" || $tipo != "Float") {
            $tipo = "'";
        }

        $field = $this->FDados($field);
        $val = $this->FDados($val);

        $sql = "SELECT * FROM eventos WHERE $field=$tipo$val$tipo";
        $this->database->Query($sql);
        return $this->database->FetchObject();
    }

// ***************************************************************************************************
// M�TODO  - SELECT APROXIMADO - RETORNA LINHAS QUE ATENDEM A CONDI��O
// ***************************************************************************************************

    function selectlike($field, $val) {

        $field = $this->FDados($field);
        $val = $this->FDados($val);

        $sql = "SELECT * FROM eventos WHERE $field LIKE '%$val%'";
        $this->database->Query($sql);
        return $this->database->FetchObject();
    }

// ***************************************************************************************************
// M�TODO - DELETE
// ***************************************************************************************************

    function delete($id) {

        $sql = "DELETE FROM eventos WHERE id = $id;";
        $result = $this->database->Query($sql);
    }

// ***************************************************************************************************
// M�TODO - INSERT
// ***************************************************************************************************

    function insert() {

        $this->id = ""; // Limpa a chave do auto incremento

        $sql = "INSERT INTO eventos (usuario_id_INT, categoria_id_INT, titulo, data_inicio, data_final, hora_inicio, hora_final, vagas_total, vagas_restantes, descricao, chamada, pago_id_INT, video, status_id_INT, link, img_mini, img_full ) VALUES ( '$this->usuario_id_INT', '$this->categoria_id_INT', '$this->titulo', '$this->data_inicio', '$this->data_final', '$this->hora_inicio','$this->hora_final','$this->vagas_total', '$this->vagas_restantes', '$this->descricao','$this->chamada','$this->pago_id_INT','$this->video','$this->status_id_INT','$this->link', '$this->img_mini', '$this->img_full')";
        $result = $this->database->Query($sql);
    }

// ***************************************************************************************************
// M�TODO - UPDATE
// ***************************************************************************************************

    function update($id, $tipo) {

        if (isset($tipo["id"])) {
            $upd.= "id = $this->id, ";
        }
        if (isset($tipo["usuario_id_INT"])) {
            $upd.= "usuario_id_INT = $this->usuario_id_INT, ";
        }
        if (isset($tipo["categoria_id_INT"])) {
            $upd.= "categoria_id_INT = '$this->categoria_id_INT', ";
        }
        if (isset($tipo["titulo"])) {
            $upd.= "titulo = '$this->titulo', ";
        }
        if (isset($tipo["periodo"])) {
            $upd.= "data_inicio = '$this->data_inicio', ";
        }
        if (isset($tipo["periodo"])) {
            $upd.= "data_final = '$this->data_final', ";
        }
        if (isset($tipo["hora_inicio"])) {
            $upd.= "hora_inicio = '$this->hora_inicio', ";
        }
        if (isset($tipo["hora_final"])) {
            $upd.= "hora_final = '$this->hora_final', ";
        }
        if (isset($tipo["vagas_total"])) {
            $upd.= "vagas_total = '$this->vagas_total', ";
        }
        if (isset($tipo["vagas_restantes"])) {
            $upd.= "vagas_restantes = '$this->vagas_restantes', ";
        }
        if (isset($tipo["descricao"])) {
            $upd.= "descricao = '$this->descricao', ";
        }
        if (isset($tipo["chamada"])) {
            $upd.= "chamada = '$this->chamada', ";
        }
        if (isset($tipo["pago_id_INT"])) {
            $upd.= "pago_id_INT = '$this->pago_id_INT', ";
        }
        if (isset($tipo["video"])) {
            $upd.= "video = '$this->video', ";
        }
        if (isset($tipo["status_id_INT"])) {
            $upd.= "status_id_INT = '$this->status_id_INT', ";
        }
        if (isset($tipo["link"])) {
            $upd.= "link = '$this->link', ";
        }
        if (isset($tipo["img_mini"])) {
            $upd.= "img_mini = '$this->img_mini', ";
        }
        if (isset($tipo["img_full"])) {
            $upd.= "img_full = '$this->img_full', ";
        }
        $upd = substr($upd, 0, -2);

        $sql = " UPDATE eventos SET $upd WHERE id = $id ";

        $result = $this->database->Query($sql);
    }

// ***************************************************************************************************
// M�TODO PARA CRIAR VARIAVEIS DE SESSAO
// ***************************************************************************************************

    function createsession() {
        $_SESSION["id"] = $this->getid();
        $_SESSION["usuario_id_INT"] = $this->getusuario_id_INT();
        $_SESSION["categoria_id_INT"] = $this->getcategoria_id_INT();
        $_SESSION["titulo"] = $this->gettitulo();
        $_SESSION["data_inicio"] = $this->getdata_inicio();
        $_SESSION["data_final"] = $this->getdata_final();
        $_SESSION["hora_inicio"] = $this->gethora_inicio();
        $_SESSION["hora_final"] = $this->gethora_final();
        $_SESSION["vagas_total"] = $this->getvagas_total();
        $_SESSION["vagas_restantes"] = $this->getvagas_restantes();
        $_SESSION["descricao"] = $this->getdescricao();
        $_SESSION["chamada"] = $this->getchamada();
        $_SESSION["pago_id_INT"] = $this->getpago_id_INT();
        $_SESSION["video"] = $this->getvideo();
        $_SESSION["status_id_INT"] = $this->getstatus_id_INT();
        $_SESSION["link"] = $this->getlink();
        $_SESSION["img_mini"] = $this->getimg_mini();
        $_SESSION["img_full"] = $this->getimg_full();
    }

// ***************************************************************************************************
// M�TODO PARA SETAR CAMPOS POR SESSAO
// ***************************************************************************************************

    function setsession() {
        $this->setid($this->FDados($_SESSION["id"]));
        $this->setusuario_id_INT($this->FDados($_SESSION["usuario_id_INT"]));
        $this->setcategoria_id_INT($this->FDados($_SESSION["categoria_id_INT"]));
        $this->settitulo($this->FDados($_SESSION["titulo"]));
        $this->setdata_inicio($this->FDados($_SESSION["data_inicio"]));
        $this->setdata_final($this->FDados($_SESSION["data_final"]));
        $this->sethora_inicio($this->FDados($_SESSION["hora_inicio"]));
        $this->sethora_final($this->FDados($_SESSION["hora_final"]));
        $this->setvagas_total($this->FDados($_SESSION["vagas_total"]));
        $this->setvagas_restantes($this->FDados($_SESSION["vagas_restantes"]));
        $this->setdescricao($this->FDados($_SESSION["descricao"]));
        $this->setchamada($this->FDados($_SESSION["chamada"]));
        $this->setpago_id_INT($this->FDados($_SESSION["pago_id_INT"]));
        $this->setvideo($this->FDados($_SESSION["video"]));
        $this->setstatus_id_INT($this->FDados($_SESSION["status_id_INT"]));
        $this->setlink($this->FDados($_SESSION["link"]));
        $this->setimg_mini($this->FDados($_SESSION["img_mini"]));
        $this->setimg_full($this->FDados($_SESSION["img_full"]));
    }

// ***************************************************************************************************
// M�TODO PARA RETIRAR OS VALORES DAS SESSOES
// ***************************************************************************************************

    function unsetsession() {
        unset($_SESSION["id"]);
        unset($_SESSION["usuario_id_INT"]);
        unset($_SESSION["categoria_id_INT"]);
        unset($_SESSION["titulo"]);
        unset($_SESSION["data_inicio"]);
        unset($_SESSION["data_final"]);
        unset($_SESSION["hora_inicio"]);
        unset($_SESSION["hora_final"]);
        unset($_SESSION["vagas_total"]);
        unset($_SESSION["vagas_restantes"]);
        unset($_SESSION["descricao"]);
        unset($_SESSION["chamada"]);
        unset($_SESSION["pago_id_INT"]);
        unset($_SESSION["video"]);
        unset($_SESSION["status_id_INT"]);
        unset($_SESSION["link"]);
        unset($_SESSION["img_mini"]);
        unset($_SESSION["img_full"]);
    }

// ***************************************************************************************************
// M�TODO PARA SETAR CAMPOS POR POST
// ***************************************************************************************************

    function setpost() {
        $this->setid($this->FDados($_POST["id"]));
        $this->setusuario_id_INT($this->FDados($_POST["usuario_id_INT"]));
        $this->setcategoria_id_INT($this->FDados($_POST["categoria_id_INT"]));
//        $this->setcategoria_id_INT(htmlentities($this->FDados($_POST["categoria_id_INT"]), ENT_COMPAT, "UTF-8"));
        $this->settitulo($this->FDados($_POST["titulo"]));
        $this->setdata_inicio($this->FDados($_POST["data_inicio"]));
        $this->setdata_final($this->FDados($_POST["data_final"]));
        $this->sethora_inicio($this->FDados($_POST["hora_inicio"]));
        $this->sethora_final($this->FDados($_POST["hora_final"]));
        $this->setvagas_total($this->FDados($_POST["vagas_total"]));
        $this->setvagas_restantes($this->FDados($_POST["vagas_restantes"]));
        $this->setdescricao($this->FDados($_POST["descricao"]));
        $this->setchamada($this->FDados($_POST["chamada"]));
        $this->setpago_id_INT($this->FDados($_POST["pago_id_INT"]));
        $this->setvideo($this->FDados($_POST["video"]));
        $this->setstatus_id_INT($this->FDados($_POST["status_id_INT"]));
        $this->setlink($this->FDados($this->montaURLNova($this->montaURL($_POST["titulo"]), $_POST["id"])));
        $this->setimg_mini($this->FDados($_POST["img_mini"]));
        $this->setimg_full($this->FDados($_POST["img_full"]));
//        $this->setcurso_valor($this->FDados($_POST["curso_valor"]));
    }
// ***************************************************************************************************
// M�TODO PARA SETAR CAMPOS POR GET
// ***************************************************************************************************

    function setget() {
        $this->setid($this->FDados($_GET["id"]));
        $this->setusuario_id_INT($this->FDados($_GET["usuario_id_INT"]));
        $this->setcategoria_id_INT($this->FDados($_GET["categoria_id_INT"]));
        $this->settitulo($this->FDados($_GET["titulo"]));
        $this->setdata_inicio($this->FDados($_GET["data_inicio"]));
        $this->setdata_final($this->FDados($_GET["data_final"]));
        $this->sethora_inicio($this->FDados($_GET["hora_inicio"]));
        $this->sethora_final($this->FDados($_GET["hora_final"]));
        $this->setvagas_total($this->FDados($_GET["vagas_total"]));
        $this->setvagas_restantes($this->FDados($_GET["vagas_restantes"]));
        $this->setdescricao($this->FDados($_GET["descricao"]));
        $this->setchamada($this->FDados($_GET["chamada"]));
        $this->setpago_id_INT($this->FDados($_GET["pago_id_INT"]));
        $this->setvideo($this->FDados($_GET["video"]));
        $this->setstatus_id_INT($this->FDados($_GET["status_id_INT"]));
        $this->setlink($this->FDados($_GET["link"]));
        $this->setimg_mini($this->FDados($_GET["img_mini"]));
        $this->setimg_full($this->FDados($_GET["img_full"]));
    }

// ***************************************************************************************************
// METODO QUE RETORNA ARRAY DE TODOS OS ID's EXISTENTES NA TABELA
// ***************************************************************************************************

    function todosIDs() {

        $sql = "SELECT id FROM eventos ORDER BY id ASC";

        $this->database->Query($sql);

        while ($row = $this->database->FetchObject()) {
            $i++;
            $retorno[$i] = $row->id;
        }

        return $retorno;
    }

    function todosIDsCategoria($categoria_id) {

        $sql = "SELECT id FROM eventos WHERE usuario_id_INT = '".$categoria_id."' ORDER BY id ASC";

        $this->database->Query($sql);

        while ($row = $this->database->FetchObject()) {
            $i++;
            $retorno[$i] = $row->id;
        }

        return $retorno;
    }

// ***************************************************************************************************
// METODO QUE ATUALIZA UM CAMPO
// ***************************************************************************************************

    function updatecampo($id, $campo, $valor, $tipo) {

        if ($tipo != "int" || $tipo != "float" || $tipo != "INT" || $tipo != "FLOAT" || $tipo != "Int" || $tipo != "Float") {
            $tipo = "'";
        }


        $sql = "UPDATE eventos SET $campo = $tipo$valor$tipo WHERE id = $id";

        $this->database->Query($sql);
    }

// ***************************************************************************************************
// FUNCAO PARA FORMATAR OS VALORES DE DATA E VALORES DECIMAIS PARA GRAVAR NO BANCO
// ***************************************************************************************************

    function dadosBanco() {


        if ($this->getvagas_total() == "") {

            $this->setvagas_total(0);
        }

        $datas = Fdados('periodo');
        $datas = explode(" - ", $datas);
        $de = $datas[0];
        $ate = $datas[1];
        
        $this->setdata_inicio($this->FDataBanco($de));
        $this->setdata_final($this->FDataBanco($ate));
        $this->setvagas_restantes($this->getvagas_total());

    }

// ***************************************************************************************************
// FUNCAO PARA FORMATAR OS VALORES DE DATA E VALORES DECIMAIS PARA EXIBI��O
// ***************************************************************************************************

    function dadosExibe() {

    }

// ***************************************************************************************************
// METODOS AUXILIARES PARA FORMATACAO DE DADOS
// ***************************************************************************************************

    function FFloatBanco($numero) {

        if ($numero == "")
            return "0.00";

        $retorno = str_replace(".", "", $numero);
        $retorno = str_replace(",", ".", $retorno);

        return $retorno;
    }

    function FFloatExibe($numero) {

        if ($numero == "")
            return "";

        return number_format($numero, 2, ",", ".");
    }

    function FDataBanco($valor) {

        if ($valor != "" && $valor != null) {
            $partes = explode("/", $valor);

            $retorno = "" . $partes[2] . "-" . $partes[1] . "-" . $partes[0];

            return $retorno;
        }
        else
            return "0000-00-00";
    }

    function FDataExibe($valor) {

        if ($valor == "")
            return "";

        $partes = explode("-", $valor);

        $retorno = substr($partes[2], 0, 2) . "/" . $partes[1] . "/" . $partes[0];

        return $retorno;
    }

    function FDataTimeBanco() {

        $retorno = date("Y-m-d H:i:s");

        return $retorno;
    }

    function FDataTimeExibe($valor) {

        if ($valor == "")
            return "";

        $retorno = substr($valor, 8, 2) . "/" . substr($valor, 5, 2) . "/" . substr($valor, 0, 4) . " " . substr($valor, 11, 8);

        return $retorno;
    }

    function FDados($dado) {

        $strDado = $dado;
        $strDado = htmlspecialchars($strDado);
        $strDado = html_entity_decode($strDado);
        if (!get_magic_quotes_gpc()) {
            $strDado = addslashes($strDado);
        }

        return $strDado;
    }


    // Monta a URL Amigável
    function montaURL($variavel) {
        $variavel = htmlentities(strtolower($variavel));
        $variavel = preg_replace("/&(.)(acute|cedil|circ|ring|tilde|uml);/", "$1", $variavel);
        $variavel = preg_replace("/([^a-z0-9]+)/", "-", html_entity_decode($variavel));
        $variavel = trim($variavel, "-");
        return $variavel;
    }

    //Verifica se já existe URL para poder Montar uma nova
    function montaURLNova($variavel, $idconteudo) {
        $query = mysql_query("select * from eventos where link  = '$variavel'");
        $num_rows = @mysql_num_rows($query);
        while ($dados = mysql_fetch_array($query)) {
            $id = $dados[0];
        }

        if ($idconteudo <> $id) {
            if ($num_rows == 1) {
                while ($num_rows <> $num_post) {
                    $novo = $variavel . "-" . $num_rows;

                    $sql2 = "select * from eventos where link = '$novo'";


                    $query2 = mysql_query($sql2);
                    $num_rows2 = @mysql_num_rows($query2);

                    if ($num_rows2 == 0) {
                        $variavel = $novo;
                        $num_post = $num_rows;
                    } else {
                        $num_rows++;
                    }
                }
            }
        }

        return $variavel;
    }


}

// CLASSE FIM
?>