<?php

// ***************************************************************************************************
//
//	DATA CRIACAO:		04.08.2016
//	CLASSE:				DAO_Presenca
//	TABELA MYSQL:		presenca
//	BANCO MYSQL:		bartsge
//
// ***************************************************************************************************
// ***************************************************************************************************
// DECLARA��O DA CLASSE
// ***************************************************************************************************

class DAO_Presenca { // CLASSE IN�CIO
// ***************************************************************************************************
// DECLARA��O DE ATRIBUTOS
// ***************************************************************************************************

    var $id;   // CHAVE PRIM�RIA - AUTO INCREMENTO
    var $participante_id_INT;   // (ATRIBUTO NORMAL)
    var $evento_id_INT;   // (ATRIBUTO NORMAL)
    var $status_id_INT;   // (ATRIBUTO NORMAL)
    var $datetime;   // (ATRIBUTO NORMAL)

    var $database; // INSTANCIA DA CLASSE DATABASE

// ***************************************************************************************************
// M�TODO CONSTRUTOR
// ***************************************************************************************************

    function DAO_Presenca() {

        $this->database = new Database();
    }

// ***************************************************************************************************
// M�TODOS - GETTER
// ***************************************************************************************************

    function getid() {

        return $this->id;
    }

    function getparticipante_id_INT() {

        return $this->participante_id_INT;
    }

    function getevento_id_INT() {

        return $this->evento_id_INT;
    }
    
    function getstatus_id_INT() {

        return $this->status_id_INT;
    }

    function getdatetime() {

        return $this->datetime;
    }

// ***************************************************************************************************
// M�TODOS - SETTER
// ***************************************************************************************************


    function setid($val) {

        $this->id = $val;
    }

    function setparticipante_id_INT($val) {

        $this->participante_id_INT = $val;
    }

    function setevento_id_INT($val) {

        $this->evento_id_INT = $val;
    }
    
    function setstatus_id_INT($val) {

        $this->status_id_INT = $val;
    }

    function setdatetime($val) {

        $this->datetime = $val;
    }


// ***************************************************************************************************
// M�TODO  - SELECT - RETORNA UMA LINHA DA TABELA
// ***************************************************************************************************

    function select($id) {

        $sql = "SELECT * FROM presenca WHERE id = '$id'";
        $result = $this->database->Query($sql);
        $row = $this->database->FetchObject();


        $this->id = $row->id;

        $this->participante_id_INT = $row->participante_id_INT;

        $this->evento_id_INT = $row->evento_id_INT;
        
        $this->status_id_INT = $row->status_id_INT;

        $this->datetime = $row->datetime;


    }

// ***************************************************************************************************
// M�TODO  - SELECT - RETORNA UMA LINHA DA TABELA
// ***************************************************************************************************

    function selectLink($link) {
		
		
        $sql = "SELECT * FROM presenca WHERE id = '$link'";
        $result = $this->database->Query($sql);
        $row = $this->database->FetchObject();


        $this->id = $row->id;
		
        $this->participante_id_INT = $row->participante_id_INT;
		
        $this->evento_id_INT = $row->evento_id_INT;
        
        $this->status_id_INT = $row->status_id_INT;

        $this->datetime = $row->datetime;

    }

// ***************************************************************************************************
// METODO QUE RETORNA A LINHA DO �LTIMO REGISTRO CRIADO NA TABELA
// ***************************************************************************************************

    function selectLastInsert() {
        $this->database->Query("select * from presenca where id = last_insert_id()");
        $row = $this->database->FetchObject();
        $this->select($row->id);
    }

// ***************************************************************************************************
// M�TODO  - SELECTBY - RETORNA LINHAS QUE ATENDEM A CONDI��O
// ***************************************************************************************************

    function selectby($field, $val, $tipo) {

        if ($tipo != "int" || $tipo != "float" || $tipo != "INT" || $tipo != "FLOAT" || $tipo != "Int" || $tipo != "Float") {
            $tipo = "'";
        }

        $field = $this->FDados($field);
        $val = $this->FDados($val);

        $sql = "SELECT * FROM presenca WHERE $field=$tipo$val$tipo";
        $this->database->Query($sql);
        return $this->database->FetchObject();
    }

// ***************************************************************************************************
// M�TODO  - SELECT APROXIMADO - RETORNA LINHAS QUE ATENDEM A CONDI��O
// ***************************************************************************************************

    function selectlike($field, $val) {

        $field = $this->FDados($field);
        $val = $this->FDados($val);

        $sql = "SELECT * FROM presenca WHERE $field LIKE '%$val%'";
        $this->database->Query($sql);
        return $this->database->FetchObject();
    }

// ***************************************************************************************************
// M�TODO - DELETE
// ***************************************************************************************************

    function delete($id) {

        $sql = "DELETE FROM presenca WHERE id = $id;";
        $result = $this->database->Query($sql);
    }
    
    public function rows($sql) {
		 $result = $this->database->Query($sql);
		 $quantidade = $this->database->rows; 
		 //echo $quantidade; break;
        //$result = $this->database->Query($sql);
        return $quantidade; 
           
    }

// ***************************************************************************************************
// M�TODO - INSERT
// ***************************************************************************************************

    function insert() {

        $this->id = ""; // Limpa a chave do auto incremento

        $sql = "INSERT INTO presenca (participante_id_INT, evento_id_INT, status_id_INT, datetime ) VALUES ( '$this->participante_id_INT', '$this->evento_id_INT', '$this->status_id_INT','$this->datetime')";
        $result = $this->database->Query($sql);
    }

// ***************************************************************************************************
// M�TODO - UPDATE
// ***************************************************************************************************

    function update($id, $tipo) {

        if (isset($tipo["id"])) {
            $upd.= "id = $this->id, ";
        }
        if (isset($tipo["participante_id_INT"])) {
            $upd.= "participante_id_INT = $this->participante_id_INT, ";
        }
        if (isset($tipo["evento_id_INT"])) {
            $upd.= "evento_id_INT = '$this->evento_id_INT', ";
        }
        if (isset($tipo["status_id_INT"])) {
            $upd.= "status_id_INT = '$this->status_id_INT', ";
        }
        if (isset($tipo["datetime"])) {
            $upd.= "datetime = '$this->datetime', ";
        }

        $upd = substr($upd, 0, -2);


        $sql = " UPDATE presenca SET $upd WHERE id = $id ";

        $result = $this->database->Query($sql);
    }

// ***************************************************************************************************
// M�TODO PARA CRIAR VARIAVEIS DE SESSAO
// ***************************************************************************************************

    function createsession() {
        $_SESSION["id"] = $this->getid();
        $_SESSION["participante_id_INT"] = $this->getparticipante_id_INT();
        $_SESSION["evento_id_INT"] = $this->getevento_id_INT();
        $_SESSION["status_id_INT"] = $this->getstatus_id_INT();
        $_SESSION["datetime"] = $this->getdatetime();

    }

// ***************************************************************************************************
// M�TODO PARA SETAR CAMPOS POR SESSAO
// ***************************************************************************************************

    function setsession() {
        $this->setid($this->FDados($_SESSION["id"]));
        $this->setparticipante_id_INT($this->FDados($_SESSION["participante_id_INT"]));
        $this->setevento_id_INT($this->FDados($_SESSION["evento_id_INT"]));
        $this->setstatus_id_INT($this->FDados($_SESSION["status_id_INT"]));
        $this->setdatetime($this->FDados($_SESSION["datetime"]));

    }

// ***************************************************************************************************
// M�TODO PARA RETIRAR OS VALORES DAS SESSOES
// ***************************************************************************************************

    function unsetsession() {
        unset($_SESSION["id"]);
        unset($_SESSION["participante_id_INT"]);
        unset($_SESSION["evento_id_INT"]);
        unset($_SESSION["status_id_INT"]);
        unset($_SESSION["datetime"]);

    }

// ***************************************************************************************************
// M�TODO PARA SETAR CAMPOS POR POST
// ***************************************************************************************************

    function setpost() {
        $this->setid($this->FDados($_POST["id"]));
        $this->setparticipante_id_INT($this->FDados($_POST["participante_id_INT"]));
        $this->setevento_id_INT($this->FDados($_POST["evento_id_INT"]));
        $this->setstatus_id_INT($this->FDados($_POST["status_id_INT"]));
        $this->setdatetime($this->FDados($_POST["datetime"]));

    }
// ***************************************************************************************************
// M�TODO PARA SETAR CAMPOS POR GET
// ***************************************************************************************************

    function setget() {
        $this->setid($this->FDados($_GET["id"]));
        $this->setparticipante_id_INT($this->FDados($_GET["participante_id_INT"]));
        $this->setevento_id_INT($this->FDados($_GET["evento_id_INT"]));
        $this->setstatus_id_INT($this->FDados($_GET["status_id_INT"]));
        $this->setdatetime($this->FDados($_GET["datetime"]));

    }

// ***************************************************************************************************
// METODO QUE RETORNA ARRAY DE TODOS OS ID's EXISTENTES NA TABELA
// ***************************************************************************************************

    function todosIDs() {

        $sql = "SELECT id FROM presenca";

        $this->database->Query($sql);

        while ($row = $this->database->FetchObject()) {
            $i++;
            $retorno[$i] = $row->id;
        }

        return $retorno;
    }

// ***************************************************************************************************
// METODO QUE ATUALIZA UM CAMPO
// ***************************************************************************************************

    function updatecampo($id, $campo, $valor, $tipo) {

        if ($tipo != "int" || $tipo != "float" || $tipo != "INT" || $tipo != "FLOAT" || $tipo != "Int" || $tipo != "Float") {
            $tipo = "'";
        }


        $sql = "UPDATE presenca SET $campo = $tipo$valor$tipo WHERE id = $id";

        $this->database->Query($sql);
    }

// ***************************************************************************************************
// FUNCAO PARA FORMATAR OS VALORES DE DATA E VALORES DECIMAIS PARA GRAVAR NO BANCO
// ***************************************************************************************************

    function dadosBanco() {

        if ($this->getparticipante_id_INT() == "") {

            $this->setparticipante_id_INT(0);
        }

        if ($this->getevento_id_INT() == "") {

            $this->setevento_id_INT(0);
        }

        if ($this->getdatetime() == "") {

            $this->setdatetime(0);
        }


    }

// ***************************************************************************************************
// FUNCAO PARA FORMATAR OS VALORES DE DATA E VALORES DECIMAIS PARA EXIBI��O
// ***************************************************************************************************

    function dadosExibe() {

    }

// ***************************************************************************************************
// METODOS AUXILIARES PARA FORMATACAO DE DADOS
// ***************************************************************************************************

    function FFloatBanco($numero) {

        if ($numero == "")
            return "0.00";

        $retorno = str_replace(".", "", $numero);
        $retorno = str_replace(",", ".", $retorno);

        return $retorno;
    }

    function FFloatExibe($numero) {

        if ($numero == "")
            return "";

        return number_format($numero, 2, ",", ".");
    }

    function FDataBanco($valor) {

        if ($valor != "" && $valor != null) {
            $partes = explode("/", $valor);

            $retorno = "" . $partes[2] . "-" . $partes[1] . "-" . $partes[0];

            return $retorno;
        }
        else
            return "0000-00-00";
    }

    function FDataExibe($valor) {

        if ($valor == "")
            return "";

        $partes = explode("-", $valor);

        $retorno = substr($partes[2], 0, 2) . "/" . $partes[1] . "/" . $partes[0];

        return $retorno;
    }

    function FDataTimeBanco() {

        $retorno = date("Y-m-d H:i:s");

        return $retorno;
    }

    function FDataTimeExibe($valor) {

        if ($valor == "")
            return "";

        $retorno = substr($valor, 8, 2) . "/" . substr($valor, 5, 2) . "/" . substr($valor, 0, 4) . " " . substr($valor, 11, 8);

        return $retorno;
    }

    function FDados($dado) {

        $strDado = $dado;
        $strDado = htmlspecialchars($strDado);
        $strDado = html_entity_decode($strDado);
        if (!get_magic_quotes_gpc()) {
            $strDado = addslashes($strDado);
        }

        return $strDado;
    }

}

// CLASSE FIM
?>