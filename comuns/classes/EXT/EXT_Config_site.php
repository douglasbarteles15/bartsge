<?php
class EXT_Config_site extends DAO_Config_site
{
	
	public function __construct()
	{
		parent::DAO_Config_site();
		
		$this->select(1);		
	}
	
	
	public function getHttpUrl()
	{
		
		if(!$this->geturl_site())
		{
			return false;
		}
		else
		{
			
			$url = $this->geturl_site();
			
			$url = (substr($url,0,7) == "http://")?$url:"http://".$url;
			
			$url = ( substr($url,(strlen($url) - 1),1) == "/")?$url:$url."/";
			
			return $url;
			
		}
	}
}
?>