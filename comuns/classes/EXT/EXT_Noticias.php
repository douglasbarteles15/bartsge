<?php

class EXT_Noticias extends DAO_Noticias {// Classe: In�cio
    const TYPE_NOTICIAS = 1;
    const TYPE_CAMPANHA = 2;
    const TYPE_VIDEO = 3;
    const TYPE_IMPRENSA = 4;
    const TYPE_ESPORTES = 5;
    const TYPE_LABORATORIO = 6;
    const TYPE_RESIDENCIA = 7;
    const TYPE_NOTICIAS_GERAIS = 8;
    private $crypt;
    private $helper;

    public function __construct() {
        parent::__construct();

        $this->crypt = new Crypt();
        $this->helper = new Helper();
    }

    public function setnoticias_video($val) {
        $youtube = new Youtube($val);
        $val = $youtube->getUrl();
        parent::setnoticias_video($val);
    }

    public function selectRandom() {
        $sql = "SELECT * FROM noticias order by noticias_id desc";
        $result = $this->database->Query($sql);
        $row = $this->database->FetchObject();

        if ($this->database->rows >= 1) {
            $this->select($row->noticias_id);
        }
    }

    public function selectNoticia($numero) {
        $sql = "SELECT * FROM noticias where noticias_tipo_INT = 1 and noticias_status_INT = 1  order by noticias_id desc limit $numero";

        $this->database->Query($sql);
?>
        <div  class="cicle-box conteudo-out" >
            <div id="noticias_cycle">
        <?php
        while ($row = $this->database->FetchObject()):
            $extNoticia = new EXT_Noticias();
            $extNoticia->select($row->noticias_id);
            ++$loop;

            $numero++;
            $class = "conteudo-out";

            $parametros = "style='margin-right:10px; border:4px solid #cccccc;' align='left' alt='destaque'";
            $media = $extNoticia->getFotoDestaque(200, 150, $parametros);
        ?>

            <div >
            <?php
            $link = "?page=noticias&noticias_id={$this->crypt->crypt64($row->noticias_id)}";
            ?>

            <a href="<?php echo $link; ?>" class="linkNoticias" target="_top" >
                <?php echo $media; ?>
            </a>


            <a href="<?php echo $link; ?>" class="linkNoticias" target="_top" >
                <span class="textos_vermelho">
                    <?php echo $this->FDataExibe($extNoticia->getnoticias_data_cadastro_DATETIME()); ?> -
                </span>
                <b>
                    <?php echo $extNoticia->getnoticias_titulo(); ?>
                </b>
                <br/>
                <?php echo $extNoticia->getnoticias_chamada_home(); ?>

                </a>

            </div><!--end div-->

        <?php endwhile; ?>

                </div><!-- id=noticias_cycle-->
            </div><!--class=cycle-box-->
            <div id="noticias_cycle_paginacao">
            </div>

            <script type="text/javascript">
                <!--
                // $j(document).ready( function(){
                destaques('noticias_cycle','noticias_cycle_paginacao',10000);
                //                    }
                //              )
                -->
            </script>

<?php
                }

                public function selectDicasCulturais($numero) {
                    $sql = "SELECT * FROM noticias where noticias_tipo_INT = " . self::TYPE_LABORATORIO . " and noticias_status_INT = 1 order by noticias_id desc limit $numero";

                    $this->database->Query($sql);

                    while ($row = $this->database->FetchObject()) {
                        ++$loop;
                        $tituloHome = $row->noticias_titulo;
                        echo"
				<b><a href=\"?page=noticias&amp;noticias_id=" . $this->crypt->crypt64($row->noticias_id) . "\" class=\"link\" target=\"_top\"  style='min-width:150px'/>{$tituloHome}</a></b>
				<br />
				<a href=\"?page=noticias&amp;noticias_id=" . $this->crypt->crypt64($row->noticias_id) . "&amp;noticias_tipo_INT=" . $this->crypt->crypt64($row->noticias_tipo_INT) . "\" class=\"link\">" . substr($row->noticias_conteudo, 0, 100) . "...</a>

				";

                        if ($loop < $this->database->rows) {
                            echo "<img src='site/img/separador.jpg' style='margin-bottom:20px; margin-top:20px;' />";
                        }
                    }
                }

                public function selectNoticiasGerais($numero) {
                    $sql = "SELECT * FROM noticias where noticias_tipo_INT = " . self::TYPE_NOTICIAS_GERAIS . " and noticias_status_INT = 1 order by noticias_id desc limit $numero";

                    $this->database->Query($sql);

                    while ($row = $this->database->FetchObject()) {
                        ++$loop;
                        $tituloHome = $row->noticias_titulo;
                        echo"
				<b><a href=\"?page=noticias&amp;noticias_id=" . $this->crypt->crypt64($row->noticias_id) . "\" class=\"link\" target=\"_top\" style='min-width:150px' />{$tituloHome}</a></b>
				<br />
				<a href=\"?page=noticias&amp;noticias_id=" . $this->crypt->crypt64($row->noticias_id) . "&amp;noticias_tipo_INT=" . $this->crypt->crypt64($row->noticias_tipo_INT) . "\" class=\"link\">" . $row->noticias_chamada_home . "&nbsp;</a>

				";

                        if ($loop < $this->database->rows) {
                            echo "<img src='site/img/separador.jpg' style='margin-bottom:20px; margin-top:20px;'>";
                        }
                    }
                }
                public function selectBlog($numero, $limite) {
                     $sql = "SELECT * FROM noticias where noticias_tipo_INT = " . $numero . " order by noticias_data desc limit $limite";
                     return $sql;

                }

                public function selectPublicacoes($numero) {
                    $sql = "SELECT * FROM noticias where noticias_tipo_INT = 4 and noticias_status_INT = 1 order by noticias_id desc limit $numero";

                    $this->database->Query($sql);

                    while ($row = $this->database->FetchObject()) {
                        ++$loop;
                        $tituloHome = $row->noticias_chamada_home ? $row->noticias_chamada_home : $row->noticias_titulo;
                        echo"
				<b><a href=\"?page=noticias&amp;noticias_id=" . $this->crypt->crypt64($row->noticias_id) . "\" class=\"link\" target=\"_top\" />{$tituloHome}</a></b>
				<br />
				<a href=\"?page=noticias&amp;noticias_id=" . $this->crypt->crypt64($row->noticias_id) . "&amp;noticias_tipo_INT=" . $this->crypt->crypt64($row->noticias_tipo_INT) . "\" class=\"link\">" . substr($row->noticias_conteudo, 0, 100) . "...</a>

				";

                        if ($loop < $this->database->rows) {
                            echo "<img src='site/img/separador.jpg' style='margin-bottom:20px; margin-top:20px;'>";
                        }
                    }
                }

                public function selectHome($numero) {
                    $sql = "SELECT * FROM noticias where noticias_tipo_INT = 1  and noticias_status_INT = 1 and noticias_destaque_INT = 1 order by noticias_id desc limit $numero";

                    $this->database->Query($sql);

                    while ($row = $this->database->FetchObject()) {
                        ++$loop;

                        if (strlen($row->noticias_img) > 4) {
                            $noticias_img = "<div class=\"box_foto_destaque\">
							   <img src='site/uploads/img_noticias/img_noticias/" . $row->noticias_img . "' width='200' height='150' alt='" . $row->noticias_titulo . "' />
							   </div>";
                        } else {
                            $noticias_img = "";
                        }

                        echo"





			<div class=\"box_destaque\">
			{$noticias_img}
				<b class='textos'>" . $row->noticias_titulo . "</b><br />
				<a href=\"?page=noticias&amp;noticias_id=" . $this->crypt->crypt64($row->noticias_id) . "&amp;noticias_tipo_INT=" . $this->crypt->crypt64($row->noticias_tipo_INT) . "\" class=\"link\">" . substr($row->noticias_conteudo, 0, 250) . "...</a>
				<br />
				<a href=\"?page=noticias&amp;noticias_id=" . $this->crypt->crypt64($row->noticias_id) . "&amp;noticias_tipo_INT=" . $this->crypt->crypt64($row->noticias_tipo_INT) . "\" class=\"linkTopo\">Leia mais</a>
			</div>
			";
                    }
                }

                public function selectArtigoHome($numero) {
                    $sql = "SELECT * FROM noticias where noticias_tipo_INT = 3  and noticias_status_INT = 1 order by noticias_id desc limit $numero";

                    $this->database->Query($sql);

                    while ($row = $this->database->FetchObject()) {
                        ++$loop;

                        if (strlen($row->noticias_img) > 4) {
                            $noticias_img = "<div class=\"box_foto_destaque\">
							   <img src='site/uploads/img_noticias/img_noticias/" . $row->noticias_img . "' width='200' height='150' alt='" . $row->noticias_titulo . "' />
							   </div>";
                        } else {
                            $noticias_img = "";
                        }

                        echo"





			
			{$noticias_img}

				<span class='fique_atento'>" . $row->noticias_titulo . "</span><br />
				" . substr($row->noticias_chamada_home, 0, 250) . "
				<br />
				<a href=\"?page=noticias&amp;noticias_id=" . $this->crypt->crypt64($row->noticias_id) . "&amp;noticias_tipo_INT=" . $this->crypt->crypt64($row->noticias_tipo_INT) . "\" class=\"linkTopo\">Leia mais</a>
			
			";
                    }
                }

                public function selectFiqueHome($numero) {
                    $sql = "SELECT * FROM noticias where noticias_tipo_INT = 4  and noticias_status_INT = 1 order by noticias_id desc limit $numero";

                    $this->database->Query($sql);

                    while ($row = $this->database->FetchObject()) {
                        ++$loop;

                        if (strlen($row->noticias_img) > 4) {
                            $noticias_img = "<div class=\"box_foto_destaque\">
							   <img src='site/uploads/img_noticias/img_noticias/" . $row->noticias_img . "' width='200' height='150' alt='" . $row->noticias_titulo . "' />
							   </div>";
                        } else {
                            $noticias_img = "";
                        }

                        echo"






			{$noticias_img}

				<span class='fique_atento'>" . $row->noticias_titulo . "</span><br />
				" . substr($row->noticias_chamada_home, 0, 250) . "
				<br />
				<a href=\"?page=noticias&amp;noticias_id=" . $this->crypt->crypt64($row->noticias_id) . "&amp;noticias_tipo_INT=" . $this->crypt->crypt64($row->noticias_tipo_INT) . "\" class=\"linkTopo\">Leia mais</a><br /><br />

			";
                    }
                }

                public function getimagem($altura, $largura) {
                    $path = "site/uploads/img_noticias/img_noticias/";

                    if (file_exists("site/uploads/img_noticias/img_noticias/" . $this->getnoticias_id() . ".jpg")) {
                        $imgURLpath = $path . $this->getnoticias_img();

                        return $this->img_resize($imgURLpath, $largura, $altura, "");
                    }
                }

                public function getImgTitulo() {
                    if (!$this->getnoticias_id()) {
                        return false;
                    }

                    switch ($this->getnoticias_tipo_INT()) {

                        case self::TYPE_NOTICIAS:
                            return "tit_noticias.jpg";
                            break;

                        case self::TYPE_AGENDA:
                            return "tit_pronunciamentos.jpg";
                            break;

                        case self::TYPE_VIDEO:
                            return "tit_artigos.jpg";
                            break;

                        case self::TYPE_BLOG:
                            return "tit_atento.png";
                            break;
                        case self::TYPE_ESPORTES:
                            return "tit_midia.jpg";
                            break;
                        case self::TYPE_LABORATORIO:
                            return "tit_dicas.jpg";
                            break;
                        case self::TYPE_RESIDENCIA:
                            return "tit_obras.jpg";
                            break;
                        case self::TYPE_NOTICIAS_GERAIS:
                            return "tit_noticias.jpg";
                            break;
                    }
                }

                public function selectLastByType($type) {
                    $this->database->Query("SELECT noticias_id FROM noticias " .
                            "WHERE noticias_tipo_INT = " . $type . " AND " .
                            "noticias_status_INT = 1 ORDER BY noticias_id " .
                            "DESC LIMIT 1");

                    return $this->select($this->database->FetchObject()->noticias_id);
                }

                public function selectUltimoRegistroData($type) {
                    $this->database->Query("SELECT noticias_id FROM noticias where noticias_tipo_INT = " . $type . " ORDER BY noticias_data desc LIMIT 1");
                    return $this->select($this->database->FetchObject()->noticias_id);
                }

                public function getUltimasNoticias($numero, $echo=true) {
                    $sql = "SELECT * FROM noticias where noticias_tipo_INT = " . $this->getnoticias_tipo_INT() . " AND " . "noticias_id <> " . $this->getnoticias_id() . " and noticias_status_INT = 1 order by noticias_id desc limit $numero";

                    $this->database->Query($sql);

                    if ($echo) {
                        while ($row = $this->database->FetchObject()) {

                            $numero++;
                            $linhaTr = ($numero % 2 == 1) ? "<tr class=\"linhaout1\" onmouseover=\"this.className='linhaover';\" onmouseout=\"this.className='linhaout1';\">" : "<tr class=\"linhaout2\" onmouseover=\"this.className='linhaover';\" onmouseout=\"this.className='linhaout2';\">";

                            echo"
				$linhaTr
					<td align='center' width='100'>
						<span class='textos_vermelho' style='padding-left:10px;'>" . $this->helper->exibeDataSemHora($row->noticias_data_cadastro_DATETIME) . " </span></a>
					</td>
					<td align='left' width='310'>
						<a href=\"?page=noticias&amp;noticias_id=" . $this->crypt->crypt64($row->noticias_id) . "&amp;noticias_tipo_INT=" . $this->crypt->crypt64($row->noticias_tipo_INT) . "\" class=\"link_noticias\"> " . $row->noticias_titulo . "</a>
					</td>
				</tr>
				";
                        }
                    }

                    return $this->database->rows;
                }

                public function tituloNoticias() {
                    if (!$this->getnoticias_id()) {
                        return false;
                    }

                    switch ($this->getnoticias_tipo_INT()) {
                        case self::TYPE_NOTICIAS:
                            return "Not�cias";
                            break;

                        case self::TYPE_AGENDA:
                            return "Pronunciamentos";
                            break;

                        case self::TYPE_VIDEO:
                            return "Artigos";
                            break;

                        case self::TYPE_BLOG:
                            return "Fique Atento";
                            break;
                        case self::TYPE_ESPORTES:
                            return "Mandato na M�dia";
                            break;
                        case self::TYPE_LABORATORIO:
                            return "Dicas Culturais";
                            break;
                        case self::TYPE_RESIDENCIA:
                            return "Obras";
                            break;
                        case self::TYPE_NOTICIAS_GERAIS:
                            return "Not�cias Gerais";
                            break;
                    }
                }

                public function showGaleriaFotos2() {
                    $objGaleria = new Galeria("site/uploads/img_noticias/", $this->getnoticias_id(), "noticias_fotos");

                    $img = $objGaleria->getFotoDestaque(150, 120);

                    return "<div id='div_img_galeria'>

					<a href='site/php/noticias_fotos_galeria.php?id=" . $this->getnoticias_id() . "' rel=\"gb_page_center[680,450]\">
		            	<img src='$img' align='left' wight='150' height='120' style='margin:0px; border:4px solid #DDD2B4;'/>
					</a>

					<div style='height:27px; margin-top:128px; _margin-left:3px; width:168px; background-color:#DDD2B4;'>
						<a href='site/php/noticias_fotos_galeria.php?id=" . $this->getnoticias_id() . "' rel=\"gb_page_center[680,450]\" class='link'>

							<img src='site/img/lupa.jpg' border='0' style='margin-left:5px;' align='absmiddle'/> Ver galeria de fotos

						</a>
					</div>

				</div>
				";
                }

                public function getFotoDestaque($width, $height, $parametros = null) {
                    $galeria = $this->hasGaleriaFotos();
                    $video = $this->getnoticias_video();



                    if ($video) {
                        $youtube = new Youtube($video);
                        $youtube->setWidth($width);
                        $youtube->setHeight($height);
                        return $youtube->getImgMiniatura($parametros);
                    } else if ($galeria) {
                        $objGaleria = new Galeria("site/uploads/img_noticias/", $this->getnoticias_id(), "noticias_fotos");
                        $img = $objGaleria->getFotoDestaque($width, $height);
                        return "<img src='$img' $parametros  width='$width' height='$height'/>";
                    }
                }

                public function showGaleriaFotos() {
                    $objGaleria = new Galeria("site/uploads/img_noticias/", $this->getnoticias_id(), "");

                    $img = $objGaleria->getFotoDestaque(150, 120);

                    return "
							<a href='site/php/noticias_fotos_galeria.php?id=" . $this->getnoticias_id() . "' rel=\"gb_page_center[680,450]\">
								<img src='$img' align='left' style='margin:0px; margin-right:10px; margin-bottom:10px; border:4px solid #cccccc;'/>
							</a>
				";
                }

                public function hasGaleriaFotos() {
                    $this->database->Query("SELECT COUNT(*) AS qtd FROM noticias_fotos " .
                            "WHERE noticias_fotos_status_INT = 1 AND " .
                            "noticias_id_INT = " . $this->getnoticias_id());
                    return $this->database->FetchObject()->qtd;
                }

                static function getPaginaGerenciamento($tipo = self::TYPE_NOTICIAS) {
                    switch ($tipo) {
                        case self::TYPE_NOTICIAS:
                            return "gerenciar_noticia";
                            break;

                        case self::TYPE_AGENDA:
                            return "gerenciar_pronunciamentos";
                            break;

                        case self::TYPE_VIDEO:
                            return "gerenciar_artigos";
                            break;

                        case self::TYPE_BLOG:
                            return "gerenciar_publicacoes";
                            break;
                        case self::TYPE_ESPORTES:
                            return "gerenciar_mandato_midia";
                            break;
                        case self::TYPE_LABORATORIO:
                            return "gerenciar_dicas_culturais";
                            break;
                        case self::TYPE_RESIDENCIA:
                            return "gerenciar_obras";
                            break;
                        case self::TYPE_NOTICIAS_GERAIS:
                            return "gerenciar_noticias_gerais";
                            break;
                    }
                }

                public function getFotos() {
                    if (!$this->getnoticias_id()) {
                        return false;
                    } else {

//			if($this->getnoticias_id() == 2)
                        if ($this->getnoticias_id() != "") {

                            $sql = "select * from noticias_fotos where noticias_id_INT = {$this->getnoticias_id()} and noticias_fotos_status_INT = 1 order by noticias_fotos_id ";

                            $this->database->Query($sql);

                            if ($this->database->rows >= 1) {
                                while ($row = $this->database->FetchObject()) {

                                    $tooltip = ($row->noticias_fotos_comentario != "") ? "onMouseOver=\"tip('','<div align=justify>{$row->noticias_fotos_comentario}</div>',this);\" onMouseOut=\"notip();\"" : "";
                                    $fotos.="
						<div style=\"float:left; margin:5px;\">
							<a href=\"site/uploads/img_noticias/{$row->noticias_img_mini}\" rel='gb_imageset[nice_pics]' >
								<img src=\"./site/uploads/img_noticias/{$row->noticias_img_mini}\" {$tooltip} style=' border: 1px solid #BA2A0C;'/><br/>
							</a>
						</div>";
                                }
                                echo "<br/><br/>";
                                echo $fotos;
                            }
                        } else {
                            return false;
                        }
                    }
                }

                //override do metodo
                public function getnoticias_img() {
                    $this->database->Query("SELECT noticias_fotos_arquivo  FROM noticias_fotos " .
                            "WHERE noticias_fotos_status_INT = 1 AND " .
                            "noticias_id_INT = '{$this->getnoticias_id()}' LIMIT 1");
                    return $this->database->FetchObject()->noticias_fotos_arquivo;
                }



                 public function selectUltimoRegistroId($tipo) {
                    $this->database->Query("SELECT noticias_id FROM noticias where noticias_tipo_INT = '$tipo' ORDER BY noticias_id desc LIMIT 1");
                    return $this->select($this->database->FetchObject()->noticias_id);
                }

                 public function selectRandon($tipo) {
                    $this->database->Query("SELECT noticias_id FROM noticias where noticias_tipo_INT = '$tipo' ORDER BY RAND() LIMIT 1");
                    return $this->select($this->database->FetchObject()->noticias_id);
                }


            }

            // Class : Fim
?>