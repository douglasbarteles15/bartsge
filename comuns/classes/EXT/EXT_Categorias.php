<?

class EXT_Categorias extends DAO_Categorias {

    private $crypt;

    public function __construct() {
        parent:: __construct();

        $this->crypt = new Crypt();
    }

    //REESCREVE O TITULO E TRANSFORMA NO LINK SEM CHARACTERES ESPECIAIS
    function montaURL($variavel) {
        $variavel = htmlentities(strtolower($variavel));
        $variavel = preg_replace("/&(.)(acute|cedil|circ|ring|tilde|uml);/", "$1", $variavel);
        $variavel = preg_replace("/([^a-z0-9]+)/", "-", html_entity_decode($variavel));
        $variavel = trim($variavel, "-");
        return $variavel;
    }

    public function createCategoriasPai() {

        if ($categoria_id != "") {
            $sql = "select * from categoria_curso where categoria_pai = '0'";

            $this->database->Query($sql);

            while ($row = $this->database->FetchObject()) {
                echo "<option value='$row->categoria_id'>$row->categoria_titulo</option>";
            }
        }

        echo "<option value='' disabled >SELECIONE UMA CATEGORIA</option>";


        $sql = "select * from categoria_curso order by categoria_titulo";


        $this->database->Query($sql);

        while ($row = $this->database->FetchObject()) {
            echo "<option value='$row->categoria_id'>$row->categoria_titulo</option>";
        }
    }


    public function exibeSubCategoriaMenu($idPai, $linkVo, $linkPai) {

        $sql = "select * from categoria_curso where categoria_pai = '$idPai' order by categoria_titulo";
        $this->database->Query($sql);
        $i = 1;
        while ($row = $this->database->FetchObject()) {
            //echo utf8_encode("<p>$row->categoria_titulo</p>");
            echo utf8_encode("<p><a href='$linkVo/$linkPai/$row->categoria_link' title='$row->categoria_titulo' >$row->categoria_titulo</a></p>");
            echo "<hr class='hr_menu'>";
        }
    }

    public function exibeSubCategoria($idPai) {

        $sql = "select * from categoria_curso where categoria_pai = '$idPai' order by categoria_titulo";
        $this->database->Query($sql);
        while ($row = $this->database->FetchObject()) {
            echo "<a href='$row->categoria_link' title='$row->categoria_titulo' name='$row->categoria_titulo'  >$row->categoria_titulo</a>";
        }
    }

    public function exibeSubCategoriaQuantidade($idPai) {

        $sql = "select * from categoria_curso where categoria_pai = '$idPai' order by categoria_titulo";
        $quantidade = mysql_num_rows($this->database->Query($sql));
        return $quantidade;
    }

    public function existeCategoria($url) {

        $sql = "select * from categoria_curso where categoria_link = '$url' order by categoria_titulo";
       $quantidade = mysql_num_rows($this->database->Query($sql));
        return $quantidade;
    }

}
?>
