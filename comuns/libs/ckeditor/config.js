/**
 * @license Copyright (c) 2003-2015, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.md or http://ckeditor.com/license
 */

CKEDITOR.editorConfig = function( config )
{
	config.filebrowserBrowseUrl = 'http://localhost/jesuitas/comuns/libs/ckeditor/ckfinder/ckfinder.html',
        config.filebrowserImageBrowseUrl = 'http://localhost/jesuitas/comuns/libs/ckeditor/ckfinder/ckfinder.html?typ­e=Images',
        config.filebrowserFlashBrowseUrl = 'http://localhost/jesuitas/comuns/libs/ckeditor/ckfinder/ckfinder.html?typ­e=Flash',
        config.filebrowserUploadUrl = 'http://localhost/jesuitas/comuns/libs/ckeditor/ckfinder/core/connector/ph­p/connector.php?command=QuickUpload&­type=Files',
        config.filebrowserImageUploadUrl = 'http://localhost/jesuitas/comuns/libs/ckeditor/ckfinder/core/connector/ph­p/connector.php?command=QuickUpload&­type=Images',
        config.filebrowserFlashUploadUrl = 'http://localhost/jesuitas/comuns/libs/ckeditor/ckfinder/core/connector/ph­p/connector.php?command=QuickUpload&­type=Flash'
};