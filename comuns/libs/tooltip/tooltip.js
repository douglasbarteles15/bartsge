var offsetxpoint=-60 //Customize x offset of tooltip
var offsetypoint= 20 //Customize y offset of tooltip
var ie=document.all
var ns6=document.getElementById && !document.all
var enabletip = false
if (ie||ns6)
    var tipobj=document.all? document.all["ToolTip"] : document.getElementById? document.getElementById("ToolTip") : ""

function ietruebody()
{
    return (document.compatMode && document.compatMode!="BackCompat")? document.documentElement : document.body
}


topColor = "#001429"
subColor = "#EFF7DF"

function tip(TTitle , TContent){
    if (ns6||ie){

        thetext = 
		'<table border="0" width="100%" cellspacing="0" cellpadding="0">'+
        '<tr><td width="100%" bgcolor="#AAC472">'+
        '<table border="0" width="100%" cellspacing="3" cellpadding="0">'+
        '<tr><td width="100%" bgcolor='+subColor+' style="padding-top:10px; padding-bottom:10px;">'+
        '<table border="0" width="95%" cellpadding="0" cellspacing="1" align="center">'+
        '<tr><td width="100%">'+
        '<font class="tooltipcontent">'+TContent+'</font>'+
        '</td></tr>'+
        '</table>'+
        '</td></tr>'+
        '</table>'+
        '</td></tr>'+
        '</table>';

        tipobj.innerHTML=thetext
        enabletip=true
        return false
    }
}

function positiontip(e){
    if (enabletip){
        var curX=(ns6)?e.pageX : event.x+ietruebody().scrollLeft;
        var curY=(ns6)?e.pageY : event.y+ietruebody().scrollTop;
        //Descobre o quao longe esta o mouse da borda da janela
        var rightedge=ie&&!window.opera? ietruebody().clientWidth-event.clientX-offsetxpoint : window.innerWidth-e.clientX-offsetxpoint-20
        var bottomedge=ie&&!window.opera? ietruebody().clientHeight-event.clientY-offsetypoint : window.innerHeight-e.clientY-offsetypoint-20

        var leftedge=(offsetxpoint<0)? offsetxpoint*(-1) : -1000

        //checa se o espaco horizontal basta
        if (rightedge<tipobj.offsetWidth)
        //move a posicao horizontal do menu para esquerda baseado na largura
            tipobj.style.left=ie? ietruebody().scrollLeft+event.clientX-tipobj.offsetWidth+"px" : window.pageXOffset+e.clientX-tipobj.offsetWidth+"px"
        else if (curX<leftedge)
            tipobj.style.left="5px"
        else
        //posiciona o menu onde o mause esta marcndo
        tipobj.style.left=curX+offsetxpoint+"px"

        //idem verticalmente
        if (bottomedge<tipobj.offsetHeight)
            tipobj.style.top=ie? ietruebody().scrollTop+event.clientY-tipobj.offsetHeight-offsetypoint+"px" : window.pageYOffset+e.clientY-tipobj.offsetHeight-offsetypoint+"px"
        else
            tipobj.style.top=curY+offsetypoint+"px"
        tipobj.style.visibility="visible"
    }
   //***carrinho***
   // coord(e);
   //***carrinho***
}

function notip(){
    if (ns6||ie){
        enabletip=false
        tipobj.style.visibility="hidden"
        tipobj.style.left="-1000px"
        tipobj.style.backgroundColor=''
        tipobj.style.width=''
    }
}

document.onmousemove=positiontip
