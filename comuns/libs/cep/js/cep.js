	function objXMLHttpCep()
	{
		var ajax = false;

		if(window.XMLHttpRequest)
		{ // Mozilla, Safari...
			var objetoXMLHttp = new XMLHttpRequest();
			return objetoXMLHttp;
		}
		else if(window.ActiveXObject)
		{ // IE
			var versoes =
				[
					"MSXML2.XMLHttp.6.0",
					"MSXML2.XMLHttp.5.0",
					"MSXML2.XMLHttp.4.0",
					"MSXML2.XMLHttp.3.0",
					"MSXML2.XMLHttp",
					"Microsoft.XMLHttp"
				];

			for(var i = 0; i < versoes.length; i++)
			{
				try
				{
					var objetoXMLHttp = new ActiveXObject(versoes[i]);
					return objetoXMLHttp;
				}
				catch(ex)
				{
					//nada
				}
			}
		}
		return false;
	}

	function getDadosCep(cep)
	{

		FCep = cep.replace("-","");

		var pagina = "comuns/libs/cep/php/cep.php?cep="+FCep;

		var ajax = objXMLHttpCep();

		if(ajax)
		{
			ajax.open("GET", pagina, true);
			ajax.onreadystatechange = function()
			{
				//enquanto estiver processando...emite a msg de carregando
				if(ajax.readyState == 1)
				{

					document.getElementById("clientes_cep").value = cep+" - Pesquisando Endere�o";
					document.getElementById("clientes_endereco").value = "Aguarde...";
					document.getElementById("clientes_bairro").value = "Aguarde...";
					document.getElementById("clientes_cidade").value = "Aguarde...";
					document.getElementById("clientes_estado").value = "Aguarde...";

				}
				if(ajax.readyState == 4)
				{
					if(ajax.status == 200)
					{
						document.getElementById("carregando").style.display = "none";
						autoCompleteCep(cep,ajax.responseText);
					}
					else
					{
						alert("Houve um problema ao carregar");
						document.getElementById('carregando').style.display = "none";
					}
				}
			}
		}

		ajax.send(null);

	}

	function autoCompleteCep(cep,arrCep)
	{
		arrCep = arrCep.split(",");
                
		if(arrCep[1] == "" && arrCep[2] == "" && arrCep[3] == "" && arrCep[4] == "")
		{
			alert("ENDERE�O N�O ENCONTRADO INFORME UM CEP V�LIDO");
		}

		document.getElementById("clientes_cep").value = cep;
		document.getElementById("clientes_endereco").value = arrCep[0].toUpperCase()+" "+arrCep[1].toUpperCase();
		document.getElementById("clientes_bairro").value = arrCep[2].toUpperCase();
		document.getElementById("clientes_cidade").value = arrCep[3].toUpperCase();
		document.getElementById("clientes_estado").value = arrCep[4].toUpperCase();

	}

	function autoPreencherCep()
	{

		var cep;

		cep = document.getElementById('clientes_cep').value;

		if(cep.length == 9)
		{
			getDadosCep(cep);
		}
		else
		{
			alert("INFORME UM CEP V�LIDO");
			document.getElementById('clientes_cep').focus();
		}
	}





// CEP ENTREGA LIGHT BOX //




	function getDadosCep2(cep)
	{

		FCep = cep.replace("-","");

		var pagina = "comuns/libs/cep/php/cep.php?cep="+FCep;

		var ajax = objXMLHttpCep();

		if(ajax)
		{
			ajax.open("GET", pagina, true);
			ajax.onreadystatechange = function()
			{
				//enquanto estiver processando...emite a msg de carregando
				if(ajax.readyState == 1)
				{

					document.getElementById("clientes_cep_entrega").value = cep+" - Pesquisando Endere�o";
					document.getElementById("clientes_endereco_entrega").value = "Aguarde...";
					document.getElementById("clientes_bairro_entrega").value = "Aguarde...";
					document.getElementById("clientes_cidade_entrega").value = "Aguarde...";
					document.getElementById("clientes_estado_entrega").value = "Aguarde...";

				}
				if(ajax.readyState == 4)
				{
					if(ajax.status == 200)
					{
						document.getElementById("carregando").style.display = "none";
						autoCompleteCep2(cep,ajax.responseText);
					}
					else
					{
						alert("Houve um problema ao carregar");
						document.getElementById('carregando').style.display = "none";
					}
				}
			}
		}

		ajax.send(null);

	}

	function autoCompleteCep2(cep,arrCep)
	{
		arrCep = arrCep.split(",");

		if(arrCep[5] == 0)
		{
			alert("ENDERE�O N�O ENCONTRADO INFORME UM CEP V�LIDO");
		}

		document.getElementById("clientes_cep_entrega").value = cep;
		document.getElementById("clientes_endereco_entrega").value = arrCep[0].toUpperCase()+" "+arrCep[1].toUpperCase();
		document.getElementById("clientes_bairro_entrega").value = arrCep[2].toUpperCase();
		document.getElementById("clientes_cidade_entrega").value = arrCep[3].toUpperCase();
		document.getElementById("clientes_estado_entrega").value = arrCep[4].toUpperCase();

	}

	function autoPreencherCep2()
	{

		var cep;

		cep = document.getElementById('clientes_cep_entrega').value;

		if(cep.length == 9)
		{
			getDadosCep2(cep);
		}
		else
		{
			alert("INFORME UM CEP V�LIDO");
			document.getElementById('clientes_cep_entrega').focus();
		}
	}




	// CEP ENTREGA ADM //




	function getDadosCep3(cep)
	{

		FCep = cep.replace("-","");

		var pagina = "../../comuns/libs/cep/php/cep.php?cep="+FCep;

		var ajax = objXMLHttpCep();

		if(ajax)
		{
			ajax.open("GET", pagina, true);
			ajax.onreadystatechange = function()
			{
				//enquanto estiver processando...emite a msg de carregando
				if(ajax.readyState == 1)
				{

					document.getElementById("representantes_cep").value = cep+" - Pesquisando Endere�o";
					document.getElementById("representantes_endereco").value = "Aguarde...";
					document.getElementById("representantes_bairro").value = "Aguarde...";
					document.getElementById("representantes_cidade").value = "Aguarde...";
					document.getElementById("representantes_estado").value = "Aguarde...";

				}
				if(ajax.readyState == 4)
				{
					if(ajax.status == 200)
					{
						autoCompleteCep3(cep,ajax.responseText);
					}
					else
					{
						alert("Houve um problema ao carregar");
					}
				}
			}
		}

		ajax.send(null);

	}

	function autoCompleteCep3(cep,arrCep)
	{
		arrCep = arrCep.split(",");

		if(arrCep[5] == 0)
		{
			alert("ENDERE�O N�O ENCONTRADO INFORME UM CEP V�LIDO");
		}

		document.getElementById("representantes_cep").value = cep;
		document.getElementById("representantes_endereco").value = arrCep[0].toUpperCase()+" "+arrCep[1].toUpperCase();
		document.getElementById("representantes_bairro").value = arrCep[2].toUpperCase();
		document.getElementById("representantes_cidade").value = arrCep[3].toUpperCase();
		document.getElementById("representantes_estado").value = arrCep[4].toUpperCase();

	}

	function autoPreencherCep3()
	{

		var cep;

		cep = document.getElementById('representantes_cep').value;

		if(cep.length == 9)
		{
			getDadosCep3(cep);
		}
		else
		{
			alert("INFORME UM CEP V�LIDO");
			document.getElementById('representantes_cep').focus();
		}
	}