<?php

$msgOk = ($h->fDados("msgOk") != "") ? $h->fDados("msgOk") : $msgOk;
if ($msgOk != "") {
    echo "
    <div class='alert alert-success alert-dismissable'>
            <i class='fa fa-ban'></i>
            <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>
            " . $msgOk . "
        </div>";

    "<div class='divOk' id='aviso'><table width='100%' border='0' align='center' cellpadding='0' cellspacing='0'><tr><td class='imgOk'>&nbsp;</td><td class='conteudosDiv'>" . $msgOk . "</td></tr></table></div>";
}
unset($msgOk);

$msgAlert = ($h->fDados("msgAlert") != "") ? $h->fDados("msgAlert") : $msgAlert;
if ($msgAlert != "") {
    echo "
    <div class='alert alert-warning alert-dismissable'>
            <i class='fa fa-ban'></i>
            <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>
            " . $msgAlert . "
        </div>";
    "<div class='divAlert' id='aviso'><table width='100%' border='0' align='center' cellpadding='0' cellspacing='0'><tr><td class='imgAlert'>&nbsp;</td><td class='conteudosDiv'>" . $msgAlert . "</td></tr></table></div>";
}
unset($msgAlert);

$msgErro = ($h->fDados("msgErro") != "") ? $h->fDados("msgErro") : $msgErro;
if ($msgErro != "") {
    echo "
        <div class='alert alert-danger alert-dismissable'>
            <i class='fa fa-ban'></i>
            <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>
            " . $msgErro . "
        </div>";
    "<div class='divErro' id='aviso'><table width='100%' border='0' align='center' cellpadding='0' cellspacing='0'><tr><td class='imgErro'>&nbsp;</td><td class='conteudosDiv'>" . $msgErro . "</td></tr></table></div>";
}
unset($msgErro);
?>
