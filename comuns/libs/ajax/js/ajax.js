	function htmlEntities(str,modo)//modo 1 = entities  - modo 2 = numeric
	{	
		var chars;
		var entities;
		var numeric;
		var strReturn;
		var strReplace;
		var strLoop;
		
		if(!str)
		{
			return "";
		}
		else
		{
			var chars = new Array ("\"","'","&","<",">"," ","�","�","�","�","�","�","�","�","�","�","�","�","�","�","�","�","�","�","�","�","�","�","�","�","�","�","�","�","�","�",
			"�","�","�","�","�","�","�","�","�","�","�","�","�","�","�","�","�","�","�","�","�","�","�","�","�","�","�","�","�","�","�","�","�","�","�","�",
			"�","�","�","�","�","�","�","�","�","�","�","�","�","�","�","�","�","�","�","�","�","�","�","�","�","�","�","�","�");
		
			var entities = new Array("&quot;","&apos;","&amp;","&lt;","&gt;","&nbsp;","&iexcl;","&cent;","&pound;","&curren;","&yen;","&brvbar;","&sect;",
			"&uml;","&copy;","&ordf;","&laquo;","&not;","&shy;","&reg;","&macr;","&deg;","&plusmn;","&sup2;","&sup3;","&acute;","&micro;","&para;","&middot;",
			"&cedil;","&sup1;","&ordm;","&raquo;","&frac14;","&frac12;","&frac34;","&iquest;","&times;","&divide;","&Agrave;","&Aacute;","&Acirc;","&Atilde;",
			"&Auml;","&Aring;","&AElig;","&Ccedil;","&Egrave;","&Eacute;","&Ecirc;","&Euml;","&Igrave;","&Iacute;","&Icirc;","&Iuml;","&ETH;","&Ntilde;","&Ograve;",
			"&Oacute;","&Ocirc;","&Otilde;","&Ouml;","&Oslash;","&Ugrave;","&Uacute;","&Ucirc;","&Uuml;","&Yacute;","&THORN;","&szlig;","&agrave;","&aacute;",
			"&acirc;","&atilde;","&auml;","&aring;","&aelig;","&ccedil;","&egrave;","&eacute;","&ecirc;","&euml;","&igrave;","&iacute;","&icirc;","&iuml;",
			"&eth;","&ntilde;","&ograve;","&oacute;","&ocirc;","&otilde;","&ouml;","&oslash;","&ugrave;","&uacute;","&ucirc;","&uuml;","&yacute;","&thorn;",
			"&yuml;");
		
			var numeric = new Array("&#34;","&#39;","&#38;","&#60;","&#62;","&#160;","&#161;","&#162;","&#163;","&#164;","&#165;","&#166;","&#167;","&#168;",
			"&#169;","&#170;","&#171;","&#172;","&#173;","&#174;","&#175;","&#176;","&#177;","&#178;","&#179;","&#180;","&#181;","&#182;","&#183;","&#184;",
			"&#185;","&#186;","&#187;","&#188;","&#189;","&#190;","&#191;","&#215;","&#247;","&#192;","&#193;","&#194;","&#195;","&#196;","&#197;","&#198;",
			"&#199;","&#200;","&#201;","&#202;","&#203;","&#204;","&#205;","&#206;","&#207;","&#208;","&#209;","&#210;","&#211;","&#212;","&#213;","&#214;",
			"&#216;","&#217;","&#218;","&#219;","&#220;","&#221;","&#222;","&#223;","&#224;","&#225;","&#226;","&#227;","&#228;","&#229;","&#230;","&#231;",
			"&#232;","&#233;","&#234;","&#235;","&#236;","&#237;","&#238;","&#239;","&#240;","&#241;","&#242;","&#243;","&#244;","&#245;","&#246;","&#248;",
			"&#249;","&#250;","&#251;","&#252;","&#253;","&#254;","&#255;");
			
			var modo = (modo == 1) ? entities : numeric;
			
			var strReturn = "";
			
			for(var i = 0; i <= str.length; i++)
			{
				var strLoop = "";
				var strReplace = str.substr(i,1);
				
				for(var e = 0; e <= chars.length; e++)
				{	
					if(strReplace == chars[e] && strReplace != " ")
					{
						strReturn+=modo[e];
						strLoop = 1;
						break;
					}
				}
				if(strLoop == "")
				{
					strReturn+=strReplace;
				}
				else if(strReplace == " ")
				{
					strReturn+=strReplace;
				}
			}
		
			return strReturn;
		}
	}

	function objXMLHttp()
	{
		var ajax = false;
		
		if(window.XMLHttpRequest)
		{ // Mozilla, Safari...
			var objetoXMLHttp = new XMLHttpRequest();
			return objetoXMLHttp;
		}
		else if(window.ActiveXObject)
		{ // IE
			var versoes = 
				[
					"MSXML2.XMLHttp.6.0",
					"MSXML2.XMLHttp.5.0",
					"MSXML2.XMLHttp.4.0",
					"MSXML2.XMLHttp.3.0",
					"MSXML2.XMLHttp",
					"Microsoft.XMLHttp"
				];
		
			for(var i = 0; i < versoes.length; i++)
			{
				try
				{
					var objetoXMLHttp = new ActiveXObject(versoes[i]);
					return objetoXMLHttp;
				}
				catch(ex)
				{
					//nada
				}
			}
		}
		return false;	
	}
	
	function msXMLDOMDoc()
	{
		 var versoes = 
		 [
		 	"MSXML2.DOMDocument.6.0",
		 	"MSXML2.DOMDocument.5.0",
		 	"MSXML2.DOMDocument.4.0",
		 	"MSXML2.DOMDocument.3.0",
		 	"MSXML2.DOMDocument",
		 	"Microsoft.XmlDom"
		 ];
		 
		 for (var i = 0; i < versoes.length; i++)
		 {
		 	try
		 	{
		 		var objXmlDom =  new ActiveXObject(versoes[i]);
		 		return objXmlDom;
		 	}
		 	catch(erro)
		 	{
		 		//nada
		 	}
		 	throw new Error("MSXML n�o est� instalado");
		 }
	}
	
	function objXML(xml)
	{
		if(window.ActiveXObject)
		{
			xmlDoc = msXMLDOMDoc();
			//xmlDoc.async = true;
		}
		else if(document.implementation && document.implementation.createDocument)
		{
			xmlDoc = document.implementation.createDocument("","",null);
		}
		
		xmlDoc.load(xml);
	}
	
	function getForm(objForm)
	{
		
		var params = new Array();
		var flagPost = true; 
		
		for(var i=0; i < objForm.elements.length; i++){
			var parametro;
			
            if (objForm.elements[i].value == "")
            {
                flagPost = false;
            }
			
			if (objForm.elements[i].type == "checkbox" || objForm.elements[i].type == "radio")
			{
				if(!objForm.elements[i].checked)
                    flagPost = false;
			}
			
			if(flagPost == true)
			{
				parametro = encodeURIComponent(objForm.elements[i].name);
                parametro += "=";
                parametro += encodeURIComponent(htmlEntities(objForm.elements[i].value,1));
                params.push(parametro);
			}
			flagPost = true;
		}
		
		return params.join("&");		
	}
	
	function enviar(action,formulario,retorno,reset)
	{
		
		var dados = getForm(formulario);
		var ajax = objXMLHttp();
		
		ajax.open("POST" , action , true);
		ajax.setRequestHeader("Content-Type","application/x-www-form-urlencoded charset=iso-8859-1");
				
		ajax.onreadystatechange = function()
		{
			//enquanto estiver processando... emite a msg de carregando
			if(ajax.readyState == 1)
			{
				//document.getElementById(retorno).innerHTML = "<div id='img_carregando'></div>";
				document.getElementById('carregando2').style.display = "block";
				//scroll(0,0);
			}
			if(ajax.readyState == 4)
			{
				if(ajax.status == 200)
				{
					document.getElementById('carregando2').style.display = "none";
					mensagem(retorno,ajax.responseText);
				}
				else
				{
					document.getElementById('carregando2').style.display = "none";
					mensagem(retorno,"Ocorreu o erro: "+ ajax.statusText);
				}
			}
		}
		ajax.send(dados);
		if(reset == true)
		{
		  formulario.reset();
		  return false;
	    }
		else
		{
			return false;
		}
		
	}
	
	function mensagem(id,msg)
	{
        document.getElementById(id).innerHTML = msg;
    }
	
	function getDados(id,pagina){
		
		if(!id)
		  id = "backgroundTabelas";
		  
		var ajax = objXMLHttp();
		
		if(ajax)
		{
			ajax.open("GET", pagina, true);
			ajax.onreadystatechange = function()
			{
				//enquanto estiver processando...emite a msg de carregando
				if(ajax.readyState == 1)
				{
					document.getElementById(id).innerHTML = "<div id='img_carregando'></div>";
					//document.getElementById('carregando').style.display = "block";
					//scroll(0,0);
				}
				if(ajax.readyState == 4)
				{
					alert(ajax.status);
					if(ajax.status == 200)
					{
						//document.getElementById('carregando').style.display = "none";
						document.getElementById(id).innerHTML = ajax.responseText;												
					}
					else
					{
						alert("Houve um problema ao carregar");
						document.getElementById('carregando').style.display = "none";
					}
				}
			}
		}
		ajax.send(null);
	}
	
	function capturaVisa(idPedido, statusPedido){

		if(statusPedido == 3){
		
			var ajax = objXMLHttp();
			
			if(ajax){
				ajax.open("GET", "../../comuns/cartoes/visa/enviaCapturaVisanet.php?pedidos_id=" + idPedido, true);
				ajax.onreadystatechange = function(){}
			}

			ajax.send(null);
		}
		
	}
	
	function getDadosCarrinhoTopo(id,pagina){
				  
		var ajax = objXMLHttp();
		
		if(ajax){
			ajax.open("GET", pagina, true);
			ajax.onreadystatechange = function()
			{
				//enquanto estiver processando...emite a msg de carregando
				if(ajax.readyState == 1)
				{
					document.getElementById(id).innerHTML = "<div class='carrinho_topo'><div id='img_loader'></div></div>";
					//document.getElementById('carregando').style.display = "block";
					//scroll(0,0);
				}
				if(ajax.readyState == 4)
				{
					if(ajax.status == 200)
					{
						//document.getElementById('carregando').style.display = "none";
						document.getElementById(id).innerHTML = ajax.responseText;												
					}
					else
					{
						alert("Houve um problema ao carregar");
						document.getElementById('carregando').style.display = "none";
					}
				}
			}
		}
		ajax.send(null);
	}
	
	function getDadosIMG(id,pagina)
	{
		  
		var ajax = objXMLHttp();
		
		if(ajax)
		{
			ajax.open("GET", pagina, true);
			ajax.onreadystatechange = function()
			{
				//enquanto estiver processando...emite a msg de carregando
				if(ajax.readyState == 1)
				{
					document.getElementById(id).innerHTML = "<div style='width:280px; height:224px;'><div id='img_carregando'></div></div>";
					//document.getElementById('carregando').style.display = "block";
					//scroll(0,0);
				}
				if(ajax.readyState == 4)
				{
					if(ajax.status == 200)
					{
						//document.getElementById('carregando').style.display = "none";
						document.getElementById(id).innerHTML = ajax.responseText;												
					}
					else
					{
						alert("Houve um problema ao carregar");
						document.getElementById('carregando').style.display = "none";
					}
				}
			}
		}
		ajax.send(null);
	}
	
	function jumpMenuAjax(pagina,selectValue)
	{
		//<select onchange="jumpMenuAjax('ajax.php',this);">
	    var page = pagina+selectValue.options[selectValue.selectedIndex].value;
	    getDados('',page);
	    
    }
    
    function jumpMenuAjax2(pagina,retorno,param,selectValue)
    {
		//<select onchange="jumpMenuAjax2('ajax.php','div_retorno',param,this);">
	    var page = pagina+param+selectValue.options[selectValue.selectedIndex].value;
	    
	    var arrValue = page.split("=");
	    
	    if(!arrValue[1])
	    {
	    	return false;
	    }
	    else
	    {
	    	document.getElementById('horas_dia').innerHTML = "";
	    	getDados(retorno,page);
	    }
    }
	
	function excluirContentsAjax(id,url,MSG)
	{
	    if(confirm(MSG))
	        getDados(id,url);
	    else
	        return false;
    }
	
	function validaTodosAjax(action,nform,retorno,reset)
	{
	    for(var i = 0; i < nform.elements.length; i++)
	    {
	        if(nform.elements[i].value == "" && nform.elements[i].type != "hidden" && nform.elements[i].type != "checkbox")
	        {
	            alert("Preencha todos os campos");
	            nform.elements[i].focus();
	            return false;
	        }
	    }
		return enviar(action,nform,retorno,reset);   
    }
    
    function validaTodosExcessaoAjax(action,nform,retorno,reset,excessao)
    {
	    for(var i = 0; i < nform.elements.length; i++)
	    {
	        if(nform.elements[i].value == "" && nform.elements[i].type != "hidden" && nform.elements[i].type != "checkbox" && nform.elements[i].name != excessao)
	        {
	            alert("Preencha todos os campos");
	            nform.elements[i].focus();
	            return false;
	        }
	    }
		return enviar(action,nform,retorno,reset);   
    }
    
    //Lan�amento Hora
    function validaTodosLancamentoAjax(action,nform,retorno,reset)
    {
	    if(nform.horas.value == "" && nform.minutos.value == "")
	    {
	    	alert("Informe ou horas ou minutos");
            nform.minutos.focus();
            return false;
	    }
	    else
	    {
		    for(var i = 0; i < nform.elements.length; i++)
		    {
		        if(nform.elements[i].value == "" && nform.elements[i].type != "hidden" && nform.elements[i].type != "checkbox" && nform.elements[i].name != "lancamento_hora_observacoes" && nform.elements[i].name != "horas" && nform.elements[i].name != "minutos")
		        {
		            alert("Preencha todos os campos");
		            nform.elements[i].focus();
		            return false;
		        }
		    }
		}
		return enviar(action,nform,retorno,reset);   
    }
	
	function resetForm(nform)
	{
		if(!nform)
		  return false;
		else 
		  return nform.reset();
	}
	
	//AJAX DOS GRUPOS E SUB GRUPOS
	function jumpMenuAjaxSubGrupo(pagina,selectValue,retorno)
	{
		//<select onchange="jumpMenuAjaxSubGrupo('ajax.php',this,'retorno');">
		
		var strValue = selectValue.options[selectValue.selectedIndex].value;
	    
	    getDadosSubGrupo(retorno,pagina+"?grupos_id="+strValue,298,15);
	    
    }
    
    function getDadosSubGrupo(id,pagina,largura,altura)
	{
		  
		var ajax = objXMLHttp();
		
		if(ajax){
			ajax.open("GET", pagina, true);
			ajax.onreadystatechange = function()
			{
				//enquanto estiver processando...emite a msg de carregando
				if(ajax.readyState == 1)
				{
					document.getElementById(id).innerHTML = "<div id='img_carregando_input' style='width:"+largura+";height:"+altura+";'></div>";
				}
				if(ajax.readyState == 4)
				{
					if(ajax.status == 200)
					{
						document.getElementById(id).innerHTML = ajax.responseText;												
					}
					else
					{
						alert("Houve um problema ao carregar");
					}
				}
			}
		}
		ajax.send(null);
	}
	
	function ajaxExcluirFoto(id,pagina)
	{	  
		var ajax = objXMLHttp();
		
		if(ajax)
		{
			ajax.open("GET", pagina, true);
			ajax.onreadystatechange = function()
			{
				//enquanto estiver processando...emite a msg de carregando
				if(ajax.readyState == 1)
				{
					document.getElementById(id).innerHTML = "<div id='img_carregando_excluir_img'></div>";
					//document.getElementById('carregando').style.display = "block";
					//scroll(0,0);
				}
				if(ajax.readyState == 4)
				{
					if(ajax.status == 200)
					{
						//document.getElementById('carregando').style.display = "none";
						document.getElementById(id).innerHTML = ajax.responseText;												
					}
					else
					{
						alert("Houve um problema ao carregar");
						document.getElementById('carregando').style.display = "none";
					}
				}
			}
		}
		ajax.send(null);
	}
	
	//AJAX DOS GRUPOS E SUB GRUPOS
	function jumpMenuAjaxOptions(pagina,parametro,value,retorno)
	{
		//<select onchange="jumpMenuAjaxSubGrupo('ajax.php',this,'retorno');">
		
		//var strValue = selectValue.options[selectValue.selectedIndex].value;
		
		if(value == "")
		{
			return false;
		}
		else
		{
			var page = pagina+"?"+parametro+"="+value;
	    
	    	getDadosOptions(retorno,page,298,15);
		}
	    
    }
    
    function getDadosOptions(id,pagina,largura,altura)
	{
		  
		var ajax = objXMLHttp();
		
		if(ajax){
			ajax.open("GET", pagina, true);
			ajax.onreadystatechange = function()
			{
				//enquanto estiver processando...emite a msg de carregando
				if(ajax.readyState == 1)
				{
					document.getElementById(id).innerHTML = "<div id='img_carregando_input' style='width:"+largura+";height:"+altura+";'></div>";
				}
				if(ajax.readyState == 4)
				{
					if(ajax.status == 200)
					{
						//document.getElementById('carregando').style.display = "none";
						document.getElementById(id).innerHTML = ajax.responseText;												
					}
					else
					{
						alert("Houve um problema ao carregar");
						//document.getElementById('carregando').style.display = "none";
					}
				}
			}
		}
		ajax.send(null);
	}
	
	function getDadosFrete(id,pagina)
	{
		  
		var ajax = objXMLHttp();
		
		if(ajax){
			ajax.open("GET", pagina, true);
			ajax.onreadystatechange = function()
			{
				//enquanto estiver processando...emite a msg de carregando
				if(ajax.readyState == 1)
				{
					document.getElementById(id).innerHTML = "<div id='img_carregando_frete'>&nbsp;</div>";
				}
				if(ajax.readyState == 4)
				{
					if(ajax.status == 200)
					{
						document.getElementById(id).innerHTML = ajax.responseText;												
					}
					else
					{
						alert("Houve um problema ao carregar");
					}
				}
			}
		}
		ajax.send(null);
	}
	
	function setFormaEntregaPedido(fEntrega,nValorFrete,observacoes)
	{
		var pagina = "site/ajax/set_forma_entrega_pedido.php?pedidos_forma_entrega="+fEntrega+"&pedidos_frete_FLOAT="+nValorFrete+"&pedidos_observacoes="+observacoes;
		var ajax = objXMLHttp();
		
		if(ajax)
		{
			ajax.open("GET", pagina, true);
			ajax.onreadystatechange = function()
			{
				if(ajax.readyState == 1)
				{
					//document.getElementById("carregando").style.display = "block";
				}
				
				if(ajax.readyState == 4)
				{
					if(ajax.status == 200)
					{
						location.href = "?page=pagamento";
						//document.write(ajax.responseText);												
					}
					else
					{
						alert("HOUVE UM ERRO AO CARREGAR");
					}
				}
			}
		}
		ajax.send(null);
		
	}
	
	function atualizarEnderecoEntrega(action,formulario)
	{
		
		var dados = getForm(formulario);
		var ajax = objXMLHttp();
		
		ajax.open("POST" , action , true);
		ajax.setRequestHeader("Content-Type","application/x-www-form-urlencoded charset=iso-8859-1");
				
		ajax.onreadystatechange = function()
		{
			//enquanto estiver processando... emite a msg de carregando
			if(ajax.readyState == 1)
			{
				document.getElementById("carregando").style.display='block';
                                document.getElementById("exibe_endereco_entrega").innerHTML = "<div id=\"img_carregando_endereco_entrega\"></div>";
			}
			if(ajax.readyState == 4)
			{
				if(ajax.status == 200)
				{
					var arr = ajax.responseText;
					arr = arr.split("|");
					
					document.getElementById("exibe_endereco_entrega").innerHTML = arr[0];
					
					if(document.getElementById("exibe_sedex"))
					{
						document.getElementById("exibe_sedex").innerHTML = arr[1];
						document.getElementById("valor_sedex").value = arr[2];
					}
					
					if(document.getElementById("exibe_e_sedex"))
					{
						document.getElementById("exibe_e_sedex").innerHTML = arr[3];
						document.getElementById("valor_e_sedex").value = arr[4];
					}
					
					if(document.getElementById("exibe_transportadora"))
					{
						document.getElementById("exibe_transportadora").innerHTML = arr[5];
						document.getElementById("valor_transportadora").value = arr[6];
					}
					
					if(document.getElementById("exibe_motoboy"))
					{
						document.getElementById("exibe_motoboy").innerHTML = arr[7];
						document.getElementById("valor_motoboy").value = arr[8];
					}
					location.href = 'index.php?page=entrega';
				}
				else
				{
					mensagem("Ocorreu o erro: "+ ajax.statusText);
				}
			}
		}
		ajax.send(dados);
		
		return false;
		
	}
	
	function ajaxPaginacao(id,pagina)
	{
		if(!id)
		  id = "backgroundTabelas";
		  
		var ajax = objXMLHttp();
		
		if(ajax)
		{
			ajax.open("GET", pagina, true);
			ajax.onreadystatechange = function()
			{
				//enquanto estiver processando...emite a msg de carregando
				if(ajax.readyState == 1)
				{
					document.getElementById(id).innerHTML = "<div style='width:100%;height:100px;'><div id='carregando_paginacao'></div></div>";
				}
				if(ajax.readyState == 4)
				{
					if(ajax.status == 200)
					{
						document.getElementById(id).innerHTML = ajax.responseText;												
					}
					else
					{
						alert("Houve um problema ao carregar");
					}
				}
			}
		}
		ajax.send(null);
	}
	
