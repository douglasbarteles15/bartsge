function objXMLHttpFrete()
{
	var ajax = false;
	
	if(window.XMLHttpRequest)
	{ // Mozilla, Safari...
		var objetoXMLHttp = new XMLHttpRequest();
		return objetoXMLHttp;
	}
	else if(window.ActiveXObject)
	{ // IE
		var versoes = 
			[
				"MSXML2.XMLHttp.6.0",
				"MSXML2.XMLHttp.5.0",
				"MSXML2.XMLHttp.4.0",
				"MSXML2.XMLHttp.3.0",
				"MSXML2.XMLHttp",
				"Microsoft.XMLHttp"
			];
	
		for(var i = 0; i < versoes.length; i++)
		{
			try
			{
				var objetoXMLHttp = new ActiveXObject(versoes[i]);
				return objetoXMLHttp;
			}
			catch(ex)
			{
				//nada
			}
		}
	}
	return false;	
}

function getFrete(cep_origem,cep_destino,peso,valor_declarado,retorno,servico)
{

	var pagina = "comuns/libs/frete/php/frete.php?cep_origem="+cep_origem+"&cep_destino="+cep_destino+"&peso="+peso+"&valor_declarado="+valor_declarado+"&servico="+servico;
	
	var ajax = objXMLHttpCep();
	
	if(ajax)
	{
		ajax.open("GET", pagina, true);
		ajax.onreadystatechange = function()
		{
			//enquanto estiver processando...emite a msg de carregando
			if(ajax.readyState == 1)
			{
				document.getElementById(retorno).innerHTML = "<div style='height:80px;'><div id='img_carregando_frete'></div></div>";			
			}
			if(ajax.readyState == 4)
			{
				if(ajax.status == 200)
				{
					//FAZER APARECER O RESULTADO DA CONSULTA DO FRETE
					document.getElementById(retorno).innerHTML = ajax.responseText;
				}
				else
				{
					alert("Houve um problema ao carregar");
					document.getElementById('carregando').style.display = "none";
				}
			}
		}
	}
	
	ajax.send(null);
	
}
	
function calculaFrete(cep_origem,cep_destino,peso,valor_declarado,retorno)
{
	
	for(var i=0; i < document.form_frete.servico.length; i++)
	{
		if(document.form_frete.servico[i].checked)
		{
			var servico = document.form_frete.servico[i].value;
		}
	}	
	
	if(!servico)
	{
		alert("SELECIONE UMA FORMA PARA ENTREGA");
		return false;
	}
	else
	{
		if(cep_destino.length < 9)
		{
			alert("INFORME O CEP PARA O C�LCULO DO FRETE");
			document.getElementById("pedidos_cep").focus();
			return false;
		}
		else
		{
			cep_destino = cep_destino.replace("-","");
			cep_origem	= cep_origem.replace("-","");
			
			getFrete(cep_origem,cep_destino,peso,valor_declarado,retorno,servico);					
		}
	}
}