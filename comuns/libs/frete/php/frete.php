<?php

require("../../../../site/includes/funcoes.php");

$frete = new Frete();
$wsfrete = new WsFrete();
$objPedido = new EXT_Pedidos();

if(isset($_SESSION["pedido"]))
{
	$objPedido->select($_SESSION["pedido"]);
	
	$valor_pedido = $objPedido->getPrecoPedido();
}

if($h->fDados("servico") == "sedex")
{
    $cod_servico = WsFrete::SEDEX;
    $correios = "OK";
}
else if($h->fDados("servico") == "e-sedex")
{
    $cod_servico = WsFrete::E_SEDEX;
    $correios = "OK";
}
else if($h->fDados("servico") == "pac")
{
    $cod_servico = WsFrete::PAC;
    $correios = "OK";
}

if($correios == "OK")
{
	$peso = ceil(str_replace(",",".",$h->fDados("peso")));

    $frete = $wsfrete->getFrete(array(
        "cod_servico" => $cod_servico,
        "cep_origem" => $_GET["cep_origem"],
        "cep_destino" => $_GET["cep_destino"],
        "peso" => $peso,
        "valor" => $h->fDados("valor_declarado")
        )
    );
		
	echo 
	"
		<b>SUB TOTAL:</b> ".$h->fMoeda($valor_pedido)."
		<br/>					
		<br/>
		<b class=\"texto_vermelho\">FRETE:</b> ".$h->fMoeda($frete->Valor)." <br />
		<br/>
		<b>TOTAL:</b> ".$h->fMoeda($valor_pedido + $h->fFloat($frete->Valor))."
	";
	
}

if($h->fDados("servico") == "transportadora")
{
	
	$freteBrassPress = $frete->getFreteBrasPress($h->fDados("peso"),$h->fDados("valor_declarado"),$h->fDados("cep_destino"));
	
	echo 
	"
		<b>SUB TOTAL:</b> ".$h->fMoeda($valor_pedido)."
		<br/>					
		<br/>
		<b class=\"texto_vermelho\">FRETE:</b> ".$h->fMoeda($freteBrassPress)." <br />
		<br/>
		<b>TOTAL:</b> ".$h->fMoeda($valor_pedido + $h->fFloat($freteBrassPress))."
	";
	
}
elseif($h->fDados("servico") == "motoboy")
{
	$boy = $frete->getFreteMotoboy($h->fDados("peso")); 
		
	echo 
	"
		<b>SUB TOTAL:</b> ".$h->fMoeda($valor_pedido)."
		<br/>					
		<br/>
		<b class=\"texto_vermelho\">FRETE:</b> ".$h->fMoeda($boy)."
		<br />
		Locais atendidos: {$frete->getCidadesMotoboy()}
		<br/>
		<br />
		<b>TOTAL:</b> ".$h->fMoeda($valor_pedido + $h->fFloat($boy))."
	";
	
}
