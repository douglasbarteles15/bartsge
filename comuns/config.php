<?php
  $base = realpath(dirname(__FILE__).'/../');
  $base = str_replace("\\",'/',$base);

  define('BASE_DIR',$base.'/');
  define('ADM_DIR',BASE_DIR.'adm/');
  define('SITE_DIR',BASE_DIR.'site/');

  //site_url
  $document_root = $_SERVER['DOCUMENT_ROOT'];
  if(!$document_root){
      $document_root = $_SERVER['SCRIPT_FILENAME'];
      $document_root = str_ireplace("\\",'/',$document_root);
      $document_root = explode($_SERVER['SCRIPT_NAME'],$document_root);
      $document_root = current($document_root);

  }
  $document_root = str_ireplace("\\",'/',$document_root);
  $base_url = str_ireplace($document_root,'',BASE_DIR);

  /*if($base_url[0]=="/")
  {
      $base_url = substr($base_url, 1);
  }*/

  $http = $_SERVER['SERVER_PORT'] == '443'?'https':'http';
  $base_url = "$http://$_SERVER[HTTP_HOST]/$base_url";

  define('BASE_URL',$base_url);
  define('ADM_URL',"$base_url"."adm/");
  define('SITE_URL',"$base_url"."site/");

  date_default_timezone_set('America/Sao_Paulo');
   