
<!-- TinyMCE -->
<script type="text/javascript" src="<?php echo $SiteHTTP ?>comuns/libs/tinymce_pt/jscripts/tiny_mce/tiny_mce.js"></script>
<script type="text/javascript"
src="<?php echo $SiteHTTP ?>comuns/libs/tinymce_pt/jscripts/tiny_mce/plugins/tinybrowser/tb_tinymce.js.php"></script>
<script type="text/javascript">
    tinyMCE.init({
        // General options
        language : "pt",
         mode : "exact",
        theme : "advanced",
        plugins : "safari,pagebreak,style,layer,table,save,advhr,advimage,advlink,emotions,iespell,inlinepopups,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template",
        elements : "editorCMS, editorCMSs",


        // Theme options
        theme_advanced_buttons1:
            "code,bold,italic,underline,strikethrough,justifyleft,justifycenter,justifyright,justifyfull,cleanup,|,bullist,numlist,link,unlink,image,table,formatselect,fontselect,fontsizeselect,forecolor,backcolor,fullscreen",

        // Theme options
        theme_advanced_buttons2 : "pastetext,pasteword,selectall,media,flash,styleselect",
        paste_auto_cleanup_on_paste : true,
        theme_advanced_buttons3 : "",
        theme_advanced_buttons4 : "",


        theme_advanced_toolbar_location : "top",
        theme_advanced_toolbar_align : "left",
        theme_advanced_statusbar_location : "bottom",
        theme_advanced_resizing : true,

        // Example content CSS (should be your site CSS)
        content_css : "<?php echo $SiteHTTP ?>estilo_editor.css",

        // Drop lists for link/image/media/template dialogs
        template_external_list_url : "lists/template_list.js",
        external_link_list_url : "lists/link_list.js",
        external_image_list_url : "lists/image_list.js",
        media_external_list_url : "lists/media_list.js",
        file_browser_callback : "tinyBrowser",
        // Replace values for the template plugin
        template_replace_values : {
            username : "Some User",
            staffid : "991234"
        }
    });
</script>
<!-- /TinyMCE -->