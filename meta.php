<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<base href="<?= $SiteHTTP ?>"/>
<script src="http://code.jquery.com/jquery-latest.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js"></script>
<link href='https://fonts.googleapis.com/css?family=Ubuntu:400,500,300' rel='stylesheet' type='text/css'>
<link href='https://fonts.googleapis.com/css?family=Ubuntu:700italic' rel='stylesheet' type='text/css'>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
<style type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css'"></style>
<link href='https://fonts.googleapis.com/css?family=Asap:400,700italic,700' rel='stylesheet' type='text/css'>
<link rel="stylesheet" href="https://code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
<link href="front-end/css/global.css" rel="stylesheet" type="text/css" media="all">
<link href="front-end/css/animate.css" rel="stylesheet" type="text/css" media="all">
<link href="front-end/css/logos-style.css" rel="stylesheet" type="text/css" media="all">
<link rel="stylesheet" type="text/css" href="lib/bootstrap/css/bootstrap.css">
<script type="text/javascript" src="lib/bootstrap/js/bootstrap.js"></script>
<link rel="stylesheet" type="text/css" href="css/style.css">
<link rel="stylesheet" type="text/css" href="css/responsive.css">
<link rel="stylesheet" type="text/css" href="css/sweetalert2.css">
<script src="js/sweetalert2.min.js"></script>