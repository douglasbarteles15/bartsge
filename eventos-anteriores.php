<?php
require("site/includes/funcoes.php");
?>
<!DOCTYPE html>
<html>
<head>
	<title>Eventos Anteriores</title>
	<?php include('meta.php'); ?>
</head>
<body>
	<?php include('header-fixo.php'); ?>
	<section class="section-page">
		<header>
			<div class="title">
				<h1>Eventos Anteriores</h1>
				<hr>
			</div>
		</header>
			<div class="content-main">
				<div class="content-box ">
                    <?php
                    $data_atual = date('Y-m-d');
                    $objEventos = new DAO_Eventos();
                    $objSubEventos = new DAO_Subeventos();
                    $buscaProximosEventos = "SELECT * FROM eventos WHERE status_id_INT='1' AND data_final < '".$data_atual."' ORDER BY data_final DESC, hora_inicio DESC";
                    $objBanco->Query($buscaProximosEventos);
                    while($exibeProximosEventos = $objBanco->FetchObject()){
                        $objEventos->select($exibeProximosEventos->id);
                        $buscaSubeventos = "SELECT * FROM subeventos WHERE evento_id_INT='".$objEventos->getid()."' AND status_id_INT='1'";
                        $totalSubeventos = $objSubEventos->rows($buscaSubeventos);
                        ?>
                        <a href="evento/<?=$objEventos->getlink()?>">
                            <div class="box-curso span3 wow flipInX">
                                <div class="img-curso"><img src="site/uploads/img_eventos/<?=$objEventos->getimg_mini()?>"></div>
                                <div class="box-title">
                                    <div class="ic-title"><?=$objEventos->gettitulo()?></div>
                                </div>
                                <div class="box-footer">
                                <span class="modulos">
                                    <?=$objEventos->FDataExibe($objEventos->getdata_inicio())?>
                                    <?php if($objEventos->getdata_final()!=$objEventos->getdata_inicio()){
                                        echo ' até '.$objEventos->FDataExibe($objEventos->getdata_final());
                                    } ?>
                                </span>
                                    <div style="clear: both;"></div>
                                    <span class="hora"><i class="fa fa-clock-o" aria-hidden="true"></i> de <?=substr($objEventos->gethora_inicio(), 0, -3)?>h às <?=substr($objEventos->gethora_final(), 0, -3)?>h</span>
                                    <div style="clear: both;"></div>
                                    <?php
                                    if($objEventos->getvagas_restantes()==0){
                                        echo '<span class="hora" style="color: #761c19">vagas esgotadas</span>';
                                    }elseif($objEventos->getvagas_restantes()==1){
                                        echo '<span class="hora">1 vaga restante</span>';
                                    }else{
                                        echo '<span class="hora">'.$objEventos->getvagas_restantes().' vagas restantes</span>';
                                    }
                                    ?>
                                </div>
                            </div>
                        </a>
                    <?php } ?>
		</div>
			</div>
	</section>
	<?php include('como-funciona.php'); ?>
	<?php //include('depoimentos.php'); ?>
	<?php include('footer.php') ?>
</body>
</html>