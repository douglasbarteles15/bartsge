<?php
require("site/includes/funcoes.php");
$evento_id = FDados("evento");
?>
<!DOCTYPE html>
<html>
<head>
	<title>Cadastro</title>
	<?php include('meta.php'); ?>
</head>
<body>
	<?php include('header-fixo.php'); ?>
	<section class="section-page">
		<header>
			<div class="title">
				<h1>Cadastre-se agora mesmo!</h1>
				<hr>
			</div>
		</header>
		<div class="content-main">
			<article>
				<!--<div class="center-page-text">
					<span class="msg-type-1">Deixe sua mensagem abaixo</span>
					<span class="msg-type-2">Dúvidas, sugestões, críticas e assuntos diversos</span>
					<hr class="line-blue">
				</div>-->
				<div class="box-form contato-form">
					<div class="text-box">Preencha os campos abaixo para se cadastrar e se inscrever no evento de seu interesse</div>
					<form action="cadastra-participante" method="post" enctype="multipart/form-data">
                        <span style="float: right;font-size: 0.8em;">campos obrigatórios *</span>
                        <div class="text-box-left">Selecione o evento em que deseja se inscrever</div>
                        <div class="col-md-12 mr-pad">
                            <select name="evento_id_INT">
                                <option value="">Selecione o Evento</option>
                                <?php
                                $data_atual = date('Y-m-d');
                                $objEventos = new DAO_Eventos();
                                $buscaEventos = "SELECT * FROM eventos WHERE status_id_INT='1' AND data_inicio >= '".$data_atual."' ORDER BY data_inicio ASC, hora_inicio ASC";
                                $objBanco->Query($buscaEventos);
                                while($exibeEventos = $objBanco->FetchObject()){
                                    $objEventos->select($exibeEventos->id);
                                ?>
                                    <?php if($objEventos->getvagas_restantes()==0){ ?>
                                        <option style="color: red" value="<?=$objEventos->getid()?>" disabled><?=$objEventos->gettitulo()?> (Esgotado)</option>
                                    <?php }else{ ?>
                                        <option value="<?=$objEventos->getid()?>" <?php if($evento_id == $objEventos->getid()){ echo 'selected';}?>><?=$objEventos->gettitulo()?></option>
                                    <?php } ?>
                                <?php }?>
                            </select>
                        </div>
						<div class="text-box-left">Dados pessoais</div>
						<div class="col-md-6 mr-pad"><input type="text" name="nome" id="nome" placeholder="Nome Completo *" required></div>
                        <div class="col-md-6 mr-pad">
                            <select name="sexo">
                                <option value="">Sexo</option>
                                <option value="1">Masculino</option>
                                <option value="2">Feminino</option>
                            </select>
                        </div>
						<div class="col-md-6 mr-pad"><input type="text" name="cpf" id="cpf" placeholder="CPF *" required></div>
						<div class="col-md-6 mr-pad"><input type="tel" name="telefone" id="telefone" placeholder="Telefone *" required></div>
						<div class="text-box-left" style="clear: both">Endereço</div>
						<div class="col-md-6 mr-pad"><input type="text" name="cep" id="cep" placeholder="CEP *" required></div>
						<div class="col-md-6 mr-pad"><input type="text" name="endereco" id="endereco" placeholder="Rua *" required></div>
						<div class="col-md-6 mr-pad">
							<div class="col-md-6 mr-pad-min"><input type="text" name="numero" id="numero" placeholder="Numero *" required></div>
							<div class="col-md-6 mr-pad-min"><input type="text" name="complemento" id="complemento" placeholder="Complemento"></div>
						</div>
						<div class="col-md-6 mr-pad"><input type="text" name="bairro" id="bairro" placeholder="Bairro *" required></div>
						<div class="col-md-6 mr-pad">
							<select name="estado" onchange="buscaCidades(this.value);" required>
                                <?php
                                echo "<option value=''>Selecione um Estado *</option>";
                                $objBancoEstados = new Database();
                                $sql = "select * from estado order by nome";
                                $objBancoEstados->Query($sql);
                                while ($row = $objBancoEstados->FetchObject()) {
                                    if ($idEstado == $row->id) {
                                        echo "<option value='".$row->id."' selected='selected' >".utf8_encode($row->nome)."</option>";
                                    } else {
                                        echo "<option value='".$row->id."' style='font-weight:bold'>".utf8_encode($row->nome)."</option>";
                                    }
                                }
                                ?>
							</select>
						</div>
						<div class="col-md-6 mr-pad">
							<select name="cidade" id="cidade" required>
                                <option value=''>Selecione primeiramente um Estado *</option>
							</select>
						</div>
                        <div style="clear: both;"></div>
                        <div class="text-box-left">Foto de Perfil</div>
                        <div class="col-md-12 mr-pad"><input type="file" name="imagem" id="imagem"></div>
						<div class="text-box-left" style="clear: both;">Dados de acesso</div>
						<div class="col-md-6 mr-pad"><input type="email" name="email" id="email" placeholder="Email *" required></div>
						<div class="col-md-6 mr-pad"><input type="password" name="senha" id="senha" placeholder="Senha *" required></div>
						<div class="col-md-12">
							<div class="col-md-6 mr-pad"><a class="cancel" href="acessar">Cancelar</a></div>
							<div class="col-md-6 mr-pad"><input type="submit" name="enviar" id="enviar" value="Enviar"></div>
						</div>
					</form>
				</div>
			</article>
		</div>
	</section>
	<?php //include('depoimentos.php'); ?>
	<?php include('footer.php') ?>
</body>
</html>
<script>
    function buscaCidades(estado) {
        if (estado) {
            $('#cidade').hide();
            $('.carregando').show();
            $.getJSON('retorna_cidades.php?estado='+estado, function(j){
                var options = '<option value="">Selecione uma Cidade *</option>';
                for (var i = 0; i < j.length; i++) {
                    options += '<option value="' + j[i].id + '">' + j[i].nome + '</option>';
                }
                //console.log(options);
                $('#cidade').html(options).show();
                $('.carregando').hide();
            });

            //console.log(estado);

        } else {
            $('#cidade').html('<option value="">Selecione primeiramente um Estado *</option>');
        }

    }
</script>