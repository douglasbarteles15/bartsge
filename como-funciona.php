<section class="box-funciona">
	<h1>Como Funciona?</h1>
	<div class="main clearfix">
		<div class="box-info wow  flipInX">
			<div class="ic-info"><img src="img/icon-comofunciona-1.png"></div>
			<div class="ic-text">Escolha o evento que deseja se inscrever</div>
		</div>
        <div class="box-info wow  flipInX">
            <div class="ic-info"><img src="img/icon-comofunciona-1.png"></div>
            <div class="ic-text">Faça seu cadastro<br>na área do participante</div>
        </div>
        <div class="box-info wow  flipInX">
            <div class="ic-info"><img src="img/icon-comofunciona-1.png"></div>
            <div class="ic-text">Confirme sua inscrição por email</div>
        </div>
        <div class="box-info wow  flipInX">
            <div class="ic-info"><img src="img/icon-comofunciona-1.png"></div>
            <div class="ic-text">E pronto!<br>Você estará inscrito</div>
        </div>
	</div>
	<!--<a class="bt-ng" href="#">saiba mais</a>-->
</section>