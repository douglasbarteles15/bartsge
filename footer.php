<section class="main-footer">
	<div class="content-main">
		<div class="logo-footer"><img src="front-end/layout/logo_rodape.png"></div>
		<div class="links-footer">
			<ul>
				<li><a href="proximos-eventos">Próximos Eventos</a></li>
				<li><a href="eventos-anteriores">Eventos Anteriores</a></li>
				<li><a href="noticias">Notícias</a></li>
				<li><a href="contato">Contato</a></li>
			</ul>
		</div>
		<div class="downfooter">
			<div class="pg">
				<img src="img/logo-puc-minas.png" width="60">
			</div>
			<div class="cd">
				<a href="http://www.pucminas.br/" target="_blank">Desenvolvimento de Aplicações Web</a>
			</div>
			<div class="pl">
				Desenvolvido por:<br>
				<a href="http://www.douglasbarteles.com.br/" target="_blank" style="color: #4c4c4c">Douglas Barteles</a>
			</div>
		</div>
	</div>
</section>
<script src="front-end/js/jquery.bxslider.min.js"></script>
<script src="front-end/js/jquery.circleGraphic.js"></script>
<script src="front-end/js/bootstrap-portfilter.min.js"></script>
<script src="front-end/js/jquery.mask.js"></script>
<script src="front-end/js/wow.min.js"></script>     
<script src="front-end/js/global.js"></script>
<script src="js/ini.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.maskedinput/1.4.1/jquery.maskedinput.js"></script>
<script>
	jQuery(function ($) {
		$("#cep").mask("99.999-999");
		$("#cpf").mask("999.999.999-99");
		$("#cnpj").mask("99.999.999/9999-99");
		$('#datetime').mask('99/99/9999 99:99:99');
	});

	$(".data").datepicker({
		dateFormat: 'dd/mm/yy',
		dayNames: ['Domingo','Segunda','Terça','Quarta','Quinta','Sexta','Sábado'],
		dayNamesMin: ['D','S','T','Q','Q','S','S','D'],
		dayNamesShort: ['Dom','Seg','Ter','Qua','Qui','Sex','Sáb','Dom'],
		monthNames: ['Janeiro','Fevereiro','Março','Abril','Maio','Junho','Julho','Agosto','Setembro','Outubro','Novembro','Dezembro'],
		monthNamesShort: ['Jan','Fev','Mar','Abr','Mai','Jun','Jul','Ago','Set','Out','Nov','Dez'],
		nextText: 'Próximo',
		prevText: 'Anterior'
	});

	//<![CDATA[
	jQuery(document).ready(function () {
		//Inicio Mascara Telefone
		jQuery('input[type=tel]').mask("(99) 9999-9999?9").ready(function (event) {
			var target, phone, element;
			target = (event.currentTarget) ? event.currentTarget : event.srcElement;
			phone = target.value.replace(/\D/g, '');
			element = $(target);
			element.unmask();
			if (phone.length > 10) {
				element.mask("(99) 99999-999?9");
			} else {
				element.mask("(99) 9999-9999?9");
			}
		});
		//Fim Mascara Telefone
	});
	(jQuery);
	//]]>
</script>
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/pt_BR/sdk.js#xfbml=1&version=v2.6";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>