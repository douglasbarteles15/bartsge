<?php
ob_start();
require("../../includes/funcoes.php");

$objLog = new DAO_Log_Admin();

$email = $h->fDados("email");
$senha = $h->fDados("senha");

if ($email != "" && $senha != "") {
    $sql = "select * from usuarios_admin where login = '$email' and senha = '{$objCrypt->crypt64($senha)}'";
    $objBanco->Query($sql);

    if ($objBanco->rows < 1) {
        header("location:" . $SiteHTTP . "admin/?msgErro=Usuario ou senha invalido(s)");
    } else {

        $row = $objBanco->FetchObject();

            $_SESSION["usuario"] = $row->id;
            $_SESSION["logado"] = "logado";
            $_SESSION["usuario_nome"] = explode(' ', $row->nome);
            $_SESSION["usuario_nome"] = $_SESSION["usuario_nome"][0];
            $_SESSION["tipo_usuario"] = $row->tipo_id_INT;

            //DELETA LOG ADMIN DE MAIS DE 30 DIAS
            $objLog->delete_log();

            //INSERE LOG ADMIN
            $objLog->insert($_SESSION["usuario"], "Entrou no sistema", date('Y-m-d H:i:s'));

            header("location:" . $SiteHTTP . "admin/sistema");

    }
} else {
        header("location:" . $SiteHTTP . "admin/?msgErro=Preecha todos os campos");
}
?>