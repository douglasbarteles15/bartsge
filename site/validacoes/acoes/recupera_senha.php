<?php
require '../../includes/funcoes.php';
require("../../../php_mailer/class.phpmailer.php");
require("../../../php_mailer/class.smtp.php");

$objBanco = new Database();
$email = FDados("email");
$objParticipante = new DAO_Participantes();
$objInstrutor = new DAO_Instrutores();

$verificaEmail = "SELECT * FROM participantes WHERE email='" . $email . "' LIMIT 1";
$totalEmail = $objParticipante->rows($verificaEmail);

$verificaEmailInstrutor = "SELECT * FROM instrutores WHERE email='" . $email . "' LIMIT 1";
$totalEmailInstrutor = $objInstrutor->rows($verificaEmailInstrutor);

if ($totalEmail == 0 && $totalEmailInstrutor == 0) {

    header("location:" . $SiteHTTP . "acessar/erro/Não encontramos este email cadastrado no sistema.");

} else {

    if($totalEmail > 0){

        $objBanco->Query($verificaEmail);
        $exibeParticipante = $objBanco->FetchObject();
        $objParticipante->select($exibeParticipante->id);

        //ENVIA EMAIL PARA OS RESPONSÁVEIS
        $mail = new PHPMailer();
#enviar via SMTP
#seu servidor smtp / dominio no meu caso "mail" mas pode mudar verifique o seu!
        $mail->Host = "mail.douglasbarteles.com.br";
#habilita smtp autenticado
        $mail->SMTPAuth = true;
#usuário deste servidor smtp. Aqui esta a solucao
        $mail->Username = "no_reply@douglasbarteles.com.br";
        $mail->Password = "teste@"; // senha
#email utilizado para o envio, pode ser o mesmo de username
        $mail->From = "no_reply@douglasbarteles.com.br";
        $mail->FromName = utf8_decode("Barteles Eventos");
        $mail->AddAddress($objParticipante->getemail());
//$mail->AddBCC('douglas@pluginweb.com.br'); // CÃ³pia Oculta
        $mail->Subject = utf8_decode("Recuperação de Senha - Barteles Eventos");
        $mail->IsHTML(true);
        $mail->Body = utf8_decode('
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    </head>
    <body style="background-color: #202020;">
        <table border="0" cellpadding="0" cellspacing="0" height="100%" width="600" id="bodyTable">
            <tr><td><img src="' . $SiteHTTP . 'front-end/layout/logo.png" style="margin-left: 36px;"></td></tr>
            <tr>
                <td align="center" valign="top">
                    <table border="0" cellpadding="20" cellspacing="0" width="520" id="emailContainer" style="background-color: white">
                        <tr>
                            <td valign="top">
                                <p>Prezado(a) <b>' . $objParticipante->getnome() . '</b> </p>

                                <p>Segue sua senha de acesso a Área do Participante: <b>'.$objCrypt->decrypt($objParticipante->getsenha()).'</b></p>

                                <p>Atenciosamente,</p>
                                <p>Barteles Eventos</p>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </body>
</html>

');

        if (!$mail->Send()) {
            echo "Mensagem não enviada";
            echo "Mailer Error: " . $mail->ErrorInfo;

            header("location:" . $SiteHTTP . "acessar/erro/Aconteceu algum erro ao recuperar sua senha.");

        } else {

            //DIRECIONA PARA A PÁGINA DE LOGIN
            header("location:" . $SiteHTTP . "acessar/sucesso/Sua senha de acesso foi enviada com sucesso para o seu email.");

        }

    }elseif($totalEmailInstrutor > 0){

        $objBanco->Query($verificaEmailInstrutor);
        $exibeInstrutor = $objBanco->FetchObject();
        $objInstrutor->select($exibeInstrutor->id);

        //ENVIA EMAIL PARA OS RESPONSÁVEIS
        $mail = new PHPMailer();
#enviar via SMTP
#seu servidor smtp / dominio no meu caso "mail" mas pode mudar verifique o seu!
        $mail->Host = "mail.douglasbarteles.com.br";
#habilita smtp autenticado
        $mail->SMTPAuth = true;
#usuário deste servidor smtp. Aqui esta a solucao
        $mail->Username = "no_reply@douglasbarteles.com.br";
        $mail->Password = "teste@"; // senha
#email utilizado para o envio, pode ser o mesmo de username
        $mail->From = "no_reply@douglasbarteles.com.br";
        $mail->FromName = utf8_decode("Barteles Eventos");
        $mail->AddAddress($objInstrutor->getemail());
//$mail->AddBCC('douglas@pluginweb.com.br'); // CÃ³pia Oculta
        $mail->Subject = utf8_decode("Recuperação de Senha - Barteles Eventos");
        $mail->IsHTML(true);
        $mail->Body = utf8_decode('
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    </head>
    <body style="background-color: #202020;">
        <table border="0" cellpadding="0" cellspacing="0" height="100%" width="600" id="bodyTable">
            <tr><td><img src="' . $SiteHTTP . 'front-end/layout/logo.png" style="margin-left: 36px;"></td></tr>
            <tr>
                <td align="center" valign="top">
                    <table border="0" cellpadding="20" cellspacing="0" width="520" id="emailContainer" style="background-color: white">
                        <tr>
                            <td valign="top">
                                <p>Prezado(a) <b>' . $objInstrutor->getnome() . '</b> </p>

                                <p>Segue sua senha de acesso a Área do Instrutor/Palestrante: <b>'.$objCrypt->decrypt($objInstrutor->getsenha()).'</b></p>

                                <p>Atenciosamente,</p>
                                <p>Barteles Eventos</p>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </body>
</html>

');

        if (!$mail->Send()) {
            echo "Mensagem não enviada";
            echo "Mailer Error: " . $mail->ErrorInfo;

            header("location:" . $SiteHTTP . "acessar/erro/Aconteceu algum erro ao recuperar sua senha.");

        } else {

            //DIRECIONA PARA A PÁGINA DE LOGIN
            header("location:" . $SiteHTTP . "acessar/sucesso/Sua senha de acesso foi enviada com sucesso para o seu email.");

        }

    }

}
