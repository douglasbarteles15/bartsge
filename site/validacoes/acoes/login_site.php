<?php
ob_start();
require("../../includes/funcoes.php");
$objBanco = new Database();
$objBancoInstrutor = new Database();

$email = FDados("email");
$senha = FDados("senha");
$senha_criptografada = $objCrypt->crypt64($senha);
$objParticipantes = new DAO_Participantes();
$objInstrutores = new DAO_Instrutores();

if ($email != "" && $senha != "") {

    $verifica_email_existe = "SELECT email FROM participantes WHERE email='" . $email . "'";
    $existe_email = $objParticipantes->rows($verifica_email_existe);

    $verifica_email_existe_instrutor = "SELECT email FROM instrutores WHERE email='" . $email . "'";
    $existe_email_instrutores = $objInstrutores->rows($verifica_email_existe_instrutor);

    if ($existe_email > 0 || $existe_email_instrutores > 0) {

        $verifica_login = "SELECT * FROM participantes WHERE email='" . $email . "' AND senha='" . $senha_criptografada . "'";

        $objBanco->Query($verifica_login);

        if ($objBanco->rows < 1) {

            $verifica_login_instrutor = "SELECT * FROM instrutores WHERE email='" . $email . "' AND senha='" . $senha_criptografada . "'";

            $objBancoInstrutor->Query($verifica_login_instrutor);

            if ($objBancoInstrutor->rows < 1) {

                header("location:" . $SiteHTTP . "acessar/erro/Seus dados estão incorretos. Tente novamente ou faça seu cadastro corretamente.");
                //echo "Seus dados estão incorretos. Tente novamente ou faça seu cadastro corretamente.";

            }else{

                $row = $objBancoInstrutor->FetchObject();
                $objInstrutores->select($row->id);

                $_SESSION["instrutor"] = $objInstrutores->getid();
                $_SESSION["logado_site"] = "logado";
                $_SESSION["instrutor_nome_completo"] = $objInstrutores->getnome();
                $_SESSION["instrutor_nome"] = explode(' ', $objInstrutores->getnome());
                $_SESSION["instrutor_nome"] = $_SESSION["instrutor_nome"][0];
                $_SESSION["tipo_usuario_site"] = "instrutor";

                header("location:" . $SiteHTTP . "area-instrutor");
                //echo "Seja bem vindo(a) a Área do Instrutor.";

            }

        } else {

            $row = $objBanco->FetchObject();
            $objParticipantes->select($row->id);

                $_SESSION["participante"] = $objParticipantes->getid();
                $_SESSION["logado_site"] = "logado";
                $_SESSION["participante_nome_completo"] = $objParticipantes->getnome();
                $_SESSION["participante_nome"] = explode(' ', $objParticipantes->getnome());
                $_SESSION["participante_nome"] = $_SESSION["participante_nome"][0];
                $_SESSION["tipo_usuario_site"] = "participante";

                header("location:" . $SiteHTTP . "area-participante");
                //echo "Seja bem vindo(a) a Área do Participante.";

        }

    } else {

        header("location:" . $SiteHTTP . "acessar/erro/Email não encontrado.");
        //echo "Email não encontrado.";

    }
} else {
    header("location:" . $SiteHTTP . "acessar/erro/Preencha todos os campos.");
}

?>