<?php
require '../../includes/funcoes.php';
require("../../../php_mailer/class.phpmailer.php");
require("../../../php_mailer/class.smtp.php");

$objSite = new DAO_Config_site();
$objSite->select(1);
$emailSite = $objSite->getemail();
$emailSite2 = $objSite->getemail_financeiro();

$objBanco = new Database();
$objEvento = new DAO_Eventos();
$evento_id = FDados("evento");
$objEvento->select($evento_id);
$vagasRestantes = $objEvento->getvagas_restantes();
$objParticipante = new DAO_Participantes();
$participante_id = FDados("participante");
$objParticipante->select($participante_id);

$objInscricao = new DAO_Inscricoes();
$inscricao_id = FDados("inscricao");
$objInscricao->select($inscricao_id);

//VERIFICA SE TEM VAGA RESTANTE
if ($vagasRestantes == 0) {
    header("location:" . $SiteHTTP . "acessar/erro/Infelizmente alguém foi mais rápido e pegou a última vaga! :(");
} else {

    //MUDA O STATUS DA INSCRIÇÃO PRA 1
    $objInscricao->updatecampo($objInscricao->getid(), "status_id_INT", 1, "INT");

    //OCUPA A VAGA DIMINUINDO UMA RESTANTE
    $objEvento->updatecampo($evento_id, "vagas_restantes", $vagasRestantes - 1, "INT");

    //ENVIA EMAIL PARA OS RESPONSÁVEIS
    $mail = new PHPMailer();
    #enviar via SMTP
    #seu servidor smtp / dominio no meu caso "mail" mas pode mudar verifique o seu!
    $mail->Host = "mail.douglasbarteles.com.br";
    #habilita smtp autenticado
    $mail->SMTPAuth = true;
    #usuário deste servidor smtp. Aqui esta a solucao
    $mail->Username = "no_reply@douglasbarteles.com.br";
    $mail->Password = "teste@"; // senha
    #email utilizado para o envio, pode ser o mesmo de username
    $mail->From = "no_reply@douglasbarteles.com.br";
    $mail->FromName = utf8_decode("Barteles Eventos");
    $mail->AddAddress($objParticipante->getemail());

//$mail->AddBCC('douglas@pluginweb.com.br'); // CÃ³pia Oculta
    $mail->Subject = utf8_decode("Confirmação de Inscrição - Barteles Eventos");
    $mail->IsHTML(true);
    $mail->Body = utf8_decode('
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    </head>
    <body style="background-color: #202020;">
        <table border="0" cellpadding="0" cellspacing="0" height="100%" width="600" id="bodyTable">
            <tr><td><img src="' . $SiteHTTP . 'front-end/layout/logo.png" style="margin-left: 36px;" ></td></tr>
            <tr>
                <td align="center" valign="top">
                    <table border="0" cellpadding="20" cellspacing="0" width="520" id="emailContainer" style="background-color: white">
                        <tr>
                            <td valign="top">
                                <p>Sua inscrição no evento <b>'.$objEvento->gettitulo().'</b> foi confirmada com sucesso!</p>
                                <p>Acesse a Área do Participante e veja todos os detalhes.</p>

                                <p>Atenciosamente,</p>
                                <p>Barteles Eventos</p>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <!--<tr height="20px"></tr>
            <table border="0" cellpadding="20" cellspacing="0" width="580" id="emailFooter">
                <tr>
                    <td align="right">
                        <a href="http://www.douglasbarteles.com.br" target="blank">
                            <img src="' . $SiteHTTP . 'assets/layout/desenvolvido-por.png">
                        </a>
                    </td>
                </tr>
            </table>-->
        </table>
    </body>
</html>

');

    if (!$mail->Send()) {
        echo "Mensagem não enviada";
        echo "Mailer Error: " . $mail->ErrorInfo;

        header("location:" . $SiteHTTP . "acessar/erro/Erro inesperado. Tente novamente mais tarde.");

    } else {

        //DIRECIONA PARA A PÁGINA DE LOGIN
        header("location:" . $SiteHTTP . "acessar/sucesso/Sua inscrição foi confirmada com sucesso! Acesse a Área do Participante com Email e Senha.");

    }

}
