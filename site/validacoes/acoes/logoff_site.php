<?php
ob_start();
require("../../includes/funcoes.php");
session_start();
unset($_SESSION["participante"]);
unset($_SESSION["instrutor"]);
unset($_SESSION["logado_site"]);
unset($_SESSION["tipo_usuario_site"]);

header("location:" . $SiteHTTP . "home");
?>