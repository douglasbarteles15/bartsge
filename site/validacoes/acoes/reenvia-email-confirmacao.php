<?php
require '../../includes/funcoes.php';
if ($_SESSION["logado_site"] != "logado") {
    header("location:" . $SiteHTTP . "acessar/erro/Você precisa estar logado pra fazer a inscrição diretamente.");
} else {
    $objParticipanteLogado = new DAO_Participantes();
    $objParticipanteLogado->select($_SESSION["participante"]);
}
require("../../../php_mailer/class.phpmailer.php");
require("../../../php_mailer/class.smtp.php");

$objSite = new DAO_Config_site();
$objSite->select(1);
$emailSite = $objSite->getemail();
$emailSite2 = $objSite->getemail_financeiro();

$objBanco = new Database();
$objEvento = new DAO_Eventos();
$evento_id = FDados("evento");
$objEvento->select($evento_id);
$vagasRestantes = $objEvento->getvagas_restantes();

$objInscricao = new DAO_Inscricoes();
$inscricao_id = FDados("inscricao");
$objInscricao->select($inscricao_id);

if($objInscricao->getstatus_id_INT() != 0){
    header("location:" . $SiteHTTP . "area-participante/erro/Aconteceu algum erro ao reenviar o email de confirmação.");
}else {

//ENVIA EMAIL PARA OS RESPONSÁVEIS
    $mail = new PHPMailer();
#enviar via SMTP
#seu servidor smtp / dominio no meu caso "mail" mas pode mudar verifique o seu!
    $mail->Host = "mail.douglasbarteles.com.br";
#habilita smtp autenticado
    $mail->SMTPAuth = true;
#usuário deste servidor smtp. Aqui esta a solucao
    $mail->Username = "no_reply@douglasbarteles.com.br";
    $mail->Password = "teste@"; // senha
#email utilizado para o envio, pode ser o mesmo de username
    $mail->From = "no_reply@douglasbarteles.com.br";
    $mail->FromName = utf8_decode("Barteles Eventos");
    $mail->AddAddress($objParticipanteLogado->getemail());
//$mail->AddBCC('douglas@pluginweb.com.br'); // CÃ³pia Oculta
    $mail->Subject = utf8_decode("Inscrição em Evento - Barteles Eventos");
    $mail->IsHTML(true);
    $mail->Body = utf8_decode('
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    </head>
    <body style="background-color: #202020;">
        <table border="0" cellpadding="0" cellspacing="0" height="100%" width="600" id="bodyTable">
            <tr><td><img src="' . $SiteHTTP . 'front-end/layout/logo.png" style="margin-left: 36px;"></td></tr>
            <tr>
                <td align="center" valign="top">
                    <table border="0" cellpadding="20" cellspacing="0" width="520" id="emailContainer" style="background-color: white">
                        <tr>
                            <td valign="top">
                                <p>Prezado(a) <b>' . $objParticipanteLogado->getnome() . '</b> </p>
                                <p>Ficamos muito felizes com seu interesse em participar do evento <b>' . $objEvento->gettitulo() . '</b></p>
                                <p>Para confirmar sua inscrição clique <a href="' . $SiteHTTP . 'confirmar-inscricao/' . $objEvento->getid() . '/' . $objParticipanteLogado->getid() . '/' . $objInscricao->getid() . '" style="font-weight:bold">AQUI</a></p>

                                <p>Atenciosamente,</p>
                                <p>Barteles Eventos</p>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <!--<tr height="20px"></tr>
            <table border="0" cellpadding="20" cellspacing="0" width="580" id="emailFooter">
                <tr>
                    <td align="right">
                        <a href="http://www.douglasbarteles.com.br" target="blank">
                            <img src="' . $SiteHTTP . 'assets/layout/desenvolvido-por.png">
                        </a>
                    </td>
                </tr>
            </table>-->
        </table>
    </body>
</html>

');

    if (!$mail->Send()) {
        echo "Mensagem não enviada";
        echo "Mailer Error: " . $mail->ErrorInfo;

        header("location:" . $SiteHTTP . "area-participante/erro/Aconteceu algum erro ao reenviar o email de confirmação.");

    } else {

        //DIRECIONA PARA A PÁGINA DE LOGIN
        header("location:" . $SiteHTTP . "area-participante/sucesso/O link de confirmação foi reenviado com sucesso para seu email.");

    }

}
