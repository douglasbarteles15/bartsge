<?php
ob_start();
require("../../includes/funcoes.php");

session_start();

$objLog = new DAO_Log_Admin();

//INSERE LOG ADMIN
$objLog->insert($_SESSION["usuario"], "Saiu do sistema", date('Y-m-d H:i:s'));

unset($_SESSION["usuario"]);
unset($_SESSION["logado"]);

//	header("location:../../../index.php");
header("location:../admin/?msgErro=Você saiu com sucesso!");
?>