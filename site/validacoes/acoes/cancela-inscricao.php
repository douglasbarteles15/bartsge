<?php
require '../../includes/funcoes.php';
//Verificar a permissão de acesso
if ($_SESSION["logado_site"] == "logado") {
    $objAlunoLogado = new DAO_Alunos();
    $objAlunoLogado->select($_SESSION["aluno"]);
    $nome_logado = $objAlunoLogado->getalunos_nome();
} elseif ($_SESSION["tipo_usuario_site"] == "adulto") {
    $nome_logado = $_SESSION["adultos_nome_completo"];
} else {
    header("location:" . $SiteHTTP . "home/erro/Para acessar é necessário estar logado!");
}

$objBanco = new Database();
$objPreInscricao = new DAO_Pre_Inscricoes();
$objPreInscricao->select(FDados("id"));
$objModalidade = new DAO_Modalidades();
$modalidades_id = $objPreInscricao->getmodalidades_id_INT();
$objModalidade->select($modalidades_id);
$vagasRestantes = $objModalidade->getmodalidades_vagas_restantes();
$objCategoria = new DAO_Categorias_Modalidades();
$objCategoria->select($objModalidade->getcategorias_modalidades_id_INT());

 
//VERIFICA SE É NATAÇÃO    
    if(($objCategoria->getcategorias_modalidades_id()==3)&&($objModalidade->getmodalidades_atividade_extra_INT()==0)){
        
        $objAvaliacoes = new DAO_Avaliacoes();
        $idAvaliacao = $objPreInscricao->getavaliacoes_id_INT();
        $objAvaliacoes->select($idAvaliacao);
        $objAvaliacoesMarcacao = new DAO_Avaliacoes_Marcacao();
        $idAvaliacaoMarcacao = $objAvaliacoes->getavaliacoes_marcacao_id_INT();
        $objAvaliacoesMarcacao->select($idAvaliacaoMarcacao);
        
        //DESATIVA A PRÉ-INSCRIÇÃO
        $objPreInscricao->updatecampo($objPreInscricao->getpre_inscricoes_id(), "pre_inscricoes_status_INT", 0, "INT");

        //ATUALIZA DATA DE CANCELAMENTO
        $objPreInscricao->updatecampo($objPreInscricao->getpre_inscricoes_id(), "pre_inscricoes_data_cancelamento_DATETIME", date('Y-m-d H:i:s'), "DATETIME");

        //DESOCUPA A MARCAÇÃO USADA
        //$objAvaliacoesMarcacao->updatecampo($idAvaliacaoMarcacao, "avaliacoes_marcacao_status_INT", 0, "INT");

        //EXCLUI A MARCAÇÃO USADA
        $objBanco->Query("SET FOREIGN_KEY_CHECKS = 0;");
        $objAvaliacoesMarcacao->delete($idAvaliacaoMarcacao);

        //DELETA A AVALIAÇÃO
        $objAvaliacoes->delete($idAvaliacao);
        $objBanco->Query("SET FOREIGN_KEY_CHECKS = 1;");
        
        //DESOCUPA OS HORÁRIOS DO ALUNO RELACIONADOS AOS HORÁRIOS DA MODALIDADE
        $objModalidadesOpcoes = new DAO_Modalidades_Opcoes();
        $buscaOpcoesModalidade = "SELECT modalidades_opcoes_id FROM modalidades_opcoes WHERE modalidades_id_INT='".$modalidades_id."'";
        $objBanco->Query($buscaOpcoesModalidade);
        while($exibeOpcoesModalidade = $objBanco->FetchObject()){
            $objModalidadesOpcoes->select($exibeOpcoesModalidade->modalidades_opcoes_id);
            $objBanco2 = new Database();
            $objBanco2->Query("DELETE FROM alunos_horarios WHERE alunos_id_INT='".$objAlunoLogado->getalunos_id()."' AND dia_semana='".$objModalidadesOpcoes->getmodalidades_opcoes_diaSemana()."' AND hora_inicio='".$objModalidadesOpcoes->getmodalidades_opcoes_horaInicio()."' AND hora_final='".$objModalidadesOpcoes->getmodalidades_opcoes_horaFinal()."'");
        }
        
        //DESOCUPA A VAGA AUMENTANDO UMA RESTANTE -- COMENTADO PORQUE A VAGA VAI FICAR PRO PRÓXIMO EDITAL
        //$objModalidade->updatecampo($modalidades_id, "modalidades_vagas_restantes", $vagasRestantes+1, "INT");

        //DIRECIONA PARA A PÁGINA DE MODALIDADES PRE INSCRITAS DO ALUNO
        header("location:" . $SiteHTTP . "modalidades-pre-inscritas/erro/Sua pré-inscrição foi cancelada com sucesso!");
        
        
    
    }else{

        //DESATIVA A PRÉ-INSCRIÇÃO
        $objPreInscricao->updatecampo($objPreInscricao->getpre_inscricoes_id(), "pre_inscricoes_status_INT", 0, "INT");

        //ATUALIZA DATA DE CANCELAMENTO
        $objPreInscricao->updatecampo($objPreInscricao->getpre_inscricoes_id(), "pre_inscricoes_data_cancelamento_DATETIME", date('Y-m-d H:i:s'), "DATETIME");

        if($objModalidade->getmodalidades_atividade_extra_INT()==0){
        //DESOCUPA OS HORÁRIOS DO ALUNO RELACIONADOS AOS HORÁRIOS DA MODALIDADE
        $objModalidadesOpcoes = new DAO_Modalidades_Opcoes();
        $buscaOpcoesModalidade = "SELECT modalidades_opcoes_id FROM modalidades_opcoes WHERE modalidades_id_INT='".$modalidades_id."'";
        $objBanco->Query($buscaOpcoesModalidade);
        while($exibeOpcoesModalidade = $objBanco->FetchObject()){
            $objModalidadesOpcoes->select($exibeOpcoesModalidade->modalidades_opcoes_id);
            $objBanco2 = new Database();
            $objBanco2->Query("DELETE FROM alunos_horarios WHERE alunos_id_INT='".$objAlunoLogado->getalunos_id()."' AND dia_semana='".$objModalidadesOpcoes->getmodalidades_opcoes_diaSemana()."' AND hora_inicio='".$objModalidadesOpcoes->getmodalidades_opcoes_horaInicio()."' AND hora_final='".$objModalidadesOpcoes->getmodalidades_opcoes_horaFinal()."'");
        }
        }
        
        //DESOCUPA A VAGA AUMENTANDO UMA RESTANTE
        $objModalidade->updatecampo($modalidades_id, "modalidades_vagas_restantes", $vagasRestantes+1, "INT");

        //DIRECIONA PARA A PÁGINA DE MODALIDADES PRE INSCRITAS DO ALUNO
        header("location:" . $SiteHTTP . "modalidades-pre-inscritas/erro/Sua pré-inscrição foi cancelada com sucesso!");
        
        }
