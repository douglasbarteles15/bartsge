<?php
require '../../includes/funcoes.php';
require("../../../php_mailer/class.phpmailer.php");
require("../../../php_mailer/class.smtp.php");
$caminho = "../../../site/uploads/img_participantes/"; //local onde ser� salvo a imagem grande
$objSite = new DAO_Config_site();
$objSite->select(1);
$emailSite = $objSite->getemail();
$emailSite2 = $objSite->getemail_financeiro();

$objBanco = new Database();
$objEvento = new DAO_Eventos();
$evento_id = FDados("evento_id_INT");
$objEvento->select($evento_id);
$vagasRestantes = $objEvento->getvagas_restantes();
$objParticipante = new DAO_Participantes();
$objParticipante->setpost();
$objParticipante->dadosBanco();
$objParticipante->setsenha($objCrypt->crypt64($objParticipante->getsenha()));

$objInscricao = new DAO_Inscricoes();

//VERIFICA SE TEM VAGA RESTANTE
if ($vagasRestantes == 0) {
    header("location:" . $SiteHTTP . "cadastro/erro/Infelizmente alguém foi mais rápido e pegou a última vaga! :(");
    //echo "Infelizmente alguém foi mais rápido e pegou a última vaga! :(";
} else {

    $verificaCPFexistente = "SELECT * FROM participantes WHERE cpf = '" . $objParticipante->getcpf() . "'";
    $totalCPFexistente = $objEvento->rows($verificaCPFexistente);
    if ($totalCPFexistente > 0) {
        header("location:" . $SiteHTTP . "cadastro/erro/Já existe um cadastro com esse CPF!");
        //echo "Já existe um cadastro com esse CPF!";
    }else {

        $verificaEmailExistente = "SELECT * FROM participantes WHERE email = '" . $objParticipante->getemail() . "'";
        $totalEmailExistente = $objEvento->rows($verificaEmailExistente);
        if ($totalEmailExistente > 0) {
            header("location:" . $SiteHTTP . "cadastro/erro/Já existe um cadastro com esse Email!");
            //echo "Já existe um cadastro com esse Email!";
        }else {

            $objParticipante->insert();
            $objParticipante->selectLastInsert();

            //++++++++++++++++++++++++++++  Manipulando a imagem +++++++++++++++++++++
            //Recebe a imagem
            $imagem = isset($_FILES['imagem']) ? $_FILES['imagem'] : NULL;
            $nomeImg = $imagem['tmp_name'];

            $nome_final = time() . rand(1, 9);

            if ($nomeImg != NULL) {

                //Chama a Class de Recorte
                require_once '../../../admin/sistema/recorte/ThumbLib.inc.php';

                //Monta a Miniatura
                $thumb = PhpThumbFactory::create("$nomeImg");
                $thumb->adaptiveResize(250, 250);
                $id = $objParticipante->getid();
                mkdir($caminho . $id, 0777);
                //mkdir("$caminho."/".$id", 0777);
                $thumb->save($caminho . "$id/" . $nome_final . '.jpg', 'jpg');

                //Monta a Imagem Full
                $thumb = PhpThumbFactory::create("$nomeImg");
                $thumb->adaptiveResize(500, 500);
                $thumb->save($caminho . "$id/" . "g_" . $nome_final . '.jpg', 'jpg');

                $imgMiniatura = $id . "/" . $nome_final . '.jpg';
                $imgFull = $id . "/" . 'g_' . $nome_final . '.jpg';

                $objParticipante->updatecampo($objParticipante->getid(), "img_mini", $imgMiniatura, "Varchar");
                $objParticipante->updatecampo($objParticipante->getid(), "img_full", $imgFull, "Varchar");
            }

            //Realiza a Inscrição do Participante
            $objInscricao->setparticipante_id_INT($objParticipante->getid());
            $objInscricao->setevento_id_INT($objEvento->getid());
            $objInscricao->setstatus_id_INT(0);
            $objInscricao->setdatetime(date('Y-m-d H:i:s'));
            $objInscricao->insert();
            $objInscricao->selectLastInsert();

            //OCUPA A VAGA DIMINUINDO UMA RESTANTE
            //$objEvento->updatecampo($evento_id, "vagas_restantes", $vagasRestantes - 1, "INT");

            //ENVIA EMAIL PARA OS RESPONSÁVEIS
            $mail = new PHPMailer();
            #enviar via SMTP
            #seu servidor smtp / dominio no meu caso "mail" mas pode mudar verifique o seu!
            $mail->Host = "mail.douglasbarteles.com.br";
            #habilita smtp autenticado
            $mail->SMTPAuth = true;
            #usuário deste servidor smtp. Aqui esta a solucao
            $mail->Username = "no_reply@douglasbarteles.com.br";
            $mail->Password = "teste@"; // senha
            #email utilizado para o envio, pode ser o mesmo de username
            $mail->From = "no_reply@douglasbarteles.com.br";
            $mail->FromName = utf8_decode("Barteles Eventos");
            $mail->AddAddress($objParticipante->getemail());
//$mail->AddBCC('douglas@pluginweb.com.br'); // CÃ³pia Oculta
            $mail->Subject = utf8_decode("Inscrição em Evento - Barteles Eventos");
            $mail->IsHTML(true);
            $mail->Body = utf8_decode('
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    </head>
    <body style="background-color: #202020;">
        <table border="0" cellpadding="0" cellspacing="0" height="100%" width="600" id="bodyTable">
            <tr><td><img src="' . $SiteHTTP . 'front-end/layout/logo.png" style="margin-left: 36px;"></td></tr>
            <tr>
                <td align="center" valign="top">
                    <table border="0" cellpadding="20" cellspacing="0" width="520" id="emailContainer" style="background-color: white">
                        <tr>
                            <td valign="top">
                                <p>Prezado(a) <b>' . $objParticipante->getnome() . '</b> </p>
                                <p>Ficamos muito felizes com seu interesse em participar do evento <b>' . $objEvento->gettitulo() . '</b></p>
                                <p>Para confirmar sua inscrição clique <a href="' . $SiteHTTP . 'confirmar-inscricao/' . $objEvento->getid() . '/' . $objParticipante->getid() . '/' . $objInscricao->getid() . '" style="font-weight:bold">AQUI</a></p>

                                <p>Atenciosamente,</p>
                                <p>Barteles Eventos</p>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <!--<tr height="20px"></tr>
            <table border="0" cellpadding="20" cellspacing="0" width="580" id="emailFooter">
                <tr>
                    <td align="right">
                        <a href="http://www.douglasbarteles.com.br" target="blank">
                            <img src="' . $SiteHTTP . 'assets/layout/desenvolvido-por.png">
                        </a>
                    </td>
                </tr>
            </table>-->
        </table>
    </body>
</html>

');

            if (!$mail->Send()) {
                echo "Mensagem não enviada";
                echo "Mailer Error: " . $mail->ErrorInfo;

                header("location:" . $SiteHTTP . "cadastro/erro/Erro inesperado. Tente novamente mais tarde.");
                //echo "Erro inesperado. Tente novamente mais tarde.";

            } else {

                //DIRECIONA PARA A PÁGINA DE LOGIN
                header("location:" . $SiteHTTP . "acessar/sucesso/Sua pré-inscrição foi realizada com sucesso! Acesse o email que acabamos de enviar para sua caixa e clique no link para confirmar sua inscrição.");

                //echo "Sua pré-inscrição foi realizada com sucesso! Acesse o email que acabamos de enviar para sua caixa e clique no link para confirmar sua inscrição.";

            }

        }

    }

}
