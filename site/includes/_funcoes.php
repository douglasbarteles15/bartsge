<?php
session_start();
function UrlSite() {
	$server = $_SERVER['SERVER_NAME']; 
	$endereco = $_SERVER ['REQUEST_URI'];
	return "http://" . $server;
}

function UrlAtual() {
	$server = $_SERVER['SERVER_NAME']; 
	$endereco = $_SERVER ['REQUEST_URI'];
	return "http://" . $server . $endereco;
}

function UrlPag() {
	$endereco = $_SERVER ['REQUEST_URI'];
	return $endereco;
}
$base = dirname(__FILE__);
$base = realpath("$base/../../");
require "$base/comuns/config.php";

ini_set("error_reporting", E_ALL & ~E_NOTICE);

set_time_limit(120);

//setlocale(LC_ALL, "pt_BR");
//header("Content-type: text/html; charset=iso-8859-1", true);

if (!$_SESSION["cliente"]) {
    $_SESSION["cliente"] = "nao_identificado";
}

function getClass($classe) {
//*********************************************************************************************
//	$dirClasses - � o �nico par�metro que deve ser configurado, � o caminho a partir do root
//	at� a pasta onde est�o as classes
    $dirClasses = "comuns/classes/";

//**********************************************************************************************

    $niveis = substr_count($_SERVER["PHP_SELF"], "/");
    $root = "";
    $classPath = "";

    for ($i = 1; $i < $niveis; $i++) {
        if (file_exists($root . $dirClasses))
            break;
        else
            $root .= "../";
    }

    $classPath = $root . $dirClasses;

    if (is_dir($classPath)) {
        if (file_exists($classPath . "class/class." . $classe . ".php")) {
            return $classPath . "class/class." . $classe . ".php"; // Classes espec�ficas - criptografia, banco...
        } elseif (file_exists($classPath . "DAO/" . $classe . ".php")) {
            return $classPath . "DAO/" . $classe . ".php"; // Classes do gerador de classes
        } elseif (file_exists($classPath . "EXT/" . $classe . ".php")) {
            return $classPath . "EXT/" . $classe . ".php"; // Classes que herdam as classes do gerador
        }
    } else {
        return false;
    }
}

function __autoload($className) {//Fun��o que ir� carregas as classes do projeto automatica e dinamicamente - s� funciona PHP 5
    $class = getClass($className);

    if (file_exists($class)) {
        require_once($class);
    }
}

$objBanco = new Database();
$objCrypt = new Crypt();

$h = new Helper();

$h->identificaAcesso();

function formataData($data) {

    $data = explode("-", $data);

    $ano = $data[0];
    $mes = $data[1];
    $dia = $data[2];

    switch ($mes) {

        case 1: $mes = "Janeiro";
            break;
        case 2: $mes = "Fevereiro";
            break;
        case 3: $mes = "Março";
            break;
        case 4: $mes = "Abril";
            break;
        case 5: $mes = "Maio";
            break;
        case 6: $mes = "Junho";
            break;
        case 7: $mes = "Julho";
            break;
        case 8: $mes = "Agosto";
            break;
        case 9: $mes = "Setembro";
            break;
        case 10: $mes = "Outubro";
            break;
        case 11: $mes = "Novembro";
            break;
        case 12: $mes = "Dezembro";
            break;
    }

    $mes = strtolower($mes);
    print ("$dia de $mes de $ano");
}

function formataMes($data) {

    $data = explode("-", $data);

    switch ($data[1]) {
        case 1:
            $mes = "Janeiro";
            break;
        case 2:
            $mes = "Fevereiro";
            break;
        case 3:
            $mes = "Março";
            break;
        case 4:
            $mes = "Abril";
            break;
        case 5:
            $mes = "Maio";
            break;
        case 6:
            $mes = "Junho";
            break;
        case 7:
            $mes = "Julho";
            break;
        case 8:
            $mes = "Agosto";
            break;
        case 9:
            $mes = "Setembro";
            break;
        case 10:
            $mes = "Outubro";
            break;
        case 11:
            $mes = "Novembro";
            break;
        case 12:
            $mes = "Dezembro";
            break;
    }

    if ($mes == "") {
        $mes = "Agosto";
    }

    $dataextenso = "$mes";

    return $dataextenso;
}

function formata_data_extenso($strDate) {
// Array com os dia da semana em português;
    $arrDaysOfWeek = array('Domingo', 'Segunda-feira', 'Terça-feira', 'Quarta-feira', 'Quinta-feira', 'Sexta-feira', 'Sábado');
// Array com os meses do ano em português;
    $arrMonthsOfYear = array(1 => 'Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio', 'Junho', 'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro');
// Descobre o dia da semana
    $intDayOfWeek = date('w', strtotime($strDate));
// Descobre o dia do mês
    $intDayOfMonth = date('d', strtotime($strDate));
// Descobre o mês
    $intMonthOfYear = date('n', strtotime($strDate));
// Descobre o ano
    $intYear = date('Y', strtotime($strDate));
// Formato a ser retornado
    return $arrDaysOfWeek[$intDayOfWeek] . ', ' . $intDayOfMonth . ' de ' . $arrMonthsOfYear[$intMonthOfYear] . ' de ' . $intYear;
}

//Formata o Video do Youtube


function criaYoutube($string, $largura, $altura) {
// force http: on www.
    $string = str_ireplace("www.", "http://www.", $string);
// eliminate duplicates after force
    $string = str_ireplace("http://http://www.", "http://www.", $string);
    $string = str_ireplace("https://http://www.", "https://www.", $string);

// The Regular Expression filter
    $reg_exUrl = "/(http|https|ftp|ftps)\:\/\/[a-zA-Z0-9\-\.]+\.[a-zA-Z]{2,3}(\/\S*)?/";
// Check if there is a url in the text

    $m = preg_match_all($reg_exUrl, $string, $match);

    if ($m) {
        $links = $match[0];
        for ($j = 0; $j < $m; $j++) {

            if (substr($links[$j], 0, 18) == 'http://www.youtube') {

                $string = '<iframe title="YouTube video player" class="youtube-player" type="text/html" width="' . $largura . '" height="' . $altura . '" src="http://www.youtube.com/embed/' . substr($links[$j], -11) . '" frameborder="0" allowFullScreen></iframe><br />';
//                $string = str_replace($links[$j], '<a href="' . $links[$j] . '" rel="nofollow" target="_blank">' . $links[$j] . '</a>', $string) . '<br /><iframe title="YouTube video player" class="youtube-player" type="text/html" width="'.$largura.'" height="'.$altura.'" src="http://www.youtube.com/embed/' . substr($links[$j], -11) . '" frameborder="0" allowFullScreen></iframe><br />';
            } else {

                $string = str_replace($links[$j], '<a href="' . $links[$j] . '" rel="nofollow" target="_blank">' . $links[$j] . '</a>', $string);
//                $string = str_replace($links[$j], '<a href="' . $links[$j] . '" rel="nofollow" target="_blank">' . $links[$j] . '</a>', $string);
            }
        }
    }

    return ($string);
}

//Encurtar URL
function encurtar_url($url) {
    $url = trim($url);
    $url = urlencode($url);
    $shorted_url = file_get_contents('http://migre.me/api.txt?url=' . $url);
    return $shorted_url;
}

?>