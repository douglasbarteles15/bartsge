<?php
require("site/includes/funcoes.php");

$inscricao_id = FDados("inscricao");
$objInscricoes = new DAO_Inscricoes();
$objInscricoes->select($inscricao_id);
$objEvento = new DAO_Eventos();
$objEvento->select($objInscricoes->getevento_id_INT());

if ($_SESSION["logado_site"] != "logado") {
    header("location:" . $SiteHTTP . "acessar/erro/Você precisa estar logado pra acessar essa área.");
} else {
    if($objInscricoes->getparticipante_id_INT() == $_SESSION["participante"]) {
        $objParticipanteLogado = new DAO_Participantes();
        $objParticipanteLogado->select($_SESSION["participante"]);
    }else{
        header("location:" . $SiteHTTP . "area-participante/erro/Aconteceu algo de errado ao solicitar seu certificado.");
    }
}
$data_atual = date("Y-m-d");
?>
<!DOCTYPE html>
<html>
<head>
    <title>Certificado - Área do Participante</title>
    <?php include('meta.php'); ?>
    <style>
        @media print {
            body * {
                visibility: hidden;
            }
            #printable, #printable *, .box-footer {
                visibility: visible;
            }
        }
    </style>
</head>
<body>
<?php include('header-fixo.php'); ?>
<section class="section-page">
    <header>
        <div class="title">
            <h1>Certificado</h1>
            <hr>
        </div>
    </header>
    <div class="box box-primary" style="font-family: cursive; padding: 0;" id="printable">
        <div class="box-header">
            <h3 class="box-title">Certificado de Participação em Evento</h3>
        </div>
        <hr>
        <div class="box-body no-padding">
            <h4><?=$objEvento->gettitulo()?></h4>
            <h3><?=$objParticipanteLogado->getnome()?></h3>
            <p>
                <strong><?=$objEvento->FDataExibe($objEvento->getdata_inicio())?>
                    <?php if($objEvento->getdata_final()!=$objEvento->getdata_inicio()){
                        echo ' até '.$objEvento->FDataExibe($objEvento->getdata_final());
                    } ?>
                </strong>
            </p>
            <p>de <?=substr($objEvento->gethora_inicio(), 0, -3)?>h às <?=substr($objEvento->gethora_final(), 0, -3)?>h</p>
        </div>
        <div class="box-footer" style="background-color: #202020; padding-top: 10px; padding-bottom: 10px">
            <img src="/front-end/layout/logo.png">
        </div>

    </div>

    <a onclick="window.print();" class="bt-ver" style="cursor: pointer;">Imprimir</a>

</section>
<?php //include('depoimentos.php'); ?>
<?php include('footer.php') ?>
</body>
</html>