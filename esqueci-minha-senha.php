<?php
require("site/includes/funcoes.php");
?>
<!DOCTYPE html>
<html>
<head>
	<title>Esqueci minha senha</title>
	<?php include('meta.php'); ?>
</head>
<body>
	<?php include('header-fixo.php'); ?>
	<section class="section-page">
		<header>
			<div class="title">
				<h1>Esqueci a senha</h1>
				<hr>
			</div>
		</header>
		<div class="content-main-form">
			<article class="page-access">
				<div class="user-newpass">
					<div class="list-create">
						<div class="text-box">
							Preencha os campos abaixo para recuperar sua senha
						</div>
						<div class="box-form-pass">
							<form method="post" action="recupera-senha">
								<input type="email" name="email" id="email" placeholder="E-mail" required><br>
								<a class="voltar" href="acessar">Cancelar</a>
								<input type="submit" name="enviar" id="enviar" value="Enviar">
								
							</form>
						</div>
					</div>
				</div>
			</article>
		</div>
	</section>
	<?php //include('depoimentos.php'); ?>
	<?php include('footer.php') ?>
</body>
</html>