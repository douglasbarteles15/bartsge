$(document).ready(function(e) {
	$( "#acord" ).accordion();
	$('.slide').bxSlider({pager:false, mode:'fade', auto:true, captions: true});
	$('#close').click(function(){
		$("#control-nav").prop("checked",false);
	});	
	// ler mais curso
	$('#minimizar').hide();
	var height_boxcurso = $('#box-descricao-curso').height();
	$('#box-descricao-curso').css('height',100+'px');
	$('#lermais').click(function(){
		$('#box-descricao-curso').css('height',height_boxcurso+20+'px');
		$('#lermais').hide();
		$('#minimizar').show();
	});
	$('#minimizar').click(function(){
		$('#box-descricao-curso').css('height',100+'px');
		$('#lermais').show();
		$('#minimizar').hide();
	});
});