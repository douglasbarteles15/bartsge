<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.maskedinput/1.4.1/jquery.maskedinput.js"></script>
<!-- daterangepicker -->
<script src="/admin/js/plugins/daterangepicker/daterangepicker.js" type="text/javascript"></script>
<script src="https://code.jquery.com/ui/1.12.0/jquery-ui.js"></script>
<script>
    jQuery(function ($) {
        $("#cep").mask("99.999-999");
        $("#cpf").mask("999.999.999-99");
        $("#cnpj").mask("99.999.999/9999-99");
        $('#datetime').mask('99/99/9999 99:99:99');
        $('.hora').mask('99:99');
    });

    $(".data").datepicker({
        dateFormat: 'dd/mm/yy',
        dayNames: ['Domingo','Segunda','Ter�a','Quarta','Quinta','Sexta','S�bado'],
        dayNamesMin: ['D','S','T','Q','Q','S','S','D'],
        dayNamesShort: ['Dom','Seg','Ter','Qua','Qui','Sex','S�b','Dom'],
        monthNames: ['Janeiro','Fevereiro','Mar�o','Abril','Maio','Junho','Julho','Agosto','Setembro','Outubro','Novembro','Dezembro'],
        monthNamesShort: ['Jan','Fev','Mar','Abr','Mai','Jun','Jul','Ago','Set','Out','Nov','Dez'],
        nextText: 'Pr�ximo',
        prevText: 'Anterior'
    });

    $('.datas').daterangepicker(
        {
            format: 'DD/MM/YYYY',
            language: 'pt-BR'
        });

    //<![CDATA[
    jQuery(document).ready(function () {
        //Inicio Mascara Telefone
        jQuery('input[type=tel]').mask("(99) 9999-9999?9").ready(function (event) {
            var target, phone, element;
            target = (event.currentTarget) ? event.currentTarget : event.srcElement;
            phone = target.value.replace(/\D/g, '');
            element = $(target);
            element.unmask();
            if (phone.length > 10) {
                element.mask("(99) 99999-999?9");
            } else {
                element.mask("(99) 9999-9999?9");
            }
        });
        //Fim Mascara Telefone
    });
    (jQuery);
    //]]>
</script>
<!-- Bootstrap -->
<script src="/admin/js/bootstrap.min.js" type="text/javascript"></script>
<!-- Morris.js charts -->
<script src="//cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
<script src="/admin/js/plugins/morris/morris.min.js" type="text/javascript"></script>
<!-- Sparkline -->
<script src="/admin/js/plugins/sparkline/jquery.sparkline.min.js" type="text/javascript"></script>
<!-- jvectormap -->
<script src="/admin/js/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js" type="text/javascript"></script>
<script src="/admin/js/plugins/jvectormap/jquery-jvectormap-world-mill-en.js" type="text/javascript"></script>
<!-- fullCalendar -->
<script src="/admin/js/plugins/fullcalendar/fullcalendar.js" type="text/javascript"></script>
<!-- jQuery Knob Chart -->
<script src="/admin/js/plugins/jqueryKnob/jquery.knob.js" type="text/javascript"></script>
<!-- bootstrap color picker -->
<script src="/admin/js/plugins/colorpicker/bootstrap-colorpicker.min.js" type="text/javascript"></script>
<!-- bootstrap time picker -->
<script src="/admin/js/plugins/timepicker/bootstrap-timepicker.min.js" type="text/javascript"></script>
<!-- Bootstrap WYSIHTML5 -->
<script src="/admin/js/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js" type="text/javascript"></script>
<!-- iCheck -->
<script src="/admin/js/plugins/iCheck/icheck.min.js" type="text/javascript"></script>

<script src="/admin/js/jquery.Jcrop.min.js" type="text/javascript"></script>

<!-- AdminLTE App -->
<script src="/admin/js/AdminLTE/app.js" type="text/javascript"></script>

<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="/admin/js/AdminLTE/dashboard.js" type="text/javascript"></script>

<script language='javascript' type='text/javascript' src='/comuns/libs/ckeditor/ckeditor.js'></script>
<script>
    CKEDITOR.replace('editor1');
    CKEDITOR.replace('editor2');
</script>