<?php
require("../site/includes/funcoes.php");
//Verificar a permissão de acesso
if ($_SESSION["logado"] == "logado") {
    $objUser = new EXT_Usuarios_Admin();
    $objUser->select($_SESSION["usuario"]);
    header("location:" . $SiteHTTP . "admin/sistema");
}
?>
<!DOCTYPE html>
<html class="bg-black">
    <head>
        <?php include ("metas.php"); ?>
        <title>Administração - <?=$objConfig_site->getrazao_social()?></title>
    </head>
    <body class="bg-black">

        <?php require("../comuns/libs/ajax/php/divRetorno_novo.php"); ?>
        

        <div class="form-box" id="login-box">
            <div class="header">
                <span style="font-size: 0.8em">Área Administrativa</span>
                <div style="clear: both"></div>
                <?php if($objConfig_site->getmarcaDagua()!=""){ ?>
                    <img src="<?=$SiteHTTP?>site/uploads/img_marcadagua/<?=$objConfig_site->getmarcaDagua()?>" style="width: 250px">
                <?php } ?>
            </div>
            <form action="login/" method="post">
                <div class="body body bg-gray">
                    <div class="form-group">
                        <input type="text" name="email" class="form-control" placeholder="login" required/>
                    </div>
                    <div class="form-group">
                        <input type="password" name="senha" class="form-control" placeholder="senha" required/>
                    </div>          
                    <div class="form-group">
                        <a href="esqueci-minha-senha">Esqueci minha senha</a>
                    </div>
                </div>
                <div class="footer">                                                               
                    <button type="submit" class="btn bg-plugin btn-block">Realizar o login</button>
                    <!--<p><a href="#">Lembrar senha</a></p>-->
                </div>
            </form>
        </div>
        <?php include ("rodape.php"); ?>
    </body>
</html>