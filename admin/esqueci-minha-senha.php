<?php
require("../site/includes/funcoes.php");
?>
<!DOCTYPE html>
<html class="bg-black">
<head>
    <?php include("metas.php"); ?>
    <title>Esqueci minha senha - Administração - <?= $objConfig_site->getrazao_social() ?></title>
</head>
<body class="bg-black">

<?php require("../comuns/libs/ajax/php/divRetorno_novo.php"); ?>


<div class="form-box" id="login-box">
    <div class="header">
        <span style="font-size: 0.8em">Área Administrativa</span>

        <div style="clear: both"></div>
        <?php if ($objConfig_site->getmarcaDagua() != "") { ?>
            <img src="<?= $SiteHTTP ?>site/uploads/img_marcadagua/<?= $objConfig_site->getmarcaDagua() ?>"
                 style="width: 203px">
        <?php } ?>
        <br><span style="font-size: 0.5em">Lembrar Senha</span>
    </div>
    <form action="esqueci-senha/" method="post">
        <div class="body body bg-gray">
            <div class="form-group">
                <input type="email" name="email" class="form-control" placeholder="email" required/>
            </div>
        </div>
        <div class="footer">
            <button type="submit" class="btn bg-plugin btn-block">Recuperar</button>
            <a href="/admin" class="btn bg-red btn-block">Voltar</a>
        </div>
    </form>
</div>

<!-- jQuery 2.0.2 -->
<script src="http://ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>
<!-- Bootstrap -->
<script src="js/bootstrap.min.js" type="text/javascript"></script>

</body>
</html>