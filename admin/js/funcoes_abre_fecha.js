function mostrarCamposExtras() {
    $("#campos_extras").show("fast", function () {
        // Animation complete.
    });
}
function esconderCamposExtras() {
    $("#campos_extras").hide("fast", function () {
        // Animation complete.
    });
}

function abreSecao(caminho,id) {
    $("#" + id).toggle("fast", function () {
        var src = ($("#mais_" + id).attr('src') === caminho + '/images/icone_mais.png')
                ? caminho + '/images/icone_menos.jpg'
                : caminho + '/images/icone_mais.png';
        $("#mais_" + id).attr('src', src);
    });
}

function abreEspecialidades(numero,categoria) {
    $.ajax({
        url: "/admin/sistema/busca-especialidades",
        type: "post",
        data: 'categoria='+categoria ,
        dataType: "html",
        success: function(data) {
            //console.log(data);
            if(data != ""){

                $('#especialidades'+numero).html(data);
                $('#especialidades'+numero).fadeIn('fast');

            }else{
                $('#especialidades'+numero).html('');
                $('#especialidades'+numero).fadeOut('fast');
            }
        },
        error: function(e)
        {
            alert('Error: ' + e);
        }
    });
}

function abreCategoria2(){
    $('#categoria2').fadeIn('fast');
    $('#btn_mais1').fadeOut('fast');
}

function abreCategoria3(){
    $('#categoria3').fadeIn('fast');
    $('#btn_mais2').fadeOut('fast');
}

function adicionaDia(id) {
    $("#btn_add_dia" + id).toggle("fast", function () {
        $("#dias_semana" + id).toggle("fast", function () {
        });
    });
}
function adicionaDependencia(id) {
    $("#btn_add_dependencia" + id).toggle("fast", function () {
        $("#dependencia" + id).toggle("fast", function () {
        });
    });
}
function adicionaAluno(id) {
    $("#btn_add_aluno" + id).toggle("fast", function () {
        $("#aluno" + id).toggle("fast", function () {
        });
    });
}
function adicionaDocumento(id) {
    $("#btn_add_documento" + id).toggle("fast", function () {
        $("#documento" + id).toggle("fast", function () {
        });
    });
}
function adicionaMarcacao(id) {
    $("#btn_add_marcacao" + id).toggle("fast", function () {
        $("#marcacao" + id).toggle("fast", function () {
        });
    });
}

function excluirDocumento(id_div, id_arquivo, id_modalidade){
    resposta = confirm("Deseja realmente excluir esse documento?");
    if (resposta){
        $.ajax({
            type: "POST",
            data: {
                id_arquivo: id_arquivo,
                id_modalidade: id_modalidade
            },
            url: "../validacoes/acoes/excluir_arquivo.php",
            success: function(data) {
                $("#documento"+id_div).hide("fast", function () {
                        alert("Documento excluído com sucesso!");
                    });
                
            },
            error: function(){
                alert("Houve algum erro ao excluir!");
            }
        });
    }
}

function excluirAvaliacaoMarcacao(id_div, id_marcacao){
    resposta = confirm("Deseja realmente excluir essa marcação?");
    if (resposta){
        $.ajax({
            type: "POST",
            data: {
                id_marcacao: id_marcacao
            },
            url: "../validacoes/acoes/excluir_avaliacao_marcacao.php",
            success: function(data) {
                $("#marcacao"+id_div).hide("fast", function () {
                        alert("Marcação excluída com sucesso!");
                    });
                
            },
            error: function(){
                alert("Houve algum erro ao excluir!");
            }
        });
    }
}

function seleciona_evento(){
    $("#seleciona_evento").submit();
}

function seleciona_sexo(){
    $("#seleciona_sexo").submit();
}

function seleciona_estado(){
    $("#seleciona_estado").submit();
}

function seleciona_cidade(){
    $("#seleciona_cidade").submit();
}