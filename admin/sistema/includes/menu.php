<section class="sidebar">
    <!-- Sidebar user panel -->
    <div class="user-panel">
        <div class="pull-left info">
            <p>Olá, <?= $objUser->getnome(); ?></p>
        </div>
    </div>
    <!-- sidebar menu: : style can be found in sidebar.less -->
    <ul class="sidebar-menu">
        <li id="home">
            <a href="<?= $SiteHTTP ?>admin/sistema">
                <i class="fa fa-home"></i> <span>Home</span>
            </a>
        </li>

        <?php if ($_SESSION['tipo_usuario'] == 1) { ?>

            <li class="treeview" id="categorias_menu">
                <?php
                $buscaCategorias = "Select COUNT(id) as total from categorias";
                $objBancoCategorias = new Database();
                $conta = $objBancoCategorias->FecthAssoc($objBancoCategorias->Query($buscaCategorias));
                ?>
                <a href="#">
                    <i class="fa fa-th-list"></i>
                    <span>Categorias</span>
                    <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li id="cadastrar_categorias"><a href="<?= $SiteHTTP ?>admin/sistema/cadastrar-categorias"><i class="fa fa-floppy-o"></i> Cadastrar </a></li>
                    <li id="gerenciar_categorias"><a href="<?= $SiteHTTP ?>admin/sistema/gerenciar-categorias"><i class="fa fa-archive"></i> Gerenciar <small class="badge pull-right bg-red"><?= $conta['total']; ?></small></a></li>
                </ul>
            </li>

            <li class="treeview" id="instrutores">
                <?php
                $sqlInstrutores = "Select COUNT(id) as total from instrutores";
                $objBancoInstrutores = new Database();
                $conta = $objBancoInstrutores->FecthAssoc($objBancoInstrutores->Query($sqlInstrutores));
                ?>
                <a href="#">
                    <i class="fa fa-users"></i>
                    <span>Instrutores</span>
                    <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li id="cadastrar_instrutores"><a href="<?= $SiteHTTP ?>admin/sistema/cadastrar-instrutores"><i
                                class="fa fa-floppy-o"></i> Cadastrar </a></li>
                    <li id="gerenciar_instrutores"><a href="<?= $SiteHTTP ?>admin/sistema/gerenciar-instrutores"><i
                                class="fa fa-archive"></i> Gerenciar
                            <small class="badge pull-right bg-red"><?= $conta['total']; ?></small>
                        </a></li>
                </ul>
            </li>

            <li class="treeview" id="eventos">
                <?php
                $buscaEventos = "Select COUNT(id) as total_eventos from eventos";
                $objBancoEventos = new Database();
                $conta_eventos = $objBancoEventos->FecthAssoc($objBancoEventos->Query($buscaEventos));
                ?>
                <a href="#">
                    <i class="fa fa-calendar"></i>
                    <span>Eventos</span>
                    <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li id="cadastrar_eventos"><a href="<?= $SiteHTTP ?>admin/sistema/cadastrar-eventos"><i class="fa fa-floppy-o"></i> Cadastrar </a></li>
                    <li id="gerenciar_eventos"><a href="<?= $SiteHTTP ?>admin/sistema/gerenciar-eventos"><i class="fa fa-archive"></i> Gerenciar <small class="badge pull-right bg-red"><?= $conta_eventos['total_eventos']; ?></small></a></li>
                </ul>
            </li>

            <!--<li class="treeview" id="subeventos">
                <?php
                $buscaSubEventos = "Select COUNT(id) as total_subeventos from subeventos";
                $objBancoSubEventos = new Database();
                $conta_subeventos = $objBancoSubEventos->FecthAssoc($objBancoSubEventos->Query($buscaSubEventos));
                ?>
                <a href="#">
                    <i class="fa fa-th-list"></i>
                    <span>SubEventos</span>
                    <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li id="cadastrar_subeventos"><a href="<?= $SiteHTTP ?>admin/sistema/cadastrar-subeventos"><i class="fa fa-floppy-o"></i> Cadastrar </a></li>
                    <li id="gerenciar_subeventos"><a href="<?= $SiteHTTP ?>admin/sistema/gerenciar-subeventos"><i class="fa fa-archive"></i> Gerenciar <small class="badge pull-right bg-red"><?= $conta_subeventos['total_subeventos']; ?></small></a></li>
                </ul>
            </li>-->

            <li class="treeview" id="inscricoes">
                <a href="#">
                    <i class="fa fa-th-list"></i>
                    <span>Inscrições</span>
                    <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li id="gerenciar_inscricoes"><a href="<?= $SiteHTTP ?>admin/sistema/gerenciar-inscricoes"><i class="fa fa-archive"></i> Gerenciar</a></li>
                </ul>
            </li>

            <li class="treeview" id="presenca">
                <a href="#">
                    <i class="fa fa-check"></i>
                    <span>Presença</span>
                    <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li id="gerenciar_presenca"><a href="<?= $SiteHTTP ?>admin/sistema/gerenciar-presenca"><i class="fa fa-archive"></i> Controlar</a></li>
                </ul>
            </li>

            <li class="treeview" id="relatorios">
                <a href="#">
                    <i class="fa fa-bar-chart"></i>
                    <span>Relatórios</span>
                    <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li id="relatorio_inscricoes"><a href="<?= $SiteHTTP ?>admin/sistema/relatorio-inscricoes"><i
                                class="fa fa-pie-chart"></i> Inscrições
                        </a></li>
                </ul>
            </li>

            <li class="treeview" id="noticias">
                <?php
                $buscaNoticias = "Select COUNT(id) as total from noticias";
                $objBancoNoticias = new Database();
                $conta = $objBancoNoticias->FecthAssoc($objBancoNoticias->Query($buscaNoticias));
                ?>
                <a href="#">
                    <i class="fa fa-rss"></i>
                    <span>Notícias</span>
                    <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li id="cadastrar_noticias"><a href="<?= $SiteHTTP ?>admin/sistema/cadastrar-noticias"><i class="fa fa-floppy-o"></i> Cadastrar </a></li>
                    <li id="gerenciar_noticias"><a href="<?= $SiteHTTP ?>admin/sistema/gerenciar-noticias"><i class="fa fa-archive"></i> Gerenciar <small class="badge pull-right bg-red"><?= $conta['total']; ?></small></a></li>
                </ul>
            </li>

            <li class="treeview" id="usuarios_admin">
                <?php
                $sqlUsuarios_Admin = "Select COUNT(id) as total from usuarios_admin";
                $objBancoUsuarios_Admin = new Database();
                $conta = $objBancoUsuarios_Admin->FecthAssoc($objBancoUsuarios_Admin->Query($sqlUsuarios_Admin));
                ?>
                <a href="#">
                    <i class="fa fa-users"></i>
                    <span>Organizadores</span>
                    <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li id="cadastrar_usuarios_admin"><a href="<?= $SiteHTTP ?>admin/sistema/cadastrar-usuarios-admin"><i
                                class="fa fa-floppy-o"></i> Cadastrar </a></li>
                    <li id="gerenciar_usuarios_admin"><a href="<?= $SiteHTTP ?>admin/sistema/gerenciar-usuarios-admin"><i
                                class="fa fa-archive"></i> Gerenciar
                            <small class="badge pull-right bg-red"><?= $conta['total']; ?></small>
                        </a></li>
                </ul>
            </li>

            <li class="treeview" id="configuracoes">
                <a href="#">
                    <i class="fa fa-cog"></i>
                    <span>Configurações</span>
                    <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li id="editar_configuracoes">
                        <a href="<?= $SiteHTTP ?>admin/sistema/editar-configuracoes"><i class="fa fa-cog"></i>
                            Configurações de Sistema </a>
                    </li>
                </ul>
            </li>

        <?php } elseif ($_SESSION['tipo_usuario'] == 2) { ?>

            <li class="treeview" id="categorias_menu">
                <?php
                $buscaCategorias = "Select COUNT(id) as total from categorias";
                $objBancoCategorias = new Database();
                $conta = $objBancoCategorias->FecthAssoc($objBancoCategorias->Query($buscaCategorias));
                ?>
                <a href="#">
                    <i class="fa fa-th-list"></i>
                    <span>Categorias</span>
                    <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li id="cadastrar_categorias"><a href="<?= $SiteHTTP ?>admin/sistema/cadastrar-categorias"><i class="fa fa-floppy-o"></i> Cadastrar </a></li>
                    <li id="gerenciar_categorias"><a href="<?= $SiteHTTP ?>admin/sistema/gerenciar-categorias"><i class="fa fa-archive"></i> Gerenciar <small class="badge pull-right bg-red"><?= $conta['total']; ?></small></a></li>
                </ul>
            </li>

            <li class="treeview" id="instrutores">
                <?php
                $sqlInstrutores = "Select COUNT(id) as total from instrutores";
                $objBancoInstrutores = new Database();
                $conta = $objBancoInstrutores->FecthAssoc($objBancoInstrutores->Query($sqlInstrutores));
                ?>
                <a href="#">
                    <i class="fa fa-users"></i>
                    <span>Instrutores</span>
                    <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li id="cadastrar_instrutores"><a href="<?= $SiteHTTP ?>admin/sistema/cadastrar-instrutores"><i
                                class="fa fa-floppy-o"></i> Cadastrar </a></li>
                    <li id="gerenciar_instrutores"><a href="<?= $SiteHTTP ?>admin/sistema/gerenciar-instrutores"><i
                                class="fa fa-archive"></i> Gerenciar
                            <small class="badge pull-right bg-red"><?= $conta['total']; ?></small>
                        </a></li>
                </ul>
            </li>

            <li class="treeview" id="eventos">
                <?php
                $buscaEventos = "Select COUNT(id) as total_eventos from eventos";
                $objBancoEventos = new Database();
                $conta_eventos = $objBancoEventos->FecthAssoc($objBancoEventos->Query($buscaEventos));
                ?>
                <a href="#">
                    <i class="fa fa-calendar"></i>
                    <span>Eventos</span>
                    <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li id="cadastrar_eventos"><a href="<?= $SiteHTTP ?>admin/sistema/cadastrar-eventos"><i class="fa fa-floppy-o"></i> Cadastrar </a></li>
                    <li id="gerenciar_eventos"><a href="<?= $SiteHTTP ?>admin/sistema/gerenciar-eventos"><i class="fa fa-archive"></i> Gerenciar <small class="badge pull-right bg-red"><?= $conta_eventos['total_eventos']; ?></small></a></li>
                </ul>
            </li>

            <!--<li class="treeview" id="subeventos">
                <?php
            $buscaSubEventos = "Select COUNT(id) as total_subeventos from subeventos";
            $objBancoSubEventos = new Database();
            $conta_subeventos = $objBancoSubEventos->FecthAssoc($objBancoSubEventos->Query($buscaSubEventos));
            ?>
                <a href="#">
                    <i class="fa fa-th-list"></i>
                    <span>SubEventos</span>
                    <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li id="cadastrar_subeventos"><a href="<?= $SiteHTTP ?>admin/sistema/cadastrar-subeventos"><i class="fa fa-floppy-o"></i> Cadastrar </a></li>
                    <li id="gerenciar_subeventos"><a href="<?= $SiteHTTP ?>admin/sistema/gerenciar-subeventos"><i class="fa fa-archive"></i> Gerenciar <small class="badge pull-right bg-red"><?= $conta_subeventos['total_subeventos']; ?></small></a></li>
                </ul>
            </li>-->

            <li class="treeview" id="inscricoes">
                <a href="#">
                    <i class="fa fa-th-list"></i>
                    <span>Inscrições</span>
                    <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li id="gerenciar_inscricoes"><a href="<?= $SiteHTTP ?>admin/sistema/gerenciar-inscricoes"><i class="fa fa-archive"></i> Gerenciar</a></li>
                </ul>
            </li>

            <li class="treeview" id="presenca">
                <a href="#">
                    <i class="fa fa-check"></i>
                    <span>Presença</span>
                    <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li id="gerenciar_presenca"><a href="<?= $SiteHTTP ?>admin/sistema/gerenciar-presenca"><i class="fa fa-archive"></i> Controlar</a></li>
                </ul>
            </li>

        <?php } ?>

    </ul>
</section>