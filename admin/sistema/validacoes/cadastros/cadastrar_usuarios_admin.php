<?php
/**
 * Arquivo para realizar o cadastro de Usu�rios Admin
 * Sistema - BartSGE
 * @Autor Douglas Barteles - douglasbarteles@gmail.com
 */
ob_start();
require("../../../../site/includes/funcoes.php");

$obj = new EXT_Usuarios_Admin();

$objCrypt = new Crypt();

$objSite = new DAO_Config_site();
$objSite->select(1);

$nome = $_POST["nome"];

if ($_POST["nome"] != "" && $_POST["email"] != "") {

    $obj->setpost();

    $obj->setsenha($objCrypt->crypt64($_POST["senha"]));
    $obj->setlogin($_POST["email"]);

    $obj->dadosBanco();

    if ($obj->getEmailJaCadastrado($_POST["email"], "") == false) {

        $obj->createsession();

        header("location:/admin/sistema/cadastrar-usuarios-admin?msgAlert=O email informado ja esta cadastrado em nosso sistema para outro usuario. Informe outro email.");
    } else {
        $obj->settipo_id_INT($_POST["tipo_id_INT"]);
        $obj->insert();
        $obj->unsetsession();
        $obj->selectLastInsert();

        header("location:/admin/sistema/gerenciar-usuarios-admin?msgOk=Usuario Admin cadastrado com sucesso");
    }
} else {
    $obj->createsession();
    header("location:/admin/sistema/gerenciar-usuarios-admin?msgErro=Erro ao cadastrar usuario, preencha todos os campos");
}
?>
