<?php

/**
 * Arquivo para realizar o cadastro de Páginas
 * Site - JFProfissionais
 * @Autor Douglas Barteles - douglas@pluginweb.com.br
 */
ob_start();
require("../../../../site/includes/funcoes.php");

//Instancia os Objeto
$obj = new DAO_Paginas();

//Pega os dados do Formulário
$obj->setpost();

//Verifica os parametros para inserir no Banco
$obj->dadosBanco();

//Faz a inclusão no Banco de Dados
$obj->insert();
//Seleciona o último registro
$obj->selectLastInsert();

//Redirecionamento
header("location:/admin/sistema/gerenciar-paginas?msgOk=Página cadastrada com sucesso!");
?>
