<?php

/**
 * Arquivo para realizar o cadastro de Arquivos
 * Site - BartSGE
 * @Autor Douglas Barteles - douglasbarteles@gmail.com
 */
ob_start();
require("../../../../site/includes/funcoes.php");
$caminho = "../../../../site/uploads/arquivos/"; //local onde ser� salvo a imagem grande
//Instancia os Objeto
$obj = new DAO_Arquivos();

$subevento_id_INT = $_POST['subevento_id_INT'];

$arquivo = $_FILES['arquivo'];
//echo $arquivo['type'];
//break;
//echo $arquivo['tmp_name'];
//break;

if ($arquivo['tmp_name'] != NULL) {
    //echo "teste"; break;
    if ($arquivo['type'] == "application/pdf") {

        //Pega os dados do Formulário
        $obj->setpost();
        //Verifica os parametros para inserir no Banco
        $obj->dadosBanco();
        //Faz a inclusão no Banco de Dados
        $obj->insert();
        //Seleciona o último registro
        $obj->selectLastInsert();

        $id = $obj->getid();

        if ($arquivo['type'] == "application/pdf") {
            $novonome = time() . rand(1, 9) . '.pdf';
        }

        if (!file_exists($caminho . $id)) {
            mkdir($caminho . $id, 0777);
        }
        $caminho = $caminho . $id . "/" . $novonome;

        move_uploaded_file($arquivo['tmp_name'], $caminho);
        //echo "moveu";

        $obj->updatecampo($obj->getid(), "link", $id . "/" . $novonome, "Varchar");

        header("location:/admin/sistema/gerenciar-arquivos/".$subevento_id_INT."&msgOk=Arquivo cadastrado com sucesso!");

    } else {

        header("location:/admin/sistema/cadastrar-arquivos/".$subevento_id_INT."&msgErro=Formato de arquivo inválido! Cadastre um PDF");
    }

} else {

    header("location:/admin/sistema/cadastrar-arquivos/".$subevento_id_INT."&msgErro=Aconteceu algum erro! Arquivo não identificado.");

}
?>
