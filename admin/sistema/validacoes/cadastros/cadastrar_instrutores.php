<?php
/**
 * Arquivo para realizar o cadastro de Instrutores
 * Sistema - BartSGE
 * @Autor Douglas Barteles - douglasbarteles@gmail.com
 */
ob_start();
require("../../../../site/includes/funcoes.php");

$obj = new DAO_Instrutores();

$objCrypt = new Crypt();

$objSite = new DAO_Config_site();
$objSite->select(1);

$nome = $_POST["nome"];

if ($_POST["nome"] != "" && $_POST["email"] != "") {

//Verifica se o email já foi cadastrado
$verificaEmail = "SELECT email FROM instrutores WHERE email = '".FDados("email")."'";
$totalEmail = $obj->rows($verificaEmail);

if($totalEmail > 0){
    header("location:/admin/sistema/cadastrar-instrutores/O email que você digitou já foi utilizado por outro instrutor/palestrante!");
}else {

    $obj->setpost();

    $obj->setsenha($objCrypt->crypt64($_POST["senha"]));
    $obj->setlogin($_POST["email"]);

    $obj->dadosBanco();

    $obj->insert();
    $obj->unsetsession();
    $obj->selectLastInsert();

    header("location:/admin/sistema/gerenciar-instrutores?msgOk=Instrutor/Palestrante cadastrado com sucesso");

}

} else {

    header("location:/admin/sistema/gerenciar-instrutores?msgErro=Erro ao cadastrar Instrutor/Palestrante, preencha todos os campos");
}
?>
