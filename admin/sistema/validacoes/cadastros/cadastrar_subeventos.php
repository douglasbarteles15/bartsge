<?php

/**
 * Arquivo para realizar o cadastro de SubEventos
 * Site - BartSGE
 * @Autor Douglas Barteles - douglasbarteles@gmail.com
 */
ob_start();
require("../../../../site/includes/funcoes.php");

//Instancia os Objeto
$obj = new DAO_Subeventos();

$idEvento = FDados("evento_id_INT");

//Pega os dados do Formulário
$obj->setpost();
//Verifica os parametros para inserir no Banco
$obj->dadosBanco();
//Faz a inclusão no Banco de Dados
$obj->insert();
//Seleciona o último registro
$obj->selectLastInsert();

//Redirecionamento
header("location:/admin/sistema/gerenciar-subeventos/".$idEvento."&msgOk=SubEvento cadastrado com sucesso!");
?>
