<?php

/**
 * Arquivo para realizar o cadastro de Notícias
 * Site - BartSGE
 * @Autor Douglas Barteles - douglasbarteles@gmail.com
 */
ob_start();
require("../../../../site/includes/funcoes.php");
$caminho = "../../../../site/uploads/img_noticias/"; //local onde ser� salvo a imagem grande

//Instancia os Objeto
$obj = new DAO_Noticias();

//Pega os dados do Formulário
$obj->setpost();
//$obj->setnoticias_link($obj->montaURLNova($obj->montaURL($obj->getnoticias_titulo()), $obj->getnoticias_id()));
//Verifica os parametros para inserir no Banco
$obj->dadosBanco();
//Faz a inclusão no Banco de Dados

//Se Data vazia = Data Atual
if($_POST['data']==""){
    $data_atual = date('Y-m-d');
    $obj->setdata($data_atual);
}

$obj->insert();
//Seleciona o último registro
$obj->selectLastInsert();

//++++++++++++++++++++++++++++  Manipulando a imagem +++++++++++++++++++++
//Recebe a imagem
$imagem = isset($_FILES['imagem']) ? $_FILES['imagem'] : NULL;
$nomeImg = $imagem['tmp_name'];

$nome_final = time() . rand(1, 9);

if ($nomeImg != NULL) {

    //Chama a Class de Recorte
    require_once '../../recorte/ThumbLib.inc.php';

    //Monta a Miniatura
    $thumb = PhpThumbFactory::create("$nomeImg");
    $thumb->adaptiveResize(230,144);
    $id = $obj->getid();
    mkdir($caminho . $id, 0777);
    //mkdir("$caminho."/".$id", 0777);
    $thumb->save($caminho . "$id/" . $nome_final . '.jpg', 'jpg');

    //Monta a Imagem Full
    $thumb = PhpThumbFactory::create("$nomeImg");
    $thumb->adaptiveResize(465, 291);
    $thumb->save($caminho . "$id/" . "g_" . $nome_final . '.jpg', 'jpg');

    $imgMiniatura = $id."/" . $nome_final . '.jpg';
    $imgFull = $id."/" . 'g_' . $nome_final . '.jpg';

    $obj->updatecampo($obj->getid(), "img_mini", $imgMiniatura, "Varchar");
    $obj->updatecampo($obj->getid(), "img_full", $imgFull, "Varchar");
}

//Redirecionamento
header("location:/admin/sistema/gerenciar-noticias?msgOk=Notícia cadastrada com sucesso!");
?>
