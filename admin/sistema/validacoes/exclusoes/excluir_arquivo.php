<?php

/**
 * Arquivo para realizar a exclusão de Arquivos
 * Site - BartSGE
 * @Autor Douglas Barteles - douglasbarteles@gmail.com
 */
ob_start();
require("../../../../site/includes/funcoes.php");

$obj = new DAO_Arquivos();

$obj->select(FDados("id"));

$id = $obj->getid();

$objSubevento = new DAO_Subeventos();
$objSubevento->select($obj->getsubevento_id_INT());

$localArquivo = $obj->getlink();

$arquivo = "../../../../site/uploads/arquivos/" . $localArquivo;

if (file_exists($arquivo)) {
    @unlink($arquivo);
}

$obj->delete(FDados("id"));

//Redirecionamento
header("location:/admin/sistema/gerenciar-arquivos/".$objSubevento->getid()."&msgErro=Arquivo Deletado com sucesso!");


?>