<?php
/**
 * Arquivo para realizar o excluir Instrutores
 * Site - BartSGE
 * @Autor Douglas Barteles - douglasbarteles@gmail.com
 */
ob_start();
require("../../../../site/includes/funcoes.php");

$obj = new DAO_Instrutores();
$obj->delete(FDados("id"));

//Redirecionamento
header("location:/admin/sistema/gerenciar-instrutores?msgOk=Instrutor/Palestrante excluído com sucesso!");
?>