<?php

/**
 * Arquivo para realizar o excluir Contatos
 * Transparência - Mister Shopping
 * @Autor Douglas Barteles - programacao4@futurocomunicacao.com
 */
ob_start();
require("../../../../site/includes/funcoes.php");

$obj = new DAO_Contato();

$obj->delete(FDados("id"));


//Redirecionamento
header("location:../../php/gerenciar_contatos.php?msgErro=Contato Deletado com sucesso!");


?>