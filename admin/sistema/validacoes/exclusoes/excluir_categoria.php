<?php

/**
 * Arquivo para realizar o excluir Categorias
 * Site - BartSGE
 * @Autor Douglas Barteles - douglasbarteles@gmail.com
 */
ob_start();
require("../../../../site/includes/funcoes.php");

$obj = new DAO_Categorias();
$obj->select(FDados("id"));
$objBancoDeleta = new Database();

//Deleta os eventos da Categoria
$objBancoDeleta->Query("DELETE FROM eventos WHERE categoria_id_INT='".$obj->getid()."'");

$obj->delete(FDados("id"));

//Redirecionamento
header("location:/admin/sistema/gerenciar-categorias?msgErro=Categoria Deletada com sucesso!");

?>