<?php
/**
 * Arquivo para realizar o excluir Usuários Admin
 * Site - JF Profissionais
 * @Autor Douglas Barteles - douglas@pluginweb.com.br
 */
ob_start();
require("../../../../site/includes/funcoes.php");

$obj = new DAO_Usuarios_Admin();
$obj->delete(FDados("id"));

//Redirecionamento
header("location:/admin/sistema/gerenciar-usuarios-admin?msgOk=Usuário Admin excluído com sucesso!");
?>