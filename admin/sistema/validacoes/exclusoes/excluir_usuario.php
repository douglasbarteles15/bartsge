<?php
/**
 * Arquivo para realizar o excluir Usuários
 * Site - JFProfissionais
 * @Autor Douglas Barteles - douglas@pluginweb.com.br
 */
ob_start();
require("../../../../site/includes/funcoes.php");

$obj = new DAO_Usuarios();
$obj->delete(FDados("id"));

//Redirecionamento
header("location:/admin/sistema/gerenciar-usuarios?msgOk=Usuário excluído com sucesso!");
?>