<?php

/**
 * Arquivo para realizar o excluir Eventos
 * Site - BartSGE
 * @Autor Douglas Barteles - douglasbarteles@gmail.com
 */
ob_start();
require("../../../../site/includes/funcoes.php");

$obj = new DAO_Eventos();

$idEvento = FDados("id");

//Deleta os SubEventos do Evento
$objBanco->Query("DELETE FROM subeventos WHERE evento_id_INT='".$idEvento."'");

$obj->delete(FDados("id"));

//Redirecionamento
header("location:/admin/sistema/gerenciar-eventos?msgErro=Evento Deletado com sucesso!");

?>