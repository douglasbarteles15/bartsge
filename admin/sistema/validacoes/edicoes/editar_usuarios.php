<?php

/**
 * Arquivo para realizar a edição de Usuários
 * Site - JFProfissionais
 * @Autor Douglas Barteles - douglas@pluginweb.com.br
 */
ob_start();
require("../../../../site/includes/funcoes.php");
$caminho = "../../../../site/uploads/img_usuarios/"; //local onde ser� salvo a imagem grande

//Instancia o objeto
$obj = new DAO_Usuarios();

//Seleciona o objeto
$obj->select((FDados("id")));

//Verifica se o email já foi cadastrado
$verificaEmail = "SELECT email FROM usuarios WHERE email = '".FDados("email")."'";
$totalEmail = $obj->rows($verificaEmail);

if(FDados("email")==$obj->getemail()){
    $totalEmail = 0;
}

if($totalEmail > 0){
    header("location:/admin/sistema/editar-usuario/".$obj->getid()."/O email que você digitou já foi utilizado por outro usuário!");
}else {

    $obj->setpost();

    $obj->setsenha($objCrypt->crypt64($_POST["senha"]));

    $obj->dadosBanco();

    $obj->update((FDados("id")), $_POST);

    $id = FDados("id");

//++++++++++++++++++++++++++++  Manipulando a imagem +++++++++++++++++++++
//Recebe a imagem
    $imagem = isset($_FILES['imagem']) ? $_FILES['imagem'] : NULL;
    $nomeImg = $imagem['tmp_name'];

    $nome_final = time() . rand(1, 9);

    if ($nomeImg != NULL) {

        //Chama a Class de Recorte
        require_once '../../recorte/ThumbLib.inc.php';

        //Monta a Miniatura
        $thumb = PhpThumbFactory::create("$nomeImg");
        $thumb->resize(250, 250);
        mkdir($caminho . $id, 0777);
        //mkdir("$caminho."/".$id", 0777);
        $thumb->save($caminho . "$id/" . $nome_final . '.jpg', 'jpg');

        //Monta a Imagem Full
        $thumb = PhpThumbFactory::create("$nomeImg");
        $thumb->adaptiveResize(800, 800);
        $thumb->save($caminho . "$id/" . "g_" . $nome_final . '.jpg', 'jpg');

        $imgMiniatura = $id . "/" . $nome_final . '.jpg';
        $imgFull = $id . "/" . 'g_' . $nome_final . '.jpg';

        $obj->updatecampo($obj->getid(), "img_mini", $imgMiniatura, "Varchar");
        $obj->updatecampo($obj->getid(), "img_full", $imgFull, "Varchar");
    }

//Redirecionamento
    header("location:/admin/sistema/gerenciar-usuarios?msgOk=Usuário atualizado com sucesso!");
}
?>
