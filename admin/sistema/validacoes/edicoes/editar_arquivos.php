<?php

/**
 * Arquivo para realizar a edição de Arquivos
 * Transparência - Mister Shopping
 * @Autor Douglas Barteles - programacao4@futurocomunicacao.com
 */
ob_start();
require("../../../../site/includes/funcoes.php");
$caminho = "../../../../site/uploads/arquivos_transparencia/"; //local onde ser� salvo a imagem grande
//Instancia os Objeto
$obj = new DAO_Arquivo_Transparencia();

$id_para_excluir = FDados("arquivo_id");
$objExcluir = new DAO_Arquivo_Transparencia();
$objExcluir->select($id_para_excluir);
$localArquivo = $objExcluir->getarquivo_arquivo();
$arquivoExcluir = "../../../../site/uploads/arquivos_transparencia/" . $localArquivo;

$arquivo_categoria = $_POST['arquivo_categoria'];

$arquivo = $_FILES['arquivo'];
    //echo $arquivo['type'];
    //break;
	//echo $arquivo['tmp_name'];
	//break;

		if ($arquivo['tmp_name'] != NULL) {
			//echo "teste"; break;
			if ($arquivo['type'] == "application/pdf") {
				
				if (file_exists($arquivoExcluir)) {
					@unlink($arquivoExcluir);
				}
				//Excluindo para salvar novamente
				$objExcluir->delete($id_para_excluir);
				
				//Pega os dados do Formulário
				$obj->setpost();
				//Verifica os parametros para inserir no Banco
				$obj->dadosBanco();
				//Faz a inclusão no Banco de Dados
				$obj->insert();
				//Seleciona o último registro
				$obj->selectLastInsert();
				
				$id = $obj->getarquivo_id();
				
				if ($arquivo['type'] == "application/pdf") {
					$novonome = time() . rand(1, 9) . '.pdf';
				}
	
				if (!file_exists($caminho . $id)) {
					mkdir($caminho . $id, 0777);
				}
				$caminho = $caminho . $id . "/" . $novonome;
	
				move_uploaded_file($arquivo['tmp_name'], $caminho);
				//echo "moveu";
			
	
			$obj->updatecampo($obj->getarquivo_id(), "arquivo_arquivo", $id . "/" . $novonome, "Varchar");
			
			if($_POST['arquivo_data']!=""){
				
				$data_salvar = $obj->FDataBanco($_POST['arquivo_data']);
				
				$obj->updatecampo($obj->getarquivo_id(), "arquivo_data", $data_salvar, "date");
				
				}
		

			header("location:../../php/cadastrar_arquivos.php?categoria=$arquivo_categoria&id=$id&msgOk=Arquivo alterado com sucesso!");
			
			} else {
				
				header("location:../../php/cadastrar_arquivos.php?categoria=$arquivo_categoria&id=$id_para_excluir&msgErro=Formato de arquivo inválido! Cadastre um PDF");
			}
 		
		}else{
			
			header("location:../../php/cadastrar_arquivos.php?categoria=$arquivo_categoria&id=$id_para_excluir&msgErro=Aconteceu algum erro! Arquivo não identificado.");
			
			}
?>
