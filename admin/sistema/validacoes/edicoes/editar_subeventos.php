<?php

/**
 * Arquivo para realizar a edição de SubEventos
 * Site - BartSGE
 * @Autor Douglas Barteles - douglasbarteles@gmail.com
 */
ob_start();
require("../../../../site/includes/funcoes.php");

//Instancia o objeto
$obj = new DAO_Subeventos();

$idEvento = FDados("evento_id_INT");

//Seleciona o objeto
$obj->select((FDados("id")));

$obj->setpost();

//Verifica os parametros para inserir no Banco
$obj->dadosBanco();

$obj->update((FDados("id")), $_POST);

$id = FDados("id");

//Redirecionamento
header("location:/admin/sistema/gerenciar-subeventos/".$idEvento."&msgOk=SubEvento atualizado com sucesso!");
?>
