<?php

/**
 * Arquivo para realizar a edição de Categorias
 * Site - BartSGE
 * @Autor Douglas Barteles - douglasbarteles@gmail.com
 */
ob_start();
require("../../../../site/includes/funcoes.php");
$caminho = "../../../../site/uploads/img_categorias/"; //local onde ser� salvo a imagem grande

//Instancia o objeto
$obj = new DAO_Categorias();

//Seleciona o objeto
$obj->select((FDados("id")));

$obj->setpost();

$obj->dadosBanco();
$obj->update((FDados("id")), $_POST);

//++++++++++++++++++++++++++++  Manipulando a imagem +++++++++++++++++++++
//Recebe a imagem
$imagem = isset($_FILES['imagem']) ? $_FILES['imagem'] : NULL;
$nomeImg = $imagem['tmp_name'];

$nome_final = time() . rand(1, 9);

if ($nomeImg != NULL) {

    //Chama a Class de Recorte
    require_once '../../recorte/ThumbLib.inc.php';

    //Monta a Miniatura
    $thumb = PhpThumbFactory::create("$nomeImg");
    $thumb->resize(237, 148);
    $id = $obj->getid();
    mkdir($caminho . $id, 0777);
    //mkdir("$caminho."/".$id", 0777);
    $thumb->save($caminho . "$id/" . $nome_final . '.jpg', 'jpg');

    //Monta a Imagem Full
    $thumb = PhpThumbFactory::create("$nomeImg");
    $thumb->adaptiveResize(800, 499);
    $thumb->save($caminho . "$id/" . "g_" . $nome_final . '.jpg', 'jpg');

    $imgMiniatura = $id . "/" . $nome_final . '.jpg';
    $imgFull = $id . "/" . 'g_' . $nome_final . '.jpg';

    $obj->updatecampo($obj->getid(), "img_mini", $imgMiniatura, "Varchar");
    $obj->updatecampo($obj->getid(), "img_full", $imgFull, "Varchar");
}

//Redirecionamento
header("location:/admin/sistema/gerenciar-categorias?msgOk=Categoria atualizada com sucesso!");

?>
