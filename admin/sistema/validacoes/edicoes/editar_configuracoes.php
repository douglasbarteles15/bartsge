<?php
/**
 * Arquivo para realizar a edição de Configurações
 * Site - Plugin Web
 * @Autor Douglas Barteles - douglas@pluginweb.com.br
 */
ob_start();
require("../../../../site/includes/funcoes.php");
$caminho = "../../../../site/uploads/img_marcadagua/"; //local onde ser� salvo a imagem grande

//Instancia o objeto
$obj = new DAO_Config_site();

//Seleciona o objeto
$obj->select((FDados("idconfig_site")));

$obj->setpost();

$obj->update((FDados("idconfig_site")), $_POST);

$id = FDados("idconfig_site");

//++++++++++++++++++++++++++++  Manipulando a imagem +++++++++++++++++++++
//Recebe a imagem
$imagem = isset($_FILES['imagem']) ? $_FILES['imagem'] : NULL;
$nomeImg = $imagem['tmp_name'];

$nome_final = time() . rand(1, 9);

if ($nomeImg != NULL) {

    require_once '../../recorte/ThumbLib.inc.php';

    $thumb = PhpThumbFactory::create("$nomeImg");
    $thumb->resize(600, 128);
    //mkdir($caminho, 0777);
    $thumb->save($caminho . $nome_final . '.jpg', 'jpg');

    $imgMiniatura = $nome_final . '.jpg';

    $obj->updatecampo($id, "marcaDagua", $imgMiniatura, "Varchar");

}

//Redirecionamento
header("location:/admin/sistema/editar-configuracoes?msgOk=Configurações alteradas com sucesso!");
?>
