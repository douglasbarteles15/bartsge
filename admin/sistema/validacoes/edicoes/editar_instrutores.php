<?php

/**
 * Arquivo para realizar a edição de Instrutores
 * Site - BartSGE
 * @Autor Douglas Barteles - douglasbarteles@gmail.com
 */
ob_start();
require("../../../../site/includes/funcoes.php");

//Instancia o objeto
$obj = new DAO_Instrutores();

//Seleciona o objeto
$obj->select((FDados("id")));

//Verifica se o email já foi cadastrado
$verificaEmail = "SELECT email FROM instrutores WHERE email = '".FDados("email")."'";
$totalEmail = $obj->rows($verificaEmail);

if(FDados("email")==$obj->getemail()){
    $totalEmail = 0;
}

if($totalEmail > 0){
    header("location:/admin/sistema/editar-instrutor/".$obj->getid()."/O email que você digitou já foi utilizado por outro instrutor/palestrante!");
}else {

    $obj->setpost();

    $obj->setsenha($objCrypt->crypt64($_POST["senha"]));

    $obj->dadosBanco();

    $obj->update((FDados("id")), $_POST);

    $id = FDados("id");


//Redirecionamento
    header("location:/admin/sistema/gerenciar-instrutores?msgOk=Instrutor/Palestrante atualizado com sucesso!");
}
?>
