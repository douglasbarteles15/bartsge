<?php
include("../../../../site/includes/funcoes.php");

$objCrypt = new Crypt();
$objLog = new DAO_Log_Admin();

$senha_atual = $h->fDados("senha_atual");
$senha_criptografada = $objCrypt->crypt64($senha_atual);
$usuario_id = $_SESSION['usuario'];

$nova_senha = $h->fDados("nova_senha");
$confirme_nova_senha = $h->fDados("confirme_nova_senha");
$nova_senha_criptografada = $objCrypt->crypt64($nova_senha);

$objBanco->Query("SELECT senha FROM usuarios_admin WHERE id='$usuario_id' AND senha = '".$senha_criptografada."'");
$row = $objBanco->FetchObject();

if ($objBanco->rows > 0) {
    
    if($nova_senha==$confirme_nova_senha){
        
        $mudaSenha = "UPDATE usuarios_admin SET senha='".$nova_senha_criptografada."' WHERE id='".$usuario_id."' ";
        $objBanco->Query($mudaSenha);

        //INSERE LOG ADMIN
        $objLog->insert($_SESSION["usuario"], "Atualizou a senha de acesso", date('Y-m-d H:i:s'));
    
        header("location:" . $SiteHTTP . "mudar-senha/?msgOk=Senha atualizada com sucesso!");
        
    }else{
        
        header("location:" . $SiteHTTP . "mudar-senha/?msgErro=As senhas não conferem!");
        
    }
    
} else {
    
    header("location:" . $SiteHTTP . "mudar-senha/?msgErro=A Senha Atual está incorreta!");
    
}
?>