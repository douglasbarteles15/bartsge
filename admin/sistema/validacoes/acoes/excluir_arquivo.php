<?php

require("../../../../site/includes/funcoes.php");

$obj = new DAO_Modalidades_Arquivos();

$id_arquivo = FDados('id_arquivo');
$id_modalidade = FDados('id_modalidade');

$obj->select($id_arquivo);

$caminho_arquivo = "../../../../site/uploads/arquivos_modalidades/" . $obj->getmodalidades_arquivos_arquivo();

if (file_exists($caminho_arquivo)) {
    @unlink($caminho_arquivo);
}

$obj->delete($id_arquivo);

?>