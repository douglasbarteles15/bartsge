<?php
ob_start();
require("../../../../site/includes/funcoes.php");

$objModalidades = new DAO_Modalidades();

$categoria_id = FDados("categoria_id");

if($categoria_id != "") {
    $todos_ids = $objModalidades->todosIDsCategoria($categoria_id);
}else{
    $todos_ids = $objModalidades->todosIDs();
}

foreach ($todos_ids as $id) {
    $nomeCampoVagasTotais = "modalidades_vagas_".$id;
    $vagasTotais = FDados($nomeCampoVagasTotais);
    $nomeCampoVagasRestantes = "modalidades_vagas_restantes_".$id;
    $vagasRestantes = FDados($nomeCampoVagasRestantes);
    
    if(isset($vagasTotais)){
        $objModalidades->updatecampo($id, "modalidades_vagas", $vagasTotais, "INT");
    }
    if(isset($vagasRestantes)){
        $objModalidades->updatecampo($id, "modalidades_vagas_restantes", $vagasRestantes, "INT");
    }
}


header("location:../../php/gerenciar_vagas.php?msgOk=Vagas atualizadas com sucesso!");
