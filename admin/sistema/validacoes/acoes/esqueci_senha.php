<?php
include("../../../../site/includes/funcoes.php");
require '../../../../php_mailer/class.phpmailer.php';

$objCrypt = new Crypt();

$email = $h->fDados("email");

$objBanco->Query("SELECT email, nome, senha FROM usuarios_admin WHERE email='$email'");
$row = $objBanco->FetchObject();

if ($objBanco->rows > 0) {

    $senha = $objCrypt->decrypt($row->senha);
    $nome = $row->nome;
    $mail = new PHPMailer();
    $mail->Host = "server01.pluginweb.com.br";
    $mail->Port = 587;
    $mail->SMTPAuth = true;
    $mail->Username = "no_reply@pluginweb.com.br";
    $mail->Password = "sucesso55@"; // senha
    $mail->From = "no_reply@pluginweb.com.br";
    $mail->FromName = utf8_decode($objConfig_site->getrazao_social());
    $mail->AddAddress("$email");
    //$mail->AddBCC('douglas@pluginweb.com.br'); // CÃ³pia Oculta
    $mail->Subject = utf8_decode("Reenvio de Senha - ".$objConfig_site->getrazao_social());
    $mail->IsHTML(true);

    $mail->Body = utf8_decode('

                <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
                    <html xmlns="http://www.w3.org/1999/xhtml">
                        <head>
                            <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
                        </head>
                        <body style="background-color: #e6e7e8;">
                            <table border="0" cellpadding="0" cellspacing="0" height="100%" width="600" id="bodyTable">
                                <tr><td><img src="'.$SiteHTTP.'admin/images/topo-email.png" ></td></tr>
                                <tr>
                                    <td align="center" valign="top">
                                        <table border="0" cellpadding="20" cellspacing="0" width="520" id="emailContainer" style="background-color: white">
                                            <tr>
                                                <td valign="top">
                                                    <h2>Reenvio de Senha - Administração '.$objConfig_site->getrazao_social().'.</h2>

                                                    <hr>

                                                    <p>Olá '.$nome.'! Segue a sua senha conforme solicitado:</p>

                                                    <p><b>Senha:</b> '.$senha.'</p>

                                                    <hr>

                                                    <p>Atenciosamente,</p>
                                                    <p>'.$objConfig_site->getrazao_social().'</p>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr height="20px"></tr>
                                <table border="0" cellpadding="20" cellspacing="0" width="580" id="emailFooter">
                                    <tr>
                                        <td align="right">
                                            <a href="http://www.pluginweb.com.br" target="blank">
                                                <img src="'.$SiteHTTP.'admin/images/desenvolvido-por.png">
                                            </a>
                                        </td>
                                    </tr>
                                </table>
                            </table>
                        </body>
                    </html>

                    ');


    if (!$mail->Send()) {
        header("location:/admin/esqueci-minha-senha?msgErro=Erro ao solicitar sua senha");
    } else {

        $mail->ClearAllRecipients();
        $mail->ClearAttachments();

        header("location:/admin/?msgOk=A senha foi enviada para seu email com sucesso.");
    }
} else {
    header("location:/admin/esqueci-minha-senha?msgErro=Email não encontrado!");
}
?>