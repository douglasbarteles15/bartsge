<?php
ob_start();
//Inicialização do Sistema
require("../../../site/includes/funcoes.php");

//Verificar a permissão de acesso
if ($_SESSION["logado"] == "logado") {
    $objUser = new EXT_Usuarios_Admin();
    $objUser->select($_SESSION["usuario"]);
} else {
    header("location:" . $SiteHTTP . "admin/?msgErro=Realize seu Login");
}

if (FDados("id") != "") {
    $action = "/admin/sistema/alterar-usuario-admin";
    $objCadastro = new DAO_Usuarios_Admin();
    $objCadastro->select(FDados("id"));
    $titulo = "Editar Usuário";
    $botao = "Editar";
} else {
    $action = "/admin/sistema/add-usuario-admin";
    $titulo = "Cadastrar Usuários";
    $botao = "Salvar";
}
?>
<!DOCTYPE html>
<html>
    <head>
        <?php include("../../metas.php"); ?>
        <title><?= $titulo ?> - Administração <?= $objConfig_site->getrazao_social(); ?></title>
    </head>
    <body class="skin-blue">
        <?php include '../includes/topo.php'; ?>
        <div class="wrapper row-offcanvas row-offcanvas-left">
            <aside class="left-side sidebar-offcanvas">
                <?php include '../includes/menu.php'; ?>
            </aside>

            <aside class="right-side">
                <section class="content-header">
                    <h1>
                        <?= $titulo ?>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="../index.php"><i class="fa fa-dashboard"></i> Home</a></li>
                        <li>Usuários</li>
                        <li class="active"><?= $titulo ?></li>
                    </ol>
                </section>

                <section class="content">

                    <div class="box box-primary">
                        <div class="box-header">

                        </div><!-- /.box-header -->
                        <?php require("../../../comuns/libs/ajax/php/divRetorno_full.php"); ?>

                        <!-- form start -->
                        <form role="form" action="<?= $action ?>" method="POST">
                            <input type="hidden" name="id" value="<?= FDados("id") ?>"/>
                            <div class="box-body">
                                <div class="form-group">
                                    <label for="nome">Nome</label>
                                    <input type="text" name="nome" class="form-control " id="nome" placeholder="Nome" required>
                                </div>
                                <div class="form-group">
                                    <label for="email">Email</label>
                                    <input type="email" name="email" class="form-control " id="nome" placeholder="Email" required>
                                </div>
                                <div class="form-group">
                                    <label for="senha">Senha</label>
                                    <input type="password" name="senha" class="form-control " id="senha" placeholder="" required>
                                </div>
                                <div class="form-group">
                                    <label for="tipo">Tipo de Usuário</label>
                                    <div class="radio">
                                        <label for="adm">
                                            <input type="radio" name="tipo_id_INT" value="1">  Administrador
                                        </label> 
                                        <div style="clear:both; height:5px"></div>
                                        <label for="comum">
                                            <input type="radio" name="tipo_id_INT" value="2" checked>  Organizador
                                        </label>                                                
                                    </div>

                                </div>
                            </div><!-- /.box-body -->

                            <div class="box-footer">
                                <button type="submit" class="btn btn-primary"><?= $botao ?></button>
                                <a style="float: right;" href="/admin/sistema/gerenciar-usuarios-admin" class="btn btn-group">Gerenciar</a>
                            </div>
                        </form>
                    </div>

                    <!-- Main row -->

                </section><!-- /.content -->
            </aside><!-- /.right-side -->
        </div><!-- ./wrapper -->

        <script>
            $(document).ready(function(){
                $("li#usuarios_admin").addClass("active");
                $("li#cadastrar_usuarios_admin").addClass("active");
            });
        </script>

        <?php include("../../rodape.php"); ?>

    </body>
</html>