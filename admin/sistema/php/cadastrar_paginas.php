<?php
ob_start();
//Inicialização do Sistema
require("../../../site/includes/funcoes.php");

//Verificar a permissão de acesso
if ($_SESSION["logado"] == "logado") {
    $objUser = new EXT_Usuarios_Admin();
    $objUser->select($_SESSION["usuario"]);
} else {
    header("location:" . $SiteHTTP . "admin/?msgErro=Realize seu Login");
}

if (FDados("id") != "") {
    $action = "/admin/sistema/alterar-pagina";
    $objPaginas = new DAO_Paginas();
    $objPaginas->select(FDados("id"));
    $titulo = "Editar Página";
    $botao = "Editar";
    $criterio = "checked";
    $id = FDados("id");
    //echo $objUsuarios->getstatus_id_INT();
} else {
    $action = "/admin/sistema/add-pagina";
    $titulo = "Cadastrar Página";
    $botao = "Cadastrar";
    $criterio = "";

}
?>
<!DOCTYPE html>
<html>
<head>
    <?php include("../../metas.php"); ?>
    <title><?= $titulo ?> - Administração <?= $objConfig_site->getrazao_social(); ?></title>
</head>
<body class="skin-blue">
<?php include '../includes/topo.php'; ?>
<div class="wrapper row-offcanvas row-offcanvas-left">
    <!-- Left side column. contains the logo and sidebar -->
    <aside class="left-side sidebar-offcanvas">
        <!-- sidebar: style can be found in sidebar.less -->
        <?php include '../includes/menu.php'; ?>
        <!-- /.sidebar -->
    </aside>

    <!-- Right side column. Contains the navbar and content of the page -->
    <aside class="right-side">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                <?= $titulo ?>
            </h1>
            <ol class="breadcrumb">
                <li><a href="../index.php"><i class="fa fa-dashboard"></i> Home</a></li>
                <li>Páginas</li>
                <li class="active"><?= $titulo ?></li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">

            <div class="box box-primary">
                <div class="box-header">
                    <h3 class="box-title"><?= $titulo ?></h3>
                </div>
                <!-- /.box-header -->
                <?php require("../../../comuns/libs/ajax/php/divRetorno_full.php"); ?>

                <!-- form start -->
                <form role="form" action="<?= $action ?>" method="POST" enctype="multipart/form-data">
                    <input type="hidden" name="id" value="<?= FDados("id") ?>"/>

                    <div class="box-body">
                        <div class="form-group">
                            <label for="titulo">Título</label>
                            <input type="text" name="titulo" class="form-control" id="titulo"
                                   placeholder="Título" value="<?php if (FDados("id") != "") {
                                echo($objPaginas->gettitulo());
                            } ?>" required>
                        </div>

                        <div class="form-group">
                            <label for="editor1">Conteúdo</label>
                                    <textarea name="conteudo" class="form-control" rows="15"
                                              id="editor1"><?php if (FDados("id") != "") {
                                            echo($objPaginas->getconteudo());
                                        } ?></textarea>
                        </div>

                    </div>
                    <!-- /.box-body -->

                    <div class="box-footer">
                        <button type="submit" class="btn btn-primary"><?= $botao ?></button>
                        <a style="float: right;" href="/admin/sistema/gerenciar-paginas" class="btn btn-group">Gerenciar</a>
                    </div>
                </form>
            </div>

        </section>
        <!-- /.content -->
    </aside>
    <!-- /.right-side -->
</div>
<!-- ./wrapper -->

<script>
    $(document).ready(function () {
        $("li#paginas").addClass("active");
        $("li#cadastrar_paginas").addClass("active");
    });
</script>
<?php include("../../rodape.php"); ?>
</body>
</html>