<?php
ob_start();
//Inicialização do Sistema
require("../../../site/includes/funcoes.php");

//Verificar a permissão de acesso
if ($_SESSION["logado"] == "logado") {
    $objUser = new EXT_Usuarios_Admin();
    $objUser->select($_SESSION["usuario"]);
} else {
    header("location:" . $SiteHTTP . "admin/?msgErro=Realize seu Login");
}

$titulo = "Gerenciar Páginas";
?>
<!DOCTYPE html>
<html>
<head>
    <?php include("../../metas.php"); ?>
    <title><?= $titulo ?> - Administração <?= $objConfig_site->getrazao_social(); ?></title>
</head>
<body class="skin-blue">
<?php include '../includes/topo.php'; ?>
<div class="wrapper row-offcanvas row-offcanvas-left">
    <!-- Left side column. contains the logo and sidebar -->
    <aside class="left-side sidebar-offcanvas">
        <!-- sidebar: style can be found in sidebar.less -->
        <?php include '../includes/menu.php'; ?>
        <!-- /.sidebar -->
    </aside>

    <!-- Right side column. Contains the navbar and content of the page -->
    <aside class="right-side">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                <?= $titulo ?>
            </h1>
            <hr style="margin-top: 10px;margin-bottom: 10px;">

            <ol class="breadcrumb">
                <li><a href="../index.php"><i class="fa fa-dashboard"></i> Home</a></li>
                <li>Páginas</li>
                <li class="active"><?= $titulo ?></li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">

            <?php require("../../../comuns/libs/ajax/php/divRetorno_full.php"); ?>

            <!-- Listagens de cadastro -->
            <div class="box box-primary">
                <div class="box-header">
                    <h3 class="box-title">Lista de Páginas</h3>

                </div>
                <!-- /.box-header -->
                <div class="box-body no-padding">
                    <table class="table">
                        <tr>
                            <th style="width: 10px">#</th>
                            <th>Título</th>
                            <th style="width: 330px; text-align:center;">Ações</th>
                        </tr>
                        <?php
                        $objListaPaginas = new DAO_Paginas();

                        $buscaPaginas = "SELECT * FROM paginas ORDER BY titulo ASC";
                        $objBancoListaPaginas = new Database();
                        $objBancoListaPaginas->Query($buscaPaginas);
                        $contador = 0;
                        while ($row = $objBancoListaPaginas->FetchObject()) {
                            $contador++;
                            $objListaPaginas->select($row->id);
                            ?>
                            <tr>
                                <td><?= $contador ?></td>
                                <td><?= $objListaPaginas->gettitulo() ?></td>
                                <td align="center">
                                    <a href="/admin/sistema/editar-pagina/<?= $objListaPaginas->getid() ?>">
                                        <button class="btn btn-primary btn-sm">Editar</button>
                                    </a>
                                    <a href="/admin/sistema/excluir-pagina/<?= $objListaPaginas->getid() ?>" onclick="return confirm('Tem certeza que deseja excluir essa página? Essa é uma ação irreverssível.')">
                                        <button class="btn btn-danger btn-sm">Excluir</button>
                                    </a>
                                </td>
                            </tr>
                            <?php
                        }
                        ?>
                    </table>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->

            <div class="box-footer">
                <a style="float: right;" href="/admin/sistema/cadastrar-paginas" class="btn btn-group">Cadastrar novo registro</a>
            </div>

            <!-- Main row -->

        </section>
        <!-- /.content -->
    </aside>
    <!-- /.right-side -->
</div>
<!-- ./wrapper -->

<script>
    $(document).ready(function () {
        $("li#paginas").addClass("active");
        $("li#gerenciar_paginas").addClass("active");
    });
</script>
<?php include("../../rodape.php"); ?>
</body>
</html>