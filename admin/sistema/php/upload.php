<?php
require("../../../site/includes/funcoes.php");
$caminho = "../../../site/uploads/"; //local onde ser� salvo a imagem grande

//++++++++++++++++++++++++++++  Manipulando a imagem +++++++++++++++++++++
//Recebe a imagem
$imagem = isset($_FILES['imagem']) ? $_FILES['imagem'] : NULL;
$nomeImg = $imagem['tmp_name'];

$nome_final = time() . rand(1, 9);

if ($nomeImg != NULL) {

    //Chama a Class de Recorte
    require_once '../recorte/ThumbLib.inc.php';

    //Monta a Miniatura
    $thumb = PhpThumbFactory::create("$nomeImg");
    $thumb->save($caminho . $nome_final . '.jpg', 'jpg');
    $imagem_salva = $nome_final . '.jpg';

}

echo $imagem_salva;

?>
