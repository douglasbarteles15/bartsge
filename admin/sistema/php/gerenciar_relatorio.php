<?php
ob_start();
//Inicialização do Sistema
require("../../../site/includes/funcoes.php");

//Verificar a permissão de acesso
if ($_SESSION["logado"] == "logado") {
    $objUser = new EXT_Funcionarios();
    $objUser->select($_SESSION["usuario"]);
} else {
    header("location:" . $SiteHTTP . "admin-promocoes/?msgErro=Realize seu Login");
}

//Configurações do site
$objConfig_site = new DAO_Config_site();
$objConfig_site->select(1);
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>ADMINISTRAÇÃO | HOME - <?= $objConfig_site->getrazao_social(); ?></title>
        <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
        <!-- bootstrap 3.0.2 -->
        <link href="../../css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <!-- font Awesome -->
        <link href="../../css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <!-- Ionicons -->
        <link href="../../css/ionicons.min.css" rel="stylesheet" type="text/css" />
        <!-- Morris charts -->
        <link href="../../css/morris/morris.css" rel="stylesheet" type="text/css" />
        <!-- Theme style -->
        <link href="../../css/AdminLTE.css" rel="stylesheet" type="text/css" />

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->
    </head>
    <body class="skin-blue">
        <!-- header logo: style can be found in header.less -->
        <?php include '../includes/topo.php'; ?>
        <div class="wrapper row-offcanvas row-offcanvas-left">
            <!-- Left side column. contains the logo and sidebar -->
            <aside class="left-side sidebar-offcanvas">
                <!-- sidebar: style can be found in sidebar.less -->
                <?php include '../includes/menu.php'; ?>
                <!-- /.sidebar -->
            </aside>

            <!-- Right side column. Contains the navbar and content of the page -->
            <aside class="right-side">
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                        Relatório
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="../index.php"><i class="fa fa-dashboard"></i> Home</a></li>
                        <li class="active">Gerenciar Relatório</li>
                    </ol>
                </section>

                <!-- Main content -->
                <section class="content">


                    <div class="box-body no-padding">

                        <div class="col-md-6">
                            <!-- DONUT CHART -->
                            <div class="box box-danger">
                                <div class="box-header">
                                    <h3 class="box-title">Total de Compras por Promoção (R$)</h3>
                                    <?php
                                    $objVinculo = new DAO_Vinculo();
                                    $objBancoVinculo = new Database();
                                    $sqlVinculo = "Select idvinculo from vinculo_loja_valor";
                                    $objBancoVinculo->Query($sqlVinculo);
                                    $conta = 1;
                                    while ($row = $objBancoVinculo->FetchObject()) {
                                        $conta++;
                                    }
                                    ?>

                                </div>
                                <div class="box-body chart-responsive">
                                    <div class="chart" id="sales-chart" style="height: 300px; position: relative;"></div>
                                    <?php
                                    $sqlGraficoPromocao = "Select * from promocoes order by nome_promocao asc";
                                    $objGraficoPromocao = new DAO_Promocoes();
                                    $objBancoGraficoPromocao = new Database();
                                    $objBancoGraficoPromocao->Query($sqlGraficoPromocao);
                                    while ($row1 = $objBancoGraficoPromocao->FetchObject()) {
                                        $objGraficoPromocao->select($row1->idpromocoes);
                                        $sqlTotal = "Select SUM(valor_vinculo) from vinculo_loja_valor where promocao_vinculo = " . $objGraficoPromocao->getidpromocoes() . "";
                                        $objBancoRelatorioPromocao = new Database();
                                        $objBancoRelatorioPromocao->Query($sqlTotal);
                                        $resultado = $objBancoRelatorioPromocao->FetchArray();
                                        if ($resultado[0] == "") {
                                            $resultado[0] = 0;
                                        }
                                        echo utf8_encode($objGraficoPromocao->getnome_promocao()) . "<div class='btn btn-info' style='margin-left:5px; background-color:" . cycle('#0073B7', '#00A65A', '#F39C12', '#F56954', '#00C0EF', '#0073B7', '#932AB6', '#39CCCC', '#85144B') . "; border:none;'></div> R$ " . number_format($resultado[0], 2, ',', '.') . "<br />";
                                    }
                                    ?>
                                </div><!-- /.box-body -->
                            </div><!-- /.box -->
                            <div class="box box-success">
                            <div class="box-header">
                                <h3 class="box-title">Dados</h3>
                                <?php
                                $sqlRelatorioPromocao = "Select SUM(valor_vinculo) from promocoes a join vinculo_loja_valor b on a.idpromocoes = b.promocao_vinculo";
                                $objBancoRelatorioPromocao = new Database();
                                $objBancoRelatorioPromocao->Query($sqlRelatorioPromocao);
                                $resultado = $objBancoRelatorioPromocao->FetchArray();
                                number_format($resultado[0], 2, ',', '.');
                                ?>
                            </div>
                            <!-- info row -->
                            <div class="row invoice-info">
                                <div class="col-sm-4 invoice-col">
                                    <address>
                                        <strong>Total de Promoções</strong><br>
                                        <?php
                                        $sqlPromocoes = "Select COUNT(idpromocoes) as total from promocoes";
                                        $objBancoPromocoes = new Database();
                                        $conta = $objBancoPromocoes->FecthAssoc($objBancoPromocoes->Query($sqlPromocoes));
                                        echo $conta['total'];
                                        ?>
                                    </address>
                                </div><!-- /.col -->
                                <div class="col-sm-4 invoice-col">
                                    <address>
                                        <strong>Total de Lojas Cadastradas</strong><br>
                                        <?php
                                        $sqlLojas = "Select COUNT(idlojas) as total from lojas";
                                        $objBancoLojas = new Database();
                                        $conta = $objBancoLojas->FecthAssoc($objBancoLojas->Query($sqlLojas));
                                        echo $conta['total'];
                                        ?>
                                    </address>
                                </div><!-- /.col -->
                                <div class="col-sm-4 invoice-col">
                                    <address>
                                        <strong>Total de Trocas realizadas</strong><br>
                                        <?php
                                        $sqlLojas = "Select COUNT(DISTINCT (idcliente_vinculo)) as total from vinculo_loja_valor";
                                        $objBancoLojas = new Database();
                                        $conta = $objBancoLojas->FecthAssoc($objBancoLojas->Query($sqlLojas));
                                        echo $conta['total'];
                                        ?>
                                    </address>
                                </div><!-- /.col -->
                                <div class="box-header">
                                    <h3 class="box-title">Últimas promoções cadastradas</h3>
                                </div>
                                <div class="box-body">
                               <?php
                                   $objUltimaPromocoes = new DAO_Promocoes();
                                   $sqlUltimasPromocoes = "Select idpromocoes from promocoes order by idpromocoes desc";
                                   $objBancoUltimasPromocoes = new Database();
                                   $objBancoUltimasPromocoes->Query($sqlUltimasPromocoes);
                                   while ($row2 = $objBancoUltimasPromocoes->FetchObject()) {
                                       $objUltimaPromocoes->select($row2->idpromocoes);
                                       echo utf8_encode($objUltimaPromocoes->getnome_promocao())."<br />";
                                   }
                               ?>
                                </div>
                            </div><!-- /.row -->
                        </div><!-- /.box -->
                            
                        </div><!-- /.col (LEFT) -->
                    </div>
                    <div class="col-md-6">
                        <!-- BAR CHART -->
                        
                        
                        <div class="box box-warning">
                               <div class="box-header">
                                   <h3 class="box-title">Relatório por Usuários</h3>
                               </div>
                            <form role="form" action="gerar-relatorio-por-usuario/" method="POST" target="_blank">
                                   <input type="hidden" name="idlojas" value="<?= FDados("id") ?>"/>
                                   <div class="box-body">

                                   <div class="form-group">
                                       <label for="promocao">Selecione a Promoção</label>
                                       <select class="form-control" name="idpromocoes">
                                           <?php
                                               $objPromocoes = new DAO_Promocoes();
                                               $sqlPromocoes = "Select idpromocoes from promocoes order by nome_promocao asc";
                                               $objBancoPromocoes = new Database();
                                               $objBancoPromocoes->Query($sqlPromocoes);
                                               while ($row3 = $objBancoPromocoes->FetchObject()) {
                                                   $objPromocoes->select($row3->idpromocoes);
                                           ?>
                                               <option value="<?= $objPromocoes->getidpromocoes() ?>"><?= utf8_encode($objPromocoes->getnome_promocao()) ?></option>
                                           <?php  } ?>
                                       </select>
                                   </div>
                               </div><!-- /.box-body -->

                               <div class="box-footer">
                                   <button type="submit" class="btn btn-primary">Gerar</button>
                               </div>
                           </form>
                       </div>
                        <div class="box box-info">
                               <div class="box-header">
                                   <h3 class="box-title">Relatório por lojas</h3>
                               </div>
                                <form role="form" action="gerar-relatorio-por-loja/" method="POST" target="_blank">
                                   <input type="hidden" name="idlojas" value="<?= FDados("id") ?>"/>
                                   <div class="box-body">

                                   <div class="form-group">
                                       <label for="promocao">Selecione a Promoção</label>
                                       <select class="form-control" name="idpromocoes">
                                           <?php
                                               $objPromocoes = new DAO_Promocoes();
                                               $sqlPromocoes = "Select idpromocoes from promocoes order by nome_promocao asc";
                                               $objBancoPromocoes = new Database();
                                               $objBancoPromocoes->Query($sqlPromocoes);
                                               while ($row3 = $objBancoPromocoes->FetchObject()) {
                                                   $objPromocoes->select($row3->idpromocoes);
                                           ?>
                                               <option value="<?= $objPromocoes->getidpromocoes() ?>"><?= utf8_encode($objPromocoes->getnome_promocao()) ?></option>
                                           <?php  } ?>
                                       </select>
                                   </div>
                               </div><!-- /.box-body -->

                               <div class="box-footer">
                                   <button type="submit" class="btn btn-primary">Gerar</button>
                               </div>
                           </form>
                       </div>
                        <div class="box box-warning">
                               <div class="box-header">
                                   <h3 class="box-title">Relatório por Vendedor</h3>
                               </div>
                                <form role="form" action="gerar-relatorio-por-vendedor/" method="POST" target="_blank">
                                   <input type="hidden" name="idlojas" value="<?= FDados("id") ?>"/>
                                   <div class="box-body">

                                   <div class="form-group">
                                       <label for="promocao">Selecione a Promoção</label>
                                       <select class="form-control" name="idpromocoes">
                                           <?php
                                               $objPromocoes = new DAO_Promocoes();
                                               $sqlPromocoes = "Select idpromocoes from promocoes order by nome_promocao asc";
                                               $objBancoPromocoes = new Database();
                                               $objBancoPromocoes->Query($sqlPromocoes);
                                               while ($row3 = $objBancoPromocoes->FetchObject()) {
                                                   $objPromocoes->select($row3->idpromocoes);
                                           ?>
                                               <option value="<?= $objPromocoes->getidpromocoes() ?>"><?= utf8_encode($objPromocoes->getnome_promocao()) ?></option>
                                           <?php  } ?>
                                       </select>
                                   </div>
                               </div><!-- /.box-body -->

                               <div class="box-footer">
                                   <button type="submit" class="btn btn-primary">Gerar</button>
                               </div>
                           </form>
                       </div>

                    </div><!-- /.col (RIGHT) -->
                </section><!-- /.content -->
            </aside>
            
            
            <!-- /.right-side -->
        </div><!-- ./wrapper -->


        <!-- jQuery 2.0.2 -->
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>
        <!-- Bootstrap -->
        <script src="../../js/bootstrap.min.js" type="text/javascript"></script>

        <!-- Morris.js charts -->
        <script src="//cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
        <script src="../../js/plugins/morris/morris.min.js" type="text/javascript"></script>

        <!-- AdminLTE App -->
        <script src="../../js/AdminLTE/app.js" type="text/javascript"></script>

        <!-- page script -->
        
        <script type="text/javascript">
            $(function() {
                "use strict";

                //DONUT CHART
                var donut = new Morris.Donut({
                    element: 'sales-chart',
                    resize: true,
                    colors: ["#0073B7", "#00A65A", "#F39C12", "#F56954", "#00C0EF", "#0073B7", "#932AB6", "#39CCCC", "#85144B"],
                    data: [
                        <?php
                        $sqlGraficoPromocao = "Select * from promocoes order by nome_promocao asc";
                        $objGraficoPromocao = new DAO_Promocoes();
                        $objBancoGraficoPromocao = new Database();
                        $objBancoGraficoPromocao->Query($sqlGraficoPromocao);
                        function formatoReal($valor) {
                                $valor = (string) $valor;
                                $regra = "/^[0-9]{1,3}([.]([0-9]{3}))*[,]([.]{0})[0-9]{0,2}$/";
                                if (preg_match($regra, $valor)) {
                                    return true;
                                } else {
                                    return false;
                                }
                            }
                        while ($row1 = $objBancoGraficoPromocao->FetchObject()) {
                            $objGraficoPromocao->select($row1->idpromocoes);
                            $sqlTotal = "Select SUM(valor_vinculo) from vinculo_loja_valor where promocao_vinculo = " . $objGraficoPromocao->getidpromocoes() . "";
                            $objBancoRelatorioPromocao = new Database();
                            $objBancoRelatorioPromocao->Query($sqlTotal);
                            $resultado = $objBancoRelatorioPromocao->FetchArray();
                            if ($resultado[0] == "") {
                                $resultado[0] = 0;
                            }

                            
                            ?>
                                {label: "<?php echo utf8_encode($objGraficoPromocao->getnome_promocao()) ?>(R$)", value: <?php echo $resultado[0] ?>},
                            <?php } ?>
                    ],
                    hideHover: 'auto'
                });

            });
        </script>
    </body>
</html>