<?php
ob_start();
//Inicialização do Sistema
require("../../../site/includes/funcoes.php");

//Verificar a permissão de acesso
if ($_SESSION["logado"] == "logado") {
    $objUser = new EXT_Usuarios_Admin();
    $objUser->select($_SESSION["usuario"]);
} else {
    header("location:" . $SiteHTTP . "admin/?msgErro=Realize seu Login");
}

$idEvento = FDados("id");
$objEvento = new DAO_Eventos();
$objEvento->select($idEvento);

$titulo = "Gerenciar SubEventos";
?>
<!DOCTYPE html>
<html>
<head>
    <?php include("../../metas.php"); ?>
    <title><?= $titulo ?> - Administração <?= $objConfig_site->getrazao_social(); ?></title>
</head>
<body class="skin-blue">
<?php include '../includes/topo.php'; ?>
<div class="wrapper row-offcanvas row-offcanvas-left">
    <!-- Left side column. contains the logo and sidebar -->
    <aside class="left-side sidebar-offcanvas">
        <!-- sidebar: style can be found in sidebar.less -->
        <?php include '../includes/menu.php'; ?>
        <!-- /.sidebar -->
    </aside>

    <!-- Right side column. Contains the navbar and content of the page -->
    <aside class="right-side">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                <?= $titulo.'<br><span style="font-size:0.6em">'.$objEvento->gettitulo().'</span>' ?>
            </h1>
            <hr style="margin-top: 10px;margin-bottom: 10px;">

            <ol class="breadcrumb">
                <li><a href="../index.php"><i class="fa fa-dashboard"></i> Home</a></li>
                <li>SubEventos</li>
                <li class="active"><?= $titulo ?></li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">

            <?php require("../../../comuns/libs/ajax/php/divRetorno_full.php"); ?>

            <!-- Listagens de cadastro -->
            <div class="box box-primary">
                <div class="box-header">
                    <h3 class="box-title">Listagem de SubEventos</h3>

                </div>
                <!-- /.box-header -->
                <div class="box-body no-padding">
                    <table class="table">
                        <tr>
                            <th style="width: 10px">#</th>
                            <th>Título</th>
                            <th>Tipo</th>
                            <th>Instrutor/Palestrante</th>
                            <th>Data</th>
                            <th>Horário</th>
                            <th style="width: 330px; text-align:center;">Ações</th>
                        </tr>
                        <?php
                        $data_atual = date('Y-m-d');
                        $objListaSubeventos = new DAO_Subeventos();
                        $objTipo = new DAO_Tipos_Subeventos();
                        $objInstrutor = new DAO_Instrutores();
                        $buscaSubeventos = "SELECT * FROM subeventos WHERE evento_id_INT='".$objEvento->getid()."' ORDER BY data ASC, hora_inicio ASC";
                        $objBancoListaSubeventos = new Database();
                        $objBancoListaSubeventos->Query($buscaSubeventos);
                        $contador = 0;
                        while ($row = $objBancoListaSubeventos->FetchObject()) {
                            $contador++;
                            $objListaSubeventos->select($row->id);
                            $objTipo->select($objListaSubeventos->gettipo_subevento_id_INT());
                            $objInstrutor->select($objListaSubeventos->getinstrutor_id_INT());
                            ?>
                            <tr>
                                <td <?php if ($objListaSubeventos->getstatus_id_INT() == 0) {
                                    $link_ativacao = "/admin/sistema/ativar-subevento/".$objListaSubeventos->getid();
                                    $texto_ativacao = "Ativar";
                                    echo 'style="background-color: #FFE4E1"';
                                } else {
                                    $link_ativacao = "/admin/sistema/desativar-subevento/".$objListaSubeventos->getid();
                                    $texto_ativacao = "Desativar";
                                    echo 'style="background-color: #90EE90"';
                                } ?>><?= $contador ?></td>
                                <td><?= ($objListaSubeventos->gettitulo()) ?></td>
                                <td><?= ($objTipo->gettitulo()) ?></td>
                                <td><?= ($objInstrutor->getnome()) ?></td>
                                <td><?= ($objListaSubeventos->FDataExibe($objListaSubeventos->getdata())) ?></td>
                                <td><?= (substr($objListaSubeventos->gethora_inicio(), 0, -3)) ?> - <?= (substr($objListaSubeventos->gethora_final(), 0, -3)) ?></td>

                                <td align="center">
                                    <a href="/admin/sistema/gerenciar-arquivos/<?= $objListaSubeventos->getid() ?>">
                                        <button class="btn btn-default btn-sm">Arquivos</button>
                                    </a>
                                    <a href="<?=$link_ativacao?>">
                                        <button class="btn btn-info btn-sm"><?=$texto_ativacao?></button>
                                    </a>
                                    <a href="/admin/sistema/editar-subevento/<?= $objEvento->getid() ?>/<?= $objListaSubeventos->getid() ?>">
                                        <button class="btn btn-primary btn-sm">Editar</button>
                                    </a>
                                    <a href="/admin/sistema/excluir-subevento/<?= $objListaSubeventos->getid() ?>" onclick="return confirm('Tem certeza que deseja excluir esse SubEvento?')">
                                        <button class="btn btn-danger btn-sm">Excluir</button>
                                    </a>
                                </td>
                            </tr>
                            <?php
                        }
                        ?>
                    </table>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->

            <div class="box-footer">
                <a href="/admin/sistema/gerenciar-eventos"><button class="btn btn-link btn-sm">Voltar</button></a>
                <a style="float: right;" href="/admin/sistema/cadastrar-subeventos/<?=$objEvento->getid()?>" class="btn btn-group">Cadastrar novo SubEvento</a>
            </div>

            <!-- Main row -->

        </section>
        <!-- /.content -->
    </aside>
    <!-- /.right-side -->
</div>
<!-- ./wrapper -->

<script>
    $(document).ready(function () {
        $("li#eventos").addClass("active");
        $("li#gerenciar_eventos").addClass("active");
    });
</script>
<?php include("../../rodape.php"); ?>
</body>
</html>