<?php
ob_start();
//Inicialização do Sistema
require("../../../site/includes/funcoes.php");

//Verificar a permissão de acesso
if ($_SESSION["logado"] == "logado") {
    $objUser = new EXT_Funcionarios();
    $objUser->select($_SESSION["usuario"]);
} else {
    header("location:" . $SiteHTTP . "admin/?msgErro=Realize seu Login");
}

$idCategoria = FDados("modalidade");

if($idCategoria!="") {
    $idCategoria = FDados("modalidade");
    $objCategoriaModalidade = new DAO_Categorias_Modalidades();
    $objCategoriaModalidade->select($idCategoria);
}

//Configurações do site
$objConfig_site = new DAO_Config_site();
$objConfig_site->select(1);
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Administração Plugin Web | LISTAGEM DE VAGAS - <?= $objConfig_site->getrazao_social(); ?></title>
        <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
        <!-- bootstrap 3.0.2 -->
        <link href="../../css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <!-- font Awesome -->
        <link href="../../css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <!-- Ionicons -->
        <link href="../../css/ionicons.min.css" rel="stylesheet" type="text/css" />
        <!-- Morris chart -->
        <link href="../../css/morris/morris.css" rel="stylesheet" type="text/css" />
        <!-- jvectormap -->
        <link href="../../css/jvectormap/jquery-jvectormap-1.2.2.css" rel="stylesheet" type="text/css" />
        <!-- fullCalendar -->
        <link href="../../css/fullcalendar/fullcalendar.css" rel="stylesheet" type="text/css" />
        <!-- Daterange picker -->
        <link href="../../css/daterangepicker/daterangepicker-bs3.css" rel="stylesheet" type="text/css" />
        <!-- bootstrap wysihtml5 - text editor -->
        <link href="../../css/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css" rel="stylesheet" type="text/css" />
        <!-- Theme style -->
        <link href="../../css/AdminLTE.css" rel="stylesheet" type="text/css" />

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.../../js/1.3.0/respond.min.js"></script>
        <![endif]-->

        <script language='javascript' type='text/javascript' src='../../js/funcoes_abre_fecha.js'></script>

    </head>
    <body class="skin-blue">
        <?php include '../includes/topo.php'; ?>
        <div class="wrapper row-offcanvas row-offcanvas-left">
            <!-- Left side column. contains the logo and sidebar -->
            <aside class="left-side sidebar-offcanvas">
                <!-- sidebar: style can be found in sidebar.less -->
                <?php include '../includes/menu.php'; ?>
                <!-- /.sidebar -->
            </aside>

            <!-- Right side column. Contains the navbar and content of the page -->
            <aside class="right-side">
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                        Gerenciar Vagas
                    </h1>
                    <hr style="margin-top: 10px;margin-bottom: 10px;">   
                    
                    <ol class="breadcrumb">
                        <li><a href="../index.php"><i class="fa fa-dashboard"></i> Home</a></li>
                        <li>Vagas</li>
                        <li class="active">Gerenciar Vagas</li>
                    </ol>
                </section>

                <!-- Main content -->
                <section class="content">

                    <?php require("../../../comuns/libs/ajax/php/divRetorno_full.php"); ?>

                    <form role="form" id="seleciona_modalidade" action="gerenciar_vagas.php" method="POST" enctype="multipart/form-data">
                        <div class="form-group" style="width: 50%; margin-right: 10px; float: left">
                            <label for="id">Filtre por modalidade</label>
                            <select name="modalidade" class="form-control" onchange="seleciona_modalidade()">
                                <option value=''> -- Todas -- </option>
                                <?php
                                $objBanco = new Database();
                                $objCategoriasModalidades = new DAO_Categorias_Modalidades();
                                $buscaModalidades = "SELECT * FROM categorias_modalidades ORDER BY categorias_modalidades_titulo ASC";
                                $objBanco->Query($buscaModalidades);
                                while($exibeModalidades = $objBanco->FetchObject()){
                                    $objCategoriasModalidades->select($exibeModalidades->categorias_modalidades_id);
                                    ?>

                                    <option value='<?=$objCategoriasModalidades->getcategorias_modalidades_id()?>' <?php if($idCategoria==$objCategoriasModalidades->getcategorias_modalidades_id()) echo 'selected';?>><?=$objCategoriasModalidades->getcategorias_modalidades_titulo()?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </form>


                    <form role="form" action="../validacoes/acoes/atualiza_vagas.php" method="POST">

                        <?php if($idCategoria!="") { ?>
                            <input type="hidden" name="categoria_id" value="<?=$idCategoria?>">
                        <?php }?>

                        <!-- Listagens de cadastro -->
                    <div class="box box-primary">
                        <div class="box box-header" style="padding: 8px">
                            <button type="submit" class="btn btn-primary" style="float: right;">Salvar Vagas</button>
                        </div>
                        <div class="box-body no-padding">
                            <table class="table">
                                <tr>
                                    <th style="width: 10px">#</th>
                                    <th>Turma</th>
                                    <th>Modalidade</th>
                                    <th style="text-align: center">Total de Vagas</th>
                                    <th style="text-align: center">Vagas Restantes</th>
                                </tr>
                                <?php
                                $data_atual = date('Y-m-d');
                                $objListaModalidades = new DAO_Modalidades();
                                $objBanco3 = new Database();
                                $objModalidadeDependencias = new DAO_Modalidades_Dependencias();
                                if($idCategoria!="") {
                                    $buscaModalidades = "SELECT * FROM modalidades WHERE categorias_modalidades_id_INT='".$idCategoria."' ORDER BY modalidades_titulo ASC";
                                }else{
                                    $buscaModalidades = "SELECT * FROM modalidades ORDER BY modalidades_titulo ASC";
                                }
                                $objBancoListaModalidades = new Database();
                                $objBancoListaModalidades->Query($buscaModalidades);
                                $contador = 0;
                                while ($row = $objBancoListaModalidades->FetchObject()) {
                                    $contador++;
                                    $objListaModalidades->select($row->modalidades_id);
                                    $objCategoria = new DAO_Categorias_Modalidades();
                                    $objCategoria->select($objListaModalidades->getcategorias_modalidades_id_INT());
                                    ?>
                                    <tr>
                                        <td <?php if($objListaModalidades->getmodalidades_status_INT()==0){ echo 'style="background-color: #FFE4E1"'; }else{ echo 'style="background-color: #90EE90"';} ?>><?= $contador ?></td>
                                        <td>
                                            <?= ($objListaModalidades->getmodalidades_titulo()) ?>
                                            <br>

                                            <?php
                                            $buscaOpcoesModalidadeInscrita = "SELECT * FROM modalidades_opcoes WHERE modalidades_id_INT='" . $objListaModalidades->getmodalidades_id() . "'";

                                            $objBanco3->Query($buscaOpcoesModalidadeInscrita);
                                            while($exibeOpcoesModalidadeInscrita = $objBanco3->FetchObject()){
                                                $diaSemanaInscrito = $objModalidadeDependencias->buscaStringDiaSemana($exibeOpcoesModalidadeInscrita->modalidades_opcoes_diaSemana);
                                                $horaInicioInscrita = substr($exibeOpcoesModalidadeInscrita->modalidades_opcoes_horaInicio, 0, -3);
                                                $horaInicioInscrita = explode(":", $horaInicioInscrita);
                                                $horaFinalInscrita = substr($exibeOpcoesModalidadeInscrita->modalidades_opcoes_horaFinal, 0, -3);
                                                $horaFinalInscrita = explode(":", $horaFinalInscrita);
                                                if($horaInicioInscrita[1]=="00"){
                                                    $horaInicioInscrita[1]="";
                                                }
                                                if($horaFinalInscrita[1]=="00"){
                                                    $horaFinalInscrita[1]="";
                                                }

                                                echo '<span style="color: #A7A9AC">'.$diaSemanaInscrito.', '.$horaInicioInscrita[0].'h'.$horaInicioInscrita[1].' às '.$horaFinalInscrita[0].'h'.$horaFinalInscrita[1].'</span><br> ';
                                            }
                                            ?>

                                        </td>
                                        <?php if($objListaModalidades->getmodalidades_atividade_extra_INT()==1){ ?>
                                        <td>Atividade Extra</td>
                                        <?php }else{ ?>
                                        <td><?= ($objCategoria->getcategorias_modalidades_titulo()) ?></td>
                                        <?php } ?>
                                        <td align="center"><input type="number" class="form-control" name="modalidades_vagas_<?=$objListaModalidades->getmodalidades_id()?>" value="<?= ($objListaModalidades->getmodalidades_vagas()) ?>" style="text-align: center;width: 70px;" ></td>
                                        <td align="center"><input type="number" class="form-control" name="modalidades_vagas_restantes_<?=$objListaModalidades->getmodalidades_id()?>" value="<?= ($objListaModalidades->getmodalidades_vagas_restantes()) ?>" style="text-align: center;width: 70px;" ></td>
                                    </tr>
                                    <?php
                                }
                                ?>
                            </table>
                        </div><!-- /.box-body -->
                    </div><!-- /.box -->
                    
                    <div class="box-footer">
                        <button type="submit" class="btn btn-primary" style="float: right">Salvar Vagas</button>
                    </div>
                    
                    </form>

                    <!-- top row -->
                    <div class="row">
                        <div class="col-xs-12 connectedSortable">

                        </div><!-- /.col -->
                    </div>
                    <!-- /.row -->

                    <!-- Main row -->

                </section><!-- /.content -->
            </aside><!-- /.right-side -->
        </div><!-- ./wrapper -->

        <!-- add new calendar event modal -->


        <!-- jQuery 2.0.2 -->
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>
        <script>
        $(document).ready(function(){
                $("li#esportes").addClass("active");
                $("li#modalidades").addClass("active");
                $("li#gerenciar_vagas").addClass("active");
             });
        </script>
        <!-- Bootstrap -->
        <script src="../../js/bootstrap.min.js" type="text/javascript"></script>
        <!-- InputMask -->
        <script src="../../js/plugins/input-mask/jquery.inputmask.js" type="text/javascript"></script>
        <script src="../../js/plugins/input-mask/jquery.inputmask.date.extensions.js" type="text/javascript"></script>
        <script src="../../js/plugins/input-mask/jquery.inputmask.extensions.js" type="text/javascript"></script>
        <!-- date-range-picker -->
        <script src="../../js/plugins/daterangepicker/daterangepicker.js" type="text/javascript"></script>
        <!-- bootstrap color picker -->
        <script src="../../js/plugins/colorpicker/bootstrap-colorpicker.min.js" type="text/javascript"></script>
        <!-- bootstrap time picker -->
        <script src="../../js/plugins/timepicker/bootstrap-timepicker.min.js" type="text/javascript"></script>
        <!-- AdminLTE App -->
        <script src="../../js/AdminLTE/app.js" type="text/javascript"></script>

        <!-- Page script -->
        <script type="text/javascript">
            $(function() {
                //Datemask dd/mm/yyyy
                $("#datemask").inputmask("dd/mm/yyyy", {"placeholder": "dd/mm/yyyy"});
                //Datemask2 mm/dd/yyyy
                $("#datemask2").inputmask("mm/dd/yyyy", {"placeholder": "mm/dd/yyyy"});
                //Money Euro
                $("[data-mask]").inputmask();

                //Date range picker
                $('#reservation').daterangepicker();
                //Date range picker with time picker
                $('#reservationtime').daterangepicker({timePicker: true, timePickerIncrement: 30, format: 'MM/DD/YYYY h:mm A'});
                //Date range as a button
                $('#daterange-btn').daterangepicker(
                        {
                            ranges: {
                                'Today': [moment(), moment()],
                                'Yesterday': [moment().subtract('days', 1), moment().subtract('days', 1)],
                                'Last 7 Days': [moment().subtract('days', 6), moment()],
                                'Last 30 Days': [moment().subtract('days', 29), moment()],
                                'This Month': [moment().startOf('month'), moment().endOf('month')],
                                'Last Month': [moment().subtract('month', 1).startOf('month'), moment().subtract('month', 1).endOf('month')]
                            },
                            startDate: moment().subtract('days', 29),
                            endDate: moment()
                        },
                function(start, end) {
                    $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
                }
                );

                //iCheck for checkbox and radio inputs
                $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
                    checkboxClass: 'icheckbox_minimal',
                    radioClass: 'iradio_minimal'
                });
                //Red color scheme for iCheck
                $('input[type="checkbox"].minimal-red, input[type="radio"].minimal-red').iCheck({
                    checkboxClass: 'icheckbox_minimal-red',
                    radioClass: 'iradio_minimal-red'
                });
                //Flat red color scheme for iCheck
                $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
                    checkboxClass: 'icheckbox_flat-red',
                    radioClass: 'iradio_flat-red'
                });

                //Colorpicker
                $(".my-colorpicker1").colorpicker();
                //color picker with addon
                $(".my-colorpicker2").colorpicker();

                //Timepicker
                $(".timepicker").timepicker({
                    showInputs: false
                });
            });
        </script>
    </body>
</html>