<?php
ob_start();
//Inicialização do Sistema
require("../../../site/includes/funcoes.php");

//Verificar a permissão de acesso
if ($_SESSION["logado"] == "logado") {
    $objUser = new EXT_Usuarios_Admin();
    $objUser->select($_SESSION["usuario"]);
} else {
    header("location:" . $SiteHTTP . "admin/?msgErro=Realize seu Login");
}

$titulo = "Controlar Presença";

$idEvento = FDados("evento");

if ($idEvento != "") {
    $objEvento = new DAO_Eventos();
    $objEvento->select($idEvento);
}

?>
<!DOCTYPE html>
<html>
<head>
    <?php include("../../metas.php"); ?>
    <title><?= $titulo ?> - Administração <?= $objConfig_site->getrazao_social(); ?></title>
</head>
<body class="skin-blue">
<?php include '../includes/topo.php'; ?>
<div class="wrapper row-offcanvas row-offcanvas-left">
    <!-- Left side column. contains the logo and sidebar -->
    <aside class="left-side sidebar-offcanvas">
        <!-- sidebar: style can be found in sidebar.less -->
        <?php include '../includes/menu.php'; ?>
        <!-- /.sidebar -->
    </aside>

    <!-- Right side column. Contains the navbar and content of the page -->
    <aside class="right-side">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Controlar Presença
            </h1>
            <hr style="margin-top: 10px;margin-bottom: 10px;">

            <ol class="breadcrumb">
                <li><a href="../index.php"><i class="fa fa-dashboard"></i> Home</a></li>
                <li>Presença</li>
                <li class="active">Controlar Presença</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">

            <?php require("../../../comuns/libs/ajax/php/divRetorno_full.php"); ?>

            <form role="form" id="seleciona_evento" action="/admin/sistema/gerenciar-presenca" method="POST" enctype="multipart/form-data">
                <div class="form-group" style="width: 40%; margin-right: 10px; float: left">
                    <label for="id">Selecione o evento</label>
                    <select name="evento" class="form-control" onchange="seleciona_evento()">
                        <option value=''> -- Selecione --</option>
                        <?php
                        $data_atual = date('Y-m-d');
                        $objBanco = new Database();
                        $objEventos = new DAO_Eventos();
                        $buscaEventos = "SELECT * FROM eventos WHERE status_id_INT='1' AND data_final >= '".$data_atual."' ORDER BY data_inicio ASC, hora_inicio ASC";
                        $objBanco->Query($buscaEventos);
                        while ($exibeEventos = $objBanco->FetchObject()) {
                            $objEventos->select($exibeEventos->id);
                            ?>

                            <option
                                value='<?= $objEventos->getid() ?>' <?php if ($idEvento == $objEventos->getid()) echo 'selected'; ?>><?= $objEventos->gettitulo() ?></option>
                        <?php } ?>

                        <?php
                        $buscaEventos = "SELECT * FROM eventos WHERE status_id_INT='1' AND data_final < '".$data_atual."' ORDER BY data_final DESC, hora_inicio DESC";
                        $objBanco->Query($buscaEventos);
                        while ($exibeEventos = $objBanco->FetchObject()) {
                            $objEventos->select($exibeEventos->id);
                            ?>

                            <option
                                value='<?= $objEventos->getid() ?>' <?php if ($idEvento == $objEventos->getid()) echo 'selected'; ?>><?= $objEventos->gettitulo() ?></option>
                        <?php } ?>
                    </select>
                </div>
            </form>

            <?php if ($idEvento != "") { ?>

            <a class="btn btn-link" style="float: right;" href="/admin/sistema/ver-lista-presenca/<?=$idEvento?>">Lista de Presença</a>

            <div class="box box-primary">
                <div class="box-header">
                    <h3 class="box-title">Listagem de Inscrições</h3>
                </div>
                <div class="box-body no-padding">
                    <table class="table">
                        <tr>
                            <th style="width: 10px">#</th>
                            <th>Participante</th>
                            <th>Evento</th>
                            <th style="text-align: center">Inscrito em</th>
                            <th style="min-width: 200px;text-align:center;">Ações</th>
                        </tr>
                        <?php
                        $data_atual = date('Y-m-d');
                        $objListaInscricoes = new DAO_Inscricoes();
                        $objEventosInscritos = new DAO_Eventos();
                        $objParticipantes = new DAO_Participantes();
                        $objBanco3 = new Database();
                        if ($idEvento != "") {
                            $buscaInscricoes = "SELECT * FROM inscricoes WHERE evento_id_INT='" . $idEvento . "' AND status_id_INT='1' ORDER BY datetime DESC";
                        }
                        $objBancoListaInscricoes = new Database();
                        $objBancoListaInscricoes->Query($buscaInscricoes);
                        $objBancoPresenca = new Database();
                        $contador = 0;
                        while ($row = $objBancoListaInscricoes->FetchObject()) {
                            $contador++;
                            $objListaInscricoes->select($row->id);
                            $objParticipantes->select($objListaInscricoes->getparticipante_id_INT());
                            $objEventosInscritos->select($objListaInscricoes->getevento_id_INT());
                            $verificaPresenca = "SELECT * FROM presenca WHERE participante_id_INT='".$objParticipantes->getid()."' AND evento_id_INT='".$objEventosInscritos->getid()."'";
                            $totalPresenca = $objEventosInscritos->rows($verificaPresenca);
                            if($totalPresenca == 0){
                                $presenca = false;
                            }else{
                                $presenca = true;
                            }
                            ?>
                            <tr>
                                <td <?php if ($presenca == false) {
                                    echo 'style="background-color: #FFE4E1"';
                                } else {
                                    echo 'style="background-color: #90EE90" ';
                                } ?> align="center"><?= $contador ?></td>
                                <td><?= ($objParticipantes->getnome()) ?></td>
                                <td><?= $objEventosInscritos->gettitulo() ?></td>
                                <td style="text-align: center"><?=$objListaInscricoes->FDataTimeExibe($objListaInscricoes->getdatetime())?></td>
                                <td align="center">
                                    <?php if($presenca == true){ ?>
                                    <a href="/admin/sistema/gerar-cracha/<?= $objListaInscricoes->getid() ?>" title="Clique aqui para gerar o crachá para o participante"><img src="/admin/img/icone-cracha.jpg" width="30"></a>
                                    <?php } ?>
                                    <a href="/admin/sistema/<?php if ($presenca == false) {echo "confirmar-presenca"; } else { echo "desconfirmar-presenca"; } ?>/<?= $objListaInscricoes->getid() ?>"><button class="btn btn-info btn-sm"><?php if ($presenca == false) { echo "Confirmar Presença"; } else { echo "Desconfirmar Presença"; } ?></button></a>
                                <!--<a href="/admin/sistema/ver-inscricao/<?= $objListaInscricoes->getid() ?>">
                                    <button class="btn btn-primary btn-sm">Detalhes</button>
                                    </a>
                                    <a href="/admin/sistema/excluir-inscricao/<?= $objListaInscricoes->getid() ?>"><button class="btn btn-danger btn-sm">Excluir</button></a>-->
                                </td>
                            </tr>
                            <?php
                        }
                        ?>
                    </table>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->

            <?php } ?>

            <!--<div class="box-footer">
                <a style="float: right;" href="cadastrar_avaliacoes_marcacao.php" class="btn btn-group">Cadastrar novo horário</a>
            </div>-->

        </section>
        <!-- /.content -->
    </aside>
    <!-- /.right-side -->
</div>
<!-- ./wrapper -->

<script>
    $(document).ready(function () {
        $("li#presenca").addClass("active");
        $("li#gerenciar_presenca").addClass("active");
    });
</script>
<?php include("../../rodape.php"); ?>
</body>
</html>