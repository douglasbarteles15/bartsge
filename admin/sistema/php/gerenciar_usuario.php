<?php
ob_start();
//Inicialização do Sistema
require("../../../site/includes/funcoes.php");

//Verificar a permissão de acesso
if ($_SESSION["logado"] == "logado") {
    $objUser = new EXT_Funcionarios();
    $objUser->select($_SESSION["usuario"]);
} else {
    header("location:" . $SiteHTTP . "admin/?msgErro=Realize seu Login");
}
$titulo = "Gerenciar Usuários";
if (FDados("filtro") != "") {
    $filtro = FDados("filtro");
}
?>
<!DOCTYPE html>
<html>
<head>
    <?php include("../../metas.php"); ?>
    <title><?= $titulo ?> - Administração <?= $objConfig_site->getrazao_social(); ?></title>
</head>
<body class="skin-blue">

<?php include '../includes/topo.php'; ?>
<div class="wrapper row-offcanvas row-offcanvas-left">
    <!-- Left side column. contains the logo and sidebar -->
    <aside class="left-side sidebar-offcanvas">
        <!-- sidebar: style can be found in sidebar.less -->
        <?php include '../includes/menu.php'; ?>
        <!-- /.sidebar -->
    </aside>

    <!-- Right side column. Contains the navbar and content of the page -->
    <aside class="right-side">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                <?= $titulo ?>
            </h1>
            <hr style="margin-top: 10px;margin-bottom: 10px;">
            <form action="gerenciar-usuarios" method="post" name="form_filtro">
                <div class="form-group" style="margin-left: 10px;">
                    <label for="filtro">Filtrar por:</label>
                    <select name="filtro" required class="form-control"
                            onchange="document.forms['form_filtro'].submit();">
                        <option value="">Todos</option>
                        <option value="1" <?php if ($filtro == 1) {
                            echo 'selected';
                        } ?> >Administradores
                        </option>
                        <option value="2" <?php if ($filtro == 2) {
                            echo 'selected';
                        } ?> >Usuários Comuns
                        </option>
                    </select>
                </div>
            </form>
            <ol class="breadcrumb">
                <li><a href="../index.php"><i class="fa fa-dashboard"></i> Home</a></li>
                <li>Usuário</li>
                <li class="active"><?= $titulo ?></li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">

            <?php require("../../../comuns/libs/ajax/php/divRetorno_full.php"); ?>

            <!-- Listagens de cadastro -->
            <div class="box box-primary">
                <div class="box-header">
                    <h3 class="box-title">Listagem de Usuários</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body no-padding">
                    <table class="table">
                        <tr>
                            <th>Nome:</th>
                            <th>Login:</th>
                            <th>Tipo:</th>
                            <th style="width: 150px; text-align:center;">Ações</th>
                        </tr>
                        <?php
                        $objListaFuncionarios = new DAO_Funcionarios();
                        if ($filtro != "") {
                            $sqlListaFuncionarios = "Select idfuncionario from funcionario where administrador='" . $filtro . "' order by administrador ASC,nome ASC";
                        } else {
                            $sqlListaFuncionarios = "Select idfuncionario from funcionario order by administrador ASC,nome ASC";
                        }
                        $objBancoListaFuncionarios = new Database();
                        $objBancoListaFuncionarios->Query($sqlListaFuncionarios);
                        while ($row = $objBancoListaFuncionarios->FetchObject()) {
                            $objListaFuncionarios->select($row->idfuncionario);
                            if ($objListaFuncionarios->getadministrador() == 1) {
                                $tipo_usuario2 = "Administrador";
                            } elseif ($objListaFuncionarios->getadministrador() == 2) {
                                $tipo_usuario2 = "Usu&aacute;rio Comum";
                            }
                            ?>
                            <tr>
                                <td><?= ($objListaFuncionarios->getnome()) ?></td>
                                <td><?= utf8_encode($objListaFuncionarios->getlogin()) ?></td>
                                <td><?= utf8_encode($tipo_usuario2) ?></td>
                                <td align="center">
                                    <a href="/admin/sistema/excluir-usuario/<?= $objListaFuncionarios->idfuncionario ?>">
                                        <button class="btn btn-danger btn-sm"
                                                onclick="return confirm('Tem certeza que deseja excluir esse usuário?')">
                                            Excluir
                                        </button>
                                    </a>
                                </td>
                            </tr>
                            <?php
                        }
                        ?>
                    </table>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->

            <div class="box-footer">
                <a style="float: right;" href="/admin/sistema/cadastrar-usuarios" class="btn btn-group">Cadastrar novo registro</a>
            </div>

            <!-- top row -->
            <div class="row">
                <div class="col-xs-12 connectedSortable">

                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->

            <!-- Main row -->

        </section>
        <!-- /.content -->
    </aside>
    <!-- /.right-side -->
</div>
<!-- ./wrapper -->

<script>
    $(document).ready(function () {
        $("li#usuarios").addClass("active");
        $("li#gerenciar_usuarios").addClass("active");
    });
</script>

<?php include("../../rodape.php"); ?>

</body>
</html>