<?php
ob_start();
//Inicialização do Sistema
require("../../../site/includes/funcoes.php");

//Verificar a permissão de acesso
if ($_SESSION["logado"] == "logado") {
    $objUser = new EXT_Usuarios_Admin();
    $objUser->select($_SESSION["usuario"]);
} else {
    header("location:" . $SiteHTTP . "admin/?msgErro=Realize seu Login");
}

if (FDados("id") != "") {
    $action = "/admin/sistema/alterar-evento";
    $objEventos = new DAO_Eventos();
    $objEventos->select(FDados("id"));
    $idCategoria = $objEventos->getcategoria_id_INT();
    $idOrganizador = $objEventos->getusuario_id_INT();
    $titulo = "Editar Evento";
    $botao = "Editar";
    $criterio = "checked";
    $id = FDados("id");
} else {
    $action = "/admin/sistema/add-evento";
    $titulo = "Cadastrar Evento";
    $botao = "Cadastrar";
    $criterio = "";
}
$data_atual = date('d/m/Y');
?>
<!DOCTYPE html>
<html>
<head>
    <?php include("../../metas.php"); ?>
    <title><?= $titulo ?> - Administração <?= $objConfig_site->getrazao_social(); ?></title>
</head>
<body class="skin-blue">
<?php include '../includes/topo.php'; ?>
<div class="wrapper row-offcanvas row-offcanvas-left">
    <!-- Left side column. contains the logo and sidebar -->
    <aside class="left-side sidebar-offcanvas">
        <!-- sidebar: style can be found in sidebar.less -->
        <?php include '../includes/menu.php'; ?>
        <!-- /.sidebar -->
    </aside>

    <!-- Right side column. Contains the navbar and content of the page -->
    <aside class="right-side">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                <?= $titulo ?>
            </h1>
            <ol class="breadcrumb">
                <li><a href="../index.php"><i class="fa fa-dashboard"></i> Home</a></li>
                <li>Eventos</li>
                <li class="active"><?= $titulo ?></li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">

            <div class="box box-primary">
                <div class="box-header">
                    <h3 class="box-title"><?= $titulo ?></h3>
                </div>
                <!-- /.box-header -->
                <?php require("../../../comuns/libs/ajax/php/divRetorno_full.php"); ?>

                <!-- form start -->
                <form role="form" action="<?= $action ?>" method="POST" enctype="multipart/form-data">
                    <input type="hidden" name="id" value="<?= FDados("id") ?>"/>

                    <div class="box-body">
                        <div class="form-group">
                            <label for="titulo">Título do Evento</label>
                            <input type="text" name="titulo" class="form-control" id="titulo"
                                   placeholder="Evento" value="<?php if (FDados("id") != "") {
                                echo($objEventos->gettitulo());
                            } ?>" required>
                        </div>

                        <div class="form-group">
                            <label for="usuario_id_INT">Organizador</label>
                            <select name="usuario_id_INT" class="form-control" required>
                                <?php
                                echo "<option value=''>Selecione um Organizador</option>";
                                $objBancoOrganizador = new Database();
                                $sql = "select * from usuarios_admin where tipo_id_INT='2' order by nome";
                                $objBancoOrganizador->Query($sql);
                                while ($row = $objBancoOrganizador->FetchObject()) {
                                    if ($idOrganizador == $row->id) {
                                        echo "<option value='$row->id' selected='selected' >$row->nome</option>";
                                    } else {
                                        echo "<option value='$row->id' style='font-weight:bold'>$row->nome</option>";
                                    }
                                }
                                ?>
                            </select>
                        </div>

                        <div class="form-group">
                            <label for="categoria_id_INT">Categoria</label>
                            <select name="categoria_id_INT" class="form-control" required>
                                <?php
                                echo "<option value=''>Selecione uma Categoria</option>";
                                $objBancoCategorias = new Database();
                                $sql = "select * from categorias order by titulo";
                                $objBancoCategorias->Query($sql);
                                while ($row = $objBancoCategorias->FetchObject()) {
                                    if ($idCategoria == $row->id) {
                                        echo "<option value='$row->id' selected='selected' >$row->titulo</option>";
                                    } else {
                                        echo "<option value='$row->id' style='font-weight:bold'>$row->titulo</option>";
                                    }
                                }
                                ?>
                            </select>
                        </div>

                        <div class="form-group">
                            <label for="periodo">Período</label>
                            <input type="text" name="periodo" class="form-control datas" id="periodo"
                                   value="<?php if (FDados("id") != "") {
                                       echo $objEventos->FDataExibe($objEventos->getdata_inicio()).' - '.$objEventos->FDataExibe($objEventos->getdata_final());
                                   } ?>" required/>
                        </div>

                        <div class="form-group">
                            <label for="hora_inicio">Horário de Início</label>
                            <input type="text" name="hora_inicio" class="form-control hora" id="hora_inicio"
                                   value="<?php if (FDados("id") != "") {
                                       echo($objEventos->gethora_inicio());
                                   } ?>"/>
                        </div>

                        <div class="form-group">
                            <label for="hora_final">Horário de Término</label>
                            <input type="text" name="hora_final" class="form-control hora" id="hora_final"
                                   value="<?php if (FDados("id") != "") {
                                       echo($objEventos->gethora_final());
                                   } ?>"/>
                        </div>

                        <div class="form-group">
                            <label for="vagas_total">Número de Vagas</label>
                            <input type="number" name="vagas_total" class="form-control" id="vagas_total"
                                   value="<?php if (FDados("id") != "") {
                                echo $objEventos->getvagas_total();
                            } ?>" <?php if (FDados("id") != "") { echo 'disabled'; } ?> required>
                        </div>

                        <div class="form-group">
                            <label for="chamada">Chamada</label>
                                    <textarea name="chamada" class="form-control" rows="5"
                                              id="chamada"><?php if (FDados("id") != "") {
                                            echo ($objEventos->getchamada());
                                        } ?></textarea>
                        </div>

                        <div class="form-group">
                            <label for="status_id_INT">Status</label>

                            <div class="radio">
                                <label for="ativo">
                                    <input type="radio" name="status_id_INT"
                                           value="1" <?php if (FDados("id") == "") {
                                        echo 'checked';
                                    } else {
                                        if ($objEventos->getstatus_id_INT() == "1") {
                                            echo 'checked';
                                        }
                                    } ?>> Ativo
                                </label>

                                <div style="clear:both; height:5px"></div>
                                <label for="inativo">
                                    <input type="radio" name="status_id_INT"
                                           value="0" <?php if (FDados("id") != "") {
                                        if ($objEventos->getstatus_id_INT() == "0") {
                                            echo 'checked';
                                        }
                                    } ?>> Inativo
                                </label>
                            </div>

                        </div>

                        <div class="form-group">
                            <label for="pago_id_INT">Tipo</label>

                            <div class="radio">
                                <label for="pago">
                                    <input type="radio" name="pago_id_INT"
                                           value="1" <?php if (FDados("id") == "") {
                                        echo 'checked';
                                    } else {
                                        if ($objEventos->getpago_id_INT() == "1") {
                                            echo 'checked';
                                        }
                                    } ?>> Pago
                                </label>

                                <div style="clear:both; height:5px"></div>
                                <label for="gratuito">
                                    <input type="radio" name="pago_id_INT"
                                           value="0" <?php if (FDados("id") != "") {
                                        if ($objEventos->getpago_id_INT() == "0") {
                                            echo 'checked';
                                        }
                                    } ?>> Gratuito
                                </label>
                            </div>

                        </div>

                        <div class="form-group">
                            <label for="editor1">Descrição</label>
                                    <textarea name="descricao" class="form-control" rows="15"
                                              id="editor1"><?php if (FDados("id") != "") {
                                            echo($objEventos->getdescricao());
                                        } ?></textarea>
                        </div>

                        <div class="form-group">
                            <label for="video">Vídeo de Destaque (iframe de incorporação)</label>
                            <input type="text" name="video" class="form-control" id="video"
                                   placeholder="http://www.youtube.com.br/" value="<?php if (FDados("id") != "") {
                                echo($objEventos->getvideo());
                            } ?>">
                        </div>

                        <div class="form-group">
                            <label for="imagem">Imagem</label>
                            <input type="file" name="imagem" class="form-control">
                            <br/>
                            <font style="font-size: 12px;">SOMENTE ARQUIVOS JPG</font>
                            <br/>
                            <font style="font-size: 12px;">RESOLUÇÃO RECOMENDADA: 465 x 291 pixels</font>

                            <?php
                            if (FDados("id") != "") {
                                if ($objEventos->getimg_mini() != "") {
                                    if (file_exists("../../../site/uploads/img_eventos/" . $objEventos->getimg_mini())) {
                                        ?>
                                        <div id="img_1"><img src="<?=$SiteHTTP?>site/uploads/img_eventos/<?= $objEventos->getimg_mini() ?>" style="margin-right:5px;max-width: 50%;"/>
                                            <!--<input type='button' value="Excluir" class="form_botao2" style="cursor: pointer;" onclick="javascript:getDados('img_1','../validacoes/acoes/excluir_img_evento.php?id=<?= $id; ?>&imgMini=<?= $objEventos->getimg_mini(); ?>&imgFull=<?= $objEventos->getimg_full() ?>');">-->
                                        </div>
                                    <?php }
                                }
                            }
                            ?>
                        </div>
                        

                    </div>
                    <!-- /.box-body -->

                    <div class="box-footer">
                        <button type="submit" class="btn btn-primary"><?= $botao ?></button>
                        <a style="float: right;" href="/admin/sistema/gerenciar-eventos" class="btn btn-group">Gerenciar</a>
                    </div>
                </form>
            </div>

            <!-- Main row -->

        </section>
        <!-- /.content -->
    </aside>
    <!-- /.right-side -->
</div>
<!-- ./wrapper -->

<script>
    $(document).ready(function () {
        $("li#eventos").addClass("active");
        $("li#cadastrar_eventos").addClass("active");
    });
</script>
<?php include("../../rodape.php"); ?>
</body>
</html>