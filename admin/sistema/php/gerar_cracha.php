<?php
ob_start();
//Inicialização do Sistema
require("../../../site/includes/funcoes.php");

//Verificar a permissão de acesso
if ($_SESSION["logado"] == "logado") {
    $objUser = new EXT_Usuarios_Admin();
    $objUser->select($_SESSION["usuario"]);
} else {
    header("location:" . $SiteHTTP . "admin/?msgErro=Realize seu Login");
}

$titulo = "Crachá do Participante";

$idInscricao = FDados("inscricao");

if ($idInscricao != "") {
    $objInscricao = new DAO_Inscricoes();
    $objInscricao->select($idInscricao);
    $objParticipante = new DAO_Participantes();
    $objParticipante->select($objInscricao->getparticipante_id_INT());
    $objEvento = new DAO_Eventos();
    $objEvento->select($objInscricao->getevento_id_INT());
    $buscaCidade = "SELECT nome, estado FROM cidade WHERE id='".$objParticipante->getcidade()."'";
    $objBanco->Query($buscaCidade);
    $exibeCidade = $objBanco->FetchObject();
    $cidade = $exibeCidade->nome;
    $buscaEstado = "SELECT uf FROM estado WHERE id='".$objParticipante->getestado()."'";
    $objBanco->Query($buscaEstado);
    $exibeEstado = $objBanco->FetchObject();
    $estado = $exibeEstado->uf;
}

?>
<!DOCTYPE html>
<html>
<head>
    <?php include("../../metas.php"); ?>
    <title><?= $titulo ?> - Administração <?= $objConfig_site->getrazao_social(); ?></title>
    <style>
        @media print {
            body * {
                visibility: hidden;
            }
            #printable, #printable * {
                visibility: visible;
            }
        }
    </style>
</head>
<body class="skin-blue">
<?php include '../includes/topo.php'; ?>
<div class="wrapper row-offcanvas row-offcanvas-left">
    <!-- Left side column. contains the logo and sidebar -->
    <aside class="left-side sidebar-offcanvas">
        <!-- sidebar: style can be found in sidebar.less -->
        <?php include '../includes/menu.php'; ?>
        <!-- /.sidebar -->
    </aside>

    <!-- Right side column. Contains the navbar and content of the page -->
    <aside class="right-side">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Crachá do Participante
            </h1>
            <hr style="margin-top: 10px;margin-bottom: 10px;">

            <ol class="breadcrumb">
                <li><a href="../index.php"><i class="fa fa-dashboard"></i> Home</a></li>
                <li>Crachá</li>
                <li class="active">Crachá do Participante</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">

            <button class="btn btn-link" style="float: right;" onclick="window.print();">Imprimir</button>

            <?php require("../../../comuns/libs/ajax/php/divRetorno_full.php"); ?>

            <?php if ($idInscricao != "") { ?>

            <div class="box box-primary" id="printable">
                <div class="box-header">
                    <h3 class="box-title">Crachá do Participante</h3>
                </div>
                <div class="box-body" style="border: 2px solid #666; border-radius: 5px; max-width: 400px; text-align: center">
                    <?php if($objParticipante->getimg_mini()!=""){ ?>
                    <p><img src="/site/uploads/img_participantes/<?=$objParticipante->getimg_mini()?>" style="max-width: 100px"></p>
                    <?php } ?>
                    <p><b>Nome: </b><?=$objParticipante->getnome()?></p>
                    <p><?=$cidade?>/<?=$estado?></p>
                    <p><b>Evento: </b><?=$objEvento->gettitulo()?></p>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->

            <?php } ?>

            <div class="box-footer">
                <a style="float: right;" href="/admin/sistema/gerenciar-presenca/<?=$objEvento->getid()?>" class="btn btn-group">Voltar</a>
            </div>

        </section>
        <!-- /.content -->
    </aside>
    <!-- /.right-side -->
</div>
<!-- ./wrapper -->

<script>
    $(document).ready(function () {
        $("li#presenca").addClass("active");
        $("li#gerenciar_presenca").addClass("active");
    });
</script>
<?php include("../../rodape.php"); ?>
</body>
</html>