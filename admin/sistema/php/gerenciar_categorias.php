<?php
ob_start();
//Inicialização do Sistema
require("../../../site/includes/funcoes.php");

//Verificar a permissão de acesso
if ($_SESSION["logado"] == "logado") {
    $objUser = new EXT_Usuarios_Admin();
    $objUser->select($_SESSION["usuario"]);
} else {
    header("location:" . $SiteHTTP . "admin/?msgErro=Realize seu Login");
}
$titulo = "Gerenciar Categorias";
?>
<!DOCTYPE html>
<html>
<head>
    <?php include("../../metas.php"); ?>
    <title><?= $titulo ?> - Administração <?= $objConfig_site->getrazao_social(); ?></title>
</head>
<body class="skin-blue">
<?php include '../includes/topo.php'; ?>
<div class="wrapper row-offcanvas row-offcanvas-left">
    <!-- Left side column. contains the logo and sidebar -->
    <aside class="left-side sidebar-offcanvas">
        <!-- sidebar: style can be found in sidebar.less -->
        <?php include '../includes/menu.php'; ?>
        <!-- /.sidebar -->
    </aside>

    <!-- Right side column. Contains the navbar and content of the page -->
    <aside class="right-side">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                <?= $titulo ?>
            </h1>
            <ol class="breadcrumb">
                <li><a href="../index.php"><i class="fa fa-dashboard"></i> Home</a></li>
                <li>Categorias</li>
                <li class="active"><?= $titulo ?></li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">

            <?php require("../../../comuns/libs/ajax/php/divRetorno_full.php"); ?>

            <!-- Listagens de cadastro -->
            <div class="box box-primary">
                <div class="box-header">
                    <h3 class="box-title">Listagem de Categorias</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body no-padding">
                    <table class="table">
                        <tr>
                            <th style="width: 10px">#</th>
                            <th>Título</th>
                            <th style="width: 330px; text-align:center;">Ações</th>
                        </tr>
                        <?php
                        $objListaCategorias = new DAO_Categorias();
                        $objListaCategoriasPai = new DAO_Categorias();
                        $sqlListaCategorias = "Select id from categorias order by titulo ASC";
                        $objBancoListaCategorias = new Database();
                        $contador = 0;
                        $objBancoListaCategorias->Query($sqlListaCategorias);
                        while ($row = $objBancoListaCategorias->FetchObject()) {
                            $contador++;
                            $objListaCategorias->select($row->id);
                            ?>
                            <tr>
                                <td><?= $contador ?></td>
                                <td><?= $objListaCategorias->gettitulo() ?></td>
                                <td align="center">
                                    <a href="/admin/sistema/editar-categoria/<?= $objListaCategorias->getid() ?>">
                                        <button class="btn btn-primary btn-sm">Editar</button>
                                    </a>
                                    <a href="/admin/sistema/excluir-categoria/<?= $objListaCategorias->getid() ?>">
                                        <button class="btn btn-danger btn-sm"
                                                onclick="return confirm('Tem certeza que deseja excluir essa categoria? Todos os eventos dessa categoria também serão excluídas.')">
                                            Excluir
                                        </button>
                                    </a>
                                </td>
                            </tr>
                            <?php
                        }
                        ?>
                    </table>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->

            <div class="box-footer">
                <a style="float: right;" href="/admin/sistema/cadastrar-categorias" class="btn btn-group">Cadastrar novo registro</a>
            </div>

        </section>
        <!-- /.content -->
    </aside>
    <!-- /.right-side -->
</div>
<!-- ./wrapper -->

<script>
    $(document).ready(function () {
        $("li#categorias_menu").addClass("active");
        $("li#gerenciar_categorias").addClass("active");
    });
</script>
<?php include("../../rodape.php"); ?>
</body>
</html>