<?php
ob_start();
//Inicialização do Sistema
require("../../../site/includes/funcoes.php");

//Verificar a permissão de acesso
if ($_SESSION["logado"] == "logado") {
    $objUser = new EXT_Usuarios_Admin();
    $objUser->select($_SESSION["usuario"]);
} else {
    header("location:" . $SiteHTTP . "admin/?msgErro=Realize seu Login");
}

if (FDados("id") != "") {
    $action = "/admin/sistema/alterar-categoria";
    $objCategorias = new DAO_Categorias();
    $objCategorias->select(FDados("id"));
    $titulo = "Editar Categoria";
    $botao = "Editar";
    $criterio = "checked";
    $id = FDados("id");

} else {
    $action = "/admin/sistema/add-categoria";
    $titulo = "Cadastrar Categoria";
    $botao = "Cadastrar";
    $criterio = "";
}
?>
<!DOCTYPE html>
<html>
<head>
    <?php include("../../metas.php"); ?>
    <title><?= $titulo ?> - Administração <?= $objConfig_site->getrazao_social(); ?></title>
</head>
<body class="skin-blue">
<?php include '../includes/topo.php'; ?>
<div class="wrapper row-offcanvas row-offcanvas-left">
    <!-- Left side column. contains the logo and sidebar -->
    <aside class="left-side sidebar-offcanvas">
        <!-- sidebar: style can be found in sidebar.less -->
        <?php include '../includes/menu.php'; ?>
        <!-- /.sidebar -->
    </aside>

    <!-- Right side column. Contains the navbar and content of the page -->
    <aside class="right-side">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Categorias
            </h1>
            <ol class="breadcrumb">
                <li><a href="../index.php"><i class="fa fa-dashboard"></i> Home</a></li>
                <li>Categorias</li>
                <li class="active"><?= $titulo ?></li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">

            <div class="box box-primary">
                <div class="box-header">
                    <h3 class="box-title"><?= $titulo ?></h3>
                </div>
                <!-- /.box-header -->
                <?php require("../../../comuns/libs/ajax/php/divRetorno_full.php"); ?>

                <!-- form start -->
                <form role="form" action="<?= $action ?>" method="POST" enctype="multipart/form-data">
                    <input type="hidden" name="id" value="<?= FDados("id") ?>"/>

                    <div class="box-body">
                        <div class="form-group">
                            <label for="titulo">Título</label>
                            <input type="text" name="titulo" class="form-control" id="titulo"
                                   placeholder="Categoria" value="<?php if (FDados("id") != "") {
                                echo $objCategorias->gettitulo();
                            } ?>" required>
                        </div>

                        <div class="form-group">
                            <label for="imagem">Imagem</label>
                            <input type="file" name="imagem" class="form-control">
                            <br/>
                            <font style="font-size: 12px;">SOMENTE ARQUIVOS JPG</font>
                            <br/>
                            <font style="font-size: 12px;">RESOLUÇÃO RECOMENDADA: 237 x 148 pixels</font>

                            <?php
                            if (FDados("id") != "") {
                                if ($objCategorias->getimg_mini() != "") {
                                    if (file_exists("../../../site/uploads/img_categorias/" . $objCategorias->getimg_mini())) {
                                        ?>
                                        <div id="img_1"><img src="<?=$SiteHTTP?>site/uploads/img_categorias/<?= $objCategorias->getimg_mini() ?>" style="margin-right:5px;max-width: 50%;"/>
                                            <!--<input type='button' value="Excluir" class="form_botao2" style="cursor: pointer;" onclick="javascript:getDados('img_1','../validacoes/acoes/excluir_img_categoria.php?id=<?= $id; ?>&imgMini=<?= $objCategorias->getimg_mini(); ?>&imgFull=<?= $objCategorias->getimg_full() ?>');">-->
                                        </div>
                                    <?php }
                                }
                            }
                            ?>
                        </div>

                    </div>
                    <!-- /.box-body -->

                    <div class="box-footer">
                        <button type="submit" class="btn btn-primary"><?= $botao ?></button>
                        <a style="float: right;" href="/admin/sistema/gerenciar-categorias" class="btn btn-group">Gerenciar</a>
                    </div>
                </form>
            </div>

        </section>
        <!-- /.content -->
    </aside>
    <!-- /.right-side -->
</div>
<!-- ./wrapper -->

<script>
    $(document).ready(function () {
        $("li#categorias_menu").addClass("active");
        $("li#cadastrar_categorias").addClass("active");
    });
</script>
<?php include("../../rodape.php"); ?>
</body>
</html>