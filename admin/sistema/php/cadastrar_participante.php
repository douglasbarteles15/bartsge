<?php
ob_start();
//Inicialização do Sistema
require("../../../site/includes/funcoes.php");

//Verificar a permissão de acesso
if ($_SESSION["logado"] == "logado") {
    $objUser = new EXT_Funcionarios();
    $objUser->select($_SESSION["usuario"]);
} else {
    header("location:" . $SiteHTTP . "admin-promocoes/?msgErro=Realize seu Login");
}

//Configurações do site
$objConfig_site = new DAO_Config_site();
$objConfig_site->select(1);

if (FDados("id") != "") {
    $action = "alterar-cadastro-troca/";
    $objCadastro = new DAO_Cadastro_promocoes();
    $objCadastro->select(FDados("id"));
    $titulo = "Editar um Cadastro";
    $botao = "Editar";
} else {
    $action = "add-cadastro-troca/";
    $titulo = "Cadastrar uma Troca";
    $botao = "Cadastrar";
}


//Selecionar a Promoção
if (FDados("promocao") != "") {
    $objPromocao = new DAO_Promocoes();
    $objPromocao->select(FDados("promocao"));

//    if ($objPromocao->getcriterio_promocao() != 1) {
//        header("location:cadastrar_participante.php");
//    }
} else {
    header("location:gerenciar_promocoes.php?msgAlert=Selecione uma Promoção!");
}

?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">

        <style type="text/css">
            .suggestionsBox {
                position: relative;
                left: 30px;
                margin: 10px 0px 0px -30px;
                width: 50%;
                background-color: #D9EDF7;
                -moz-border-radius: 7px;
                -webkit-border-radius: 7px;
                border: 2px solid #C9EAF3;	
                color: #659CD8;
            }

            .suggestionList {
                margin: 0px;
                padding: 0px;
            }

            .suggestionList ul {
                list-style:none;
            }

            .suggestionList li {
                list-style:none;
                margin: 0px 0px 3px 0px;
                padding: 3px;
                cursor: pointer;
            }

            .suggestionList li:hover {
                zbackground-color: #659CD8;
            }
        </style>

        <title>ADMINISTRAÇÃO | HOME - <?= $objConfig_site->getrazao_social(); ?></title>
        <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
        <!-- bootstrap 3.0.2 -->
        <link href="../../css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <!-- font Awesome -->
        <link href="../../css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <!-- Ionicons -->
        <link href="../../css/ionicons.min.css" rel="stylesheet" type="text/css" />
        <!-- Morris chart -->
        <link href="../../css/morris/morris.css" rel="stylesheet" type="text/css" />
        <!-- jvectormap -->
        <link href="../../css/jvectormap/jquery-jvectormap-1.2.2.css" rel="stylesheet" type="text/css" />
        <!-- fullCalendar -->
        <link href="../../css/fullcalendar/fullcalendar.css" rel="stylesheet" type="text/css" />
        <!-- Daterange picker -->
        <link href="../../css/daterangepicker/daterangepicker-bs3.css" rel="stylesheet" type="text/css" />
        <!-- bootstrap wysihtml5 - text editor -->
        <link href="../../css/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css" rel="stylesheet" type="text/css" />
        <!-- Theme style -->
        <link href="../../css/AdminLTE.css" rel="stylesheet" type="text/css" />

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.../../js/1.3.0/respond.min.js"></script>
        <![endif]-->
    </head>
    <body class="skin-blue">
        <?php include '../includes/topo.php'; ?>
        <div class="wrapper row-offcanvas row-offcanvas-left">
            <!-- Left side column. contains the logo and sidebar -->
            <aside class="left-side sidebar-offcanvas">
                <!-- sidebar: style can be found in sidebar.less -->
                <?php include '../includes/menu.php'; ?>
                <!-- /.sidebar -->
            </aside>

            <!-- Right side column. Contains the navbar and content of the page -->
            <aside class="right-side">
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                        Promoção <?= utf8_encode($objPromocao->getnome_promocao()) ?>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="../index.php"><i class="fa fa-dashboard"></i> Home</a></li>
                        <li>Cadastro para Troca</li>
                        <li class="active"><?= $titulo ?></li>
                    </ol>
                </section>

                <!-- Main content -->
                <section class="content">

                    <div class="box box-primary">
                        <div class="box-header">
                            <h3 class="box-title">Dados do Cliente</h3>
                        </div><!-- /.box-header -->
                        <?php require("../../../comuns/libs/ajax/php/divRetorno_full.php"); ?>

                        <!-- form start -->
                        <form role="form" action="<?= $action ?>" method="POST">
                            <input type="hidden" name="idpromocoes" value="<?= FDados("id") ?>"/>
                            <div class="box-body">
                                <div class="form-group">
                                    <label for="clientes_nome">Nome Completo</label>
                                    <input type="text" name="clientes_nome" class="form-control" id="clientes_nome" placeholder="Nome" value="">
                                </div>
                                <div class="form-group">
                                    <label for="clientes_nascimento_DATE">Data de Nascimento</label>
                                    <input type="text" name="clientes_nascimento_DATE" class="form-control" id="clientes_nascimento_DATE" placeholder="" data-inputmask="'alias': 'dd/mm/yyyy'" data-mask/>
                                </div>
                                <div class="form-group">
                                    <label for="clientes_email">Email</label>
                                    <input type="text" name="clientes_email" class="form-control" id="clientes_email" placeholder="Email" value="<?php if (FDados("id") != "") {
                            echo utf8_encode();
                        } ?>">
                                </div>
                                <div class="form-group">
                                    <label for="clientes_cpf">*CPF</label>
                                    <input type="text" name="clientes_cpf" class="form-control " id="clientes_cpf" placeholder="CPF" data-inputmask='"mask": "999.999.999-99"' data-mask value="<?= $_GET["cpf"] ?>" >
                                </div>
                                <div class="form-group">
                                    <label for="clientes_telefone">Telefone</label>
                                    <input type="text" class="form-control" name="clientes_telefone" data-inputmask='"mask": "(99) 999999999"' data-mask/>
                                </div>
                                <div class="form-group">
                                    <label for="clientes_telefone2">Celular</label>
                                    <input type="text" class="form-control" name="clientes_telefone2" data-inputmask='"mask": "(99) 999999999"' data-mask/>
                                </div>
                                <div class="form-group">
                                    <label for="clientes_cep">CEP</label>
                                    <input type="text" name="clientes_cep" class="form-control" id="cep"  onblur="getEndereco()" placeholder="CEP" data-inputmask='"mask": "99999-999"' data-mask value="<?php if (FDados("id") != "") {
                            echo utf8_encode();
                        } ?>">
                                </div>

                                <div class="form-group">
                                    <label for="clientes_endereco">Rua</label>
                                    <input type="text" name="clientes_endereco" id="endereco"  class="form-control" placeholder="Endereço" value="<?php if (FDados("id") != "") {
                            echo utf8_encode();
                        } ?>">
                                </div>

                                <div class="form-group">
                                    <label for="clientes_numero">Número</label>
                                    <input type="text" name="clientes_numero"  class="form-control" placeholder="Número" value="<?php if (FDados("id") != "") {
                            echo utf8_encode();
                        } ?>">
                                </div>
                                <div class="form-group">
                                    <label for="clientes_complemento">Complemento</label>
                                    <input type="text" name="clientes_complemento"  class="form-control" placeholder="" value="<?php if (FDados("id") != "") {
                            echo utf8_encode();
                        } ?>">
                                </div>
                                <div class="form-group">
                                    <label for="clientes_bairro">Bairro</label>
                                    <input type="text" name="clientes_bairro" id="bairro"  class="form-control" placeholder="Bairro" value="<?php if (FDados("id") != "") {
                            echo utf8_encode();
                        } ?>">
                                </div>
                                <div class="form-group">
                                    <label for="clientes_cidade">Cidade</label>
                                    <input type="text" name="clientes_cidade" id="cidade"  class="form-control" placeholder="Cidade" value="<?php if (FDados("id") != "") {
                            echo utf8_encode();
                        } ?>">
                                </div>
                                <div class="form-group">
                                    <label for="clientes_estado">Estado</label>
                                    <input type="text" name="clientes_estado" id="estado"  class="form-control" placeholder="Estado" value="<?php if (FDados("id") != "") {
                            echo utf8_encode();
                        } ?>">
                                </div>
                            </div><!-- /.box-body -->


                            <div class="box box-danger">
                                <div class="box-header">
                                    <h3 class="box-title">Dados da Compra</h3>
                                </div>
                            </div>
                            <div class="form-group" style="margin-left: 10px;">
                                <label for="promocoes_idpromocoes">*Seleciona a Promoção</label>
                                <div class="form-group">
                                     <select class="form-control" name="promocoes_idpromocoes">
                                                <option value="<?= $objPromocao->getidpromocoes() ?>" selected ><?= utf8_encode($objPromocao->getnome_promocao()) ?></option>
                                            </select>
                                </div>
                            </div>
<!--                            <div class="form-group">
                                <label for="lojas">*Loja</label>
                                <input type="text" name="loja" class="form-control" id="inputString" onkeyup="lookup(this.value);"  placeholder="Loja">
                                <div class="suggestionsBox" id="suggestions" style="display: none;">
                                    <div class="suggestionList" id="autoSuggestionsList">
                                        &nbsp;
                                    </div>
                                </div>
                            </div>-->
                            <div class="loja">
                                <div class="form-group" style="margin-left: 10px;">
                                    <label for="valor_compra[]">Valor da compra</label>
                                    <input type="text" name="valor_compra[]" class="form-control" id="valor_compra[]" value="" >
                                </div>

                                 <div class="form-group" style="margin-left: 10px;">
                                    <label for="lojas">*Loja</label>
                                    <select class="form-control"  name="nomeloja[]">
                                            <?php 
                                                $objSelectLoja = new DAO_Lojas();
                                                $objBancoSelectLoja = new Database();
                                                $sqlSelectLoja = "Select idlojas from lojas order by nome_loja asc";
                                                $objBancoSelectLoja->Query($sqlSelectLoja);
                                                while ($row = $objBancoSelectLoja->FetchObject()) {
                                                    $objSelectLoja->select($row->idlojas);
                                            ?>
                                            <option value="<?= $objSelectLoja->getidlojas() ?>"><?= utf8_encode($objSelectLoja->getnome_loja()) ?></option>
                                                <?php } ?>
                                        </select>

    <!--                                <input type="text" name="loja[]" class="form-control loja" id="inputString" onkeyup="lookup(this.value);"  placeholder="Loja">-->
                                </div>
                                
                                <div class="form-group" style="margin-left: 10px;">
                                    <label for="vendedor">Vendedor</label>
                                    <input type="text" name="vendedor[]" id="vendedor"  class="form-control" placeholder="vendedor" value="">
                                </div>
                                
                            </div>

                            

                            <a href="javascript:void(0);" id="add_loja">Adicionar Campo</a>
                            
                            

                            <div class="box-footer">
                                <button type="submit" class="btn btn-primary"><?= $botao ?></button>
                            </div>
                        </form>
                    </div>

                    <!-- top row -->
                    <div class="row">
                        <div class="col-xs-12 connectedSortable">

                        </div><!-- /.col -->
                    </div>
                    <!-- /.row -->

                    <!-- Main row -->

                </section><!-- /.content -->
            </aside><!-- /.right-side -->
        </div><!-- ./wrapper -->

        <!-- add new calendar event modal -->


        <!-- jQuery 2.0.2 -->
<!--                <script src="http://ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>-->
        <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.0/jquery.min.js"></script>
        <!-- Bootstrap -->
        <script src="../../js/bootstrap.min.js" type="text/javascript"></script>
        <script src="../../js/ui-bootstrap-tpls-0.11.0.min.js" type="text/javascript"></script>
        <script src="../../js/ui-utils-0.1.1/ui-utils.js" type="text/javascript"></script>
        <!-- InputMask -->
        <script src="../../js/plugins/input-mask/jquery.inputmask.js" type="text/javascript"></script>
        <script src="../../js/plugins/input-mask/jquery.inputmask.date.extensions.js" type="text/javascript"></script>
        <script src="../../js/plugins/input-mask/jquery.inputmask.extensions.js" type="text/javascript"></script>
        <!-- date-range-picker -->
        <script src="../../js/plugins/daterangepicker/daterangepicker.js" type="text/javascript"></script>
        <!-- bootstrap color picker -->
        <script src="../../js/plugins/colorpicker/bootstrap-colorpicker.min.js" type="text/javascript"></script>
        <!-- bootstrap time picker -->
        <script src="../../js/plugins/timepicker/bootstrap-timepicker.min.js" type="text/javascript"></script>
        <!-- AdminLTE App -->
        <script src="../../js/AdminLTE/app.js" type="text/javascript"></script>

        <!-- Page script -->
        <script type="text/javascript">
                                    $(function() {
                                        //Datemask dd/mm/yyyy
                                        $("#datemask").inputmask("dd/mm/yyyy", {"placeholder": "dd/mm/yyyy"});
                                        //Datemask2 mm/dd/yyyy
                                        $("#datemask2").inputmask("mm/dd/yyyy", {"placeholder": "mm/dd/yyyy"});
                                        $("#datemaskCPF").inputmask("000.000.000-00", {"placeholder": "000.000.000-00"});
                                        //Money Euro
                                        $("[data-mask]").inputmask();

                                        //Date range picker
                                        $('#reservation').daterangepicker();
                                        //Date range picker with time picker
                                        $('#reservationtime').daterangepicker({timePicker: true, timePickerIncrement: 30, format: 'MM/DD/YYYY h:mm A'});
                                        //Date range as a button
                                        $('#daterange-btn').daterangepicker(
                                                {
                                                    ranges: {
                                                        'Today': [moment(), moment()],
                                                        'Yesterday': [moment().subtract('days', 1), moment().subtract('days', 1)],
                                                        'Last 7 Days': [moment().subtract('days', 6), moment()],
                                                        'Last 30 Days': [moment().subtract('days', 29), moment()],
                                                        'This Month': [moment().startOf('month'), moment().endOf('month')],
                                                        'Last Month': [moment().subtract('month', 1).startOf('month'), moment().subtract('month', 1).endOf('month')]
                                                    },
                                                    startDate: moment().subtract('days', 29),
                                                    endDate: moment()
                                                },
                                        function(start, end) {
                                            $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
                                        }
                                        );

                                        //iCheck for checkbox and radio inputs
                                        $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
                                            checkboxClass: 'icheckbox_minimal',
                                            radioClass: 'iradio_minimal'
                                        });
                                        //Red color scheme for iCheck
                                        $('input[type="checkbox"].minimal-red, input[type="radio"].minimal-red').iCheck({
                                            checkboxClass: 'icheckbox_minimal-red',
                                            radioClass: 'iradio_minimal-red'
                                        });
                                        //Flat red color scheme for iCheck
                                        $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
                                            checkboxClass: 'icheckbox_flat-red',
                                            radioClass: 'iradio_flat-red'
                                        });

                                        //Colorpicker
                                        $(".my-colorpicker1").colorpicker();
                                        //color picker with addon
                                        $(".my-colorpicker2").colorpicker();

                                        //Timepicker
                                        $(".timepicker").timepicker({
                                            showInputs: false
                                        });
                                    });
        </script>


        <!-- CEP -->
        <script type="text/javascript" src="https://maps.google.com/maps/api/js?sensor=false"></script>
        <script type="text/javascript" src="../../js/cep2/script.js"></script>
        <script type="text/javascript" src="../../js/cep2/gmaps.js"></script>

<!--        <script type="text/javascript" language="javascript" src="../../js/jquery.maskedinput-1.1.4.pack.js"></script>
        <script>
                                    $(document).ready(function() {
                                        $(".telefone").mask("(99) 9999-9999"); //Aqui montamos a máscara que queremos
                                        $(".cpf").mask("999.999.999-99");
                                        $(".cnpj").mask("99.999.999/9999-99");
                                        $(".data").mask("99/99/9999"); //Aqui montamos a máscara que queremos
                                    });
        </script>-->

        <script type="text/javascript">
            function lookup(inputString) {
                if (inputString.length == 0) {
                    // Hide the suggestion box.
                    $('#suggestions').hide();
                } else {
                    $.post("rpc.php", {queryString: "" + inputString + ""}, function(data) {
                        if (data.length > 0) {
                            $('#suggestions').show();
                            $('#autoSuggestionsList').html(data);
                        }
                    });
                }
            } // lookup

            function fill(thisValue) {
                $('#inputString').val(thisValue);
                setTimeout("$('#suggestions').hide();", 200);
            }
        </script>
        
        <script type="text/javascript">
            $(document).ready(function() {
                var $link = $('#add_loja');
                var $model = $link.prev('.loja');
                $link.click(function() {
                    var $clone = $model.clone(false);
                    $(this).before($clone);
                    $clone.wrap('<div class="form-group"></div>')
                            .after('<a href="javascript:void(0)" class="remove">Remover</a>')
                            .addClass('added')
                            .parent()
                            .hide()
                            .fadeIn();
                });

                $('a.remove').live('click', function() {
                    $(this).parent().fadeOut('normal', function() {
                        $(this).remove();
                    });
                });
            });
        </script>
        
        <!--Validação dos campos -->
        <script src="<?= $SiteHTTP ?>admin-promocoes/js/validacao/jquery.validate.js" type="text/javascript"></script>
        <script type="text/javascript">
            $('form').validate({
                rules: {
                    valor_compra: {
                        minlength: 3,
                        maxlength: 15,
                        required: true
                    },
                    clientes_nome: {
                        minlength: 3,
                        required: true
                    },
                    clientes_email: {
                        minlength: 3,
                        required: true
                    },
                    clientes_cpf: {
                        minlength: 3,
                        maxlength: 15,
                        required: true
                    },
                },
                
                messages: {
			clientes_nome: "É necessário preencher o Nome",
			clientes_email: "É necessário preencher o Email",
			clientes_cpf: "É necessário preencher o CPF",
		},
                
                highlight: function(element) {
                    $(element).closest('.form-group').addClass('has-error');
                },
                unhighlight: function(element) {
                    $(element).closest('.form-group').removeClass('has-error');
                },
                errorElement: 'span',
                errorClass: 'help-block',
                errorPlacement: function(error, element) {
                    if(element.parent('.input-group').length) {
                        error.insertAfter(element.parent());
                    } else {
                        error.insertAfter(element);
                    }
                }
            });
        </script>

    </body>
</html>