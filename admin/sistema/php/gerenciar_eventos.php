<?php
ob_start();
//Inicialização do Sistema
require("../../../site/includes/funcoes.php");

//Verificar a permissão de acesso
if ($_SESSION["logado"] == "logado") {
    $objUser = new EXT_Usuarios_Admin();
    $objUser->select($_SESSION["usuario"]);
} else {
    header("location:" . $SiteHTTP . "admin/?msgErro=Realize seu Login");
}

$titulo = "Gerenciar Eventos";
?>
<!DOCTYPE html>
<html>
<head>
    <?php include("../../metas.php"); ?>
    <title><?= $titulo ?> - Administração <?= $objConfig_site->getrazao_social(); ?></title>
</head>
<body class="skin-blue">
<?php include '../includes/topo.php'; ?>
<div class="wrapper row-offcanvas row-offcanvas-left">
    <!-- Left side column. contains the logo and sidebar -->
    <aside class="left-side sidebar-offcanvas">
        <!-- sidebar: style can be found in sidebar.less -->
        <?php include '../includes/menu.php'; ?>
        <!-- /.sidebar -->
    </aside>

    <!-- Right side column. Contains the navbar and content of the page -->
    <aside class="right-side">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                <?= $titulo ?>
            </h1>
            <hr style="margin-top: 10px;margin-bottom: 10px;">

            <ol class="breadcrumb">
                <li><a href="../index.php"><i class="fa fa-dashboard"></i> Home</a></li>
                <li>Eventos</li>
                <li class="active"><?= $titulo ?></li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">

            <?php require("../../../comuns/libs/ajax/php/divRetorno_full.php"); ?>

            <!-- Listagens de cadastro -->
            <div class="box box-primary">
                <div class="box-header">
                    <h3 class="box-title">Próximos Eventos</h3>

                </div>
                <!-- /.box-header -->
                <div class="box-body no-padding">
                    <table class="table">
                        <tr>
                            <th style="width: 10px">#</th>
                            <th>Título</th>
                            <th>Período</th>
                            <th>Categoria</th>
                            <th style="width: 330px; text-align:center;">Ações</th>
                        </tr>
                        <?php
                        $data_atual = date('Y-m-d');
                        $objListaEventos = new DAO_Eventos();
                        $objCategoria = new DAO_Categorias();
                        $objOrganizador = new DAO_Usuarios_Admin();
                        $buscaEventos = "SELECT * FROM eventos WHERE data_final >= '".$data_atual."' ORDER BY data_inicio ASC, hora_inicio ASC";
                        $objBancoListaEventos = new Database();
                        $objBancoListaEventos->Query($buscaEventos);
                        $contador = 0;
                        while ($row = $objBancoListaEventos->FetchObject()) {
                            $contador++;
                            $objListaEventos->select($row->id);
                            $objCategoria->select($objListaEventos->getcategoria_id_INT());
                            $objOrganizador->select($objListaEventos->getusuario_id_INT());
                            ?>
                            <tr>
                                <td <?php if ($objListaEventos->getstatus_id_INT() == 0) {
                                    $link_ativacao = "/admin/sistema/ativar-evento/".$objListaEventos->getid();
                                    $texto_ativacao = "Ativar";
                                    echo 'style="background-color: #FFE4E1"';
                                } else {
                                    $link_ativacao = "/admin/sistema/desativar-evento/".$objListaEventos->getid();
                                    $texto_ativacao = "Desativar";
                                    echo 'style="background-color: #90EE90"';
                                } ?>><?= $contador ?></td>
                                <td><?= ($objListaEventos->gettitulo()) ?></td>
                                <td><?= ($objListaEventos->FDataExibe($objListaEventos->getdata_inicio())) ?> - <?= ($objListaEventos->FDataExibe($objListaEventos->getdata_final())) ?></td>
                                <td><?= ($objCategoria->gettitulo()) ?></td>

                                <td align="center">
                                    <a href="/admin/sistema/gerenciar-subeventos/<?= $objListaEventos->getid() ?>">
                                        <button class="btn btn-default btn-sm">SubEventos</button>
                                    </a>
                                    <a href="<?=$link_ativacao?>">
                                        <button class="btn btn-info btn-sm"><?=$texto_ativacao?></button>
                                    </a>
                                    <a href="/admin/sistema/editar-evento/<?= $objListaEventos->getid() ?>">
                                        <button class="btn btn-primary btn-sm">Editar</button>
                                    </a>
                                    <a href="/admin/sistema/excluir-evento/<?= $objListaEventos->getid() ?>" onclick="return confirm('Tem certeza que deseja excluir esse evento?')">
                                        <button class="btn btn-danger btn-sm">Excluir</button>
                                    </a>
                                </td>
                            </tr>
                            <?php
                        }
                        ?>
                    </table>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->

            <div class="box box-primary">
                <div class="box-header">
                    <h3 class="box-title">Eventos Anteriores</h3>

                </div>
                <!-- /.box-header -->
                <div class="box-body no-padding">
                    <table class="table">
                        <tr>
                            <th style="width: 10px">#</th>
                            <th>Título</th>
                            <th>Período</th>
                            <th>Categoria</th>
                            <th style="width: 330px; text-align:center;">Ações</th>
                        </tr>
                        <?php
                        $data_atual = date('Y-m-d');
                        $objListaEventos = new DAO_Eventos();
                        $objCategoria = new DAO_Categorias();
                        $objOrganizador = new DAO_Usuarios_Admin();
                        $buscaEventos = "SELECT * FROM eventos WHERE data_final < '".$data_atual."' ORDER BY data_inicio DESC, hora_inicio DESC";
                        $objBancoListaEventos = new Database();
                        $objBancoListaEventos->Query($buscaEventos);
                        $contador = 0;
                        while ($row = $objBancoListaEventos->FetchObject()) {
                            $contador++;
                            $objListaEventos->select($row->id);
                            $objCategoria->select($objListaEventos->getcategoria_id_INT());
                            $objOrganizador->select($objListaEventos->getusuario_id_INT());
                            ?>
                            <tr>
                                <td <?php if ($objListaEventos->getstatus_id_INT() == 0) {
                                    $link_ativacao = "/admin/sistema/ativar-evento/".$objListaEventos->getid();
                                    $texto_ativacao = "Ativar";
                                    echo 'style="background-color: #FFE4E1"';
                                } else {
                                    $link_ativacao = "/admin/sistema/desativar-evento/".$objListaEventos->getid();
                                    $texto_ativacao = "Desativar";
                                    echo 'style="background-color: #90EE90"';
                                } ?>><?= $contador ?></td>
                                <td><?= ($objListaEventos->gettitulo()) ?></td>
                                <td><?= ($objListaEventos->FDataExibe($objListaEventos->getdata_inicio())) ?> - <?= ($objListaEventos->FDataExibe($objListaEventos->getdata_final())) ?></td>
                                <td><?= ($objCategoria->gettitulo()) ?></td>

                                <td align="center">
                                    <a href="/admin/sistema/gerenciar-subeventos/<?= $objListaEventos->getid() ?>">
                                        <button class="btn btn-default btn-sm">SubEventos</button>
                                    </a>
                                    <a href="<?=$link_ativacao?>">
                                        <button class="btn btn-info btn-sm"><?=$texto_ativacao?></button>
                                    </a>
                                    <a href="/admin/sistema/editar-evento/<?= $objListaEventos->getid() ?>">
                                        <button class="btn btn-primary btn-sm">Editar</button>
                                    </a>
                                    <a href="/admin/sistema/excluir-evento/<?= $objListaEventos->getid() ?>" onclick="return confirm('Tem certeza que deseja excluir esse evento?')">
                                        <button class="btn btn-danger btn-sm">Excluir</button>
                                    </a>
                                </td>
                            </tr>
                        <?php
                        }
                        ?>
                    </table>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->

            <div class="box-footer">
                <a style="float: right;" href="/admin/sistema/cadastrar-eventos" class="btn btn-group">Cadastrar novo registro</a>
            </div>

            <!-- Main row -->

        </section>
        <!-- /.content -->
    </aside>
    <!-- /.right-side -->
</div>
<!-- ./wrapper -->

<script>
    $(document).ready(function () {
        $("li#eventos").addClass("active");
        $("li#gerenciar_eventos").addClass("active");
    });
</script>
<?php include("../../rodape.php"); ?>
</body>
</html>