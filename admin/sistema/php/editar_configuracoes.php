<?php
ob_start();
//Inicialização do Sistema
require("../../../site/includes/funcoes.php");

//Verificar a permissão de acesso
if ($_SESSION["logado"] == "logado") {
    $objUser = new EXT_Usuarios_Admin();
    $objUser->select($_SESSION["usuario"]);
} else {
    header("location:" . $SiteHTTP . "admin/?msgErro=Realize seu Login");
}

$action = "alterar-configuracoes";
//Configurações do site
$objConfig_site = new DAO_Config_site();
$objConfig_site->select(1);
$titulo = "Editar Configurações";
$botao = "Editar";
$criterio = "checked";
$id = FDados("id");

?>
<!DOCTYPE html>
<html>
<head>
    <?php include("../../metas.php"); ?>
    <title><?= $titulo ?> - Administração <?= $objConfig_site->getrazao_social(); ?></title>
</head>
<body class="skin-blue">
<?php include '../includes/topo.php'; ?>
<div class="wrapper row-offcanvas row-offcanvas-left">
    <!-- Left side column. contains the logo and sidebar -->
    <aside class="left-side sidebar-offcanvas">
        <!-- sidebar: style can be found in sidebar.less -->
        <?php include '../includes/menu.php'; ?>
        <!-- /.sidebar -->
    </aside>

    <!-- Right side column. Contains the navbar and content of the page -->
    <aside class="right-side">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Configurações
            </h1>
            <ol class="breadcrumb">
                <li><a href="../index.php"><i class="fa fa-dashboard"></i> Home</a></li>
                <li>Configurações</li>
                <li class="active"><?= $titulo ?></li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">

            <div class="box box-primary">
                <div class="box-header">
                    <h3 class="box-title"><?= $titulo ?></h3>
                </div>
                <!-- /.box-header -->
                <?php require("../../../comuns/libs/ajax/php/divRetorno_full.php"); ?>

                <!-- form start -->
                <form role="form" action="<?= $action ?>" method="POST" enctype="multipart/form-data">
                    <input type="hidden" name="idconfig_site" value="1"/>

                    <div class="box-body">

                        <!----------------------------------------------------------------------------------------------------->

                        <h4 style="text-decoration: underline;">Configurações Gerais</h4>

                        <div class="form-group">
                            <label for="nome_fantasia">Nome Fantasia</label>
                            <input type="text" name="nome_fantasia" class="form-control" id="nome_fantasia"
                                   placeholder="Plugin Web" value="<?php echo($objConfig_site->getnome_fantasia()); ?>">
                        </div>

                        <div class="form-group">
                            <label for="razao_social">Razão Social</label>
                            <input type="text" name="razao_social" class="form-control" id="razao_social"
                                   placeholder="Plugin Web" value="<?php echo($objConfig_site->getrazao_social()); ?>">
                        </div>

                        <div class="form-group">
                            <label for="cnpj">CNPJ</label>
                            <input type="text" name="cnpj" class="form-control" id="cnpj"
                                   placeholder="11.111.111/0001-11" value="<?php echo($objConfig_site->getcnpj()); ?>">
                        </div>

                        <div class="form-group">
                            <label for="url_site">URL do Site</label>
                            <input type="text" name="url_site" class="form-control" id="url_site"
                                   placeholder="http://pluginweb.com.br"
                                   value="<?php echo($objConfig_site->geturl_site()); ?>">
                        </div>

                        <div class="form-group">
                            <label for="metatag_titulo">Metatag Título</label>
                            <input type="text" name="metatag_titulo" class="form-control" id="metatag_titulo"
                                   placeholder="Plugin Web"
                                   value="<?php echo($objConfig_site->getmetatag_titulo()); ?>">
                        </div>

                        <div class="form-group">
                            <label for="metatag_descricao">Metatag Descrição</label>
                            <textarea name="metatag_descricao" class="form-control" id="metatag_descricao"
                                      placeholder="Website Plugin Web"><?php echo($objConfig_site->getmetatag_descricao()); ?></textarea>
                        </div>

                        <div class="form-group">
                            <label for="metatag_palavrachave">Metatag Palavras Chaves</label>
                            <input type="text" name="metatag_palavrachave" class="form-control"
                                   id="metatag_palavrachave" placeholder="site, plugin, web, agencia, juiz de fora, jf"
                                   value="<?php echo($objConfig_site->getmetatag_palavrachave()); ?>">
                        </div>

                        <div class="form-group">
                            <label for="email">Email Principal</label>
                            <input type="email" name="email" class="form-control" id="email"
                                   placeholder="douglas@pluginweb.com.br"
                                   value="<?php echo($objConfig_site->getemail()); ?>" required>
                        </div>

                        <div class="form-group">
                            <label for="email_financeiro">Email Secundário</label>
                            <input type="email" name="email_financeiro" class="form-control" id="email_financeiro"
                                   placeholder="douglas@pluginweb.com.br"
                                   value="<?php echo($objConfig_site->getemail_financeiro()); ?>">
                        </div>

                        <div class="form-group">
                            <label for="endereco">Endereço</label>
                            <input type="text" name="endereco" class="form-control" id="endereco"
                                   placeholder="Av. Rio Branco" value="<?php echo($objConfig_site->getendereco()); ?>">
                        </div>

                        <div class="form-group">
                            <label for="numero">Número</label>
                            <input type="text" name="numero" class="form-control" id="numero" placeholder="3925"
                                   value="<?php echo($objConfig_site->getnumero()); ?>">
                        </div>

                        <div class="form-group">
                            <label for="complemento">Complemento</label>
                            <input type="text" name="complemento" class="form-control" id="complemento"
                                   placeholder="2º Piso, Loja 1"
                                   value="<?php echo($objConfig_site->getcomplemento()); ?>">
                        </div>

                        <div class="form-group">
                            <label for="bairro">Bairro</label>
                            <input type="text" name="bairro" class="form-control" id="bairro"
                                   placeholder="Alto dos Passos" value="<?php echo($objConfig_site->getbairro()); ?>">
                        </div>

                        <div class="form-group">
                            <label for="cidade">Cidade</label>
                            <input type="text" name="cidade" class="form-control" id="cidade" placeholder="Juiz de Fora"
                                   value="<?php echo($objConfig_site->getcidade()); ?>">
                        </div>

                        <div class="form-group">
                            <label for="cep">CEP</label>
                            <input type="text" name="cep" class="form-control" id="cep" placeholder="36021630"
                                   value="<?php echo($objConfig_site->getcep()); ?>">
                        </div>

                        <div class="form-group">
                            <label for="estado">Estado</label>
                            <input type="text" name="estado" class="form-control" id="estado" placeholder="Minas Gerais"
                                   value="<?php echo($objConfig_site->getestado()); ?>">
                        </div>

                        <div class="form-group">
                            <label for="tel1">Telefone</label>
                            <input type="tel" name="tel1" class="form-control" id="tel1" placeholder="(32)3061-3179"
                                   value="<?php echo($objConfig_site->gettel1()); ?>">
                        </div>

                        <div class="form-group">
                            <label for="cel1">Celular</label>
                            <input type="tel" name="cel1" class="form-control" id="cel1" placeholder="(32)9998-9836"
                                   value="<?php echo($objConfig_site->getcel1()); ?>">
                        </div>

                        <div class="form-group">
                            <label for="direitos">Direitos Reservados</label>
                            <input type="text" name="direitos" class="form-control" id="direitos"
                                   placeholder="Copyright  Todos os direitos reservados"
                                   value="<?php echo($objConfig_site->getdireitos()); ?>">
                        </div>

                        <div class="form-group">
                            <label for="imagem">Imagem de Apresentação</label>
                            <input type="file" name="imagem" class="form-control" >
                            <br/>
                            <font style="font-size: 12px;">SOMENTE ARQUIVOS JPG</font>
                            <br/>
                            <font style="font-size: 12px;">RESOLUÇÃO RECOMENDADA: 600 x 128 pixels</font>

                            <?php
                                if ($objConfig_site->getmarcaDagua() != "") {
                                    if (file_exists("../../../site/uploads/img_marcadagua/" . $objConfig_site->getmarcaDagua())) {
                                        ?>
                                        <div id="img_1"><img src="<?=$SiteHTTP?>site/uploads/img_marcadagua/<?= $objConfig_site->getmarcaDagua() ?>" style="margin-right:5px;max-width: 50%;"/>
                                            <!--<input type='button' value="Excluir" class="form_botao2" style="cursor: pointer;" onclick="javascript:getDados('img_1','../validacoes/acoes/excluir_img_apresentacao.php?id=<?= $id; ?>&imgMini=<?= $objConfig_site->getmarcaDagua(); ?>&imgFull=<?= $objConfig_site->getmarcaDagua() ?>');">-->
                                        </div>
                                    <?php }
                            }
                            ?>
                        </div>

                        <div class="form-group">
                            <label for="analytcs">Analytics</label>
                            <textarea name="analytcs" class="form-control" rows="5"
                                      id="analytcs"><?php echo utf8_encode($objConfig_site->getanalytcs()); ?></textarea>
                        </div>

                    </div>
                    <!-- /.box-body -->

                    <div class="box-footer">
                        <button type="submit" class="btn btn-primary"><?= $botao ?></button>
                    </div>
                </form>
            </div>

            <!-- Main row -->

        </section>
        <!-- /.content -->
    </aside>
    <!-- /.right-side -->
</div>

<script>
    $(document).ready(function () {
        $("li#configuracoes").addClass("active");
        $("li#editar_configuracoes").addClass("active");
    });
</script>

<?php include("../../rodape.php"); ?>

</body>
</html>