<?php
ob_start();
//Inicialização do Sistema
require("../../../site/includes/funcoes.php");

//Verificar a permissão de acesso
if ($_SESSION["logado"] == "logado") {
    $objUser = new EXT_Usuarios_Admin();
    $objUser->select($_SESSION["usuario"]);
} else {
    header("location:" . $SiteHTTP . "admin/?msgErro=Realize seu Login");
}

$titulo = "Lista de Presença";

$idEvento = FDados("evento");

if ($idEvento != "") {
    $objEvento = new DAO_Eventos();
    $objEvento->select($idEvento);
}

?>
<!DOCTYPE html>
<html>
<head>
    <?php include("../../metas.php"); ?>
    <title><?= $titulo ?> - Administração <?= $objConfig_site->getrazao_social(); ?></title>
    <style>
        @media print {
            body * {
                visibility: hidden;
            }
            #printable, #printable * {
                visibility: visible;
            }
        }
    </style>
</head>
<body class="skin-blue">
<?php include '../includes/topo.php'; ?>
<div class="wrapper row-offcanvas row-offcanvas-left">
    <!-- Left side column. contains the logo and sidebar -->
    <aside class="left-side sidebar-offcanvas">
        <!-- sidebar: style can be found in sidebar.less -->
        <?php include '../includes/menu.php'; ?>
        <!-- /.sidebar -->
    </aside>

    <!-- Right side column. Contains the navbar and content of the page -->
    <aside class="right-side">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Lista de Presença
            </h1>
            <hr style="margin-top: 10px;margin-bottom: 10px;">

            <ol class="breadcrumb">
                <li><a href="../index.php"><i class="fa fa-dashboard"></i> Home</a></li>
                <li>Presença</li>
                <li class="active">Lista de Presença</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">

            <button class="btn btn-link" style="float: right;" onclick="window.print();">Imprimir</button>

            <?php require("../../../comuns/libs/ajax/php/divRetorno_full.php"); ?>

            <?php if ($idEvento != "") { ?>

            <div class="box box-primary" id="printable">
                <div class="box-header">
                    <h3 class="box-title">Lista de Presença - <?=$objEvento->gettitulo()?></h3>
                </div>
                <div class="box-body no-padding">
                    <table class="table">
                        <tr>
                            <th style="width: 10px">#</th>
                            <th>Participante</th>
                            <th style="text-align: center">Confirmada em</th>
                        </tr>
                        <?php
                        $data_atual = date('Y-m-d');
                        $objListaPresenca = new DAO_Presenca();
                        $objEventos = new DAO_Eventos();
                        $objParticipantes = new DAO_Participantes();
                        $buscaPresencas = "SELECT * FROM presenca WHERE evento_id_INT='" . $idEvento . "' AND status_id_INT='1' ORDER BY datetime ASC";
                        $objBancoListaPresenca = new Database();
                        $objBancoListaPresenca->Query($buscaPresencas);
                        $objBancoPresenca = new Database();
                        $contador = 0;
                        while ($row = $objBancoListaPresenca->FetchObject()) {
                            $contador++;
                            $objListaPresenca->select($row->id);
                            $objParticipantes->select($objListaPresenca->getparticipante_id_INT());
                            $objEventos->select($objListaPresenca->getevento_id_INT());
                            ?>
                            <tr>
                                <td><?= $contador ?></td>
                                <td><?= ($objParticipantes->getnome()) ?></td>
                                <td style="text-align: center"><?=$objListaPresenca->FDataTimeExibe($objListaPresenca->getdatetime())?></td>
                            </tr>
                            <?php
                        }
                        ?>
                    </table>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->

            <?php } ?>

            <div class="box-footer">
                <a style="float: right;" href="/admin/sistema/gerenciar-presenca/<?=$idEvento?>" class="btn btn-group">Voltar</a>
            </div>

        </section>
        <!-- /.content -->
    </aside>
    <!-- /.right-side -->
</div>
<!-- ./wrapper -->

<script>
    $(document).ready(function () {
        $("li#presenca").addClass("active");
        $("li#gerenciar_presenca").addClass("active");
    });
</script>
<?php include("../../rodape.php"); ?>
</body>
</html>