<?php
ob_start();
//Inicialização do Sistema
require("../../../site/includes/funcoes.php");

//Verificar a permissão de acesso
if ($_SESSION["logado"] == "logado") {
    $objUser = new EXT_Usuarios_Admin();
    $objUser->select($_SESSION["usuario"]);
} else {
    header("location:" . $SiteHTTP . "admin/?msgErro=Realize seu Login");
}

$objEvento = new DAO_Eventos();
$idEvento = FDados("id");
$objEvento->select($idEvento);

if (FDados("subevento") != "") {
    $action = "/admin/sistema/alterar-subevento";
    $objSubeventos = new DAO_Subeventos();
    $objSubeventos->select(FDados("subevento"));
    $idTipo = $objSubeventos->gettipo_subevento_id_INT();
    $idInstrutor = $objSubeventos->getinstrutor_id_INT();
    $titulo = "Editar SubEvento";
    $botao = "Editar";
    $criterio = "checked";
    $id = FDados("subevento");
} else {
    $action = "/admin/sistema/add-subevento";
    $titulo = "Cadastrar SubEvento";
    $botao = "Cadastrar";
    $criterio = "";
}
$data_atual = date('d/m/Y');
?>
<!DOCTYPE html>
<html>
<head>
    <?php include("../../metas.php"); ?>
    <title><?= $titulo ?> - Administração <?= $objConfig_site->getrazao_social(); ?></title>
</head>
<body class="skin-blue">
<?php include '../includes/topo.php'; ?>
<div class="wrapper row-offcanvas row-offcanvas-left">
    <!-- Left side column. contains the logo and sidebar -->
    <aside class="left-side sidebar-offcanvas">
        <!-- sidebar: style can be found in sidebar.less -->
        <?php include '../includes/menu.php'; ?>
        <!-- /.sidebar -->
    </aside>

    <!-- Right side column. Contains the navbar and content of the page -->
    <aside class="right-side">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                <?= $titulo.'<br><span style="font-size:0.6em">'.$objEvento->gettitulo().'</span>' ?>
            </h1>
            <ol class="breadcrumb">
                <li><a href="../index.php"><i class="fa fa-dashboard"></i> Home</a></li>
                <li>SubEventos</li>
                <li class="active"><?= $titulo ?></li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">

            <div class="box box-primary">
                <div class="box-header">
                    <h3 class="box-title"><?= $titulo ?></h3>
                </div>
                <!-- /.box-header -->
                <?php require("../../../comuns/libs/ajax/php/divRetorno_full.php"); ?>

                <!-- form start -->
                <form role="form" action="<?= $action ?>" method="POST" enctype="multipart/form-data">
                    <input type="hidden" name="id" value="<?= FDados("subevento") ?>"/>
                    <input type="hidden" name="evento_id_INT" value="<?= $objEvento->getid() ?>"/>

                    <div class="box-body">
                        <div class="form-group">
                            <label for="titulo">Título do SubEvento</label>
                            <input type="text" name="titulo" class="form-control" id="titulo"
                                   placeholder="SubEvento" value="<?php if (FDados("subevento") != "") {
                                echo($objSubeventos->gettitulo());
                            } ?>" required>
                        </div>

                        <!--<div class="form-group">
                            <label for="evento_id_INT">Evento</label>
                            <select name="evento_id_INT" class="form-control" required>
                                <?php
                                echo "<option value=''>Selecione um Evento</option>";
                                $objBancoEvento = new Database();
                                $sql = "select * from eventos where data_inicio >= '".$data_atual."' order by data_inicio ASC, hora_inicio ASC";
                                $objBancoEvento->Query($sql);
                                while ($row = $objBancoEvento->FetchObject()) {
                                    if ($idEvento == $row->id) {
                                        echo "<option value='$row->id' selected='selected' >$row->titulo</option>";
                                    } else {
                                        echo "<option value='$row->id' style='font-weight:bold'>$row->titulo</option>";
                                    }
                                }
                                ?>
                            </select>
                        </div>-->

                        <div class="form-group">
                            <label for="tipo_subevento_id_INT">Tipo</label>
                            <select name="tipo_subevento_id_INT" class="form-control" required>
                                <?php
                                echo "<option value=''>Selecione um Tipo</option>";
                                $objBancoTipos = new Database();
                                $sql = "select * from tipos_subeventos order by titulo";
                                $objBancoTipos->Query($sql);
                                while ($row = $objBancoTipos->FetchObject()) {
                                    if ($idTipo == $row->id) {
                                        echo "<option value='$row->id' selected='selected' >$row->titulo</option>";
                                    } else {
                                        echo "<option value='$row->id' style='font-weight:bold'>$row->titulo</option>";
                                    }
                                }
                                ?>
                            </select>
                        </div>

                        <div class="form-group">
                            <label for="instrutor_id_INT">Instrutor/Palestrante</label>
                            <select name="instrutor_id_INT" class="form-control" required>
                                <?php
                                echo "<option value=''>Selecione um Instrutor/Palestrante</option>";
                                $objBancoInstrutores = new Database();
                                $sql = "select * from instrutores order by nome";
                                $objBancoInstrutores->Query($sql);
                                while ($row = $objBancoInstrutores->FetchObject()) {
                                    if ($idInstrutor == $row->id) {
                                        echo "<option value='$row->id' selected='selected' >$row->nome</option>";
                                    } else {
                                        echo "<option value='$row->id' style='font-weight:bold'>$row->nome</option>";
                                    }
                                }
                                ?>
                            </select>
                        </div>

                        <div class="form-group">
                            <label for="data">Data</label>
                            <input type="text" name="data" class="form-control data_subevento" id="data"
                                   value="<?php if (FDados("subevento") != "") {
                                       echo $objSubeventos->FDataExibe($objSubeventos->getdata());
                                   } ?>" required/>
                        </div>

                        <div class="form-group">
                            <label for="hora_inicio">Horário de Início</label>
                            <input type="text" name="hora_inicio" class="form-control hora" id="hora_inicio"
                                   value="<?php if (FDados("subevento") != "") {
                                       echo($objSubeventos->gethora_inicio());
                                   } ?>"/>
                        </div>

                        <div class="form-group">
                            <label for="hora_final">Horário de Término</label>
                            <input type="text" name="hora_final" class="form-control hora" id="hora_final"
                                   value="<?php if (FDados("subevento") != "") {
                                       echo($objSubeventos->gethora_final());
                                   } ?>"/>
                        </div>

                        <div class="form-group">
                            <label for="status_id_INT">Status</label>

                            <div class="radio">
                                <label for="ativo">
                                    <input type="radio" name="status_id_INT"
                                           value="1" <?php if (FDados("subevento") == "") {
                                        echo 'checked';
                                    } else {
                                        if ($objSubeventos->getstatus_id_INT() == "1") {
                                            echo 'checked';
                                        }
                                    } ?>> Ativo
                                </label>

                                <div style="clear:both; height:5px"></div>
                                <label for="inativo">
                                    <input type="radio" name="status_id_INT"
                                           value="0" <?php if (FDados("subevento") != "") {
                                        if ($objSubeventos->getstatus_id_INT() == "0") {
                                            echo 'checked';
                                        }
                                    } ?>> Inativo
                                </label>
                            </div>

                        </div>

                        <div class="form-group">
                            <label for="editor1">Descrição</label>
                                    <textarea name="descricao" class="form-control" rows="15"
                                              id="editor1"><?php if (FDados("subevento") != "") {
                                            echo($objSubeventos->getdescricao());
                                        } ?></textarea>
                        </div>

                        <div class="form-group">
                            <label for="carga_horaria">Carga Horária</label>
                            <input type="number" name="carga_horaria" class="form-control" id="carga_horaria"
                                   value="<?php if (FDados("subevento") != "") {
                                       echo $objSubeventos->getcarga_horaria();
                                   } ?>">
                        </div>
                        

                    </div>
                    <!-- /.box-body -->

                    <div class="box-footer">
                        <button type="submit" class="btn btn-primary"><?= $botao ?></button>
                        <a style="float: right;" href="/admin/sistema/gerenciar-subeventos/<?=$objEvento->getid()?>" class="btn btn-group">Gerenciar</a>
                    </div>
                </form>
            </div>

            <!-- Main row -->

        </section>
        <!-- /.content -->
    </aside>
    <!-- /.right-side -->
</div>
<!-- ./wrapper -->

<script>
    $(document).ready(function () {
        $("li#eventos").addClass("active");
        $("li#cadastrar_eventos").addClass("active");
    });
</script>
<?php include("../../rodape.php"); ?>
<script>
    $(".data_subevento").datepicker({
        dateFormat: 'dd/mm/yy',
        dayNames: ['Domingo','Segunda','Terça','Quarta','Quinta','Sexta','Sábado'],
        dayNamesMin: ['D','S','T','Q','Q','S','S','D'],
        dayNamesShort: ['Dom','Seg','Ter','Qua','Qui','Sex','Sáb','Dom'],
        monthNames: ['Janeiro','Fevereiro','Março','Abril','Maio','Junho','Julho','Agosto','Setembro','Outubro','Novembro','Dezembro'],
        monthNamesShort: ['Jan','Fev','Mar','Abr','Mai','Jun','Jul','Ago','Set','Out','Nov','Dez'],
        nextText: 'Próximo',
        prevText: 'Anterior',
        minDate: '<?=$objEvento->FDataExibe($objEvento->getdata_inicio())?>',
        maxDate: '<?=$objEvento->FDataExibe($objEvento->getdata_final())?>'
    });
</script>
</body>
</html>