<?php
ob_start();
//Inicialização do Sistema
require("../../../site/includes/funcoes.php");

//Verificar a permissão de acesso
if ($_SESSION["logado"] == "logado") {
    $objUser = new EXT_Funcionarios();
    $objUser->select($_SESSION["usuario"]);
} else {
    header("location:" . $SiteHTTP . "admin/?msgErro=Realize seu Login");
}

//Configurações do site
$objConfig_site = new DAO_Config_site();
$objConfig_site->select(1);

$objGalerias = new DAO_Galerias();

$galerias_id = FDados("id");

$objGalerias->select($galerias_id);
$objGalerias->dadosExibe();
$idGaleria = $objGalerias->getgalerias_id();
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Administração Plugin Web | GERENCIAR FOTOS - <?= $objConfig_site->getrazao_social(); ?></title>
        <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
        <!-- bootstrap 3.0.2 -->
        <link href="../../css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <!-- font Awesome -->
        <link href="../../css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <!-- Ionicons -->
        <link href="../../css/ionicons.min.css" rel="stylesheet" type="text/css" />
        <!-- Morris chart -->
        <link href="../../css/morris/morris.css" rel="stylesheet" type="text/css" />
        <!-- jvectormap -->
        <link href="../../css/jvectormap/jquery-jvectormap-1.2.2.css" rel="stylesheet" type="text/css" />
        <!-- fullCalendar -->
        <link href="../../css/fullcalendar/fullcalendar.css" rel="stylesheet" type="text/css" />
        <!-- Daterange picker -->
        <link href="../../css/daterangepicker/daterangepicker-bs3.css" rel="stylesheet" type="text/css" />
        <!-- bootstrap wysihtml5 - text editor -->
        <link href="../../css/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css" rel="stylesheet" type="text/css" />
        <!-- Theme style -->
        <link href="../../css/AdminLTE.css" rel="stylesheet" type="text/css" />

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.../../js/1.3.0/respond.min.js"></script>
        <![endif]-->
    </head>
    <body class="skin-blue">
<?php include '../includes/topo.php'; ?>
        <div class="wrapper row-offcanvas row-offcanvas-left">
            <!-- Left side column. contains the logo and sidebar -->
            <aside class="left-side sidebar-offcanvas">
                <!-- sidebar: style can be found in sidebar.less -->
<?php include '../includes/menu.php'; ?>
                <!-- /.sidebar -->
            </aside>

            <!-- Right side column. Contains the navbar and content of the page -->
            <aside class="right-side">
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                        Gerenciar Fotos
                    </h1>
                    <hr style="margin-top: 10px;margin-bottom: 10px;">   
                    <!--<form action="gerenciar_eventos.php" method="post" name="form_filtro">
                        <div class="form-group" style="margin-left: 10px;">
                            <label for="filtro">Filtrar por:</label>
                            <select name="filtro" required class="form-control" onchange="document.forms['form_filtro'].submit();">
                                <option value="">Pr&oacute;ximos Eventos</option>
                                <option value="1" <? if($filtro==1){ echo 'selected';}?> >Todos</option>
                                <option value="2" <? if($filtro==2){ echo 'selected';}?> >Eventos Antigos</option>
                            </select>
                        </div>
                    </form>-->
                    <ol class="breadcrumb">
                        <li><a href="../index.php"><i class="fa fa-dashboard"></i> Home</a></li>
                        <li>Fotos</li>
                        <li class="active">Gerenciar Fotos</li>
                    </ol>
                </section>

                <!-- Main content -->
                <section class="content">

<?php require("../../../comuns/libs/ajax/php/divRetorno_full.php"); ?>

                    <!-- Listagens de cadastro -->
                    <div class="box box-primary">
                        <div class="box-header">
                            <h3 class="box-title">Listagem das Fotos - <?=  ($objGalerias->getgalerias_nome())?></h3>
                            <!--                            <div class="box-tools">
                                                            <ul class="pagination pagination-sm no-margin pull-right">
                                                                <li><a href="#">&laquo;</a></li>
                                                                <li><a href="#">1</a></li>
                                                                <li><a href="#">2</a></li>
                                                                <li><a href="#">3</a></li>
                                                                <li><a href="#">&raquo;</a></li>
                                                            </ul>
                                                        </div>-->
                        </div><!-- /.box-header -->
                        <div class="box-body no-padding">
                            <form name="formimagens" id="formpaginas" action="../validacoes/cadastros/cadastrar_legenda.php" method="post" style="margin: 0px;" enctype="multipart/form-data">
                                <input type="hidden" name="id_galeria" value="<?= $idGaleria ?>" />
                                
                                <table class="table">
                                    <tr>
                                        <th style="width: 10px">Imagem</th>
                                        <th>Data de Cadastro:</th>
                                        <th>Legenda:</th>
                                        <th style="width: 330px; text-align:center;">Ações</th>
                                    </tr>
                                    <?php
                                   
                                    $buscaFotos = "select * from galerias_fotos where galerias_id_INT=" . $galerias_id;

                                    $objBancoListaFotos = new Database();
                                    $objBancoListaFotos->Query($buscaFotos);
                                    
                                    if($objBancoListaFotos->rows < 1)
                                    {
                                        echo "<tr><th align='center' colspan='15' valign='middle' height='40'><b>NENHUMA FOTO CADASTRADA PARA ESSE EVENTO</b></th></tr>";
                                    }
                                    else
                                    {
                                    
                                    while ($row = $objBancoListaFotos->FetchObject()) {
                                        ?>
                                        <tr>
                                            <td><img src="../../../site/uploads/img_galerias/<?=$row->galerias_fotos_img_mini?>" align="middle" width="100" style="border:1px solid #999999;"/></td>

                                            <td><?= $objGalerias->FDataTimeExibe($row->galerias_fotos_data_cadastro_DATETIME) ?></td>

                                            <td><div class="form-group"><input type='text' name='galerias_fotos_comentario[]' value='<?=$row->galerias_fotos_comentario?>' class="form-control" style='width: 86%;'/></div></td>
                                            <input type='hidden' name='idfoto[]' value='<?=$row->galerias_fotos_id?>' />
                                            <td align="center">
                                                <a href="../validacoes/exclusoes/excluir_fotos.php?galerias_id=<?= $row->galerias_id_INT ?>&idFoto=<?=$row->galerias_fotos_id?>" class="btn btn-danger btn-sm">Excluir</a>
                                            </td>
                                        </tr>
                                            <?php
                                        }

                                    }
                                        ?>
                                        <tr>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td align="center"><button type="submit" class="btn btn-primary"  >SALVAR LEGENDA</button></td>
                                        </tr>
                                </table>
                                
                                
                            </form>
                        </div><!-- /.box-body -->
                    </div><!-- /.box -->


                    <!-- top row -->
                    <div class="row">
                        <div class="col-xs-12 connectedSortable">

                        </div><!-- /.col -->
                    </div>
                    <!-- /.row -->

                    <!-- Main row -->

                </section><!-- /.content -->
            </aside><!-- /.right-side -->
        </div><!-- ./wrapper -->

        <!-- add new calendar event modal -->


        <!-- jQuery 2.0.2 -->
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>
        <!-- Bootstrap -->
        <script src="../../js/bootstrap.min.js" type="text/javascript"></script>
        <!-- InputMask -->
        <script src="../../js/plugins/input-mask/jquery.inputmask.js" type="text/javascript"></script>
        <script src="../../js/plugins/input-mask/jquery.inputmask.date.extensions.js" type="text/javascript"></script>
        <script src="../../js/plugins/input-mask/jquery.inputmask.extensions.js" type="text/javascript"></script>
        <!-- date-range-picker -->
        <script src="../../js/plugins/daterangepicker/daterangepicker.js" type="text/javascript"></script>
        <!-- bootstrap color picker -->
        <script src="../../js/plugins/colorpicker/bootstrap-colorpicker.min.js" type="text/javascript"></script>
        <!-- bootstrap time picker -->
        <script src="../../js/plugins/timepicker/bootstrap-timepicker.min.js" type="text/javascript"></script>
        <!-- AdminLTE App -->
        <script src="../../js/AdminLTE/app.js" type="text/javascript"></script>

        <!-- Page script -->
        <script type="text/javascript">
            $(function () {
                //Datemask dd/mm/yyyy
                $("#datemask").inputmask("dd/mm/yyyy", {"placeholder": "dd/mm/yyyy"});
                //Datemask2 mm/dd/yyyy
                $("#datemask2").inputmask("mm/dd/yyyy", {"placeholder": "mm/dd/yyyy"});
                //Money Euro
                $("[data-mask]").inputmask();

                //Date range picker
                $('#reservation').daterangepicker();
                //Date range picker with time picker
                $('#reservationtime').daterangepicker({timePicker: true, timePickerIncrement: 30, format: 'MM/DD/YYYY h:mm A'});
                //Date range as a button
                $('#daterange-btn').daterangepicker(
                        {
                            ranges: {
                                'Today': [moment(), moment()],
                                'Yesterday': [moment().subtract('days', 1), moment().subtract('days', 1)],
                                'Last 7 Days': [moment().subtract('days', 6), moment()],
                                'Last 30 Days': [moment().subtract('days', 29), moment()],
                                'This Month': [moment().startOf('month'), moment().endOf('month')],
                                'Last Month': [moment().subtract('month', 1).startOf('month'), moment().subtract('month', 1).endOf('month')]
                            },
                            startDate: moment().subtract('days', 29),
                            endDate: moment()
                        },
                function (start, end) {
                    $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
                }
                );

                //iCheck for checkbox and radio inputs
                $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
                    checkboxClass: 'icheckbox_minimal',
                    radioClass: 'iradio_minimal'
                });
                //Red color scheme for iCheck
                $('input[type="checkbox"].minimal-red, input[type="radio"].minimal-red').iCheck({
                    checkboxClass: 'icheckbox_minimal-red',
                    radioClass: 'iradio_minimal-red'
                });
                //Flat red color scheme for iCheck
                $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
                    checkboxClass: 'icheckbox_flat-red',
                    radioClass: 'iradio_flat-red'
                });

                //Colorpicker
                $(".my-colorpicker1").colorpicker();
                //color picker with addon
                $(".my-colorpicker2").colorpicker();

                //Timepicker
                $(".timepicker").timepicker({
                    showInputs: false
                });
            });
        </script>






    </body>
</html>