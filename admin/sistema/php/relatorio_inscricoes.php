<?php
ob_start();
//Inicialização do Sistema
require("../../../site/includes/funcoes.php");
//Verificar a permissão de acesso
if ($_SESSION["logado"] == "logado") {
    $objUser = new EXT_Usuarios_Admin();
    $objUser->select($_SESSION["usuario"]);
} else {
    header("location:" . $SiteHTTP . "admin/?msgErro=Realize seu Login");
}

$idEvento = FDados('evento');

if ($idEvento != "") {
    $objEvento = new DAO_Eventos();
    $objEvento->select($idEvento);
}

$idEstado = FDados('estado');
$idCidade = FDados('cidade');
//$idSexo = FDados('sexo');

$data_atual = date('d/m/Y');

$titulo = "Relatório de Inscrições";

?>
<!DOCTYPE html>
<html>
<head>
    <?php include("../../metas.php"); ?>
    <title><?= $titulo ?> - Administração <?= $objConfig_site->getrazao_social(); ?></title>
</head>
<body class="skin-blue">
<?php include '../includes/topo.php'; ?>
<div class="wrapper row-offcanvas row-offcanvas-left">
    <!-- Left side column. contains the logo and sidebar -->
    <aside class="left-side sidebar-offcanvas">
        <!-- sidebar: style can be found in sidebar.less -->
        <?php include '../includes/menu.php'; ?>
        <!-- /.sidebar -->
    </aside>

    <!-- Right side column. Contains the navbar and content of the page -->
    <aside class="right-side">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                <?= $titulo ?>
            </h1>
            <ol class="breadcrumb">
                <li><a href="../index.php"><i class="fa fa-dashboard"></i> Home</a></li>
                <li>Inscrições</li>
                <li class="active"><?= $titulo ?></li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">

            <?php require("../../../comuns/libs/ajax/php/divRetorno_full.php"); ?>

            <form role="form" id="seleciona_evento" action="/admin/sistema/relatorio-inscricoes" method="POST">
                <input type="hidden" name="estado" value="<?= Fdados('estado') ?>">
                <input type="hidden" name="cidade" value="<?= Fdados('cidade') ?>">


                <div class="form-group" style="margin-right: 10px; float: left">
                    <label for="id">Filtre por Evento</label>
                    <select name="evento" class="form-control" onchange="seleciona_evento()">
                        <option value=''> -- Todos --</option>
                        <?php
                        $objBanco = new Database();
                        $objEventos = new DAO_Eventos();
                        $buscaEventos = "SELECT * FROM eventos WHERE status_id_INT='1' ORDER BY titulo ASC";
                        $objBanco->Query($buscaEventos);
                        while ($exibeEventos = $objBanco->FetchObject()) {
                            $objEventos->select($exibeEventos->id);
                            ?>
                            <option
                                value='<?= $objEventos->getid() ?>' <?php if ($idEvento == $objEventos->getid()) echo 'selected'; ?>><?= $objEventos->gettitulo() ?></option>
                        <?php } ?>
                    </select>
                </div>
            </form>

            <!--<form role="form" id="seleciona_sexo" action="admin/sistema/relatorio-inscricoes" method="POST" style="width: 33%; float: left">
                <input type="hidden" name="estado" value="<?= Fdados('estado') ?>">
                <input type="hidden" name="cidade" value="<?= Fdados('cidade') ?>">
                <input type="hidden" name="evento" value="<?= Fdados('evento') ?>">

                <div class="form-group" style="margin-right: 10px; float: left">
                    <label for="sexo">Filtre por Sexo</label>
                    <select name="sexo" class="form-control" onchange="seleciona_sexo()">
                        <option value=''> -- Todos -- </option>
                        <option value='1' <?php if ($idSexo == 1) echo 'selected'; ?>> Masculino </option>
                        <option value='2' <?php if ($idSexo == 2) echo 'selected'; ?>> Feminino </option>
                    </select>
                </div>
            </form>-->

            <div style="clear: both;"></div>

            <form role="form" id="seleciona_estado" action="/admin/sistema/relatorio-inscricoes" method="POST" >
                <input type="hidden" name="evento" value="<?= Fdados('evento') ?>">
                <input type="hidden" name="cidade" value="<?= Fdados('cidade') ?>">

                <div class="form-group" style="margin-right: 10px; float: left">
                    <label for="id">Filtre por Estado</label>
                    <select name="estado" class="form-control" onchange="seleciona_estado()">
                        <option value=''> -- Todos --</option>
                        <?php
                        $objBanco = new Database();
                        $buscaEstados = "SELECT * FROM estado ORDER BY nome ASC";
                        $objBanco->Query($buscaEstados);
                        while ($exibeEstados = $objBanco->FetchObject()) {
                            ?>
                            <option value='<?= $exibeEstados->id ?>' <?php if ($idEstado == $exibeEstados->id) echo 'selected'; ?>><?= utf8_encode($exibeEstados->nome) ?></option>
                        <?php } ?>
                    </select>
                </div>
            </form>

            <?php if($idEstado != ""){ ?>

                <div style="clear: both;"></div>

            <form role="form" id="seleciona_cidade" action="/admin/sistema/relatorio-inscricoes" method="POST">
                <input type="hidden" name="evento" value="<?= Fdados('evento') ?>">
                <input type="hidden" name="estado" value="<?= Fdados('estado') ?>">

                <div class="form-group" style="margin-right: 10px; float: left">
                    <label for="id">Filtre por Cidade</label>
                    <select name="cidade" class="form-control" onchange="seleciona_cidade()">
                        <option value=''> -- Todas --</option>
                        <?php
                        $objBanco = new Database();
                        $buscaCidades = "SELECT * FROM cidade WHERE estado='".$idEstado."' ORDER BY nome ASC";
                        $objBanco->Query($buscaCidades);
                        while ($exibeCidades = $objBanco->FetchObject()) {
                            ?>
                            <option value='<?= $exibeCidades->id ?>' <?php if ($idCidade == $exibeCidades->id) echo 'selected'; ?>><?= utf8_encode($exibeCidades->nome) ?></option>
                        <?php } ?>
                    </select>
                </div>
            </form>

            <?php } ?>

            <hr>

            <div style="clear: both; height: 50px"></div>

            <div class="container">

                <div class="row" style="text-align: center">

                    <!-- DONUT CHART -->
                    <div class="box" style="max-width: 50%">
                        <div class="box-header with-border">
                            <h3 class="box-title">Número de Inscrições</h3>

                            <div class="box-tools pull-right">
                                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i
                                        class="fa fa-minus"></i>
                                </button>
                                <button type="button" class="btn btn-box-tool" data-widget="remove"><i
                                        class="fa fa-times"></i></button>
                            </div>
                        </div>
                        <div class="box-body">
                            <canvas id="pieChart" style="height:250px"></canvas>
                        </div>

                        <div class="box-footer" style="text-align: left;float: left;width: 100%;">
                            <div style="float: left" id="legenda"></div>
                            <div style="float: right" id="total_inscricoes"></div>
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->

                </div>

            </div>

            <div style="clear: both; height: 20px"></div>

        </section>
        <!-- /.content -->
    </aside>
    <!-- /.right-side -->
</div>
<!-- ./wrapper -->

<script>
    $(document).ready(function () {
        $("li#relatorios").addClass("active");
        $("li#relatorio_inscricoes").addClass("active");
    });
</script>

<?php include("../../rodape.php"); ?>

<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/1.0.2/Chart.js" type="text/javascript"></script>

<script>
    //$(".data").mask("99/99/9999");  //static mask
    //Date range picker
    $('.data').daterangepicker(
        {
            format: 'DD/MM/YYYY',
            language: 'pt-BR',
            maxDate: "<?=$data_atual?>"
        });

    //-------------
    //- GRÁFICO DE NÚMERO DE CONSULTAS -
    //-------------
    // Get context with jQuery - using jQuery's .get() method.
    var pieChartCanvas = $("#pieChart").get(0).getContext("2d");
    var pieChart = new Chart(pieChartCanvas);
    var PieData = [

        <?php

        $objInscricoes = new DAO_Inscricoes();
        $objParticipantes = new DAO_Participantes();

        $filtra_query = "";

        if(FDados('evento')!=""){
            $filtra_query .= " AND i.evento_id_INT = '".$idEvento."'";
        }
        if(FDados('estado')!=""){
            $filtra_query .= " AND p.estado = '".$idEstado."'";
        }
        if(FDados('cidade')!=""){
            $filtra_query .= " AND p.cidade = '".$idCidade."'";
        }

        //BUSCA INSCRIÇÕES DO SEXO MASCULINO
        $buscaNumeroInscricoesMasculino = "SELECT * FROM inscricoes i INNER JOIN participantes p ON i.participante_id_INT = p.id WHERE p.sexo = '1' $filtra_query ";
        $totalInscricoesMasculino = $objInscricoes->rows($buscaNumeroInscricoesMasculino);

        ?>

        {
            value: <?=$totalInscricoesMasculino?>,
            color: "#00a65a",
            highlight: "#00a65a",
            label: "Sexo Masculino"
        },

        <?php

        //BUSCA INSCRIÇÕES DO SEXO FEMININO
        $buscaNumeroInscricoesFeminino = "SELECT * FROM inscricoes i INNER JOIN participantes p ON i.participante_id_INT = p.id WHERE p.sexo = '2' $filtra_query ";
        $totalInscricoesFeminino = $objInscricoes->rows($buscaNumeroInscricoesFeminino);

        ?>

        {
            value: <?=$totalInscricoesFeminino?>,
            color: "#f39c12",
            highlight: "#f39c12",
            label: "Sexo Feminino"
        }
    ];
    var pieOptions = {
        //Boolean - Whether we should show a stroke on each segment
        segmentShowStroke: true,
        //String - The colour of each segment stroke
        segmentStrokeColor: "#fff",
        //Number - The width of each segment stroke
        segmentStrokeWidth: 2,
        //Number - The percentage of the chart that we cut out of the middle
        percentageInnerCutout: 50, // This is 0 for Pie charts
        //Number - Amount of animation steps
        animationSteps: 100,
        //String - Animation easing effect
        animationEasing: "easeOutBounce",
        //Boolean - Whether we animate the rotation of the Doughnut
        animateRotate: true,
        //Boolean - Whether we animate scaling the Doughnut from the centre
        animateScale: false,
        //Boolean - whether to make the chart responsive to window resizing
        responsive: true,
        // Boolean - whether to maintain the starting aspect ratio or not when responsive, if set to false, will take up entire container
        maintainAspectRatio: true,
        //String - A legend template
        legendTemplate: "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<segments.length; i++){%><li><span style=\"background-color:<%=segments[i].fillColor%>\"></span><%if(segments[i].label){%><%=segments[i].label%><%}%></li><%}%></ul>"
    };
    //Create pie or douhnut chart
    // You can switch between pie and douhnut using the method below.
    pieChart.Doughnut(PieData, pieOptions);

    <?php
    $totalInscricoes = $totalInscricoesMasculino + $totalInscricoesFeminino;
    ?>
    $('#legenda').html('<p style="color:#00a65a"><b>Sexo Masculino:</b> <?=$totalInscricoesMasculino?> Inscrições </p><p style="color:#f39c12"><b>Sexo Feminino:</b> <?=$totalInscricoesFeminino?> Inscrições </p>');
    $('#total_inscricoes').html('<b>Total: <?=$totalInscricoes?> Inscrições </b>');

</script>
</body>
</html>