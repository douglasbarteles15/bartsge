<?php
$idPortifolio = $_POST["idPortifolio"];
$idPagina = $_POST["idPagina"];
if (!empty($_FILES)) {
    
    $targetPath = $_SERVER['DOCUMENT_ROOT'] . $_REQUEST['folder'] . '/';
    $targetFile = str_replace('//', '/', $targetPath);

//++++++++++++++++++++++++++++  Manipulando a imagem +++++++++++++++++++++
//Recebe a imagem
    $imagem = isset($_FILES['Filedata']) ? $_FILES['Filedata'] : NULL;
    $nomeImg = $_FILES['Filedata']['tmp_name'];
    $nome_final = time() . rand(1, 9);

    if ($nomeImg != NULL) {

        //Chama a Class de Recorte
        require_once '../../recorte/ThumbLib.inc.php';

        //Monta a Miniatura
        $thumb = PhpThumbFactory::create("$nomeImg");
        if($idPagina!=""){
        $thumb->adaptiveResize(273, 273);
        }else{
            $thumb->adaptiveResize(150, 150);
        }
        $thumb->save($targetFile . $nome_final . ".jpg", "jpg");
        echo str_replace($_SERVER['DOCUMENT_ROOT'], '', $targetFile);


        //Monta a Imagem Full
        $thumb = PhpThumbFactory::create("$nomeImg");
        $thumb->resize(800, 800);
        $thumb->save($targetFile . "g_" . $nome_final . ".jpg", "jpg");

        //Pega Dados enviados pelo Formulario de Uploads
        
        
        $dateTime = date("Y/m/d H:i:s ");
        
        include '../../../../site/includes/funcoes.php';
        
        if($idPagina!=""){
        
        $imgMiniatura = $idPagina . "/" . $nome_final . '.jpg';
        $imgFull = $idPagina . "/" . 'g_' . $nome_final . '.jpg';
        
        $sql = "INSERT INTO paginas_fotos (paginas_id_INT,paginas_fotos_arquivo,paginas_fotos_comentario,paginas_fotos_data_cadastro_DATETIME, paginas_fotos_status_INT, paginas_fotos_img_destaque,paginas_fotos_img_mini,paginas_fotos_img_full, paginas_fotos_capa) VALUES ('$idPagina','$imagemAtual','','$dateTime', '1', '', '$imgMiniatura', '$imgFull','0')";
        
        }else{
            
        $imgMiniatura = $idPortifolio . "/" . $nome_final . '.jpg';
        $imgFull = $idPortifolio . "/" . 'g_' . $nome_final . '.jpg';
        
        $sql = "INSERT INTO portifolio_fotos (portifolio_id_INT,portifolio_fotos_arquivo,portifolio_fotos_comentario,portifolio_fotos_data_cadastro_DATETIME, portifolio_fotos_status_INT, portifolio_fotos_img_destaque,portifolio_fotos_img_mini,portifolio_fotos_img_full, portifolio_fotos_capa) VALUES ('$idPortifolio','$imagemAtual','','$dateTime', '1', '', '$imgMiniatura', '$imgFull','0')";
            
        }
        
        $ob = new Database();
        $ob->Query($sql);
    }
}
?>