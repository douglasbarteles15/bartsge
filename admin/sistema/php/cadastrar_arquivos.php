<?php
ob_start();
//Inicialização do Sistema
require("../../../site/includes/funcoes.php");

//Verificar a permissão de acesso
if ($_SESSION["logado"] == "logado") {
    $objUser = new EXT_Usuarios_Admin();
    $objUser->select($_SESSION["usuario"]);
} else {
    header("location:" . $SiteHTTP . "admin/?msgErro=Realize seu Login");
}

$objSubevento = new DAO_Subeventos();
$idSubevento = FDados("id");
$objSubevento->select($idSubevento);

if (FDados("arquivo") != "") {
    $action = "/admin/sistema/alterar-arquivo";
    $objArquivos = new DAO_Arquivos();
    $objArquivos->select(FDados("arquivo"));
    $titulo = "Editar Arquivo";
    $botao = "Editar";
    $criterio = "checked";
    $id = FDados("arquivo");
} else {
    $action = "/admin/sistema/add-arquivo";
    $titulo = "Cadastrar Arquivo";
    $botao = "Cadastrar";
    $criterio = "";
}
$data_atual = date('d/m/Y');
?>
<!DOCTYPE html>
<html>
<head>
    <?php include("../../metas.php"); ?>
    <title><?= $titulo ?> - Administração <?= $objConfig_site->getrazao_social(); ?></title>
</head>
<body class="skin-blue">
<?php include '../includes/topo.php'; ?>
<div class="wrapper row-offcanvas row-offcanvas-left">
    <!-- Left side column. contains the logo and sidebar -->
    <aside class="left-side sidebar-offcanvas">
        <!-- sidebar: style can be found in sidebar.less -->
        <?php include '../includes/menu.php'; ?>
        <!-- /.sidebar -->
    </aside>

    <!-- Right side column. Contains the navbar and content of the page -->
    <aside class="right-side">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                <?= $titulo.'<br><span style="font-size:0.6em">'.$objSubevento->gettitulo().'</span>' ?>
            </h1>
            <ol class="breadcrumb">
                <li><a href="../index.php"><i class="fa fa-dashboard"></i> Home</a></li>
                <li>Arquivos</li>
                <li class="active"><?= $titulo ?></li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">

            <div class="box box-primary">
                <div class="box-header">
                    <h3 class="box-title"><?= $titulo ?></h3>
                </div>
                <!-- /.box-header -->
                <?php require("../../../comuns/libs/ajax/php/divRetorno_full.php"); ?>

                <!-- form start -->
                <form role="form" action="<?= $action ?>" method="POST" enctype="multipart/form-data">
                    <input type="hidden" name="id" value="<?= FDados("arquivo") ?>"/>
                    <input type="hidden" name="subevento_id_INT" value="<?= $objSubevento->getid() ?>"/>

                    <div class="box-body">
                        <div class="form-group">
                            <label for="titulo">Título do Arquivo</label>
                            <input type="text" name="titulo" class="form-control" id="titulo"
                                   placeholder="Arquivo" value="<?php if (FDados("arquivo") != "") {
                                echo($objArquivos->gettitulo());
                            } ?>" required>
                        </div>

                        <div class="form-group">
                            <label for="arquivo">Arquivo</label>

                            <input type="file" name="arquivo" class="form-control" id="arquivo" style="width: 100%;">
                            <br />
                            <font style="font-size: 1em;">SOMENTE ARQUIVOS <b>PDF</b></font>

                        </div>
                        

                    </div>
                    <!-- /.box-body -->

                    <div class="box-footer">
                        <button type="submit" class="btn btn-primary"><?= $botao ?></button>
                        <a style="float: right;" href="/admin/sistema/gerenciar-arquivos/<?=$objSubevento->getid()?>" class="btn btn-group">Gerenciar</a>
                    </div>
                </form>
            </div>

            <!-- Main row -->

        </section>
        <!-- /.content -->
    </aside>
    <!-- /.right-side -->
</div>
<!-- ./wrapper -->

<script>
    $(document).ready(function () {
        $("li#eventos").addClass("active");
        $("li#gerenciar_eventos").addClass("active");
    });
</script>
<?php include("../../rodape.php"); ?>
</body>
</html>