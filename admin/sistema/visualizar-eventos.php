<?php
ob_start();
//Inicialização do Sistema
require("../../site/includes/funcoes.php");

//Verificar a permissão de acesso
if ($_SESSION["logado"] == "logado") {
    $objUser = new EXT_Funcionarios_Transparencia();
    $objUser->select($_SESSION["usuario"]);
} else {
    header("location:" . $SiteHTTP . "transparencia/?msgErro=Realize seu Login");
}

//Configurações do site
$objConfig_site = new DAO_Config_site();
$objConfig_site->select(1);
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Administração Plugin Web | VISUALIZAR EVENTOS - <?= $objConfig_site->getrazao_social(); ?></title>
        <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
        <!-- bootstrap 3.0.2 -->
        <link href="../css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <!-- font Awesome -->
        <link href="../css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <!-- Ionicons -->
        <link href="../css/ionicons.min.css" rel="stylesheet" type="text/css" />
        <!-- Morris chart -->
        <link href="../css/morris/morris.css" rel="stylesheet" type="text/css" />
        <!-- jvectormap -->
        <link href="../css/jvectormap/jquery-jvectormap-1.2.2.css" rel="stylesheet" type="text/css" />
        <!-- fullCalendar -->
        <link href="../css/fullcalendar/fullcalendar.css" rel="stylesheet" type="text/css" />
        <!-- Daterange picker -->
        <link href="../css/daterangepicker/daterangepicker-bs3.css" rel="stylesheet" type="text/css" />
        <!-- bootstrap wysihtml5 - text editor -->
        <link href="../css/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css" rel="stylesheet" type="text/css" />
        <!-- Theme style -->
        <link href="../css/AdminLTE.css" rel="stylesheet" type="text/css" />

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.../js/1.3.0/respond.min.js"></script>
        <![endif]-->


        <!-- jQuery 2.0.2 -->
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>
        <!-- jQuery UI 1.10.3 -->
        <script src="../js/jquery-ui-1.10.3.min.js" type="text/javascript"></script>
        <script>
            function mouseDentro(id)
            {
                document.getElementById("img" + id).src = "../img/pdf-icon.png"
            }

            function mouseFora(id)
            {
                document.getElementById("img" + id).src = "../img/pdf-icon-branco.png"
            }

            function initMenu() {

                $('#menu ul').hide();

                $('#menu li a').click(
                        function () {

                            $(this).next().slideToggle('normal');

                        }

                );

            }

            $(document).ready(function () {
                initMenu();
            });

        </script>
    </head>
    <body class="skin-blue">
        <?php include 'includes/topo.php'; ?>
        <div class="wrapper row-offcanvas row-offcanvas-left">
            <!-- Left side column. contains the logo and sidebar -->
            <aside class="left-side sidebar-offcanvas">
                <!-- sidebar: style can be found in sidebar.less -->
                <?php include 'includes/menu.php'; ?>
                <?php include 'includes/busca_arquivos.php'; ?>
                <!-- /.sidebar -->
            </aside>

            <!-- Right side column. Contains the navbar and content of the page -->
            <aside class="right-side">
                <?php require("../../comuns/libs/ajax/php/divRetorno_novo.php"); ?>
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                        Visualizar Eventos
                        <small>Mister Transparência - <?= $objConfig_site->getrazao_social(); ?></small>
                    </h1>
                    <hr style="margin-top: 10px;margin-bottom: 10px;">   
                    <form action="visualizar-eventos.php" method="post" name="form_filtro">
                        <div class="form-group" style="margin-left: 10px;">
                            <label for="filtro">Filtrar por:</label>
                            <select name="filtro" required class="form-control" onchange="document.forms['form_filtro'].submit();">
                                <option value="">Pr&oacute;ximos Eventos</option>
                                <option value="1" <?php
                                if ($filtro == 1) {
                                    echo 'selected';
                                }
                                ?> >Todos</option>
                                <option value="2" <?php
                                if ($filtro == 2) {
                                    echo 'selected';
                                }
                                ?> >Eventos Antigos</option>
                            </select>
                        </div>
                    </form>
                    <ol class="breadcrumb">
                        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                        <li class="active">Visualizar Eventos</li>
                    </ol>
                </section>

                <!-- Main content -->
                <section class="content">

                    <!-- Small boxes (Stat box) -->
                    <div class="row">

                        <?php
                        $data_atual = date('Y-m-d');
                        $BancoEventos = new Database();
                        $objEvento = new DAO_Eventos();
                        if ($filtro == "") { // MOSTRA OS PROXIMOS EVENTOS
                            $buscaEventos = "SELECT * FROM eventos WHERE eventos_data>='" . $data_atual . "' ORDER BY eventos_data ASC";
                        } elseif ($filtro == 1) { // MOSTRA TODOS OS EVENTOS
                            $buscaEventos = "SELECT * FROM eventos ORDER BY eventos_data ASC";
                        } elseif ($filtro == 2) { // MOSTRA OS EVENTOS ANTIGOS
                            $buscaEventos = "SELECT * FROM eventos WHERE eventos_data<'" . $data_atual . "' ORDER BY eventos_data ASC";
                        }
                        $total = $objEvento->rows($buscaEventos);
                        if ($total == 0) {
                            ?>
                            <h4 style="text-align:center">NENHUM EVENTO PR&Oacute;XIMO</h4>
                            <?php
                        } else {
                            $BancoEventos->Query($buscaEventos);
                            $contador = 1;
                            while ($exibeEventos = $BancoEventos->FetchObject()) {
                                $objEvento->select($exibeEventos->eventos_id);
                                ?>


                                <div class="col-lg-3 col-xs-6">
                                    <!-- small box -->
                                    <div class="small-box bg-aqua">
                                        <div class="inner">
                                            <h3 style="font-size: 20px;font-weight: lighter;">
                                                <?= $objEvento->geteventos_titulo() ?>
                                            </h3>
                                            <p>
                                                <?php if ($objEvento->FDataExibe($objEvento->geteventos_data()) == $objEvento->FDataExibe($objEvento->geteventos_data_fim())) { ?>
            <?= $objEvento->FDataExibe($objEvento->geteventos_data()) ?>
                                                    <? }else{?>
            <?= $objEvento->FDataExibe($objEvento->geteventos_data()) ?> - <?= $objEvento->FDataExibe($objEvento->geteventos_data_fim()) ?>
        <?php } ?>
                                            </p>
                                        </div>
                                        <div class="icon">
                                            <!--<i class="ion ion-calendar"></i>-->
                                            <img src="../../site/uploads/img_eventos/<?= $objEvento->geteventos_img_mini() ?>" style="width: 100px; height: 75px;opacity: 0.7;filter: alpha(opacity=70);margin-top: -10px;" />
                                        </div>
                                        <a href="ver-evento.php?evento=<?= $objEvento->geteventos_id() ?>" class="small-box-footer">
                                            Visualizar Evento <i class="fa fa-arrow-circle-right"></i>
                                        </a>
                                    </div>
                                </div><!-- ./col -->


                                <?php
                                $contador++;
                            }
                        }
                        ?>

                    </div><!-- /.row -->

                    <!-- top row -->
                    <div class="row">
                        <div class="col-xs-12 connectedSortable">

                        </div><!-- /.col -->
                    </div>
                    <!-- /.row -->

                    <!-- Main row -->

                </section><!-- /.content -->
            </aside><!-- /.right-side -->
        </div><!-- ./wrapper -->

        <!-- add new calendar event modal -->

        <!-- Bootstrap -->
        <script src="../js/bootstrap.min.js" type="text/javascript"></script>
        <!-- Morris.js charts -->
        <script src="//cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
        <script src="../js/plugins/morris/morris.min.js" type="text/javascript"></script>
        <!-- Sparkline -->
        <script src="../js/plugins/sparkline/jquery.sparkline.min.js" type="text/javascript"></script>
        <!-- jvectormap -->
        <script src="../js/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js" type="text/javascript"></script>
        <script src="../js/plugins/jvectormap/jquery-jvectormap-world-mill-en.js" type="text/javascript"></script>
        <!-- fullCalendar -->
        <script src="../js/plugins/fullcalendar/fullcalendar.min.js" type="text/javascript"></script>
        <!-- jQuery Knob Chart -->
        <script src="../js/plugins/jqueryKnob/jquery.knob.js" type="text/javascript"></script>
        <!-- daterangepicker -->
        <script src="../js/plugins/daterangepicker/daterangepicker.js" type="text/javascript"></script>
        <!-- Bootstrap WYSIHTML5 -->
        <script src="../js/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js" type="text/javascript"></script>
        <!-- iCheck -->
        <script src="../js/plugins/iCheck/icheck.min.js" type="text/javascript"></script>

        <!-- AdminLTE App -->
        <script src="../js/AdminLTE/app.js" type="text/javascript"></script>

        <!-- AdminLTE dashboard demo (This is only for demo purposes) -->
        <script src="../js/AdminLTE/dashboard.js" type="text/javascript"></script>        

    </body>
</html>