<?php
ob_start();
//Inicialização do Sistema
require("../../site/includes/funcoes.php");

//Verificar a permissão de acesso
if ($_SESSION["logado"] == "logado") {
    $objUser = new EXT_Funcionarios_Transparencia();
    $objUser->select($_SESSION["usuario"]);
} else {
    header("location:" . $SiteHTTP . "transparencia/?msgErro=Realize seu Login");
}

$arquivo_categoria_pai = FDados("categoriaPai");
if ($arquivo_categoria_pai != "") {
    $objCategoriaPai = new DAO_Categorias_Transparencia();
    $objCategoriaPai->select($arquivo_categoria_pai);
    $nome_categoria_pai = "  -  " . $objCategoriaPai->getcategoria_titulo();
}

//Configurações do site
$objConfig_site = new DAO_Config_site();
$objConfig_site->select(1);
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Administração Plugin Web | VISUALIZAR CATEGORIAS - <?= $objConfig_site->getrazao_social(); ?></title>
        <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
        <!-- bootstrap 3.0.2 -->
        <link href="../css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <!-- font Awesome -->
        <link href="../css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <!-- Ionicons -->
        <link href="../css/ionicons.min.css" rel="stylesheet" type="text/css" />
        <!-- Morris chart -->
        <link href="../css/morris/morris.css" rel="stylesheet" type="text/css" />
        <!-- jvectormap -->
        <link href="../css/jvectormap/jquery-jvectormap-1.2.2.css" rel="stylesheet" type="text/css" />
        <!-- fullCalendar -->
        <link href="../css/fullcalendar/fullcalendar.css" rel="stylesheet" type="text/css" />
        <!-- Daterange picker -->
        <link href="../css/daterangepicker/daterangepicker-bs3.css" rel="stylesheet" type="text/css" />
        <!-- bootstrap wysihtml5 - text editor -->
        <link href="../css/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css" rel="stylesheet" type="text/css" />
        <!-- Theme style -->
        <link href="../css/AdminLTE.css" rel="stylesheet" type="text/css" />

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.../js/1.3.0/respond.min.js"></script>
        <![endif]-->

        <!-- jQuery 2.0.2 -->
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>
        <!-- jQuery UI 1.10.3 -->
        <script src="../js/jquery-ui-1.10.3.min.js" type="text/javascript"></script>
        <script>
            function mouseDentro(id)
            {
                document.getElementById("img" + id).src = "../img/pdf-icon.png"
            }

            function mouseFora(id)
            {
                document.getElementById("img" + id).src = "../img/pdf-icon-branco.png"
            }

            function initMenu() {

                $('#menu ul').hide();

                $('#menu li a').click(
                        function () {

                            $(this).next().slideToggle('normal');

                        }

                );

            }

            $(document).ready(function () {
                initMenu();
            });

        </script>
    </head>
    <body class="skin-blue">
        <?php include 'includes/topo.php'; ?>
        <div class="wrapper row-offcanvas row-offcanvas-left">
            <!-- Left side column. contains the logo and sidebar -->
            <aside class="left-side sidebar-offcanvas">
                <!-- sidebar: style can be found in sidebar.less -->
                <?php include 'includes/menu.php'; ?>
                <?php include 'includes/busca_arquivos.php'; ?>
                <!-- /.sidebar -->
            </aside>

            <!-- Right side column. Contains the navbar and content of the page -->
            <aside class="right-side">
                <?php require("../../comuns/libs/ajax/php/divRetorno_novo.php"); ?>
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                        Visualizar Categorias
                        <small><?= $nome_categoria_pai ?> - Transparência <?= $objConfig_site->getrazao_social(); ?></small>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                        <li class="active">Visualizar Categorias</li>
                    </ol>
                </section>

                <!-- Main content -->
                <section class="content">

                    <!-- Small boxes (Stat box) -->
                    <div class="row">
                        <!--VERIFICA SE EXISTE ARQUIVO CADASTRADO NA CATEGORIA PAI-->
                        <div id="arquivos">

                            <?php
                            $objBancoArquivos = new Database();
                            $objArquivo = new DAO_Arquivo_Transparencia();
                            $buscaArquivos = "SELECT * FROM arquivo_transparencia WHERE arquivo_categoria='" . $arquivo_categoria_pai . "' ORDER BY arquivo_id DESC";
                            $total = $objArquivo->rows($buscaArquivos);
                            if ($total != 0) {
                                ?>

                                <h4>Arquivos Cadastrados <?= $nome_categoria_pai ?></h4>

                                <?php
                                $objBancoArquivos->Query($buscaArquivos);
                                $contador = 1;
                                while ($exibeArquivos = $objBancoArquivos->FetchObject()) {
                                    $objArquivo->select($exibeArquivos->arquivo_id);
                                    ?>

                                    <a href="../../site/uploads/arquivos_transparencia/<?= $objArquivo->getarquivo_arquivo() ?>" target="_blank">
                                        <div class="arquivos_pdf" id="<?= $contador ?>" onmouseover="mouseDentro(this.id)" onmouseout="mouseFora(this.id)">
                                            <div class="envolve_titulo"><span class="titulo"><?= $objArquivo->getarquivo_nome() ?></span></div>
                                            <div class="envolve_img"><img src="../img/pdf-icon-branco.png" id="img<?= $contador ?>"/></div>
                                            <div class="envolve_visualizar"><span class="visualizar">Visualizar</span></div>
                                        </div>
                                    </a>

                                    <?php
                                    $contador++;
                                }
                                ?>
                                <div style="clear:both; height:2em"></div>
                                <hr />
                                <div style="clear:both; height:2em"></div>
                            <?php }
                            ?>

                        </div>

                        <?php
                        $BancoCategorias = new Database();
                        $objCategoria = new DAO_Categorias_Transparencia();
                        $buscaCategorias = "SELECT * FROM categoria_curso_transparencia WHERE categoria_pai='" . $arquivo_categoria_pai . "' ORDER BY categoria_titulo ASC";
                        $total = $objCategoria->rows($buscaCategorias);
                        if ($total == 0) {
                            ?>
                            <h4 style="text-align:center">NENHUMA SUB-CATEGORIA CADASTRADA NESSA CATEGORIA</h4>
                            <?php
                        } else {
                            $BancoCategorias->Query($buscaCategorias);
                            $contador = 1;
                            while ($exibeCategorias = $BancoCategorias->FetchObject()) {
                                $objCategoria->select($exibeCategorias->categoria_id);
                                ?>


                                <div class="col-lg-3 col-xs-6">
                                    <!-- small box -->
                                    <div class="small-box bg-red">
                                        <div class="inner">
                                            <h3>
                                                <?php
                                                $sqlArquivos = "Select COUNT(arquivo_id) as total from arquivo_transparencia WHERE arquivo_categoria='" . $objCategoria->getcategoria_id() . "'";
                                                $objBancoArquivos = new Database();
                                                $data = $objBancoArquivos->FecthAssoc($objBancoArquivos->Query($sqlArquivos));
                                                echo $data['total'];
                                                ?>
                                            </h3>
                                            <p>
        <?= $objCategoria->getcategoria_titulo() ?>
                                            </p>
                                        </div>
                                        <div class="icon">
                                            <i class="ion ion-folder"></i>
                                        </div>
        <?php if ($data['total'] != 0) { ?>
                                            <a href="visualizar-arquivos.php?categoria=<?= $objCategoria->getcategoria_id() ?>" class="small-box-footer">
                                                Visualizar Arquivos <i class="fa fa-arrow-circle-right"></i>
                                            </a>
        <?php } else { ?>
                                            <div class="small-box-footer">
                                                Nenhum Arquivo Cadastrado <i class="fa fa-arrow-circle-right"></i>
                                            </div>
        <?php } ?>
                                    </div>
                                </div><!-- ./col -->


                                <?php
                                $contador++;
                            }
                        }
                        ?>

                    </div><!-- /.row -->

                    <!-- top row -->
                    <div class="row">
                        <div class="col-xs-12 connectedSortable">

                        </div><!-- /.col -->
                    </div>
                    <!-- /.row -->

                    <!-- Main row -->

                </section><!-- /.content -->
            </aside><!-- /.right-side -->
        </div><!-- ./wrapper -->

        <!-- add new calendar event modal -->

        <!-- Bootstrap -->
        <script src="../js/bootstrap.min.js" type="text/javascript"></script>
        <!-- Morris.js charts -->
        <script src="//cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
        <script src="../js/plugins/morris/morris.min.js" type="text/javascript"></script>
        <!-- Sparkline -->
        <script src="../js/plugins/sparkline/jquery.sparkline.min.js" type="text/javascript"></script>
        <!-- jvectormap -->
        <script src="../js/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js" type="text/javascript"></script>
        <script src="../js/plugins/jvectormap/jquery-jvectormap-world-mill-en.js" type="text/javascript"></script>
        <!-- fullCalendar -->
        <script src="../js/plugins/fullcalendar/fullcalendar.min.js" type="text/javascript"></script>
        <!-- jQuery Knob Chart -->
        <script src="../js/plugins/jqueryKnob/jquery.knob.js" type="text/javascript"></script>
        <!-- daterangepicker -->
        <script src="../js/plugins/daterangepicker/daterangepicker.js" type="text/javascript"></script>
        <!-- Bootstrap WYSIHTML5 -->
        <script src="../js/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js" type="text/javascript"></script>
        <!-- iCheck -->
        <script src="../js/plugins/iCheck/icheck.min.js" type="text/javascript"></script>

        <!-- AdminLTE App -->
        <script src="../js/AdminLTE/app.js" type="text/javascript"></script>

        <!-- AdminLTE dashboard demo (This is only for demo purposes) -->
        <script src="../js/AdminLTE/dashboard.js" type="text/javascript"></script>        

    </body>
</html>