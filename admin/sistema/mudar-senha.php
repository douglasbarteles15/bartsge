<?php
require("../../site/includes/funcoes.php");
if ($_SESSION["logado"] == "logado") {
    $objUser = new EXT_Usuarios_Admin();
    $objUser->select($_SESSION["usuario"]);
} else {
    header("location:" . $SiteHTTP . "admin/?msgErro=Realize seu Login");
}

$titulo = "Mudar Senha";
?>
<!DOCTYPE html>
<html class="bg-black">
    <head>
        <?php include("../metas.php"); ?>
        <title><?= $titulo ?> - Administração <?= $objConfig_site->getrazao_social(); ?></title>
    </head>
    <body class="bg-black">

        <?php require("../../comuns/libs/ajax/php/divRetorno_novo.php"); ?>
        

        <div class="form-box" id="login-box">
            <div class="header">
                <span style="font-size: 0.8em">Área Administrativa</span>
                <div style="clear: both"></div>
                <?php if($objConfig_site->getmarcaDagua()!=""){ ?>
                    <img src="<?=$SiteHTTP?>site/uploads/img_marcadagua/<?=$objConfig_site->getmarcaDagua()?>" style="width: 250px">
                <?php } ?>
                <br><span style="font-size: 0.5em">Mudar Senha</span>
            </div>
            <form action="atualiza-senha/" method="post">
                <div class="body body bg-gray">
                    <div class="form-group">
                        <label for="senha_atual" style="color: #666">Senha atual</label>
                        <input type="password" name="senha_atual" class="form-control " id="senha_atual" required>
                    </div>
                    <div class="form-group">
                        <label for="nova_senha" style="color: #666">Nova senha</label>
                        <input type="password" name="nova_senha" class="form-control " id="nova_senha" required>
                    </div>
                    <div class="form-group">
                        <label for="confirme_nova_senha" style="color: #666">Confirme a nova senha</label>
                        <input type="password" name="confirme_nova_senha" class="form-control " id="confirme_nova_senha" required>
                    </div>
                </div>
                <div class="footer">                                                               
                    <button type="submit" class="btn bg-plugin btn-block">Atualizar</button>
                    
                    <a href="<?=$SiteHTTP?>admin/sistema/index.php" class="btn bg-red btn-block">Voltar</a>
                   
                </div>
            </form>
        </div>

        <!-- jQuery 2.0.2 -->
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>
        <!-- Bootstrap -->
        <script src="../js/bootstrap.min.js" type="text/javascript"></script>        

    </body>
</html>