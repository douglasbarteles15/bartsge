<?php
ob_start();
//Inicialização do Sistema
require("../../site/includes/funcoes.php");
//echo $_SESSION["tipo_usuario"];
//Verificar a permissão de acesso
if ($_SESSION["logado"] == "logado") {
    $objUser = new EXT_Usuarios_Admin();
    $objUser->select($_SESSION["usuario"]);
} else {
    header("location:" . $SiteHTTP . "admin/?msgErro=Realize seu Login");
}
$titulo = "Home";
?>
<!DOCTYPE html>
<html>
<head>
    <?php include("../metas.php"); ?>
    <title><?= $titulo ?> - Administração <?= $objConfig_site->getrazao_social(); ?></title>
</head>
<body class="skin-blue">
<?php include 'includes/topo.php'; ?>
<div class="wrapper row-offcanvas row-offcanvas-left">
    <aside class="left-side sidebar-offcanvas">
        <?php include 'includes/menu.php'; ?>
        <?php //include 'includes/busca_arquivos.php';  ?>
    </aside>

    <aside class="right-side">
        <?php require("../../comuns/libs/ajax/php/divRetorno_novo.php"); ?>
        <section class="content-header">
            <h1>
                <?= $titulo ?>
                <small><?= $objConfig_site->getrazao_social(); ?></small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-home"></i> <?= $titulo ?></a></li>
            </ol>
        </section>

        <section class="content">

            <div class="row">

                <center>

                    <div id="texto-descritivo">
                        <h3><?= $objConfig_site->getrazao_social(); ?></h3>

                        <p>Seja bem vindo ao sistema de gerenciamento <?= $objConfig_site->getrazao_social(); ?></p>
                    </div>

                    <hr>
                    <!--<h2 style="color: #666;font-size: 2em">Calend&aacute;rio de Eventos</h2>
                    <div id="calendar" style="width: 90%"></div>-->

                </center>

            </div>

        </section>

    </aside>

</div>

<script>
    $(document).ready(function(){
        $("li#home").addClass("active");
    });
</script>

<?php include("../rodape.php"); ?>

</body>
</html>