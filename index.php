<?php
require("site/includes/funcoes.php");
?>
<!DOCTYPE html>
<html>
<head>
	<title>Barteles Eventos</title>
	<?php include('meta.php'); ?>
</head>
<body>
<?php include('header-home.php'); ?>
<?php include('inc-eventos.php'); ?>
<?php include('como-funciona.php'); ?>
<?php include('inc-noticias.php'); ?>
<?php include('box-azul.php'); ?>    
<?php include('footer.php') ?>
</body>
</html>