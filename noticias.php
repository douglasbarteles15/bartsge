<?php
require("site/includes/funcoes.php");
?>
<!DOCTYPE html>
<html>
<head>
    <title>Notícias</title>
    <?php include('meta.php'); ?>
</head>
<body>
    <?php include('header-fixo.php'); ?>
        <section class="section-page">
            <header>
                <div class="title">
                    <h1>Notícias</h1>
                    <hr>
                </div>
            </header>
        </section>
        <section id="noticias-home">
        	<div class="main clearfix">
                    <div class="span9">
                    <!---->
                    <?php
                    $objNoticias = new DAO_Noticias();
                    if ($_POST['busca'] != "") {
                        $busca = $_POST['busca'];
                        $buscaNoticias = "SELECT * FROM noticias WHERE status_id_INT='1' AND titulo LIKE '%" . $busca . "%' ORDER BY data DESC, id DESC";
                        $msgNaoEncontrou = 'Nenhuma notícia encontrada com a busca "' . $busca . '"';
                    } else {
                        $buscaNoticias = "SELECT * FROM noticias WHERE status_id_INT='1' ORDER BY data DESC, id DESC";
                        $msgNaoEncontrou = "Nenhuma notícia cadastrada até o momento";
                    }
                    $totalNoticias = $objNoticias->rows($buscaNoticias);
                    if($totalNoticias==0){
                        echo '<center><p style="color: #00609e; font-size: 1.2em">' . $msgNaoEncontrou . '</p></center>';
                    }else{
                    $objBanco->Query($buscaNoticias);
                    while($exibeNoticias = $objBanco->FetchObject()) {
                        $objNoticias->select($exibeNoticias->id);
                        ?>
                        <div class="bx-noticia-min clearfix wow fadeInUp">
                            <a href="noticia/<?=$objNoticias->getlink()?>">
                                <?php if($objNoticias->getimg_mini()!=""){ ?>
                                <img src="site/uploads/img_noticias/<?=$objNoticias->getimg_mini()?>">
                                <?php } ?>

                                <div style="padding: 10px;">
                                    <!--<div class="status">Lingua Inglesa</div>-->
                                    <h3><?=$objNoticias->gettitulo()?></h3>

                                    <p><?=$objNoticias->getchamada()?></p>
                                </div>
                            </a>
                        </div>
                    <?php } } ?>
                    <!---->
                </div>
                <div class="span3">
                    <?php include'aside.php' ?>
                </div>
            </div>
        </section>
    <?php //include('depoimentos.php'); ?>
    <?php include('footer.php') ?>
</body>
</html>