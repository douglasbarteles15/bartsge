<section class="section-content">
    <div class="content-main ">
        <div class="title-section">
            <h2 class="sec-title">Fique ligado nas principias notícias</h2>
            <hr class="line-red"> 
        </div>


        <div class="content-box clearfix">
            <?php
            $objNoticias = new DAO_Noticias();
            $buscaNoticias = "SELECT * FROM noticias WHERE status_id_INT='1' ORDER BY data DESC, id DESC LIMIT 4";
            $objBanco->Query($buscaNoticias);
            while($exibeNoticias = $objBanco->FetchObject()) {
            $objNoticias->select($exibeNoticias->id);
            ?>
            <a href="noticia/<?=$objNoticias->getlink()?>">
            <div class="box-not wow  bounceInUp">
                <div class="ic-not-img"><img src="site/uploads/img_noticias/<?=$objNoticias->getimg_mini()?>"></div>
                <div class="text">
                    <div class="title"><?=$objNoticias->gettitulo()?></div>
                    <div class="text-not"><?=$objNoticias->getchamada()?></div>
                </div>
            </div>
            </a>
        <?php } ?>
        <a class="bt-ver" href="noticias">veja todas as notícias</a>
        </div>
    </div>
</section>