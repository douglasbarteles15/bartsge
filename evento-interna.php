<?php
require("site/includes/funcoes.php");
$evento_link = FDados("link");
$objEvento = new DAO_Eventos();
$objEvento->selectLink($evento_link);
$objCategoria = new DAO_Categorias();
$objCategoria->select($objEvento->getcategoria_id_INT());
$data_atual = date("Y-m-d");
?>
<!DOCTYPE html>
<html>
<head>
	<title>Evento</title>
	<?php include('meta.php'); ?>
    <style>
        iframe{
            width: 100%;
        }
    </style>
</head>
<body>
	<?php include('header-fixo.php'); ?>
	<section class="section-page">
		<header>
			<div class="title">
				<h1><?=$objCategoria->gettitulo()?></h1>
				<hr>
			</div>
		</header>
			<div class="content-main">
				<div class="col-md-4">
					<div class="box-view-curso">
						<div class="box-curso">
							<div class="img-curso"><img src="site/uploads/img_eventos/<?=$objEvento->getimg_full()?>"></div>
							<div class="box-title" style="min-height: inherit;">
								<!--<div class="ic-curso"><img src="img/icon-play.png"></div>-->
								<div class="ic-title" style="width: 95%;"><?=$objEvento->gettitulo()?></div>
                                <div style="clear: both;"></div>
                                <?php
                                if($objEvento->getvagas_restantes()==0){
                                    echo '<p style="color: #761c19; text-align: right">Vagas Esgotadas</p>';
                                }elseif($objEvento->getvagas_restantes()==1){
                                    echo '<p style="text-align: right">1 Vaga Restante</p>';
                                }else{
                                    echo '<p style="text-align: right">'.$objEvento->getvagas_restantes().' Vagas Restantes</p>';
                                }
                                ?>
							</div>
							<div class="box-text" id="box-descricao-curso">
								<?=$objEvento->getdescricao()   ?>
							</div>
							<button id="lermais">Continuar Lendo</button>
							<button id="minimizar"><i class="fa fa-angle-up" aria-hidden="true"></i><br>Minimizar</button>
							<div class="box-footer">
								<span class="modulos">
                                    <?=$objEvento->FDataExibe($objEvento->getdata_inicio())?>
                                    <?php if($objEvento->getdata_final()!=$objEvento->getdata_inicio()){
                                        echo ' até '.$objEvento->FDataExibe($objEvento->getdata_final());
                                        $data_unica = false;
                                    }else{
                                        $data_unica = true;
                                    } ?>
                                </span>
								<span class="hora"><i class="fa fa-clock-o" aria-hidden="true"></i> de <?=substr($objEvento->gethora_inicio(), 0, -3)?>h às <?=substr($objEvento->gethora_final(), 0, -3)?>h</span>
							</div>
						</div>
					</div>
                    <?php if($objEvento->getdata_inicio() >= $data_atual){ ?>
                    <?php if ($_SESSION["logado_site"] != "logado") { ?>
                        <?php if($objEvento->getvagas_restantes()==0){ ?>
                            <a class="bt-inscrito" style="margin-top: 10px; width:100%; cursor: not-allowed; background-color: #761c19">Vagas Esgotadas</a>
                            <?php }else{ ?>
                                <a href="acessar/<?=$objEvento->getid()?>" class="bt-ver" style="margin-top: 10px; width:100%; cursor: pointer">Fazer Inscrição</a>
                            <?php } ?>
                    <?php }else{
                            if($_SESSION["tipo_usuario_site"] == "participante") {
                                $objParticipanteLogado = new DAO_Participantes();
                                $objParticipanteLogado->select($_SESSION["participante"]);
                                $verificaInscrito = "SELECT * FROM inscricoes WHERE participante_id_INT='" . $objParticipanteLogado->getid() . "' AND evento_id_INT='" . $objEvento->getid() . "'";
                                $totalInscrito = $objEvento->rows($verificaInscrito);
                                if ($totalInscrito > 0) {
                                    echo '<a class="bt-inscrito" style="margin-top: 10px; width:100%;">Já Inscrito</a>';
                                } else {
                                    if ($objEvento->getvagas_restantes() == 0) {
                                        echo '<a class="bt-inscrito" style="margin-top: 10px; width:100%; cursor: not-allowed; background-color: #761c19">Vagas Esgotadas</a>';
                                    } else {
                                        echo '<a href="inscrever/' . $objEvento->getid() . '" class="bt-ver" style="margin-top: 10px; width:100%; cursor: pointer">Fazer Inscrição</a>';
                                    }
                                }
                            }

                        ?>

                    <?php }?>
                    <?php }?>

				</div>
                <?php if($objEvento->getvideo() != ""){ ?>
                <div class="col-md-8 col-curso">
                    <header>
                        <div class="title-curso">
                            <span class="tl-curso">Vídeo de Apresentação</span>
                            <hr>
                        </div>
                    </header>
                    <?=$objEvento->getvideo()?>
                </div>
                <?php } ?>
                <div class="col-md-8 col-curso">
					<header>
						<div class="title-curso">
							<span class="tl-curso">Programação</span>
							<hr>
						</div>
					</header>
					<div class="list-modulos">

                        <?php
                        $objSubEventos = new DAO_Subeventos();
                        $objInstrutor = new DAO_Instrutores();
                        $objTipoSubevento = new DAO_Tipos_Subeventos();
                        $buscaSubeventos = "SELECT * FROM subeventos WHERE evento_id_INT='".$objEvento->getid()."' AND status_id_INT='1' ORDER BY data ASC, hora_inicio ASC";
                        $objBanco->Query($buscaSubeventos);
                        while($exibeSubeventos = $objBanco->FetchObject()){
                            $objSubEventos->select($exibeSubeventos->id);
                            $objInstrutor->select($objSubEventos->getinstrutor_id_INT());
                            $objTipoSubevento->select($objSubEventos->tipo_subevento_id_INT);
                        ?>

                        <div class="col-md-12 box-modulo completo">
                            <div class="left-box-module">
                                <div class="title-curso" style="padding-bottom: 0px"><?=$objSubEventos->gettitulo()?></div>
                                <?php if($objInstrutor->getnome() != "Credenciador"){ ?>
                                <div class="title-curso" style="font-size: 0.9em; font-weight: normal"><?=$objInstrutor->getnome()?></div>
                                <?php } ?>
                                <div class="title-text"><?=$objSubEventos->getdescricao()?></div>
                                <div class="title-clock"><i class="fa fa-clock-o" aria-hidden="true"></i> de <?=substr($objSubEventos->gethora_inicio(), 0, -3)?>h às <?=substr($objSubEventos->gethora_final(), 0, -3)?>h</div>
                                <?php if($data_unica == false){ ?>
                                <p><?=$objSubEventos->FDataExibe($objSubEventos->getdata())?></p>
                                <?php } ?>
                            </div>
                            <div class="right-box-module">
                                <div class="title-com"><?=$objTipoSubevento->gettitulo()?></div>
                                <!--<div class="title-mod">curso completo</div>
                                <div class="title-val">R$ 99,00</div>-->
                            </div>
						</div>

                        <?php } ?>

					</div>
				</div>				
			</div>
	</section>
	<section class="section-content-page">
	<div class="content-main ">
		<div class="title-section">
			<h2 class="sec-title">Confira outros eventos</h2>
		</div>

		<div class="content-box-m clearfix tbs-sl">
            <?php
            $data_atual = date('Y-m-d');
            $objEventos = new DAO_Eventos();
            $objSubEventos = new DAO_Subeventos();
            $buscaProximosEventos = "SELECT * FROM eventos WHERE status_id_INT='1' AND data_final >= '".$data_atual."' AND id <> '".$objEvento->getid()."' ORDER BY data_inicio ASC, hora_inicio ASC LIMIT 4";
            $objBanco->Query($buscaProximosEventos);
            while($exibeProximosEventos = $objBanco->FetchObject()){
            $objEventos->select($exibeProximosEventos->id);
            $buscaSubeventos = "SELECT * FROM subeventos WHERE evento_id_INT='".$objEventos->getid()."' AND status_id_INT='1'";
            $totalSubeventos = $objSubEventos->rows($buscaSubeventos);
            ?>
                <a href="evento/<?=$objEventos->getlink()?>">
                    <div class="box-curso span3 wow flipInX">
                        <div class="img-curso"><img src="site/uploads/img_eventos/<?=$objEventos->getimg_mini()?>"></div>
                        <div class="box-title">
                            <div class="ic-title"><?=$objEventos->gettitulo()?></div>
                        </div>
                        <div class="box-footer">
					<span class="modulos">
                        <?=$objEventos->FDataExibe($objEventos->getdata_inicio())?>
                        <?php if($objEventos->getdata_final()!=$objEventos->getdata_inicio()){
                            echo ' até '.$objEventos->FDataExibe($objEventos->getdata_final());
                        } ?>
                    </span>
                            <div style="clear: both;"></div>
                            <span class="hora"><i class="fa fa-clock-o" aria-hidden="true"></i> de <?=substr($objEventos->gethora_inicio(), 0, -3)?>h às <?=substr($objEventos->gethora_final(), 0, -3)?>h</span>
                            <div style="clear: both;"></div>
                            <?php
                            if($objEventos->getvagas_restantes()==0){
                                echo '<span class="hora" style="color: #761c19">vagas esgotadas</span>';
                            }elseif($objEventos->getvagas_restantes()==1){
                                echo '<span class="hora">1 vaga restante</span>';
                            }else{
                                echo '<span class="hora">'.$objEventos->getvagas_restantes().' vagas restantes</span>';
                            }
                            ?>
                        </div>
                    </div>
                </a>
            <?php } ?>
		</div>
		<a class="bt-ver-m" href="proximos-eventos">veja todos os eventos</a>
	</div>
</section>
	<?php include('como-funciona.php'); ?>
	<?php //include('depoimentos.php'); ?>
	<?php include('footer.php') ?>
</body>
</html>