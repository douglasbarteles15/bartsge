<?php
require("site/includes/funcoes.php");
$noticia_link = FDados("link");
$objNoticia = new DAO_Noticias();
$objNoticia->selectLink($noticia_link);
?>
<!DOCTYPE html>
<html>
<head>
    <title>Notícias</title>
    <?php include('meta.php'); ?>
</head>
<body>
<?php include('header-fixo.php'); ?>
<section class="section-page">
    <header>
        <div class="title">
            <h1>Notícias</h1>
            <hr>
        </div>
    </header>
</section>
<section id="noticias-home">
<div class="main clearfix">
    <div class="span9">
    	<article class="artigo">   
       		<header class="cabecalho-artigo">
            	<!--<span class="status">Lingua Inglesa</span>-->
                <h2><?=$objNoticia->gettitulo()?></h2>
                <div class="social-bt clearfix">
                 	<ul>
                    	<li><div class="g-plusone" data-annotation="none" data-width="300"></div></li>
                        <li style="padding-top:7px"><a href="https://twitter.com/share" class="twitter-share-button">Tweet</a></li>
                        <li><div class="fb-like" data-href="https://developers.facebook.com/docs/plugins/" data-layout="button_count" data-action="like" data-show-faces="true" data-share="true"></div></li>
                    </ul>
                 </div>
            </header>
            <div class="body-artigo">
            	<img src="site/uploads/img_noticias/<?=$objNoticia->getimg_full()?>" style="display:block; margin:0 auto 20px">
           	    <?=$objNoticia->getconteudo()?>
            </div>
        </article>
        <div class="titlenew"><span class="title-new">outras notícias</span>
            <a href="noticias"><button>VER TODAS</button></div></a>

        <?php
        $objNoticias = new DAO_Noticias();
        $buscaOutrasNoticias = "SELECT * FROM noticias WHERE status_id_INT='1' AND id <> '".$objNoticia->getid()."' ORDER BY data DESC, id DESC LIMIT 2";
        $objBanco->Query($buscaOutrasNoticias);
        while($exibeOutrasNoticias = $objBanco->FetchObject()){
            $objNoticias->select($exibeOutrasNoticias->id);
        ?>
        <div class="bx-noticia-min clearfix wow fadeInUp">
            <a href="noticia/<?=$objNoticias->getlink()?>">
                <img src="site/uploads/img_noticias/<?=$objNoticias->getimg_mini()?>">
                <div style="padding: 10px;">
                    <!--<div class="status">Lingua Inglesa</div>-->
                    <h3><?=$objNoticias->gettitulo()?></h3>
                    <p><?=$objNoticias->getchamada()?></p>
                </div>
             </a>
        </div>

        <?php } ?>

    </div>
    <div class="span3">
        <?php include'aside.php' ?>
    </div>
</div>
</section>
    <?php //include('depoimentos.php'); ?>
    <?php include('footer.php') ?>
</body>
</html>
