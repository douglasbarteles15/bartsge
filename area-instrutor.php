<?php
require("site/includes/funcoes.php");
if ($_SESSION["logado_site"] != "logado") {
    header("location:" . $SiteHTTP . "acessar/erro/Você precisa estar logado pra acessar essa área.");
} else {
    $objInstrutorLogado = new DAO_Instrutores();
    $objInstrutorLogado->select($_SESSION["instrutor"]);
}
$data_atual = date("Y-m-d");
?>
<!DOCTYPE html>
<html>
<head>
    <title>Área do Instrutor/Palestrante</title>
    <?php include('meta.php'); ?>
</head>
<body>
<?php include('header-fixo.php'); ?>
<section class="section-page">
    <header>
        <div class="title">
            <h1>Meus Certificados</h1>
            <hr>
        </div>
    </header>
    <section class="form-section">
        <div class="form-section-content">
            <div>
                <!-- Tab panes -->
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane active" id="home">
                        <?php
                        $objBanco = new Database();
                        $objEventos = new DAO_Eventos();
                        $objSubeventos = new DAO_Subeventos();
                        $buscaSubEventosInstrutor = "SELECT * FROM subeventos WHERE instrutor_id_INT='".$objInstrutorLogado->getid()."' AND status_id_INT='1' ORDER BY data DESC";
                        $totalSubEventosInstrutor = $objSubeventos->rows($buscaSubEventosInstrutor);
                        if($totalSubEventosInstrutor == 0 ){
                            echo '<h4 style="text-align: center;">Você ainda não foi Instrutor/Palestrante de um evento. </h4>';
                        }
                        $objBanco->Query($buscaSubEventosInstrutor);
                        while($exibeSubEventosInstrutor = $objBanco->FetchObject()){
                            $objSubeventos->select($exibeSubEventosInstrutor->id);
                            $objEventos->select($objSubeventos->getevento_id_INT());

                        ?>
                            <div class="col-md-12 view-curso">
                                <div class="col-md-6 mr-pad-2">
                                    <div class="ic-title"><?=$objSubeventos->gettitulo()?></div>
                                    <ul class="list-curso-detalhes">
                                        <li><?=$objEventos->gettitulo()?></li>
                                        <li>
                                            <strong><?=$objSubeventos->FDataExibe($objSubeventos->getdata())?></strong>
                                        </li>
                                        <li><i class="fa fa-clock-o" aria-hidden="true"></i>
                                        de <?=substr($objSubeventos->gethora_inicio(), 0, -3)?>h às <?=substr($objSubeventos->gethora_final(), 0, -3)?>h</li>
                                    </ul>
                                </div>
                                <div class="col-md-2 mr-pad-2">
                                    <div class="ic-title">&nbsp&nbsp&nbsp&nbsp</div>
                                    <ul class="list-curso-detalhes">
                                        <li>&nbsp&nbsp&nbsp&nbsp</li>
                                        <li>&nbsp&nbsp&nbsp&nbsp</li>
                                    </ul>
                                </div>
                                <div class="col-md-2 mr-pad-2">
                                    <div class="ic-title">&nbsp&nbsp&nbsp&nbsp</div>
                                    <ul class="list-curso-detalhes">
                                        <li>&nbsp&nbsp&nbsp&nbsp</li>
                                        <li>&nbsp&nbsp&nbsp&nbsp</li>
                                    </ul>
                                </div>
                                <div class="col-md-2 mr-pad-2">
                                    <div class="ic-title">&nbsp&nbsp&nbsp&nbsp</div>
                                    <ul class="list-curso-detalhes">
                                        <li>&nbsp&nbsp&nbsp&nbsp</li>
                                        <li>&nbsp&nbsp&nbsp&nbsp</li>
                                    </ul>
                                </div>
                                <?php if($objSubeventos->getdata() < $data_atual){ ?>
                                <a href="certificado-instrutor/<?=$objInstrutorLogado->getid()?>/<?=$objSubeventos->getid()?>" class="btn btn-link" style="float: right;margin-right: 5px;border: 1px solid #999">Baixar Certificado</a>
                                <?php }else{ ?>
                                    <a class="btn btn-link" style="float: right;margin-right: 5px;opacity: 0.5;filter: alpha(opacity=50); cursor: not-allowed; border: 1px solid #999" title="Por enquanto, você ainda não pode baixar o certificado de instrutor/palestrante deste evento. Espere até o final do evento.">Baixar Certificado</a>
                                <?php } ?>

                            </div>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>
    </section>
</section>
<?php //include('depoimentos.php'); ?>
<?php include('footer.php') ?>
</body>
</html>